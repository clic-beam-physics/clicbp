/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef interpolate_hh
#define interpolate_hh

namespace {

  // linear interpolation
  template <typename T>
  inline T LINT(const T &y1, const T & y2, const double &t ) // linear interpolation
  { return y1*(1.-t)+y2*t; }
  
  template <typename T>
  inline T LINT_deriv(const T &y1, const T & y2 ) // linear interpolation, first derivative
  { return y2-y1; }

  // cubic interpolation
  template <typename T> // for t=0 gives y0
  inline T CINT0(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { double t3=t*t*t; return (t3*y2+(6*t-2*t3)*y1+(t3-6*t+6)*y0)/6.0; }

  template <typename T> // for t=0 gives y1
  inline T CINT1(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { double t2=t*t, t3=t*t*t; return (t3*y3+(-3*t3+3*t2+3*t+1)*y2+(3*t3-6*t2+4)*y1+(-t3+3*t2-3*t+1)*y0)/6.0; }

  template <typename T> // for t=0 gives y2
  inline T CINT2(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { double t2=t*t, t3=t*t*t; return ((2*t3+9*t2+9*t+3)*y3+(-6*t3-18*t2+12)*y2+(6*t3+9*t2-9*t+3)*y1-2*t3*y0)/18.0; }

  // cubic interpolation, derivative
  template <typename T> // for t=0 gives y0
  inline T CINT0_deriv(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { double t2=t*t; return (t2*y2+(2-2*t2)*y1+(t2-2)*y0)/2.0; }

  template <typename T> // for t=0 gives y1
  inline T CINT1_deriv(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { double t2=t*t; return (t2*y3+(-3*t2+2*t+1)*y2+(3*t2-4*t)*y1+(-t2+2*t-1)*y0)/2.0; }

  template <typename T> // for t=0 gives y2
  inline T CINT2_deriv(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { double t2=t*t; return ((2*t2+6*t+3)*y3+(-6*t2-12*t)*y2+(6*t2+6*t-3)*y1-2*t2*y0)/6.0; }

  // cubic interpolation, 2nd derivative
  template <typename T> // for t=0 gives y0
  inline T CINT0_deriv2(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { return t*y2-2*t*y1+t*y0; }

  template <typename T> // for t=0 gives y1
  inline T CINT1_deriv2(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { return t*y3+(1-3*t)*y2+(3*t-2)*y1+(1-t)*y0; }

  template <typename T> // for t=0 gives y2
  inline T CINT2_deriv2(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { return ((2*t+3)*y3+(-6*t-6)*y2+(6*t+3)*y1-2*t*y0)/3.0; }

  template <typename T>
  inline T CINT0_deriv3(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { return y2-2*y1+y0; }
  
  template <typename T>
  inline T CINT1_deriv3(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { return y3-3*y2+3*y1-y0; }
  
  template <typename T>
  inline T CINT2_deriv3(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { return (2*y3-6*y2+6*y1-2*y0)/3.0; }

}

#endif /* interpolate_hh */
