clear all
close all

%read the input file generated with Superfish
fname='\\cern.ch\dfs\Users\s\sbenedet\TERA\bw TW\Beam dynamics\Benchmark\superfish_file_Avni.txt';
A=textread(fname);

%Input data
r_mesh=0.1;                 % radial mesh in mm
r_end=2.5;                  % max radius in mm
z_mesh=0.1;                 % longitudinal mesh in mm   
z_end=81.2;                 % max length in mm
ncell=1;                    % insert the number of cells    

r=0:r_mesh:r_end;
rr=length(r);
z=0:z_mesh:z_end;
zz=length(z);

% Generate new r and z vectors
z=0:z_mesh:(z_end*ncell);
z_vector=repmat(z,1,rr);
r_vector=[];
for i=1:rr
    r_vec(i,:)=[r(i)*ones(1,ncell*(zz))];
    r_vector=[r_vector, r_vec(i,:)];
end
A(:,1)=z_vector';
A(:,2)=r_vector';


% find the polynomial coeff for interpolation
Er=vec2mat(A(:,4),(zz)*ncell);
Ezr=vec2mat(A(:,3),(zz)*ncell);
Bteta=vec2mat(A(:,6),(zz)*ncell);
rrr=repmat(r',1,(zz)*ncell);
for n=1:(zz)*ncell
    pEr(:,n)=polyfit(rrr(:,n),Er(:,n),2);
    pEzr(:,n)=polyfit(rrr(:,n),Ezr(:,n),2);
    pBteta(:,n)=polyfit(rrr(:,n),Bteta(:,n),2);
end


% Generate cartesian and cylindrical grid
x=-r_end:r_mesh:0;                   % just one half of the accelerating structure   
y=-r_end:r_mesh:0;                      % just one half of the accelerating structure
for n=1:length(x)
    for j=1:length(y)
        for k=1:length(z)
            cart(k+(j-1)*length(z)+(n-1)*length(y)*length(z),:)=[x(n),y(j),z(k)];
            if sqrt(x(n)^2+y(j)^2)>3
                cylin(k+(j-1)*length(z)+(n-1)*length(y)*length(z),:)=[atan(y(j)/x(n)),NaN,z(k)];
            else
                cylin(k+(j-1)*length(z)+(n-1)*length(y)*length(z),:)=[atan(y(j)/x(n)),sqrt(x(n)^2+y(j)^2),z(k)];
            end
        end
    end
end

%convert E field in cartesian coordinates
pEr=repmat(pEr,1,rr*rr);
pEzr=repmat(pEzr,1,rr*rr);
for n=1:(rr*rr)*(zz)*ncell
    Erinterp(n)=polyval(pEr(:,n),cylin(n,2)); 
    Ez(n)=polyval(pEzr(:,n),cylin(n,2)); 
    if cart(n,1)<0
        Ex(n)=Erinterp(n)'*-cos(cylin(n,1));
        Ey(n)=Erinterp(n)'*-sin(cylin(n,1));
    else
        Ex(n)=Erinterp(n)'*cos(cylin(n,1));
        Ey(n)=Erinterp(n)'*sin(cylin(n,1));
    end
end

for n=((rr*rr)*(zz)*ncell-zz):(rr*rr)*(zz)*ncell
    if isnan(Ex(n)) 
        Ex(n)=0;
    end
    if isnan(Ey(n)) 
        Ey(n)=0;
    end
end

%convert the B field in cartesian coordinates
pBteta=repmat(pBteta,1,rr*rr);
for n=1:1:(rr*rr)*zz*ncell
    Binterp(n)=polyval(pBteta(:,n),cylin(n,2));
    if cart(n,1)<0
        Bx(n)=Binterp(n)'*-sin(cylin(n,1));
        By(n)=Binterp(n)'*cos(cylin(n,1));
    else
        Bx(n)=Binterp(n)'*sin(cylin(n,1));
        By(n)=Binterp(n)'*-cos(cylin(n,1));
    end
end

for n=((rr*rr)*(zz)*ncell-zz):(rr*rr)*(zz)*ncell
    if isnan(Bx(n)) 
        Bx(n)=0;
    end
    if isnan(By(n)) 
        By(n)=0;
    end
end

Ecart=[cart*10^(-3), Ex'*10^(6), zeros(rr*rr*zz*ncell,1), Ey'*10^(6), zeros(rr*rr*zz*ncell,1), Ez'*10^(6), zeros(rr*rr*zz*ncell,1)];
Hcart=[cart*10^(-3), zeros(rr*rr*zz*ncell,1), -Bx' , zeros(rr*rr*zz*ncell,1), -By', zeros(rr*rr*zz*ncell,1), zeros(rr*rr*zz*ncell,1)];



% print the files
fileID1 = fopen('Efield.fld','w');
fileID2 = fopen('Hfield.fld','w');
formatSpec = '%f %f %f %f %f %f %f %f %f \n';
[nrows,ncols] = size(Ecart);
for row=1:nrows
    fprintf(fileID1,formatSpec,Ecart(row,:));
    fprintf(fileID2,formatSpec,Hcart(row,:));
end
