#ifndef static_field_hh
#define static_field_hh

#include <complex>
#include <mutex>
#include <array>

#include "element.hh"
#include "fftw_mesh3d.hh"
#include "parallel_ode_solver.hh"

// StaticField in vacuum
class StaticField : public Element, public Parallel_ODE_Solver {
  fftwMesh3d_CINT mesh_Phi; // V/m*mm
  fftwMesh3d_CINT mesh_Ax; // T*mm
  fftwMesh3d_CINT mesh_Ay; // T*mm
  fftwMesh3d_CINT mesh_Az; // T*mm
  fftwMesh3d_CINT mesh_PhiM; // T*mm magnetic scalar potential
  StaticVector<3> E0; // V/m
  StaticVector<3> B0; // T
  // cartesian bounding box
  double x0, y0, z0, z1; // mm
  double hx, hy, hz; // mm
  bool use_Phi; // customizing the field
  bool use_A; // customizing the field
  bool use_PhiM; // customizing the field
  void track0_initialize(Bunch6d &bunch ); // initialize
  void track0_finalize(); // finalize
  void track0(Particle &particle, double S, size_t start_step, size_t end_step, size_t thread = 0 ) const;
  // auxiliary variables for GSL ODE integration
  std::vector<gsl_odeiv2_system> sys;
  mutable bool gsl_error;
public:
  StaticField(const Mesh3d &Phi = DefaultMesh3d,
	      const Mesh3d &Ax = DefaultMesh3d,
	      const Mesh3d &Ay = DefaultMesh3d,
	      const Mesh3d &Az = DefaultMesh3d,
	      const Mesh3d &PhiM = DefaultMesh3d,
	      double x0 = 0.0, double y0 = 0.0, // m
	      double hx = 1.0, double hy = 1.0, double hz = 1.0, // m
	      double length = -1.0); // if -1 takes the default : (Ez.size()-1) * hz
  StaticField(const Mesh3d &Ex,
	      const Mesh3d &Ey,
	      const Mesh3d &Ez,
	      const Mesh3d &Bx,
	      const Mesh3d &By,
	      const Mesh3d &Bz,
	      double x0, double y0, // m
	      double hx, double hy, double hz, // m
	      double length = -1.0, // if -1 takes the default : (Ez.size()-1) * hz
	      double quality = 1.0 ); // quality factor for the FFT smoothing (0..1)
  ~StaticField() = default;
  virtual StaticField *clone() { return new StaticField(*this); }
  double get_length() const { return (z1-z0)/1e3; } // return m
  double get_hx() const { return hx/1e3; } // return m
  double get_hy() const { return hy/1e3; } // return m
  double get_hz() const { return hz/1e3; } // return m
  double get_nx() const { return mesh_Phi.size1(); }
  double get_ny() const { return mesh_Phi.size2(); }
  double get_nz() const { return mesh_Phi.size3(); }
  double get_x0() const { return x0/1e3; } // return m
  double get_y0() const { return y0/1e3; } // return m
  double get_z0() const { return z0/1e3; } // return m
  double get_x1() const { return (x0 + (mesh_Phi.size1()-1)*hx)/1e3; } // return m
  double get_y1() const { return (y0 + (mesh_Phi.size2()-1)*hy)/1e3; } // return m
  double get_z1() const { return z1/1e3; } // return m
  void set_hx(double hx_ ) { hx = hx_*1e3; } // accepts m
  void set_hy(double hy_ ) { hy = hy_*1e3; } // accepts m
  void set_hz(double hz_ ) { hz = hz_*1e3; } // accepts m
  void set_x0(double x0_ ) { x0 = x0_*1e3; } // accepts m
  void set_y0(double y0_ ) { y0 = y0_*1e3; } // accepts m
  void set_z0(double z0_ ) { z0 = z0_*1e3; } // accepts m
  void set_z1(double z1_ ) { z1 = z1_*1e3; } // accepts m
  void set_length(double length ); // accepts m
  void set_Ex_Ey_Ez(const Mesh3d &Ex,
		    const Mesh3d &Ey,
		    const Mesh3d &Ez, double quality = 1.0 );
  void set_Bx_By_Bz(const Mesh3d &Bx,
		    const Mesh3d &By,
		    const Mesh3d &Bz, double quality = 1.0 );
  bool is_A_enabled() const { return use_A; }
  bool is_Phi_enabled() const { return use_Phi; }
  bool is_PhiM_enabled() const { return use_PhiM; }
  void enable_A() { use_A = true; }
  void enable_Phi() { use_Phi = true; }
  void enable_PhiM() { use_PhiM = true; }
  void disable_A() { use_A = false; }
  void disable_Phi() { use_Phi = false; }
  void disable_PhiM() { use_PhiM = false; }
  Mesh3d get_Ax() const { return mesh_Ax; }
  Mesh3d get_Ay() const { return mesh_Ay; }
  Mesh3d get_Az() const { return mesh_Az; }
  Mesh3d get_Phi() const { return mesh_Phi; }
  Mesh3d get_PhiM() const { return mesh_PhiM; }
  double get_divB(double x, double y, double z ) const;
  std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t = 0.0 ); // x,y,z [mm] ; t [mm/c]
  std::pair<StaticMatrix<3,3>, StaticMatrix<3,3>> get_field_jacobian(double x, double y, double z, double t = 0.0 ); // x,y,z [mm] t [mm/c], returns V/m/mm, and T/mm

};

#endif /* static_field_hh */
