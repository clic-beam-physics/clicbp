/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

/* RF_Track.i */

%{
#include <utility>
#include "RF_Track.hh"
#include "lattice.hh"
#include "element.hh"
#include "generic_field.hh"
#include "drift.hh"
#include "quadrupole.hh"
#include "transfer_line.hh"
#include "bunch6dt.hh"
#include "bunch6d.hh"
#include "particle.hh"
#include "static_field.hh"
#include "rf_field.hh"
#include "rf_field_1d.hh"
#include "plasma.hh"
#include "space_charge.hh"
#include "space_charge_p2p.hh"
#include "space_charge_pic.hh"
#include "space_charge_pic_long_cylinder.hh"
#include "space_charge_pic_horizontal_plates.hh"
#include "electron_cooler.hh"
#include "adiabatic_matching_device.hh"
#include "broadband_dielectric_structure.hh"
#include "test_coils.hh"
#include "particle_tracker.hh"
%}

%ignore RF_Track_Globals::rng;
%ignore SpaceCharge::compute_force(const Bunch6d &, MatrixNd & );
%ignore SpaceCharge::compute_force(const Bunch6dT &, MatrixNd & );

%include "std_shared_ptr.i"
%include "std_string.i"
%include "std_vector.i"
%include "matrixnd.i"
%include "static_vector_3.i"
%include "static_vector_4.i"
%include "static_matrix_33.i"
%include "static_matrix_44.i"
%include "static_matrix_66.i"
%include "mesh1d.i"
%include "mesh3d.i"

#if defined(SWIGOCTAVE)
%typemap(out) std::pair<std::array<std::complex<double>,3>, std::array<std::complex<double>,3>> {
  ComplexMatrix E(3,1);
  ComplexMatrix B(3,1);
  E(0) = Complex(($1).first[0]);
  E(1) = Complex(($1).first[1]);
  E(2) = Complex(($1).first[2]);
  B(0) = Complex(($1).second[0]);
  B(1) = Complex(($1).second[1]);
  B(2) = Complex(($1).second[2]);
  octave_value_list ret;
  ret.append(E);
  ret.append(B);
  $result = ret;
 }

%typemap(out) std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>> {
  ComplexMatrix E(3,1);
  ComplexMatrix B(3,1);
  E(0) = Complex(($1).first[0]);
  E(1) = Complex(($1).first[1]);
  E(2) = Complex(($1).first[2]);
  B(0) = Complex(($1).second[0]);
  B(1) = Complex(($1).second[1]);
  B(2) = Complex(($1).second[2]);
  octave_value_list ret;
  ret.append(E);
  ret.append(B);
  $result = ret;
 }

%typemap(out) std::pair<StaticVector<3>, StaticVector<3>> {
  Matrix E(3,1);
  Matrix B(3,1);
  E(0) = ($1).first[0];
  E(1) = ($1).first[1];
  E(2) = ($1).first[2];
  B(0) = ($1).second[0];
  B(1) = ($1).second[1];
  B(2) = ($1).second[2];
  octave_value_list ret;
  ret.append(E);
  ret.append(B);
  $result = ret;
 }
#endif

#if defined(SWIGOCTAVE)
%typemap(out) std::pair<StaticMatrix<3,3>, StaticMatrix<3,3>> {
  Matrix E(3,3);
  Matrix B(3,3);
  for (size_t i=0; i<3; i++) {
    for (size_t j=0; j<3; j++) {
      E(i,j) = ($1).first[i][j];
      B(i,j) = ($1).second[i][j];
    }
 }
  octave_value_list ret;
  ret.append(E);
  ret.append(B);
  $result = ret;
 }
#endif



#if defined(SWIGPYTHON)
%typemap(out) std::pair<StaticVector<3>, StaticVector<3>> {
  npy_intp dimensions[2] = { 3, 1 };
  PyArrayObject *res1 = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  PyArrayObject *res2 = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res1);
  char *data = PyArray_BYTES(res1);
  *(double*)(data + 0*strides[0]) = ($1).first[0];
  *(double*)(data + 1*strides[0]) = ($1).first[1];
  *(double*)(data + 2*strides[0]) = ($1).first[2];
  data = PyArray_BYTES(res1);
  *(double*)(data + 0*strides[0]) = ($1).second[0];
  *(double*)(data + 1*strides[0]) = ($1).second[1];
  *(double*)(data + 2*strides[0]) = ($1).second[2];
  $result = SWIG_Python_AppendOutput($result, PyArray_Return(res1));
  $result = SWIG_Python_AppendOutput($result, PyArray_Return(res2));
 }
#endif

%template(ElementPtr_vector) std::vector<Element*>;
%template(DriftPtr_vector) std::vector<Drift*>;
%template(StaticFieldPtr_vector) std::vector<StaticField*>;
%template(RF_FieldPtr_vector) std::vector<RF_Field*>;
%template(QuadrupolePtr_vector) std::vector<Quadrupole*>;
%rename(is_valid) Particle::operator bool;
%rename(is_valid) ParticleT::operator bool;
%rename(__assign__) SpaceCharge::operator=;
%ignore Lattice::operator[]; 
%ignore Bunch6d::operator[]; 
%ignore Bunch6dT::operator[]; 
%ignore init_rf_track;
%ignore Element::Aperture;

%apply SWIGTYPE *DISOWN {TestCoils *};
%apply SWIGTYPE *DISOWN {BroadbandDielectricStructure *};
%apply SWIGTYPE *DISOWN {AdiabaticMatchingDevice *};
%apply SWIGTYPE *DISOWN {ElectronCooler *};
%apply SWIGTYPE *DISOWN {Quadrupole *};
%apply SWIGTYPE *DISOWN {StaticField *};
%apply SWIGTYPE *DISOWN {RF_Field *};
%apply SWIGTYPE *DISOWN {Drift *};
%apply SWIGTYPE *DISOWN {Element *};

%include "parallel_ode_solver.hh"
%include "tracking_options.hh"
%include "particle_tracker.hh"
%include "aperture.hh"
%include "offset.hh"
%include "RF_Track.hh"
%include "element.hh"
%include "generic_field.hh"
%include "drift.hh"
%include "transfer_line.hh"
%include "quadrupole.hh"
%include "particle.hh"
%include "bunch6dt.hh"
%include "bunch6d.hh"
%include "bunch6d_info.hh"
%include "bunch6dt_info.hh"
%include "bunch6d_twiss.hh"
%include "static_field.hh"
%include "rf_field.hh"
%include "rf_field_1d.hh"
%include "adiabatic_matching_device.hh"
%include "lattice.hh"
%include "plasma.hh"

%include "space_charge.hh"
%include "space_charge_p2p.hh"
%include "space_charge_pic.hh"
%include "electron_cooler.hh"
%include "broadband_dielectric_structure.hh"
%include "test_coils.hh"

%template(SpaceCharge_PIC_FreeSpace) SpaceCharge_PIC<GreensFunction::IntegratedRetardedCoulomb>;
%template(SpaceCharge_PIC_LongCylinder_T) SpaceCharge_PIC<GreensFunction::IntegratedRetardedCoulomb_LongCylinder>;
%template(SpaceCharge_PIC_HorizontalPlates_T) SpaceCharge_PIC<GreensFunction::IntegratedRetardedCoulomb_HorizontalPlates>;

%include "space_charge_pic_long_cylinder.hh"
%include "space_charge_pic_horizontal_plates.hh"

%extend Lattice {
  Element* __brace__(int j) {
    if (j>=1 && j <= int($self->size()))
      return $self->operator[](j-1).get();
#if defined(SWIGOCTAVE)
    error("index out of range");
#elif defined(SWIGPYTHON)
    std::cerr << "error: index out of range\n";
#endif
    return NULL;
  }
 };

%extend Bunch6d {
  Particle __brace__(int j) {
    if (j>=1 && j <= int($self->size()))
      return $self->get_particle(j-1);
#if defined(SWIGOCTAVE)
    error("index out of range");
#elif defined(SWIGPYTHON)
    std::cerr << "error: index out of range\n";
#endif
    return Particle();
  }
 };

%extend Bunch6dT {
  ParticleT __brace__(int j) {
    if (j>=1 && j <= int($self->size()))
      return $self->get_particle(j-1);
#if defined(SWIGOCTAVE)
    error("index out of range");
#elif defined(SWIGPYTHON)
    std::cerr << "error: index out of range\n";
#endif
    return ParticleT();
  }
 };

%init %{
  init_rf_track();
  %}
