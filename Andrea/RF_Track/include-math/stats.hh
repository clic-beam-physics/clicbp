/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef stats_hh
#define stats_hh

#include <cstring>
#include "static_matrix.hh"

// one-pass variance
struct Weighted_incremental_variance { // https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
  double sumweight;
  StaticVector<3> mean;
  StaticVector<3> M2;
  size_t count; 
  Weighted_incremental_variance() : sumweight(0.0), mean(0.0), M2(0.0), count(0) {}
  void append(StaticVector<3> x, double weight )
  {
    if (weight>0.0) {
      double temp = weight + sumweight;
      const auto &dx = x - mean;
      const auto &R = dx * weight / temp;
      mean += R;
      for (size_t i=0; i<3; i++)
	M2[i] += sumweight * dx[i] * R[i];
      sumweight = temp;
      count++;
    }
  }
  StaticVector<3> variance() const { return count>1 ? (M2 * double(count)) / (sumweight  * double(count-1)) : StaticVector<3>(0.0); }
};

// one paass covariance
struct Incremental_covariance_matrix {
  size_t count;
  StaticVector<3> mean;
  StaticMatrix<3,3> M2;
  Incremental_covariance_matrix() : count(0), mean(0.0), M2(0.0) {}
  void append(const StaticVector<3> &x )
  {
    count++;
    const auto &dx = x - mean;
    mean += dx / double(count);
    const auto &tmp = x - mean;
    for (size_t i = 0; i < 3; i++)
      for (size_t j = 0; j < 3; j++)
	M2[i][j] += dx[i] * tmp[j];
  }
  StaticMatrix<3,3> covariance() const { return count>1 ? M2 / double(count-1) : StaticMatrix<3,3>(0.0); }
};

extern double stats_wcovariance_m(const double w[], size_t wstride, const double data1[], const size_t stride1, const double data2[], const size_t stride2, const size_t n, const double mean1, const double mean2 );

#endif /* stats_hh */
