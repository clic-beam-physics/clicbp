/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef fftw_allocator_hh
#define fftw_allocator_hh

#include "fftw_complex.hh"
#include <fftw3.h>

template <class Tp>
struct fftwAllocator {
  typedef Tp value_type;
  fftwAllocator() = default;
  // template <class T> fftwAllocator(const fftwAllocator<T> &other);
  Tp *allocate(std::size_t n) { return (Tp*)fftw_malloc(sizeof(Tp) * n); }
  void deallocate(Tp *p, std::size_t n) { fftw_free(p); }
};

template <class T, class U>
bool operator==(const fftwAllocator<T>&, const fftwAllocator<U>&) { return true; }
template <class T, class U>
bool operator!=(const fftwAllocator<T>&, const fftwAllocator<U>&) { return false; }

#endif /* fftw_allocator_hh */
