/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef vector_field_curl_free_hh
#define vector_field_curl_free_hh

#include <complex>
#include <mutex>
#include <array>

#include "scalar_field.hh"

class VectorField_curlFree : public ScalarField { // always curl free (e.g. a static electric field)
public:
  VectorField_curlFree() = default;
  VectorField_curlFree(const Mesh3d &Ex,
		       const Mesh3d &Ey,
		       const Mesh3d &Ez,
		       double x0, double y0, // mm
		       double hx, double hy, double hz ); // mm
  virtual ~VectorField_curlFree() = default;
  virtual VectorField_curlFree *clone() { return new VectorField_curlFree(*this); }
  void set_Vx_Vy_Vz(const Mesh3d &Ex, const Mesh3d &Ey, const Mesh3d &Ez );
  StaticVector<3> operator () (double x, double y, double z ) const; // x,y,z [mm]
  StaticMatrix<3,3> jacobian(double x, double y, double z ) const; // x,y,z [mm]

};

#endif /* vector_field_curl_free_hh */
