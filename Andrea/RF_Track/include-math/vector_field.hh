/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef vector_field_hh
#define vector_field_hh

#include <complex>
#include <mutex>
#include <array>

#include "mesh3d_cint.hh"

class VectorField {
  Mesh3d_CINT mesh_Vx; //
  Mesh3d_CINT mesh_Vy; //
  Mesh3d_CINT mesh_Vz; //
  // cartesian bounding box
  double x0, y0; // mm
  double hx, hy, hz; // mm
public:
  VectorField() = default;
  VectorField(const Mesh3d &Vx,
	      const Mesh3d &Vy,
	      const Mesh3d &Vz,
	      double x0, double y0, // mm
	      double hx, double hy, double hz ); // mm
  virtual ~VectorField() = default;
  virtual VectorField *clone() { return new VectorField(*this); }
  double get_hx() const { return hx; } // return mm
  double get_hy() const { return hy; } // return mm
  double get_hz() const { return hz; } // return mm
  double get_nx() const { return mesh_Vx.size1(); }
  double get_ny() const { return mesh_Vx.size2(); }
  double get_nz() const { return mesh_Vx.size3(); }
  double get_x0() const { return x0; } // return mm
  double get_y0() const { return y0; } // return mm
  double get_x1() const { return x0 + (mesh_Vx.size1()-1)*hx; } // return mm
  double get_y1() const { return y0 + (mesh_Vx.size2()-1)*hy; } // return mm
  double get_length() const { return (mesh_Vx.size3()-1)*hz; } // return mm
  void set_hx(double hx_ ) { hx = hx_; } // accepts mm
  void set_hy(double hy_ ) { hy = hy_; } // accepts mm
  void set_hz(double hz_ ) { hz = hz_; } // accepts mm
  void set_x0(double x0_ ) { x0 = x0_; } // accepts mm
  void set_y0(double y0_ ) { y0 = y0_; } // accepts mm
  void set_Vx_Vy_Vz(const Mesh3d &Vx, const Mesh3d &Vy, const Mesh3d &Vz ) { mesh_Vx = Vx; mesh_Vy = Vy; mesh_Vz = Vz; }
  VectorField anti_curl() const;
  ScalarField anti_grad() const;
  StaticMatrix<3,3> jacobian(double x, double y, double z ) const; // x,y,z [mm]
  StaticVector<3> curl(double x, double y, double z ) const; // x,y,z [mm]
  StaticVector<3> operator () (double x, double y, double z ) const; // x,y,z [mm]

};

#endif /* vector_field_hh */
