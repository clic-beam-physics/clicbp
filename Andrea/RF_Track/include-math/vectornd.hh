/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef vectornd_hh
#define vectornd_hh

#include <iostream>
#include <cstdarg>
#include <array>
#include <cmath>
#include <gsl/gsl_vector.h>

#include "stream.hh"

class VectorNd {

  gsl_vector *vector;
	
public:

  VectorNd(gsl_vector_view *src ) { vector = &src->vector; }
  VectorNd(const gsl_vector *src ) { gsl_vector_memcpy(vector = gsl_vector_alloc(src->size), src); }
  VectorNd(const VectorNd &v ) { gsl_vector_memcpy(vector = gsl_vector_alloc(v.vector->size), v.vector); }
  VectorNd(size_t size0, double x ) { vector = gsl_vector_alloc(size0); gsl_vector_set_all(vector, x); }
  explicit VectorNd(size_t size0 = 1 ) { vector = gsl_vector_alloc(size0); }
  VectorNd(size_t size0, double v0, double v1, ... )
  {
    vector = gsl_vector_alloc(size0);
    gsl_vector_set(vector,0,v0);
    if (size0 >= 2) {
      gsl_vector_set(vector,1,v1);
      if (size0 >= 3) {
	va_list ap;
	va_start(ap, v1);
	for (size_t i = 2; i < size0; i++)
	  gsl_vector_set(vector,i,va_arg(ap, double));
	va_end(ap);
      }
    }
  }
  template <size_t N>
  VectorNd(const std::array<double,N> &X ) {
    vector = gsl_vector_alloc(N);
    for (size_t i=0; i<N; i++)
      gsl_vector_set(vector,i,X[i]);
  }
  ~VectorNd() { gsl_vector_free(vector); }

  operator gsl_vector *() { return vector; }
  operator const gsl_vector *() const { return vector; }

  void clear() { gsl_vector_set_zero(vector); }
  size_t size() const { return vector->size; }
  void resize(size_t s ) { if (s!=vector->size) { gsl_vector_free(vector); vector=gsl_vector_alloc(s); } }

  gsl_vector *gsl_vector_ptr() { return vector; }
  const gsl_vector *gsl_vector_ptr() const { return vector; }
	
  VectorNd get_subvector(size_t k1, size_t n1 ) const	{ const gsl_vector &v = gsl_vector_const_subvector(vector, k1, n1).vector; return VectorNd(&v); }
	
  friend double dot(const VectorNd &a, const VectorNd &b ) { return a*b; }
  friend double norm_sqr(const VectorNd &v )
  {
    double t = gsl_vector_get(v.vector, 0);
    double n = t * t;
    for (size_t i=1; i<v.vector->size; i++) {
      t = gsl_vector_get(v.vector, i);
      n += t * t;
    }
    return n;
  }

  friend double norm(const VectorNd &v )
  {
    return sqrt(norm_sqr(v));
  }

  const VectorNd &operator = (const VectorNd &v )
  {
    if (this!=&v) {
      if (vector->size!=v.vector->size) {
	gsl_vector_free(vector);
	vector=gsl_vector_alloc(v.vector->size);
      }
      gsl_vector_memcpy(vector,v.vector);
    }
    return *this;
  }

  double &operator [] (size_t i )						{ return *::gsl_vector_ptr(vector, i); }
  const	double &operator [] (size_t i )	const					{ return *::gsl_vector_ptr(vector, i); }

  const double *c_ptr() const							{ return ::gsl_vector_ptr(vector, 0); }
  double *c_ptr()								{ return ::gsl_vector_ptr(vector, 0); }
	
  bool operator == (const VectorNd &a )					
  {
    if (vector->size!=a.vector->size)
      return false;
    for (size_t i=0;i<vector->size;i++)
      if (gsl_vector_get(vector, i)!=gsl_vector_get(a.vector,i))
	return false;
    return true;
  }
	
  const VectorNd &operator += (const VectorNd &a )				{ gsl_vector_add(vector, a.vector); return *this; }
  const VectorNd &operator -= (const VectorNd &a )				{ gsl_vector_sub(vector, a.vector); return *this; }
  const VectorNd &operator *= (double x )					{ gsl_vector_scale(vector, x); return *this; }
  const VectorNd &operator /= (double x )					{ gsl_vector_scale(vector, 1/x); return *this; }
	
  friend VectorNd operator + (VectorNd a, const VectorNd &b )			{ gsl_vector_add(a.vector, b.vector); return a; }
  friend VectorNd operator - (VectorNd a, const VectorNd &b )			{ gsl_vector_sub(a.vector, b.vector); return a; }
  friend VectorNd operator - (VectorNd a )					{ for (size_t i=0;i<a.vector->size;i++) a[i]=-a[i]; return a; }
	
  friend double	operator * (const VectorNd &a, const VectorNd &b )		{ double result=a[0]*b[0]; for (size_t i=1;i<a.vector->size;i++) result+=a[i]*b[i]; return result; }
  friend VectorNd operator * (double x, VectorNd a )				{ gsl_vector_scale(a.vector, x); return a; }
  friend VectorNd operator * (VectorNd a, double x )				{ gsl_vector_scale(a.vector, x); return a; }
  friend VectorNd operator / (VectorNd a, double x )				{ gsl_vector_scale(a.vector, 1/x); return a; }
	
  friend std::ostream &operator << (std::ostream &stream, const VectorNd &v )	{ for (size_t i = 0; i < v.vector->size; i++) stream << ' ' << v[i]; return stream; }
	
  friend OStream &operator << (OStream &stream, const VectorNd &v )
  {
    stream << v.vector->size;
    for (size_t i=0;i<v.vector->size;i++)
      stream << v[i];
    return stream; 
  }
	
  friend IStream &operator >> (IStream &stream, VectorNd &v )
  { 
    size_t size;
    if (stream >> size) {
      if (size != v.vector->size) {
	gsl_vector_free(v.vector);
	v.vector = gsl_vector_alloc(size);
      }	
      for (size_t i=0;i<size;i++)
	stream >> v[i];
    }
    return stream; 
  }

};

extern VectorNd DefaultVectorNd;

#endif /* vectornd_hh */
