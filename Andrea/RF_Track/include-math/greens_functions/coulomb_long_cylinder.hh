/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef greens_functions_coulomb_long_cylinder_hh
#define greens_functions_coulomb_long_cylinder_hh

#include "greens_functions/coulomb.hh"
#include <gsl/gsl_sf_bessel.h>
#include "hypot.hh"

namespace GreensFunction {
  struct Coulomb_LongCylinder {
    enum { is_even = 1 };
    double a; // mm, aperture of the beam pipe
    Coulomb_LongCylinder(double a_ = 1.0 ) : a(a_) {}
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz )
    {
      double sum = 0.0;
      const double rho = hypot(x0+dx/2,y0+dy/2);
      for (size_t n=1; n<=32; n++) {
	const double xmn = gsl_sf_bessel_zero_J0(n);
	const double kmn = xmn/a; // 1/mm
	auto sqr = [] (double x ) { return x*x; };
	sum += gsl_sf_bessel_J0(kmn*rho)*exp(-kmn*fabs(z0+dz/2))/(a*xmn*sqr(gsl_sf_bessel_J1(kmn*a)));
      }
      return Coulomb()(x0,y0,z0,dx,dy,dz) + sum/M_PI;
    }
    inline double operator() (double L_coasting, double x0, double y0, double z0, double dx, double dy, double dz )
    {
      double sum = 0.0;
      const double rho = hypot(x0+dx/2,y0+dy/2);
      for (size_t n=1; n<=32; n++) {
	const double xmn = gsl_sf_bessel_zero_J0(n);
	const double kmn = xmn/a; // 1/mm
	auto sqr = [] (double x ) { return x*x; };
	const double f_ = 1.0/(exp(kmn*L_coasting)-1.0); // assume(L>0,kmn>0)$ simpsum:true$ ratsimp(sum(exp(-kmn*abs(n*L)),n,1,inf));
	sum += gsl_sf_bessel_J0(kmn*rho)*(exp(-kmn*fabs(z0+dz/2))+2*cosh(-kmn*fabs(z0+dz/2))*f_)/(a*xmn*sqr(gsl_sf_bessel_J1(kmn*a)));
      }
      return Coulomb()(L_coasting,x0,y0,z0,dx,dy,dz) + sum/M_PI;
    }
  };
  struct RetardedCoulomb_LongCylinder {
    enum { is_even = 1 };
    double g; // relativistic gamma
    double a; // mm, aperture of the beam pipe
    RetardedCoulomb_LongCylinder(double g_ = 1.0, double a_ = 1.0 ) : g(g_), a(a_) {}
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz )
    {
      double sum = 0.0;
      const double rho = hypot(x0+dx/2,y0+dy/2);
      for (size_t n=1; n<=32; n++) {
	const double xmn = gsl_sf_bessel_zero_J0(n);
	const double kmn = xmn/a; // 1/mm
	auto sqr = [] (double x ) { return x*x; };
	sum += gsl_sf_bessel_J0(kmn*rho)*exp(-kmn*fabs(g*(z0+dz/2)))/(a*xmn*sqr(gsl_sf_bessel_J1(kmn*a)));
      }
      return RetardedCoulomb(g)(x0,y0,z0,dx,dy,dz) + sum/M_PI;
    }
    inline double operator() (double L_coasting, double x0, double y0, double z0, double dx, double dy, double dz )
    {
      double sum = 0.0;
      const double rho = hypot(x0+dx/2,y0+dy/2);
      for (size_t n=1; n<=32; n++) {
	const double xmn = gsl_sf_bessel_zero_J0(n);
	const double kmn = xmn/a; // 1/mm
	auto sqr = [] (double x ) { return x*x; };
	const double f_ = 1.0/(exp(kmn*g*L_coasting)-1.0); // assume(L>0,kmn>0)$ simpsum:true$ ratsimp(sum(exp(-kmn*abs(n*g*L)),n,1,inf));
	sum += gsl_sf_bessel_J0(kmn*rho)*(exp(-kmn*fabs(g*(z0+dz/2)))+2*cosh(-kmn*fabs(z0+dz/2))*f_)/(a*xmn*sqr(gsl_sf_bessel_J1(kmn*a)));
      }
      return RetardedCoulomb(g)(L_coasting,x0,y0,z0,dx,dy,dz) + sum/M_PI;
    }
  };
  struct IntegratedCoulomb_LongCylinder {
    enum { is_even = 1 };
    double a; // mm, aperture of the beam pipe
    IntegratedCoulomb_LongCylinder(double a_ = 1.0 ) : a(a_) {}
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz )
    {
      double sum = 0.0;
      const double rho = hypot(x0+dx/2,y0+dy/2);
      for (size_t n=1; n<=32; n++) {
	const double xmn = gsl_sf_bessel_zero_J0(n);
	const double kmn = xmn/a; // 1/mm
	auto sqr = [] (double x ) { return x*x; };
	sum += gsl_sf_bessel_J0(kmn*rho)*exp(-kmn*fabs(z0+dz/2))/(a*xmn*sqr(gsl_sf_bessel_J1(kmn*a)));
      }
      return IntegratedCoulomb()(x0,y0,z0,dx,dy,dz) + sum/M_PI;
    }
    inline double operator() (double L_coasting, double x0, double y0, double z0, double dx, double dy, double dz )
    {
      double sum = 0.0;
      const double rho = hypot(x0+dx/2,y0+dy/2);
      for (size_t n=1; n<=32; n++) {
	const double xmn = gsl_sf_bessel_zero_J0(n);
	const double kmn = xmn/a; // 1/mm
	auto sqr = [] (double x ) { return x*x; };
	const double f_ = 1.0/(exp(kmn*L_coasting)-1.0); // assume(L>0,kmn>0)$ simpsum:true$ ratsimp(sum(exp(-kmn*abs(n*g*L)),n,1,inf));
	sum += gsl_sf_bessel_J0(kmn*rho)*(exp(-kmn*fabs(z0+dz/2))+2*cosh(-kmn*fabs(z0+dz/2))*f_)/(a*xmn*sqr(gsl_sf_bessel_J1(kmn*a)));
      }
      return IntegratedCoulomb()(L_coasting,x0,y0,z0,dx,dy,dz) + sum/M_PI;
    }
  };
  struct IntegratedRetardedCoulomb_LongCylinder {
    enum { is_even = 1 };
    double g; // relativistic gamma
    double a; // mm, aperture of the beam pipe
    IntegratedRetardedCoulomb_LongCylinder(double g_ = 1.0, double a_ = 1.0 ) : g(g_), a(a_) {}
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz )
    {
      const double rho = hypot(x0+dx/2,y0+dy/2);
      if (g>1000) {
	const double rho_ = a*a/rho; // mirror charge
	return (-log(rho)+log(rho_)-log(a/rho))/(2*M_PI*dz);
      } else {
	double sum = 0.0;
	for (size_t n=1; n<=32; n++) {
	  const double xmn = gsl_sf_bessel_zero_J0(n);
	  const double kmn = xmn/a; // 1/mm
	  auto sqr = [] (double x ) { return x*x; };
	  sum += gsl_sf_bessel_J0(kmn*rho)*exp(-kmn*fabs(g*(z0+dz/2)))/(a*xmn*sqr(gsl_sf_bessel_J1(kmn*a)));
	}
	return IntegratedRetardedCoulomb(g)(x0,y0,z0,dx,dy,dz) + sum/M_PI;
      }
    }
    inline double operator() (double L_coasting, double x0, double y0, double z0, double dx, double dy, double dz )
    {
      if (g>1000) {
	return operator() (x0,y0,z0,dx,dy,dz);
      } else {
	double sum = 0.0;
	const double rho = hypot(x0+dx/2,y0+dy/2);
	for (size_t n=1; n<=32; n++) {
	  const double xmn = gsl_sf_bessel_zero_J0(n);
	  const double kmn = xmn/a; // 1/mm
	  auto sqr = [] (double x ) { return x*x; };
	  const double f_ = 1.0/(exp(kmn*g*L_coasting)-1.0); // assume(L>0,kmn>0)$ simpsum:true$ ratsimp(sum(exp(-kmn*abs(n*g*L)),n,1,inf));
	  sum += gsl_sf_bessel_J0(kmn*rho)*(exp(-kmn*fabs(g*(z0+dz/2)))+2*cosh(-kmn*fabs(g*(z0+dz/2)))*f_)/(a*xmn*sqr(gsl_sf_bessel_J1(kmn*a)));
	}
	return IntegratedRetardedCoulomb(g)(L_coasting,x0,y0,z0,dx,dy,dz) + sum/M_PI;
      }
    }
  };

}

#endif /* greens_functions_coulomb_long_cylinder_hh */
