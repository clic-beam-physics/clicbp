/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef greens_functions_coulomb_2d_hh
#define greens_functions_coulomb_2d_hh

#include "hypot.hh"

namespace GreensFunction {
  struct Coulomb_2d {
    enum { is_even = 1 };
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz )
    {
      if (z0!=0.0)
	return 0.0;
      auto sqr = [] (double x ) { return x*x; };
      return -log(sqr(x0+dx/2)+sqr(y0+dy/2))/(4*M_PI*dz);
    }
  };
  struct IntegratedCoulomb_2d {
    enum { is_even = 1 };
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz )
    {
      if (z0!=0.0)
	return 0.0;
      if (x0==0.0 && y0==0.0)
	return Coulomb_2d()(x0,y0,z0,dx,dy,dz);
      double sum;
      /* infinitely-long line charge */
      // integrate(integrate(-log(y^2+x^2),x),y);
      auto f = [] (double x, double y ) { double x2=x*x, y2=y*y; return x*y*(3-log(y2+x2))-x2*atan(y/x)-atan(x/y)*y2; };
      sum  = f(x0+dx,y0+dy);
      sum += f(x0,y0);
      sum -= f(x0+dx,y0);
      sum -= f(x0,y0+dy);
      return sum/(4*M_PI*dx*dy*dz);
    }
  };
}

#endif /* greens_functions_coulomb_2d_hh */
