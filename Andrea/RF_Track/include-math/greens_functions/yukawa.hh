/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef greens_functions_yukawa_hh
#define greens_functions_yukawa_hh

#include "greens_functions/coulomb.hh"

#define N_coasting 8

namespace GreensFunction {
  struct Yukawa {
    enum { is_even = 1 };
    double r0; // mm
    Yukawa(double r0_ = 0.0 ) : r0(r0_) {}
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz )
    {
      if (r0==0.0) return 0.0;
      const double r = hypot(x0+dx/2,y0+dy/2,z0+dz/2);
      return exp(-r/r0) / (4*M_PI*r);
    }
    inline double operator() (double L_coasting, double x0, double y0, double z0, double dx, double dy, double dz )
    {
      double sum = (*this)(x0,y0,z0,dx,dy,dz);
      for (size_t n=1; n<=N_coasting; n++) {
	const double zp = n*L_coasting+z0;
	const double zm = n*L_coasting-z0;
	const double value = (*this)(x0,y0,zm,dx,dy,dz) + (*this)(x0,y0,zp,dx,dy,dz);
	sum += value;
      }
      return sum;
    }
  };
  struct RetardedYukawa {
    enum { is_even = 1 };
    double r0; // mm
    double g;
    RetardedYukawa(double r0_ = 0.0, double g_ = 1.0 ) : r0(r0_), g(g_) {}
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz )
    {
      if (r0==0.0) return 0.0;
       // not entirely sure about these...
      if (g>1000) {
	const double r = hypot(x0+dx/2,y0+dy/2);
	return exp(-r/r0) * Coulomb_2d()(x0,y0,z0,dx,dy,dz);
      }
      const double r = hypot(x0+dx/2,y0+dy/2,g*(z0+dz/2));
      return exp(-r/r0) / (4*M_PI*r);
    }
    inline double operator() (double L_coasting, double x0, double y0, double z0, double dx, double dy, double dz )
    {
      double sum = (*this)(x0,y0,z0,dx,dy,dz);
      for (size_t n=1; n<=N_coasting; n++) {
	const double zp = n*L_coasting+z0;
	const double zm = n*L_coasting-z0;
	const double value = (*this)(x0,y0,zm,dx,dy,dz) + (*this)(x0,y0,zp,dx,dy,dz);
	sum += value;
      }
      return sum;
    }
  };
}

#endif /* greens_functions_yukawa_hh */
