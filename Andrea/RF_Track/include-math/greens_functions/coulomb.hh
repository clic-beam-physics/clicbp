/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef greens_functions_coulomb_hh
#define greens_functions_coulomb_hh

#include "greens_functions/coulomb_2d.hh"

#define N_coasting 8

namespace GreensFunction {
  struct Coulomb {
    enum { is_even = 1 };
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz ) { return 1.0/(4*M_PI*hypot(x0+dx/2,y0+dy/2,z0+dz/2)); }
    inline double operator() (double L_coasting, double x0, double y0, double z0, double dx, double dy, double dz )
    {
      double sum = (*this)(x0,y0,z0,dx,dy,dz);
      for (size_t n=1; n<=N_coasting; n++) {
	const double zp = n*L_coasting+z0;
	const double zm = n*L_coasting-z0;
	const double value = (*this)(x0,y0,zm,dx,dy,dz) + (*this)(x0,y0,zp,dx,dy,dz);
	sum += value;
      }
      return sum;
    }
  };
  struct RetardedCoulomb {
    enum { is_even = 1 };
    double g;
    RetardedCoulomb(double g_ = 1.0 ) : g(g_) {}
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz )
    {
      if (g>1000)
	return Coulomb_2d()(x0,y0,z0,dx,dy,dz);
      return Coulomb()(x0,y0,g*z0,dx,dy,g*dz);
    }
    inline double operator() (double L_coasting, double x0, double y0, double z0, double dx, double dy, double dz )
    {
      double sum = (*this)(x0,y0,z0,dx,dy,dz);
      for (size_t n=1; n<=N_coasting; n++) {
	const double zp = n*L_coasting+z0;
	const double zm = n*L_coasting-z0;
	const double value = (*this)(x0,y0,zm,dx,dy,dz) + (*this)(x0,y0,zp,dx,dy,dz);
	sum += value;
      }
      return sum;
    }
  };
  struct IntegratedCoulomb {
    enum { is_even = 1 };
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz )
    {
      if (x0==0.0 && y0==0.0 && z0==0.0)
	return Coulomb()(x0,y0,z0,dx,dy,dz);
      // indefinite integral: integrate(integrate(integrate(1/sqrt(x^2+y^2+(g*z)^2),x),y),z)/(dx*dy*dz);
      auto f = [&](double x, double y, double z ) {
	const double r = hypot(x, y, z);
	const double x2 = x*x;
	const double y2 = y*y;
	const double z2 = z*z;
	const double xy = x*y;
	const double rx = r*x;
	const double ry = r*y;
	const double zx = z*x;
	const double zy = z*y;
	if (z==0.0) return xy*log(r);
	if (y==0.0) return zx*(log(r)-1)+x2*atan(z/x);
	if (x==0.0) return (z2*atan(y/z)+zy*(2*log(r)-3)+y2*atan(z/y))/2;
	return (2*xy*log((z+r))-2*x2*atan(zy/rx)-y2*atan2(zy,y2+x2+rx)-2*y2*atan(zx/ry)+y2*atan(z/y)-x2*atan2(zx,y2+x2+ry)+2*x2*atan(z/x)+y2*atan2(z,y)+x2*atan2(z,x)+(atan(y/z)-atan(xy/(z*r)))*z2+(2*x*log(y+r)+(2*log(x+r)-3)*y-2*x)*z)/2;
      };
      double sum =
	+f(x0+dx, y0+dy, z0+dz)
	+f(x0,    y0,    z0+dz)
	+f(x0,    y0+dy, z0   )
	+f(x0+dx, y0,    z0   )
	-f(x0,    y0+dy, z0+dz)
	-f(x0+dx, y0,    z0+dz)
	-f(x0+dx, y0+dy, z0   )
	-f(x0,    y0,    z0   );
      return sum/(4*M_PI*dx*dy*dz);
    }
    inline double operator() (double L_coasting, double x0, double y0, double z0, double dx, double dy, double dz )
    {
      double sum = (*this)(x0,y0,z0,dx,dy,dz);
      Coulomb coulomb;
      for (size_t n=1; n<=N_coasting; n++) {
	const double zp = n*L_coasting+z0;
	const double zm = n*L_coasting-z0;
	const double value = coulomb(x0,y0,zm,dx,dy,dz) + coulomb(x0,y0,zp,dx,dy,dz);
	sum += value;
      }
      return sum;
    }
  };
  struct IntegratedRetardedCoulomb {
    enum { is_even = 1 };
    double g;
    IntegratedRetardedCoulomb(double g_ = 1.0 ) : g(g_) {}
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz )
    {
      if (g>1000) {
	return IntegratedCoulomb_2d()(x0,y0,z0,dx,dy,dz);
      }
      if (x0==0.0 && y0==0.0 && z0==0.0)
	return RetardedCoulomb(g)(x0,y0,z0,dx,dy,dz);
      const double g2 = g*g;
      // indefinite integral: integrate(integrate(integrate(1/sqrt(x^2+y^2+(g*z)^2),x),y),z)/(dx*dy*dz);
      auto f = [&](double x, double y, double z ) {
	const double x2 = x*x;
	const double y2 = y*y;
	const double z2 = z*z;
	const double gz = g*z;
	const double r = hypot(x, y, gz);
	const double rx = r*x;
	const double ry = r*y;
	const double xy = x*y;
	const double gzx = gz*x;
	const double gzy = gz*y;
	if (z==0.0) return xy*log(g2*r)/g;
	if (y==0.0) return (gzx*(log(r)-1)+x2*atan((gz)/x))/g;
	if (x==0.0) return (g2*z2*atan(y/(gz))+gzy*(2*log(r)-3)+y2*atan((gz)/y))/(2*g);
	return (2*xy*log(g2*(gz+r))-2*x2*atan(gzy/rx)-y2*atan2(gzy,y2+x2+rx)-2*y2*atan(gzx/ry)+y2*atan(gz/y)-x2*atan2(gzx,y2+x2+ry)+2*x2*atan(gz/x)+y2*atan2(gz,y)+x2*atan2(gz,x)+(g2*atan(y/gz)-g2*atan(xy/(gz*r)))*z2+(2*gzx*log(y+r)+(2*gz*log(x+r)-3*gz)*y-2*gzx))/(2*g);
      };
      double sum =
	+f(x0+dx, y0+dy, z0+dz)
	+f(x0,    y0,    z0+dz)
	+f(x0,    y0+dy, z0   )
	+f(x0+dx, y0,    z0   )
	-f(x0,    y0+dy, z0+dz)
	-f(x0+dx, y0,    z0+dz)
	-f(x0+dx, y0+dy, z0   )
	-f(x0,    y0,    z0   );
      return sum/(4*M_PI*dx*dy*dz);
    }
    inline double operator() (double L_coasting, double x0, double y0, double z0, double dx, double dy, double dz )
    {
      double sum = (*this)(x0,y0,z0,dx,dy,dz);
      RetardedCoulomb coulomb(g);
      for (size_t n=1; n<=N_coasting; n++) {
	const double zp = n*L_coasting+z0;
	const double zm = n*L_coasting-z0;
	const double value = coulomb(x0,y0,zm,dx,dy,dz) + coulomb(x0,y0,zp,dx,dy,dz);
	sum += value;
      }
      return sum;
    }
  };
}

#endif /* greens_functions_coulomb_hh */
