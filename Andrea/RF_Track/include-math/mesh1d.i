/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

/* mesh1d.i */

#if defined(SWIGOCTAVE)

// Octave array -> C++ Mesh1d
%typemap(in) const ComplexMesh1d & {
  const ComplexColumnVector &array1d = $input.complex_array_value();
  const size_t W = array1d.numel();
  $1 = new ComplexMesh1d(W);
  for (size_t i=0; i<W; i++) {
    (*$1).elem(i) = array1d(i);
  }
 }

%typemap(freearg) const ComplexMesh1d & {
  if ($1) delete $1;
 }

%typemap(typecheck) const ComplexMesh1d &  {
  octave_value obj = $input;
  $1 = false;
  if (obj.is_complex_matrix() || obj.is_real_matrix()) {
    const ComplexColumnVector array1d = obj.complex_array_value();
    dim_vector dims = array1d.dims();
    if (dims.length()==2) {
      size_t W = dims.elem(0);
      size_t H = dims.elem(1);
      if (W==1 || H==1) {
	$1 = true;
      }
    }
  }
}

// C++ Mesh1d -> Octave array
%typemap(out) const ComplexMesh1d & {
  ComplexColumnVector ret((*$1).size1());
  for (size_t i=0; i<(*$1).size1(); i++) {
    ret(i) = (*$1).elem(i);
  }
  $result = ret;
}

// T_ijk as an output argument
/*
%typemap(in, numinputs=0) ComplexMesh1d & (ComplexMesh1d temp ) {
  $1 = &temp;
}

%typemap(argout) ComplexMesh1d & {
  ComplexColumnVector T((*$1).size1());
  for (size_t i=0; i<(*$1).size1(); i++) {
    T(i) = (*$1).elem(i);
  }
  $result->append(T);
}
*/

// Octave array -> C++ Mesh1d
%typemap(in) const Mesh1d & {
  const ColumnVector &array1d = $input.array_value();
  size_t W = array1d.numel();
  $1 = new Mesh1d(W);
  for (size_t i=0; i<W; i++) {
    (*$1).elem(i) = array1d(i);
  }
 }

%typemap(freearg) const Mesh1d & {
  if ($1) delete $1;
 }

%typemap(typecheck) const Mesh1d &  {
  octave_value obj = $input;
  $1 = false;
  if (obj.is_real_matrix()) {
    const ColumnVector array1d = obj.array_value();
    dim_vector dims = array1d.dims();
    if (dims.length()==2) {
      size_t W = dims.elem(0);
      size_t H = dims.elem(1);
      if (W==1 || H==1) {
	$1 = true;
      }
    }
  }
}

// C++ Mesh1d -> Octave array
%typemap(out) Mesh1d {
  ColumnVector ret(($1).size1());
  for (size_t i=0; i<($1).size1(); i++) {
    ret(i) = ($1).elem(i);
  }
  $result = ret;
}


#endif
