/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef scalar_field_hh
#define scalar_field_hh

#include <complex>
#include <mutex>
#include <array>

#include "mesh3d_cint.hh"

class ScalarField {
protected:
  Mesh3d_CINT mesh_Phi; //
  // cartesian bounding box
  double x0, y0; // mm
  double hx, hy, hz; // mm
public:
  ScalarField() = default;
  ScalarField(const Mesh3d &Phi,
	      double x0, double y0, // mm
	      double hx, double hy, double hz ); // mm
  virtual ~ScalarField() = default;
  virtual ScalarField *clone() { return new ScalarField(*this); }
  double get_hx() const { return hx; } // return mm
  double get_hy() const { return hy; } // return mm
  double get_hz() const { return hz; } // return mm
  double get_nx() const { return mesh_Phi.size1(); }
  double get_ny() const { return mesh_Phi.size2(); }
  double get_nz() const { return mesh_Phi.size3(); }
  double get_x0() const { return x0; } // return m
  double get_y0() const { return y0; } // return m
  double get_x1() const { return x0 + (mesh_Phi.size1()-1)*hx; } // return m
  double get_y1() const { return y0 + (mesh_Phi.size2()-1)*hy; } // return m
  double get_length() const { return (mesh_Phi.size3()-1)*hz; } // return mm
  void set_hx(double hx_ ) { hx = hx_; } // accepts m
  void set_hy(double hy_ ) { hy = hy_; } // accepts m
  void set_hz(double hz_ ) { hz = hz_; } // accepts m
  void set_x0(double x0_ ) { x0 = x0_; } // accepts m
  void set_y0(double y0_ ) { y0 = y0_; } // accepts m
  void set_Phi(const Mesh3d &Phi ) { mesh_Phi = Phi; }
  StaticVector<3> grad(double x, double y, double z ) const; // x,y,z [mm]
  double operator () (double x, double y, double z ) const; // x,y,z [mm]

};

#endif /* scalar_field_hh */
