/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef vector_field_div_free_hh
#define vector_field_div_free_hh

#include <complex>
#include <mutex>
#include <array>

#include "mesh3d_cint.hh"

class VectorField_divFree { // always divergence free (e.g. a static magnetic field)
  Mesh3d_CINT mesh_Ax; //
  Mesh3d_CINT mesh_Ay; //
  Mesh3d_CINT mesh_Az; //
  // cartesian bounding box
  double x0, y0; // mm
  double hx, hy, hz; // mm
public:
  VectorField_divFree() = default;
  VectorField_divFree(const Mesh3d &Bx,
		      const Mesh3d &By,
		      const Mesh3d &Bz,
		      double x0, double y0, // mm
		      double hx, double hy, double hz ); // mm
  virtual ~VectorField_divFree() = default;
  virtual VectorField_divFree *clone() { return new VectorField_divFree(*this); }
  double get_hx() const { return hx; } // return mm
  double get_hy() const { return hy; } // return mm
  double get_hz() const { return hz; } // return mm
  double get_nx() const { return mesh_Ax.size1(); }
  double get_ny() const { return mesh_Ax.size2(); }
  double get_nz() const { return mesh_Ax.size3(); }
  double get_x0() const { return x0; } // return mm
  double get_y0() const { return y0; } // return mm
  double get_x1() const { return x0 + (mesh_Ax.size1()-1)*hx; } // return mm
  double get_y1() const { return y0 + (mesh_Ax.size2()-1)*hy; } // return mm
  double get_length() const { return (mesh_Ax.size3()-1)*hz; } // return mm
  Mesh3d get_Ax() const { return mesh_Ax; }
  Mesh3d get_Ay() const { return mesh_Ay; }
  Mesh3d get_Az() const { return mesh_Az; }
  void set_hx(double hx_ ) { hx = hx_; } // accepts mm
  void set_hy(double hy_ ) { hy = hy_; } // accepts mm
  void set_hz(double hz_ ) { hz = hz_; } // accepts mm
  void set_x0(double x0_ ) { x0 = x0_; } // accepts mm
  void set_y0(double y0_ ) { y0 = y0_; } // accepts mm
  void set_Vx_Vy_Vz(const Mesh3d &Bx, const Mesh3d &By, const Mesh3d &Bz );
  StaticVector<3> operator () (double x, double y, double z ) const; // x,y,z [mm]
  StaticMatrix<3,3> jacobian(double x, double y, double z ) const; // x,y,z [mm]

};

#endif /* vector_field_div_free_hh */
