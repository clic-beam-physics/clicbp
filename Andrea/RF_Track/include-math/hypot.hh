/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef hypot_hh
#define hypot_hh

#include <cmath>
#include <limits>

static inline double hypot(double x, double y, double z )
{
  x = fabs(x);
  y = fabs(y);
  z = fabs(z);
  if (x>y) {
    if (x>z) {
      if (x>std::numeric_limits<double>::epsilon()) {
	y /= x;
	z /= x;
	return x*sqrt(1.+y*y+z*z);
      }
    } else {
      if (z>std::numeric_limits<double>::epsilon()) {
	x /= z;
	y /= z;
	return z*sqrt(1.+x*x+y*y);
      }
    }
  } else {
    if (y>z) {
      if (y>std::numeric_limits<double>::epsilon()) {
	x /= y;
	z /= y;
	return y*sqrt(1.+x*x+z*z);
      }
    } else {
      if (z>std::numeric_limits<double>::epsilon()) {
	x /= z;
	y /= z;
	return z*sqrt(1.+x*x+y*y);
      }
    }
  }
  return 0.0;
}

#endif /* hypot_hh */
