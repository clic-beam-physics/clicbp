/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef mesh3d_lint_hh
#define mesh3d_lint_hh

#include "mesh3d.hh"

template <class T, class Alloc = std::allocator<T>>
class TMesh3d_LINT : public TMesh3d<T,Alloc> {
protected:
  using TMesh3d<T,Alloc>::nx;
  using TMesh3d<T,Alloc>::ny;
  using TMesh3d<T,Alloc>::nz;
public:
  using TMesh3d<T,Alloc>::TMesh3d;
  using TMesh3d<T,Alloc>::elem;
  using TMesh3d<T,Alloc>::operator=;
  
  template <class Alloc1> TMesh3d_LINT(const TMesh3d<T,Alloc1> &m ) : TMesh3d<T,Alloc>(m) {}

  inline T operator()(double x, double y, double z ) const // trilinear interpolation
  {
    if (x<0.0) x = 0.0; else if (x>double(nx)-1.0) x = double(nx)-1.0;
    if (y<0.0) y = 0.0; else if (y>double(ny)-1.0) y = double(ny)-1.0;
    if (z<0.0) z = 0.0; else if (z>double(nz)-1.0) z = double(nz)-1.0;
    double iinteger, jinteger, kinteger;
    double ifrac = modf(x, &iinteger);
    double jfrac = modf(y, &jinteger);
    double kfrac = modf(z, &kinteger);
    size_t i = size_t(iinteger);
    size_t j = size_t(jinteger);
    size_t k = size_t(kinteger);
    auto i0j0k0 = elem(i,j,k);
    if (i+1<nx) { // X OK
      auto i1j0k0 = elem(i+1,j,k);
      auto ifj0k0 = LINT(i0j0k0, i1j0k0, ifrac);
      if (j+1<ny) { // X & Y OK
        auto i0j1k0 = elem(i,j+1,k);
        auto i1j1k0 = elem(i+1,j+1,k);
        auto ifj1k0 = LINT(i0j1k0, i1j1k0, ifrac);
        auto ifjfk0 = LINT(ifj0k0, ifj1k0, jfrac);
        if (k+1<nx) { // X & Y & Z OK
          auto i0j0k1 = elem(i,j,k+1);
          auto i1j0k1 = elem(i+1,j,k+1);
          auto i0j1k1 = elem(i,j+1,k+1);
          auto i1j1k1 = elem(i+1,j+1,k+1);
          auto ifj0k1 = LINT(i0j0k1, i1j0k1, ifrac);
          auto ifj1k1 = LINT(i0j1k1, i1j1k1, ifrac);
          auto ifjfk1 = LINT(ifj0k1, ifj1k1, jfrac);
          auto ifjfkf = LINT(ifjfk0, ifjfk1, kfrac);
          return ifjfkf;
        } else { // X & Y OK ; Z NOT OK
          return ifjfk0;
        }
      } else {
        if (k+1<nx) { // X & Z OK ; Y NOT OK
          auto i0j0k1 = elem(i,j,k+1);
          auto i1j0k1 = elem(i+1,j,k+1);
          auto ifj0k1 = LINT(i0j0k1, i1j0k1, ifrac);
          auto ifj0kf = LINT(ifj0k0, ifj0k1, kfrac);
          return ifj0kf;
        } else { // X OK ; Y & Z NOT OK
          return ifj0k0;
        }
      }
    } else {
      if (j+1<ny) {
        auto i0j1k0 = elem(i,j+1,k);
        auto i0jfk0 = LINT(i0j0k0, i0j1k0, jfrac);
        if (k+1<nx) { // X NOT OK ; Y & Z OK
          auto i0j0k1 = elem(i,j,k+1);
          auto i0j1k1 = elem(i,j+1,k+1);
          auto i0jfk1 = LINT(i0j0k1, i0j1k1, jfrac);
          auto i0jfkf = LINT(i0jfk0, i0jfk1, kfrac);
          return i0jfkf;
        } else { // X & Z NOT OK ; Y OK
          return i0jfk0;
        }
      } else {
        if (k+1<nx) { // X & Y NOT OK ; Z OK
          auto i0j0k1 = elem(i,j,k+1);
          auto i0j0kf = LINT(i0j0k0, i0j0k1, kfrac);
          return i0j0kf;
        } else { // X & Y & Z NOT OK
          return i0j0k0;
        }
      }
    }
  }

  inline T deriv_x(double x, double y, double z ) const // trilinear interpolation
  {
    if (x<0.0) x = 0.0; else if (x>double(nx)-1.0) x = double(nx)-1.0;
    if (y<0.0) y = 0.0; else if (y>double(ny)-1.0) y = double(ny)-1.0;
    if (z<0.0) z = 0.0; else if (z>double(nz)-1.0) z = double(nz)-1.0;
    auto z_interpolate = [&] (size_t i, size_t j ) {
      double kinteger, kfrac = modf(z, &kinteger);
      const size_t k = size_t(kinteger);
      if (k+1<nz) {
	T p0 = elem(i,j,k);
	T p1 = elem(i,j,k+1);
	return LINT(p0,p1,kfrac);
      }
      T p0 = elem(i,j,k);
      return p0;
    };
    auto yz_interpolate = [&] (size_t i ) {
      double jinteger, jfrac = modf(y, &jinteger);
      const size_t j = size_t(jinteger);
      if (j+1<ny) {
	T p0 = z_interpolate(i,j);
	T p1 = z_interpolate(i,j+1);
	return LINT(p0,p1,jfrac);
      }
      T p0 = z_interpolate(i,j);
      return p0;
    };
    auto xyz_interpolate = [&]() {
      double iinteger = floor(x);
      const size_t i = size_t(iinteger);
      if (i+1<nx) {
	T p0 = yz_interpolate(i);
	T p1 = yz_interpolate(i+1);
	return LINT_deriv(p0,p1);
      }
      T p0 = yz_interpolate(i-1);
      T p1 = yz_interpolate(i);
      return LINT_deriv(p0,p1); // backward differentiation
    };
    return xyz_interpolate();
  }

  inline T deriv_y(double x, double y, double z ) const // trilinear interpolation
  {
    if (x<0.0) x = 0.0; else if (x>double(nx)-1.0) x = double(nx)-1.0;
    if (y<0.0) y = 0.0; else if (y>double(ny)-1.0) y = double(ny)-1.0;
    if (z<0.0) z = 0.0; else if (z>double(nz)-1.0) z = double(nz)-1.0;
    auto z_interpolate = [&] (size_t i, size_t j ) {
      double kinteger, kfrac = modf(z, &kinteger);
      const size_t k = size_t(kinteger);
      if (k+1<nz) {
	T p0 = elem(i,j,k);
	T p1 = elem(i,j,k+1);
	return LINT(p0,p1,kfrac);
      }
      T p0 = elem(i,j,k);
      return p0;
    };
    auto yz_interpolate = [&] (size_t i ) {
      double jinteger = floor(y);
      const size_t j = size_t(jinteger);
      if (j+1<ny) {
	T p0 = z_interpolate(i,j);
	T p1 = z_interpolate(i,j+1);
	return LINT_deriv(p0,p1);
      }
      T p0 = z_interpolate(i,j-1);
      T p1 = z_interpolate(i,j);
      return LINT_deriv(p0,p1); // backward differentiation
    };
    auto xyz_interpolate = [&]() {
      double iinteger, ifrac = modf(x, &iinteger);
      const size_t i = size_t(iinteger);
      if (i+1<nx) {
	T p0 = yz_interpolate(i);
	T p1 = yz_interpolate(i+1);
       	return LINT(p0,p1,ifrac);
      }
      T p0 = yz_interpolate(i);
      return p0;
    };
    return xyz_interpolate();
  }
  
  inline T deriv_z(double x, double y, double z ) const // trilinear interpolation
  {
    if (x<0.0) x = 0.0; else if (x>double(nx)-1.0) x = double(nx)-1.0;
    if (y<0.0) y = 0.0; else if (y>double(ny)-1.0) y = double(ny)-1.0;
    if (z<0.0) z = 0.0; else if (z>double(nz)-1.0) z = double(nz)-1.0;
    auto z_interpolate = [&] (size_t i, size_t j ) {
      double kinteger = floor(z);
      const size_t k = size_t(kinteger);
      if (k+1<nz) {
	T p0 = elem(i,j,k);
	T p1 = elem(i,j,k+1);
	return LINT_deriv(p0,p1);
      }
      T p0 = elem(i,j,k-1);
      T p1 = elem(i,j,k);
      return LINT_deriv(p0,p1); // backward differentiation
    };
    auto yz_interpolate = [&] (size_t i ) {
      double jinteger, jfrac = modf(y, &jinteger);
      const size_t j = size_t(jinteger);
      if (j+1<ny) {
	T p0 = z_interpolate(i,j);
	T p1 = z_interpolate(i,j+1);
	return LINT(p0,p1,jfrac);
      }
      T p0 = z_interpolate(i,j);
      return p0;
    };
    auto xyz_interpolate = [&]() {
      double iinteger, ifrac = modf(x, &iinteger);
      const size_t i = size_t(iinteger);
      if (i+1<nx) {
	T p0 = yz_interpolate(i);
	T p1 = yz_interpolate(i+1);
	return LINT(p0,p1,ifrac);
      }
      T p0 = yz_interpolate(i);
      return p0;
    };
    return xyz_interpolate();
  }

};

#endif /* mesh3d_lint_hh */
