/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

/* static_matrix_33.i */

%{
#include "static_matrix.hh"
%}

#if defined(SWIGOCTAVE)

// Octave Matrix -> C++ StaticMatrix<3,3>
%typemap(in) const StaticMatrix<3,3> & {
  const Matrix &matrix = $input.matrix_value();
  $1 = new StaticMatrix<3,3>;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      (*$1)[i][j] = matrix(i,j);
 }

%typemap(freearg) const StaticMatrix<3,3> & {
  if ($1) delete $1;
 }

%typemap(typecheck) const StaticMatrix<3,3> & {
  octave_value obj = $input;
  if (obj.is_real_scalar() || obj.is_real_matrix()) {
    const Matrix &matrix = obj.matrix_value();
    if (matrix.rows()==3 && matrix.columns()==3) {
      $1 = 1;
    } else {
      $1 = 0;
    }
  } else {
    $1 = 0;
  }
 }

%typemap(out) const StaticMatrix<3,3> & {
  Matrix ret(3, 3);
  for (size_t i=0; i<3; i++)
    for (size_t j=0; j<3; j++)
      ret(i,j) = (*$1)[i][j];
  $result = ret;
 }

// C++ StaticMatrix<3,3> -> Octave Matrix (as an output argument)
%typemap(in, numinputs=0) StaticMatrix<3,3> & (StaticMatrix<3,3> temp ) {
  $1 = &temp;
 }

%typemap(argout) StaticMatrix<3,3> & {
  Matrix ret(3,3);
  for (size_t i=0; i<3; i++)
    for (size_t j=0; j<3; j++)
      ret(i,j) = (*$1)[i][j];
  $result->append(ret);
 }

%typemap(freearg,noblock=1) StaticMatrix<3,3> & {
 }


#endif

#if defined(SWIGPYTHON)
%{
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include <numpy/arrayobject.h>
#define is_array(a) ((a) && PyArray_Check((PyArrayObject *)a))
%}

%init %{
  import_array();
  %}

// NumPy Matrix -> C++ StaticMatrix<3,3>
%typemap(in) const StaticMatrix<3,3> & {
  if (is_array($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_DOUBLE, 1, 2);
    npy_intp strides[2] = { 0, 0 };
    if (PyArray_NDIM(array) == 1) {
      strides[0] = 0;
      strides[1] = PyArray_STRIDES(array)[0];
    } else {
      strides[0] = PyArray_STRIDES(array)[0];
      strides[1] = PyArray_STRIDES(array)[1];
    }
    char *data = PyArray_BYTES(array);
    $1 = new StaticMatrix<3,3>;
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
        (*$1)[i][j] = *(double *)(data + i*strides[0] + j*strides[1]);
  } else {
    $1 = new StaticMatrix<3,3>(1, 1);
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
	(*$1)[i][j] = PyFloat_AsDouble($input);
  }
 }

%typemap(freearg) const StaticMatrix<3,3> & {
  if ($1) delete $1;
 }

%typemap(typecheck) const StaticMatrix<3,3> & {
  if (is_array($input) || PyFloat_Check($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_DOUBLE, 1, 2);
    npy_intp strides[2] = { 0, 0 };
    int rows, cols;
    if (PyArray_NDIM(array) == 1) {
      rows = 1;
      cols = PyArray_DIMS(array)[0];
      strides[0] = 0;
      strides[1] = PyArray_STRIDES(array)[0];
    } else {
      rows = PyArray_DIMS(array)[0];
      cols = PyArray_DIMS(array)[1];
      strides[0] = PyArray_STRIDES(array)[0];
      strides[1] = PyArray_STRIDES(array)[1];
    }
    if (rows==3 && cols==3) {
      $1 = 1;
    } else {
      $1 = 0;
    }
  } else {
    $1 = 0;
  }
 }

// C++ StaticMatrix<3,3> -> NumPy Matrix (as a return value)
%typemap(out) StaticMatrix<3,3> {
  npy_intp dimensions[2] = { 3, 3 };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      *(double*)(data + i*strides[0] + j*strides[1]) = $1[i][j];
  $result = PyArray_Return(res);
 }

%typemap(out) const StaticMatrix<3,3> & {
  npy_intp dimensions[2] = { 3, 3 };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      *(double*)(data + i*strides[0] + j*strides[1]) = (*$1)[i][j];
  $result = PyArray_Return(res);
}

// C++ StaticMatrix<3,3> -> NumPy Matrix (as an output argument)
%typemap(in, numinputs=0) StaticMatrix<3,3> & (StaticMatrix<3,3> temp ) {
  $1 = &temp;
 }

%typemap(argout) StaticMatrix<3,3> & {
  npy_intp dimensions[2] = { 3, 3 };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (size_t i=0; i<3; i++)
    for (size_t j=0; j<3; j++)
      *(double*)(data + i*strides[0] + j*strides[1]) = (*$1)[i][j];
  $result = SWIG_Python_AppendOutput($result, PyArray_Return(res));
 }

%typemap(freearg,noblock=1) StaticMatrix<3,3> & {
 }

#endif
