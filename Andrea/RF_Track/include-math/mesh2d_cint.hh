/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef mesh2d_cint_hh
#define mesh2d_cint_hh

#include "mesh2d.hh"

template <class T, class Alloc = std::allocator<T>>
class TMesh2d_CINT : public TMesh2d<T,Alloc> {
protected:
  using TMesh2d<T,Alloc>::nx;
  using TMesh2d<T,Alloc>::ny;
public:
  using TMesh2d<T,Alloc>::TMesh2d;
  using TMesh2d<T,Alloc>::elem;

  inline T operator()(double x, double y ) const // bicubic interpolation
  {
    if (x<0.0) x = 0.0; else if (x>double(nx)-1.0) x = double(nx)-1.0;
    if (y<0.0) y = 0.0; else if (y>double(ny)-1.0) y = double(ny)-1.0;
    auto y_interpolate = [&] (size_t i ) {
      double jinteger, jfrac = modf(y, &jinteger);
      const size_t j = size_t(jinteger);
      if (j>0 && j+2<ny) {
	T p0 = elem(i,j-1);
	T p1 = elem(i,j);
	T p2 = elem(i,j+1);
	T p3 = elem(i,j+2);
	return CINT1(p0,p1,p2,p3,jfrac);
      }
      if (j>1 && j+1<ny) {
	T p0 = elem(i,j-2);
	T p1 = elem(i,j-1);
	T p2 = elem(i,j);
	T p3 = elem(i,j+1);
	return CINT2(p0,p1,p2,p3,jfrac);
      }
      if (j==0) {
	T p0 = elem(i,j);
	T p1 = elem(i,j+1);
	T p2 = elem(i,j+2);
	T p3 = elem(i,j+3);
	return CINT0(p0,p1,p2,p3,jfrac);
      }
      return elem(i,j);
    };
    auto xy_interpolate = [&] () {
      double iinteger, ifrac = modf(x, &iinteger);
      const size_t i = size_t(iinteger);
      if (i>0 && i+2<nx) {
	T p0 = y_interpolate(i-1);
	T p1 = y_interpolate(i);
	T p2 = y_interpolate(i+1);
	T p3 = y_interpolate(i+2);
	return CINT1(p0,p1,p2,p3,ifrac);
      }
      if (i>1 && i+1<nx) {
	T p0 = y_interpolate(i-2);
	T p1 = y_interpolate(i-1);
	T p2 = y_interpolate(i);
	T p3 = y_interpolate(i+1);
	return CINT2(p0,p1,p2,p3,ifrac);
      }
      if (i==0) {
	T p0 = y_interpolate(i);
	T p1 = y_interpolate(i+1);
	T p2 = y_interpolate(i+2);
	T p3 = y_interpolate(i+3);
	return CINT0(p0,p1,p2,p3,ifrac);
      }
      return y_interpolate(i);
    };
    return xy_interpolate();
  }

};

#endif /* mesh2d_cint_hh */
