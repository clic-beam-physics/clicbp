/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef mesh2d_hh
#define mesh2d_hh

#include <cmath>
#include <complex>
#include <assert.h>
#include <valarray>
#include <algorithm>

#include "numtools.hh"
#include "fftw_complex.hh"

template <class T, class Alloc = std::allocator<T>>
class TMesh2d {
protected:
  size_t nx, ny; /// width, height
  std::vector<T, Alloc> data_;
  T dummy;
public:
  template <class Alloc1> TMesh2d(const TMesh2d<T,Alloc1> &m ) : nx(m.size1()), ny(m.size2()), data_(m.size()) { for (size_t i=0; i<m.size(); i++) { data_[i] = m.data()[i]; } } 
  explicit TMesh2d(size_t Nx=4, size_t Ny=4 ) : nx(Nx), ny(Ny), data_(Nx*Ny) {}
  TMesh2d(size_t Nx, size_t Ny, T val ) : nx(Nx), ny(Ny), data_(Nx*Ny,val) {}
  void resize(size_t Nx, size_t Ny, T val ) { nx=Nx; ny=Ny; data_.resize(Nx*Ny,val); }
  void resize(size_t Nx, size_t Ny ) { nx=Nx; ny=Ny; data_.resize(Nx*Ny); }
  size_t size() const { return data_.size(); }
  size_t size1() const { return nx; }
  size_t size2() const { return ny; }
  T *data() { return data_.data(); }
  const T *data() const { return data_.data(); }
  template <class Alloc1> const TMesh2d &operator = (const TMesh2d<T,Alloc1> &m ) { resize(m.size1(), m.size2()); for (size_t i=0; i<m.size(); i++) { data_[i] = m.data()[i]; } return *this; }
  const TMesh2d &operator -= (const TMesh2d &m ) { for (size_t i=0; i<m.size(); i++) { data_[i] -= m.data()[i]; } return *this; }
  const TMesh2d &operator += (const TMesh2d &m ) { for (size_t i=0; i<m.size(); i++) { data_[i] += m.data()[i]; } return *this; }
  const TMesh2d &operator = (double a ) { for (T &x : data_) x = a; return *this; }
  const TMesh2d &operator *= (double a ) { for (T &x : data_) x *= a; return *this; }
  const TMesh2d &operator /= (double a ) { for (T &x : data_) x /= a; return *this; }
  friend TMesh2d operator / (const TMesh2d &m, double a ) { TMesh2d n = m; n/=a; return n; }
  inline const T &elem(size_t i, size_t j ) const { return data_[j + ny * i]; }
  inline T &elem(size_t i, size_t j )
  {
    if (i>=nx||j>=ny) return dummy;
    return data_[j + ny * i];
  }

  inline void assign_value(double x, double y, const T &value )
  {
    if (x<0.0) x = 0.0; else if (x>double(nx)-1.0) x = double(nx)-1.0;
    if (y<0.0) y = 0.0; else if (y>double(ny)-1.0) y = double(ny)-1.0;
    if (x==double(nx)-1.0 && y==double(ny)-1.0) {
      elem(nx-1,ny-1) += value;
      return;
    }
    if (x==double(nx)-1.0) {
      double jinteger;
      const double jfrac = modf(y, &jinteger);
      const size_t i = nx-1;
      const size_t j = size_t(jinteger);
      elem(i,   j+1) += value * (jfrac);
      elem(i,   j  ) += value * (1.0 - jfrac);
      return;
    }
    if (y==double(ny)-1.0) {
      double iinteger;
      const double ifrac = modf(x, &iinteger);
      const size_t i = size_t(iinteger);
      const size_t j = ny-1;
      elem(i+1, j) += value * (ifrac);
      elem(i,   j) += value * (1.0 - ifrac);
      return;
    }
    double iinteger, jinteger;
    const double ifrac = modf(x, &iinteger);
    const double jfrac = modf(y, &jinteger);
    const size_t i = size_t(iinteger);
    const size_t j = size_t(jinteger);
    elem(i+1, j+1) += value * (ifrac) * (jfrac);
    elem(i+1, j  ) += value * (ifrac) * (1.0 - jfrac);
    elem(i,   j+1) += value * (1.0 - ifrac) * (jfrac);
    elem(i,   j  ) += value * (1.0 - ifrac) * (1.0 - jfrac);
  }
};

//

#include "mesh2d_lint.hh"
#include "mesh2d_cint.hh"

//

typedef TMesh2d     <double> Mesh2d;
typedef TMesh2d_LINT<double> Mesh2d_LINT;
typedef TMesh2d_CINT<double> Mesh2d_CINT;

typedef TMesh2d     <fftwComplex> ComplexMesh2d;
typedef TMesh2d_LINT<fftwComplex> ComplexMesh2d_LINT;
typedef TMesh2d_CINT<fftwComplex> ComplexMesh2d_CINT;

extern ComplexMesh2d DefaultComplexMesh2d;
extern Mesh2d DefaultMesh2d;

#endif /* mesh2d_hh */
