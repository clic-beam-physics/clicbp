/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef sinhc_hh
#define sinhc_hh

#include <limits>
#include <cmath>

namespace {
  // taken from http://www.boost.org/doc/libs/1_57_0/boost/math/special_functions/sinc.hpp
  inline double sinhc(const double &x )
  {
    const double taylor_0_bound = sqrt(std::numeric_limits<double>::epsilon());
    const double taylor_2_bound = sqrt(taylor_0_bound);
    const double taylor_4_bound = sqrt(taylor_2_bound);

    if (fabs(x) >= taylor_4_bound)
      return sinh(x)/x;

    // approximation by taylor series in x at 0 up to order 0
    double result = 1.0;
    if (fabs(x) >= taylor_0_bound) {
      double x2 = x*x;
      // approximation by taylor series in x at 0 up to order 2
      result += x2/6.0;
      if (fabs(x) >= taylor_2_bound) {
	// approximation by taylor series in x at 0 up to order 4
	result += (x2*x2)/120.0;
      }
    } 
    return result;
  }
}

#endif /* sinhc_hh */
