/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef mesh1d_cint_hh
#define mesh1d_cint_hh

#include "mesh1d.hh"

template <class T, class Alloc = std::allocator<T>>
class TMesh1d_CINT : public TMesh1d<T,Alloc> {
protected:
  using TMesh1d<T,Alloc>::nx;
public:
  using TMesh1d<T,Alloc>::TMesh1d;
  using TMesh1d<T,Alloc>::elem;

  template <class Alloc1> TMesh1d_CINT(const TMesh1d<T,Alloc1> &m ) : TMesh1d<T,Alloc>(m) {}

  inline T operator()(double x ) const // cubic interpolation
  {
    if (x<0.0) x = 0.0; else if (x>double(nx)-1.0) x = double(nx)-1.0;
    double iinteger, ifrac = modf(x, &iinteger);
    size_t i = size_t(iinteger);
    if (i>0 && i+2<nx) {
      T p0 = elem(i-1);
      T p1 = elem(i);
      T p2 = elem(i+1);
      T p3 = elem(i+2);
      return CINT1(p0,p1,p2,p3,ifrac);
    }
    if (i>1 && i+1<nx) {
      T p0 = elem(i-2);
      T p1 = elem(i-1);
      T p2 = elem(i);
      T p3 = elem(i+1);
      return CINT2(p0,p1,p2,p3,ifrac);
    }
    if (i==0) {
      T p0 = elem(i);
      T p1 = elem(i+1);
      T p2 = elem(i+2);
      T p3 = elem(i+3);
      return CINT0(p0,p1,p2,p3,ifrac);
    }
    return elem(i);
  }

  inline T deriv(double x ) const // first derivative
  {
    if (x<0.0) x = 0.0; else if (x>double(nx)-1.0) x = double(nx)-1.0;
    double iinteger, ifrac = modf(x, &iinteger);
    size_t i = size_t(iinteger);
    if (i>0 && i+2<nx) {
      T p0 = elem(i-1);
      T p1 = elem(i);
      T p2 = elem(i+1);
      T p3 = elem(i+2);
      return CINT1_deriv(p0,p1,p2,p3,ifrac);
    }
    if (i>1 && i+1<nx) {
      T p0 = elem(i-2);
      T p1 = elem(i-1);
      T p2 = elem(i);
      T p3 = elem(i+1);
      return CINT2_deriv(p0,p1,p2,p3,ifrac);
    }
    if (i==0) {
      T p0 = elem(i);
      T p1 = elem(i+1);
      T p2 = elem(i+2);
      T p3 = elem(i+3);
      return CINT0_deriv(p0,p1,p2,p3,ifrac);
    }
    T p0 = elem(i-3);
    T p1 = elem(i-2);
    T p2 = elem(i-1);
    T p3 = elem(i);
    return CINT2_deriv(p0,p1,p2,p3,1.0); // backward differentiation
  }

  inline T deriv2(double x ) const // second derivative
  {
    if (x<0.0) x = 0.0; else if (x>double(nx)-1.0) x = double(nx)-1.0;
    double iinteger, ifrac = modf(x, &iinteger);
    size_t i = size_t(iinteger);
    if (i>0 && i+2<nx) {
      T p0 = elem(i-1);
      T p1 = elem(i);
      T p2 = elem(i+1);
      T p3 = elem(i+2);
      return CINT1_deriv2(p0,p1,p2,p3,ifrac);
    }
    if (i>1 && i+1<nx) {
      T p0 = elem(i-2);
      T p1 = elem(i-1);
      T p2 = elem(i);
      T p3 = elem(i+1);
      return CINT2_deriv2(p0,p1,p2,p3,ifrac);
    }
    if (i==0) {
      T p0 = elem(i);
      T p1 = elem(i+1);
      T p2 = elem(i+2);
      T p3 = elem(i+3);
      return CINT0_deriv2(p0,p1,p2,p3,ifrac);
    }
    T p0 = elem(i-3);
    T p1 = elem(i-2);
    T p2 = elem(i-1);
    T p3 = elem(i);
    return CINT2_deriv2(p0,p1,p2,p3,1.0); // backward differentiation
  }
  
  inline T deriv3(double x ) const // third derivative
  {
    if (x<0.0) x = 0.0; else if (x>double(nx)-1.0) x = double(nx)-1.0;
    double iinteger, ifrac = modf(x, &iinteger);
    size_t i = size_t(iinteger);
    if (i>0 && i+2<nx) {
      T p0 = elem(i-1);
      T p1 = elem(i);
      T p2 = elem(i+1);
      T p3 = elem(i+2);
      return CINT1_deriv3(p0,p1,p2,p3,ifrac);
    }
    if (i>1 && i+1<nx) {
      T p0 = elem(i-2);
      T p1 = elem(i-1);
      T p2 = elem(i);
      T p3 = elem(i+1);
      return CINT2_deriv3(p0,p1,p2,p3,ifrac);
    }
    if (i==0) {
      T p0 = elem(i);
      T p1 = elem(i+1);
      T p2 = elem(i+2);
      T p3 = elem(i+3);
      return CINT0_deriv3(p0,p1,p2,p3,ifrac);
    }
    T p0 = elem(i-3);
    T p1 = elem(i-2);
    T p2 = elem(i-1);
    T p3 = elem(i);
    return CINT2_deriv3(p0,p1,p2,p3,1.0); // backward differentiation
  }
};

#endif /* mesh1d_cint_hh */
