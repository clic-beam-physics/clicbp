/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef mesh3d_hh
#define mesh3d_hh

#include <cmath>
#include <complex>
#include <assert.h>
#include <valarray>
#include <algorithm>

#include "numtools.hh"
#include "fftw_complex.hh"

template <class T, class Alloc = std::allocator<T>>
class TMesh3d {
protected:
  size_t nx, ny, nz; /// width, height, depth
  std::vector<T, Alloc> data_;
  T dummy;
public:
  template <class Alloc1> TMesh3d(const TMesh3d<T,Alloc1> &m ) : nx(m.size1()), ny(m.size2()), nz(m.size3()), data_(m.size()) { for (size_t i=0; i<m.size(); i++) { data_[i] = m.data()[i]; } } 
  explicit TMesh3d(size_t Nx=4, size_t Ny=4, size_t Nz=4 ) : nx(Nx), ny(Ny), nz(Nz), data_(Nx*Ny*Nz) {}
  TMesh3d(size_t Nx, size_t Ny, size_t Nz, T val ) : nx(Nx), ny(Ny), nz(Nz), data_(Nx*Ny*Nz,val) {}
  void resize(size_t Nx, size_t Ny, size_t Nz, T val ) { nx=Nx; ny=Ny; nz=Nz; data_.resize(Nx*Ny*Nz,val); }
  void resize(size_t Nx, size_t Ny, size_t Nz ) { nx=Nx; ny=Ny; nz=Nz; data_.resize(Nx*Ny*Nz); }
  size_t size() const { return data_.size(); }
  size_t size1() const { return nx; }
  size_t size2() const { return ny; }
  size_t size3() const { return nz; }
  T *data() { return data_.data(); }
  const T *data() const { return data_.data(); }
  template <class Alloc1> const TMesh3d &operator = (const TMesh3d<T,Alloc1> &m ) { resize(m.size1(), m.size2(), m.size3()); for (size_t i=0; i<m.size(); i++) { data_[i] = m.data()[i]; } return *this; }
  const TMesh3d &operator -= (const TMesh3d &m ) { for (size_t i=0; i<m.size(); i++) { data_[i] -= m.data()[i]; } return *this; }
  const TMesh3d &operator += (const TMesh3d &m ) { for (size_t i=0; i<m.size(); i++) { data_[i] += m.data()[i]; } return *this; }
  const TMesh3d &operator = (double a ) { for (T &x : data_) x = a; return *this; }
  const TMesh3d &operator *= (double a ) { for (T &x : data_) x *= a; return *this; }
  const TMesh3d &operator /= (double a ) { for (T &x : data_) x /= a; return *this; }
  friend TMesh3d operator / (const TMesh3d &m, double a ) { TMesh3d n = m; n/=a; return n; }
  inline const T &elem(size_t i, size_t j, size_t k ) const { return data_[k + nz * (j + ny * i)]; }
  inline T &elem(size_t i, size_t j, size_t k )
  {
    if (i>=nx||j>=ny||k>=nz) return dummy;
    return data_[k + nz * (j + ny * i)];
  }

  inline void assign_value(double x, double y, double z, const T &value )
  {
    if (x<0.0) x = 0.0; else if (x>double(nx)-1.0) x = double(nx)-1.0;
    if (y<0.0) y = 0.0; else if (y>double(ny)-1.0) y = double(ny)-1.0;
    if (z<0.0) z = 0.0; else if (z>double(nz)-1.0) z = double(nz)-1.0;
    double iinteger, jinteger, kinteger;
    const double ifrac = modf(x, &iinteger);
    const double jfrac = modf(y, &jinteger);
    const double kfrac = modf(z, &kinteger);
    const size_t i = size_t(iinteger);
    const size_t j = size_t(jinteger);
    const size_t k = size_t(kinteger);
    if (i+1<nx) {
      if (j+1<ny) {
        if (k+1<nz) {
	  elem(i+1, j+1, k+1) += value * (ifrac) * (jfrac) * (kfrac);
        }
	elem(i+1, j+1, k  ) += value * (ifrac) * (jfrac) * (1.0 - kfrac);
      }
      if (k+1<nz) {
        elem(i+1, j,   k+1) += value * (ifrac) * (1.0 - jfrac) * (kfrac);
      }
      elem(i+1, j,   k  ) += value * (ifrac) * (1.0 - jfrac) * (1.0 - kfrac);
    }
    if (j+1<ny) {
      if (k+1<nz) {
	elem(i,   j+1, k+1) += value * (1.0 - ifrac) * (jfrac) * (kfrac);
      }
      elem(i,   j+1, k  ) += value * (1.0 - ifrac) * (jfrac) * (1.0 - kfrac);
    }
    if (k+1<nz) {
      elem(i,   j,   k+1) += value * (1.0 - ifrac) * (1.0 - jfrac) * (kfrac);
    }
    elem(i,   j,   k  ) += value * (1.0 - ifrac) * (1.0 - jfrac) * (1.0 - kfrac);
  }
};

//

#include "mesh3d_lint.hh"
#include "mesh3d_cint.hh"

//

typedef TMesh3d     <double> Mesh3d;
typedef TMesh3d_LINT<double> Mesh3d_LINT;
typedef TMesh3d_CINT<double> Mesh3d_CINT;

typedef TMesh3d     <fftwComplex> ComplexMesh3d;
typedef TMesh3d_LINT<fftwComplex> ComplexMesh3d_LINT;
typedef TMesh3d_CINT<fftwComplex> ComplexMesh3d_CINT;

extern ComplexMesh3d DefaultComplexMesh3d;
extern Mesh3d DefaultMesh3d;

#endif /* mesh3d_hh */

