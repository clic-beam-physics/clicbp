/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

/* mesh3d.i */

#if defined(SWIGOCTAVE)

// Octave array -> C++ Mesh3d
%typemap(in) const ComplexMesh3d & {
  const ComplexNDArray &array3d = $input.complex_array_value();
  dim_vector dims = array3d.dims();
  size_t W = dims.elem(0);
  size_t H = dims.elem(1);
  size_t D = dims.elem(2);
  $1 = new ComplexMesh3d(W, H, D);
  for (size_t i=0; i<W; i++) {
    for (size_t j=0; j<H; j++) {
      for (size_t k=0; k<D; k++) {
	(*$1).elem(i,j,k) = array3d(i,j,k);
      }
    }
  }
 }

%typemap(freearg) const ComplexMesh3d & {
  if ($1) delete $1;
 }

%typemap(typecheck) const ComplexMesh3d &  {
  octave_value obj = $input;
  $1 = false;
  if (obj.is_complex_matrix() || obj.is_real_matrix()) {
    const ComplexNDArray array3d = obj.complex_array_value();
    if (array3d.dims().length() == 3) {
      $1 = true;
    }
  }
}

// C++ Mesh3d -> Octave array
%typemap(out) const ComplexMesh3d & {
  dim_vector dv3;
  dv3.resize(3);
  dv3.elem(0)=(*$1).size1();
  dv3.elem(1)=(*$1).size2();
  dv3.elem(2)=(*$1).size3();
  ComplexNDArray ret(dv3);
  for (int i=0; i<dv3.elem(0); i++) {
    for (int j=0; j<dv3.elem(1); j++) {
      for (int k=0; k<dv3.elem(2); k++) {
        ret(i,j,k) = (*$1).elem(i,j,k);
      }
    }
  }
  $result = ret;
}

// T_ijk as an output argument
/*
%typemap(in, numinputs=0) ComplexMesh3d & (ComplexMesh3d temp ) {
  $1 = &temp;
}

%typemap(argout) ComplexMesh3d & {
  dim_vector dv3;
  dv3.resize(3);
  dv3.elem(0)=(*$1).size1();
  dv3.elem(1)=(*$1).size2();
  dv3.elem(2)=(*$1).size3();
  ComplexNDArray T(dv3);
  for (int i=0; i<dv3.elem(0); i++) {
    for (int j=0; j<dv3.elem(1); j++) {
      for (int k=0; k<dv3.elem(2); k++) {
        T(i,j,k) = (*$1).elem(i,j,k);
      }
    }
  }
  $result->append(T);
}
*/

// Octave array -> C++ Mesh3d
%typemap(in) const Mesh3d & {
  const NDArray &array3d = $input.array_value();
  dim_vector dims = array3d.dims();
  size_t W = dims.elem(0);
  size_t H = dims.elem(1);
  size_t D = dims.elem(2);
  $1 = new Mesh3d(W, H, D);
  for (size_t i=0; i<W; i++) {
    for (size_t j=0; j<H; j++) {
      for (size_t k=0; k<D; k++) {
	(*$1).elem(i,j,k) = array3d(i,j,k);
      }
    }
  }
 }

%typemap(freearg) const Mesh3d & {
  if ($1) delete $1;
 }

%typemap(typecheck) const Mesh3d &  {
  octave_value obj = $input;
  $1 = false;
  if (obj.is_real_matrix()) {
    const NDArray array3d = obj.array_value();
    if (array3d.dims().length() == 3) {
      $1 = true;
    }
  }
}

// C++ Mesh3d -> Octave array
%typemap(out) Mesh3d {
  dim_vector dv3;
  dv3.resize(3);
  dv3.elem(0)=($1).size1();
  dv3.elem(1)=($1).size2();
  dv3.elem(2)=($1).size3();
  NDArray ret(dv3);
  for (size_t i=0; i<($1).size1(); i++) {
    for (size_t j=0; j<($1).size2(); j++) {
      for (size_t k=0; k<($1).size3(); k++) {
        ret(i,j,k) = ($1).elem(i,j,k);
      }
    }
  }
  $result = ret;
}


#endif
