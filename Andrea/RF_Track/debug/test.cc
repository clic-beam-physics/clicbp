/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <cmath>
#include "RF_Track.hh"
#include "lattice.hh"
#include "quadrupole.hh"
#include "drift.hh"
#include "electron_cooler.hh"

int main()
{
  init_rf_track();
  
  RF_Track_Globals::number_of_threads = 1;
  
  auto Ion_mass = 207.2 * RF_Track_Globals::protonmass;
  auto Ion_Np = 5e9; // total number of protons
  auto Ion_K = 4.2 * 207.2; // MeV
  auto Ion_E = Ion_mass + Ion_K; // MeV
  auto Ion_P = sqrt(Ion_E*Ion_E - Ion_mass*Ion_mass);
  auto Ion_V = Ion_P / Ion_E;
  auto Ion_P_spread = 0.4; // 0.4 percent
  auto Ion_gamma = 1 / sqrt(1 - Ion_V*Ion_V); //
  auto Ion_sigmaz = 1; // mm
  auto Ion_Q = 54;

  Bunch6d_twiss BunchT;
  BunchT.Pc = Ion_P; // MeV
  BunchT.emitt_x = 40; // mm.mrad == micron
  BunchT.emitt_y = 40; // mm.mrad == micron
  BunchT.emitt_z = Ion_V * Ion_gamma * Ion_sigmaz * (Ion_P_spread / 100) * 1e3; // mm.mrad
  BunchT.alpha_x = 0;
  BunchT.alpha_y = 0;
  BunchT.alpha_z = 0;
  BunchT.beta_x = 3.690910796;
  BunchT.beta_y = 14.3080807;
  BunchT.beta_z = Ion_sigmaz * Ion_sigmaz / BunchT.emitt_z * Ion_V * Ion_gamma; // m

  auto N_particles = 10000;

  Bunch6d B0(Ion_mass, Ion_Np, Ion_Q, BunchT, N_particles);

  auto Electron_Ne = 8.27e13;
  auto Electron_V = 0.092245;

  auto Nx = 16;
  auto Ny = 16;
  auto Nz = 16;

  auto Qd = Quadrupole(0.2);
  Qd.set_K1L(Ion_P / Ion_Q, -1.77972 * 0.2);

  auto Qf = Quadrupole(0.4);
  Qf.set_K1L(Ion_P / Ion_Q,  1.00313 * 0.4);

  auto d0 = Drift(0.6);
  auto d1 = Drift(1.3);

  auto Cooler_L = 3; // m
  auto Cooler_B = 0.07; // T
  auto Cooler_r0 = 200; // mm (should be 25)
  
  auto E = ElectronCooler(Cooler_L, Cooler_r0/1e3, Cooler_r0/1e3);
  E.set_Q(-1);
  E.set_nsteps(Nz);
  E.set_electron_mesh_template(Electron_Ne, 0, 0, Electron_V, Nx, Ny);
  E.set_static_Bfield(0.0, 0.0, Cooler_B);
  
  auto L = Lattice();
  L.append(&Qd);
  L.append(&d0);
  L.append(&Qf);
  L.append(&d1);
  L.append(&E);
  L.append(&d1);
  L.append(&Qf);
  L.append(&d0);
  L.append(&Qd);

  auto B1 = L.track(B0);
  
  return 0;
}
