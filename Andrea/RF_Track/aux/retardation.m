Xa = linspace(-1,1,50);
Ya = linspace(-1,1,50);
[X,Y] = meshgrid(Xa,Ya);
R=hypot(X,Y);
R(find(R==0)) = 1;
R(find(R==0)) = 1;
nX = X ./ R;
nY = Y ./ R;
v =  [ 0.001 0 ];
vx = v(1);
t=0;
z=0
x=X;
y=Y;

k = 1 - nX .* v(1) - nY .* v(2);
k = ((2.*vx.*x+vx.^4+(-2.*t-1).*vx.^2).*sqrt((1-vx.^2).*z.^2+(1-vx.^2).*y.^2+x.^2-2.*t.*vx.*x+t.^2.*vx.^2)+(1-vx.^2).*z.^2+(1-vx.^2).*y.^2+(vx.^2+1).*x.^2+((1-2.*t).*vx.^3+(-2.*t-1).*vx).*x+(t.^2-t).*vx.^4+(t.^2+t).*vx.^2)./(vx.^4-2.*vx.^2+1);


surf(Xa, Ya, 1 ./ (real(k)))
xlabel('x');
ylabel('y');