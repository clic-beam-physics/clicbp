/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/


  auto H     = TMesh3d<double, fftwAllocator<double>>(18, 18, 4);
  auto H_hat = TMesh3d<fftwComplex, fftwAllocator<fftwComplex>>(18, 18, 4);
  auto H_ret = TMesh3d<double, fftwAllocator<double>>(18, 18, 4);

  auto pforward  = fftw_plan_dft_r2c_3d(18, 18, 4, H.data(), (fftw_complex *)H_hat.data(), FFTW_ESTIMATE);
  auto pbackward = fftw_plan_dft_c2r_3d(18, 18, 4, (fftw_complex *)H_hat.data(), H_ret.data(), FFTW_ESTIMATE);

  for (size_t i=0; i<18; i++) {
    for (size_t j=0; j<18; j++) {
      for (size_t k=0; k<4; k++) {
	H.elem(i,j,k) = 1.0/(double(i+1) + double(j+1) - 1.0);
      }
    }
  }

  fftw_execute(pforward);
  fftw_execute(pbackward);
  H_ret /= 18*18*4;
  std::cout << "H = " << std::endl;
  for (size_t k=0; k<4; k++) {
    for (size_t i=0; i<18; i++) {
      for (size_t j=0; j<18; j++) {
	std::cout << ' ' << H.elem(i,j,k);
      }
      std::cout << '\n';
    }
    std::cout << '\n';
  }
  std::cout << "Hret = " << std::endl;
  for (size_t k=0; k<4; k++) {
    for (size_t i=0; i<18; i++) {
      for (size_t j=0; j<18; j++) {
	std::cout << ' ' << H_ret.elem(i,j,k);
      }
      std::cout << '\n';
    }
    std::cout << '\n';
  }

