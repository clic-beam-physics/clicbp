pkg load quaternion

C_LIGHT = 299792458; % m/s
mass = 0.5109989275678128; % MeV
particle_Q = 1;

dt_mm = 1;

X = [ 10 20 30 ];
V = [ 0.0 0.9 0.1 ];
gamma = 1 / sqrt(1 - dot(V,V));
P = mass * V * gamma;

Etotal = mass * gamma;

norm_Bfield = 5;
nz = [ 0 0 1 ]; 
Bfield = norm_Bfield * nz; 
  
omega = particle_Q * Bfield / Etotal * (C_LIGHT / 1e9); % rad / (mm/c), angular velocity
f = -omega * dt_mm;
rotation = exp(0.5*quaternion(0.0, f(1), f(2), f(3)))

radius_vect = cross(omega, V) / dot(omega, omega);

%% updaed variables
P_new = Rotate(rotation, P); % MeV/c
dX = Rotate(rotation, radius_vect) - radius_vect;

%%F = particle_Q * cross(V, Bfield) * (C_LIGHT / 1e9); % MeV/mm
%%a = F / Etotal; % c^2/mm
%%dX = (V + 0.5 * a * dt_mm) * dt_mm; % mm
%%dP = F * dt_mm; % MeV/c

