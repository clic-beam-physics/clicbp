%% read Superfish file
A = load("fieldmap_rf.dat");

%% prepare data for interp2
I.za = unique(A(:,1)); % cm
I.ra = unique(A(:,2)); % cm
Nz = length(I.za);
Nr = length(I.ra);
I.Ez = zeros(Nr, Nz); % MV/m
I.Er = zeros(Nr, Nz); % MV/m
I.H  = zeros(Nr, Nz); % A/m

for line=1:size(A,1)
    zi = find(I.za == A(line,1));
    ri = find(I.ra == A(line,2));
    I.Ez(ri,zi) = A(line,3); % MV/m
    I.Er(ri,zi) = A(line,4); % MV/m
    I.H (ri,zi) = A(line,6); % A/m
end


%% Prepare M, structure for RF-Track
rho_TW = 0.02; % m
Length = 0.15; % m
Length_cell = 0.05; % m

frequency = 2e9; % Hz

N_rho = 20;
N_theta = 40;
N_length = 150;

M.ra = linspace(0,rho_TW,N_rho); % m
M.ta = linspace(-pi,pi,N_theta); % rad
M.za = linspace(0,Length,N_length); % m

M.Ex = zeros(N_rho,N_theta,N_length);
M.Ey = zeros(N_rho,N_theta,N_length);
M.Ez = zeros(N_rho,N_theta,N_length);
M.Bx = zeros(N_rho,N_theta,N_length);
M.By = zeros(N_rho,N_theta,N_length);

wave_number = 2*pi/(299792458./frequency); % rad/m

kl = wave_number * Length_cell; % rad
permeability = 4 * pi * 1.0e-7; % H/m 

II = sqrt(-1);

for i_rho = 1:N_rho
    for j_theta = 1:N_theta
        for k_length = 1:N_length
            
            rho   = M.ra(i_rho) * 1e2; % cm
            za    = M.za(k_length) * 1e2; % cm
            theta = M.ta(j_theta); % rad
            
            sEr = interp2(I.za, I.ra, I.Er, za, rho) * 1e6; % V/m
            sEz = interp2(I.za, I.ra, I.Ez, za, rho) * 1e6; % V/m
            sHt = interp2(I.za, I.ra, I.H,  za, rho); % H

            sEr_p = interp2(I.za, I.ra, I.Er, za + Length_cell*1e2, rho) * 1e6; % V/m
            sEz_p = interp2(I.za, I.ra, I.Ez, za + Length_cell*1e2, rho) * 1e6; % V/m
            sHt_p = interp2(I.za, I.ra, I.H,  za + Length_cell*1e2, rho); % H
            
            E_field_r = II * (sEr_p - sEr * exp(II*kl))/(2*sin(kl)); % V/m
            E_field_z = II * (sEz_p - sEz * exp(II*kl))/(2*sin(kl)); % V/m
            H_field_theta = II * (sHt_p - sHt * exp(II*kl))/(2*sin(kl)); % H
            
            M.Ex(i_rho,j_theta,k_length) = E_field_r*cos(theta); % V/m
            M.Ey(i_rho,j_theta,k_length) = E_field_r*sin(theta); % V/m
            M.Ez(i_rho,j_theta,k_length) = E_field_z; % V/m
            
            %% Rotate 90 degree
            M.Bx(i_rho,j_theta,k_length) = -H_field_theta*sin(theta) * permeability; % T
            M.By(i_rho,j_theta,k_length) =  H_field_theta*cos(theta) * permeability; % T
        end
    end
end

save('field_TW_4cells_method2_AddH.dat.gz', '-zip', 'M');
