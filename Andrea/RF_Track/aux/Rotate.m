function V1 = Rotate(R, V0)
  V1_ = R * quaternion(0.0, V0(1), V0(2), V0(3)) * conj(R);
  V1 = [ V1_.x V1_.y V1_.z ];
