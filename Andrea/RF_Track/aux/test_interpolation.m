y0 = 0;
y1 = 5;
y2 = 0;
y3 = 3;
y4 = -5;

CINT0 = @(t,y0,y1,y2,y3) (t^3*y2+(6*t-2*t^3)*y1+(t^3-6*t+6)*y0)/6;
CINT1 = @(t,y0,y1,y2,y3) (t^3*y3+(-3*t^3+3*t^2+3*t+1)*y2+(3*t^3-6*t^2+4)*y1+(-t^3+3*t^2-3*t+1)*y0)/6;
CINT2 = @(t,y0,y1,y2,y3) -((t^3-3*t^2-3*t-1)*y3+(-2*t^3+6*t^2-4)*y2+(t^3-3*t^2+3*t-1)*y1)/6;

CINT0_deriv = @(t,y0,y1,y2,y3) (t^2*y2+(2-2*t^2)*y1+(t^2-2)*y0)/2;
CINT1_deriv = @(t,y0,y1,y2,y3) (t^2*y3+(-3*t^2+2*t+1)*y2+(3*t^2-4*t)*y1+(-t^2+2*t-1)*y0)/2;
CINT2_deriv = @(t,y0,y1,y2,y3) -((t^2-2*t-1)*y3+(4*t-2*t^2)*y2+(t^2-2*t+1)*y1)/2;

CINT0_deriv2 = @(t,y0,y1,y2,y3) t*y2-2*t*y1+t*y0;
CINT1_deriv2 = @(t,y0,y1,y2,y3) t*y3+(1-3*t)*y2+(3*t-2)*y1+(1-t)*y0;
CINT2_deriv2 = @(t,y0,y1,y2,y3) (1-t)*y3+(2*t-2)*y2+(1-t)*y1;

T0 = [];
T1 = [];
T2 = [];
T3 = [];
for t=linspace(0,1,20)
    T0 = [ T0 ; 0+t CINT0(t,y0,y1,y2,y3) ];
    T1 = [ T1 ; 1+t CINT1(t,y0,y1,y2,y3) ];
    T2 = [ T2 ; 2+t CINT1(t,y1,y2,y3,y4) ];
    T3 = [ T3 ; 3+t CINT2(t,y1,y2,y3,y4) ]; 
end
figure(1);
plot(T0(:,1), T0(:,2), 'r-', ...
     T1(:,1), T1(:,2), 'r-', ...
     T2(:,1), T2(:,2), 'r-', ...
     T3(:,1), T3(:,2), 'r-');
title('function');

T0 = [];
T1 = [];
T2 = [];
T3 = [];
for t=linspace(0,1,20)
    T0 = [ T0 ; 0+t CINT0_deriv(t,y0,y1,y2,y3) ];
    T1 = [ T1 ; 1+t CINT1_deriv(t,y0,y1,y2,y3) ];
    T2 = [ T2 ; 2+t CINT1_deriv(t,y1,y2,y3,y4) ];
    T3 = [ T3 ; 3+t CINT2_deriv(t,y1,y2,y3,y4) ]; 
end
figure(2);
plot(T0(:,1), T0(:,2), 'r-', ...
     T1(:,1), T1(:,2), 'r-', ...
     T2(:,1), T2(:,2), 'r-', ...
     T3(:,1), T3(:,2), 'r-');
title('function''s first derivative');

T0 = [];
T1 = [];
T2 = [];
T3 = [];
for t=linspace(0,1,20)
    T0 = [ T0 ; 0+t CINT0_deriv2(t,y0,y1,y2,y3) ];
    T1 = [ T1 ; 1+t CINT1_deriv2(t,y0,y1,y2,y3) ];
    T2 = [ T2 ; 2+t CINT1_deriv2(t,y1,y2,y3,y4) ];
    T3 = [ T3 ; 3+t CINT2_deriv2(t,y1,y2,y3,y4) ]; 
end
figure(3);
plot(T0(:,1), T0(:,2), 'r-', ...
     T1(:,1), T1(:,2), 'r-', ...
     T2(:,1), T2(:,2), 'r-', ...
     T3(:,1), T3(:,2), 'r-');
title('function''s second derivative');

pkg load splines
X = 0:4;
Y = [ y0 y1 y2 y3 y4 ];

PP = catmullrom(X,Y);

figure(1);
hold on
t=linspace(0,4,100);
V=ppval(PP, t);

plot(t, V, '*');    

figure(2);
PP = ppder(PP);
hold on
t=linspace(0,4,100);
V=ppval(PP, t);

plot(t, V, '*');    

figure(3);
PP = ppder(PP);
hold on
t=linspace(0,4,100);
V=ppval(PP, t);

plot(t, V, '*');    
