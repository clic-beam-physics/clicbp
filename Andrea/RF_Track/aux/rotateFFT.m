%% From: https://www.ocf.berkeley.edu/~fricke/projects/israel/fourier_rotation/fourier_rotation.html

%% Let g(x) (ℜ2→ℜ) represent the image f(x) rotated by an angle Θ. 

%% This can be expressed as a simple change of coordinates
%% g(x)=f(R^-1x) where R=R(Θ) is the rotation matrix (or, in
%% general, any orthogonal transformation).

%%Let G(k) be the Fourier Transform of g(x).

%% Then
%% G(k) = Integral[g(x)Exp[-2πi⟨k,x⟩dx].
%% Let k'=Rk and substitute f(R^-1x) for g(x), then
%% G(k') = Integral[f(R^-1x)Exp[-2πi⟨Rk,x⟩]dx]. 

%% Because R (and R^-1=RT) is ⟨x,k⟩ = ⟨R^-1Rx,R^-1k⟩ =
%% ⟨x,R^-1k⟩. Thus we have
%% G(Rk) = Integral[f(R^-1x)Exp[-2πi⟨k,R^-1x⟩]dx].

%% Because the integral is over all x, and thus over all R^-1x we
%% can perform  the change of variable R^-1x→x to get
%% G(Rk)=Integral[f(x)Exp[-2πi⟨k,x⟩]dx] which is just F(k), the
%% Fourier Transform of f(x).

%% Finally we have G(Rk)=F(k) or G(k)=F(R^-1k). Thus we see that a
%% rotation in x-space has the same effect as a rotation in
%% k-space, i.e. Rotation commutes with the Fourier Transform. 

% rotation
mu = 30;
R = [ cosd(mu ) sind(mu) ; -sind(mu) cosd(mu) ];

% B field
Bx = hilb(10);
By = randn(10);

% rotate B
RB  = R * [ Bx(:) By(:) ]';
RBx = reshape(RB(1,:), 10, 10); % R(Bx)
RBy = reshape(RB(2,:), 10, 10); % R(By)

% Fourier transform of R(B)
FRBx = fft2(RBx); % FT(R(Bx))
FRBy = fft2(RBy); % FT(R(By))

% Fourier transform B
FBx = fftn(Bx); % FT(Bx)
FBy = fftn(By); % FT(By)

% rotate the Fourier transform of B
RFB  = R * [ FBx(:) FBy(:) ]';
RFBx = reshape(RFB(1,:), 10, 10); % R(FT(Bx))
RFBy = reshape(RFB(2,:), 10, 10); % R(FT(By))

% zeros
max(max(abs(FRBx - conj(RFBx))))
max(max(abs(FRBy - conj(RFBy))))

% inverse Fourier transform of the rotated B
iFRFBx = conj(ifftn(conj(RFBx)));
iFRFBy = conj(ifftn(conj(RFBy)));

% inverse rotation of the inverse Fourier transform of the rotated B
iRiFRFB  = inv(R) * [ iFRFBx(:) iFRFBy(:) ]';
iRiFRFBx = reshape(iRiFRFB(1,:), 10, 10); % Bx !
iRiFRFBy = reshape(iRiFRFB(2,:), 10, 10); % By !

% zeros
max(max(abs(iRiFRFBx - Bx)))
max(max(abs(iRiFRFBy - By)))