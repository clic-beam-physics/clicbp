Nx = 16;

full_Xa = linspace(-1,1,Nx+1);
hx = hy = diff(full_Xa)(1);

[X,Y] = ndgrid(full_Xa,full_Xa);

Phi0 = real((X .+ sqrt(-1) .* Y).**2);
Az0 = -imag((X .+ sqrt(-1) .* Y).**2);

Fx = 0*-diff(Phi0,1,1)(1:Nx,1:Nx) / hx;
Fy = 0*-diff(Phi0,1,2)(1:Nx,1:Nx) / hy;

Fx .+= diff(Az0,1,2)(1:Nx,1:Nx) / hy;
Fy .-= diff(Az0,1,1)(1:Nx,1:Nx) / hx;

FPhi0 = fft2(Phi0);
FAz0  = fft2(Az0);

Xa = linspace(0, Nx-1, Nx) * hx;
Ka = 2 * pi * circshift(linspace(-0.5, 0.5, Nx+1)(1:Nx), Nx/2, 2) / (Nx*hx);

[X,Y]   = ndgrid(Xa,Xa);
[KX,KY] = ndgrid(Ka,Ka);
Za = zeros(Nx*Nx,1);
K2 = dot([ KX(:) KY(:) ], [ KX(:) KY(:) ], 2);

Phi0 = Phi0(1:Nx, 1:Nx);
Az0  = Az0(1:Nx, 1:Nx);
Fx   = Fx(1:Nx, 1:Nx);
Fy   = Fy(1:Nx, 1:Nx);

%%

T = [];
for i=0

    nFx = Fx .* cosd(i) .- Fy .* sind(i);
    nFy = Fy .* sind(i) .+ Fy .* cosd(i);
  
    mean_nFx = mean(nFx(:));
    mean_nFy = mean(nFy(:));
    
    nFx -= mean_nFx;
    nFy -= mean_nFy;
    
    FFx = fft2(nFx);
    FFy = fft2(nFy);

    FPhi = sqrt(-1) * dot  ([ KX(:) KY(:) ], [ FFx(:) FFy(:) ], 2);
    FA   = sqrt(-1) * cross([ KX(:) KY(:) Za ], [ FFx(:) FFy(:) Za ], 2);
    FPhi = reshape(FPhi    ./ K2, Nx, Nx);
    FAx  = reshape(FA(:,1) ./ K2, Nx, Nx);
    FAy  = reshape(FA(:,2) ./ K2, Nx, Nx);
    FAz  = reshape(FA(:,3) ./ K2, Nx, Nx);
    FPhi(1) = FAx(1) = FAy(1) = FAz(1) = 0;
    
    Phi = real(ifft2(FPhi));
    Ax  = real(ifft2(FAx));
    Ay  = real(ifft2(FAy));
    Az  = real(ifft2(FAz));
    
    rFx = mean_nFx ...
    + diff(Az,1,2)(1:Nx-1,1:Nx-1)/hy ...
    - diff(Phi,1,1)(1:Nx-1,1:Nx-1)/hx ...
          ;
    rFy = mean_nFy ...
    - diff(Az,1,1)(1:Nx-1,1:Nx-1)/hx ...
    - diff(Phi,1,2)(1:Nx-1,1:Nx-1)/hy ...
          ;
    
    figure(1)
    clf
    hold on
    quiver(Xa, Xa, nFx, nFy);
    quiver(Xa(1:Nx-1), Xa(1:Nx-1), rFx(1:Nx-1,1:Nx-1), rFy(1:Nx-1,1:Nx-1), 'r');
    
    figure(2)
    quiver(Ka, Ka, real(FFx), real(FFy));
        
    figure(3)
    quiver(Ka, Ka, imag(FFx), imag(FFy));
            
    tmp = sum(sum(hypot(diff(Phi,1,1)(1:Nx-1,1:Nx-1), diff(Phi,1,2)(1:Nx-1,1:Nx-1))));
        
    FPhi_sum = sum(sum(abs(FPhi)))
   
    T = [ T ; i FPhi_sum tmp ];
    
end