function Q = Rotation(angle, axis)
  V = sin(angle/2) * axis / norm(axis);
  Q = quaternion(cos(angle/2), V(1), V(2), V(3));
