/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef bunch6dt_hh
#define bunch6dt_hh

#include <vector>
#include <utility>

#include "matrixnd.hh"
#include "particle.hh"
#include "constants.hh"
#include "bunch6d_twiss.hh"
#include "bunch6dt_info.hh"

// input phase space matrix X must containt 6 columns t x [mm] xp [mrad] y [mm] yp [mrad] z [mm] Pc [MeV]

struct Bunch6d;
struct Bunch6dT {
  double t; ///< time in mm/c
  std::vector<ParticleT> particles; ///< particles;
  double coasting_ = 0.0; // mm, period or 0.0 for bunched beam (default)
public:
  Bunch6dT(size_t N=0 ) : t(0.0), particles(N) {}
  Bunch6dT(double mass, double population, double charge, const MatrixNd &X );
  Bunch6dT(double mass, double population, double charge, const Bunch6d_twiss &T, size_t N, double sigma_cut = 3.0 );
  Bunch6dT(const Bunch6d &b );
  Bunch6dT(const MatrixNd &X );

  size_t size() const { return particles.size(); }

  double coasting() const { return coasting_; }
  void set_coasting(double period /* mm */ ) { coasting_ = period; }

  Bunch6dT_info get_info() const;

  // void set_population(double _population ) { population = _population; }
  // void set_mass(double _mass ) { mass = _mass; }
  void set_phase_space(const MatrixNd &X );
  // void set_S(double S_ ) { S = S_; }

  const ParticleT &get_particle(size_t i ) const { return particles[i]; }
  ParticleT &get_particle(size_t i ) { return particles[i]; }
  double get_S_min_mm() const; // mm
  double get_S_max_mm() const; // mm
  double get_S(const double dt = 0.0 ) const; // returns S0 such that <dt> = <(S-S0)/Vz> = dt [m]
  double get_t() const { return t; } // returns the average time [mm/c]
  // double get_mass() const { return mass; }
  // double get_population() const { return population; }
  // Particle get_average_particle_t0(double dS=0.0 ) const; // returns the average particle such that <Z> = dS (dS in meters )
  StaticVector<3> get_center_of_mass_Vx_Vy_Vz() const;
  StaticVector<3> get_average_Vx_Vy_Vz() const;
  ParticleT get_average_particle() const;
  double get_reference_momentum() const;

  
  StaticVector<3> get_bunch_temperature(); // eV

  MatrixNd get_phase_space(const char *fmt = "%x %xp %y %yp %t %Pc", const char *select = "good" ) const; // "good" or "all"
  MatrixNd get_lost_particles() const;
  MatrixNd get_lost_particles_mask() const;
  size_t get_ngood() const;
  size_t get_nlost() const; // Nparticles - get_ngood()

  void apply_force(const MatrixNd &force, double dt_mm ); // force is a "Nparticles x 3-column" matrix in MeV/m; dt is in mm/c (1 mm = ~3.33 ps)
  void kick(const MatrixNd &force, double dt_mm ); // force is a "Nparticles x 3-column" matrix in MeV/m; dt is in mm/c (1 mm = ~3.33 ps)
  void drift(double dt_mm ); // force is a "Nparticles x 3-column" matrix in MeV/m; dt is in mm/c (1 mm = ~3.33 ps)

  bool save_as_dst_file(const char *filename, double frequency_MHz ) const;
};

#endif /* bunch6dt_hh */
