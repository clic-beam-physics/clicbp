/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef bunch6dt_info_hh
#define bunch6dt_info_hh

struct Bunch6dT_info {
  double t; // mm/c
  double mean_X; // mm
  double mean_Y; // mm
  double mean_S; // mm
  double mean_Px; // MeV/c
  double mean_Py; // MeV/c
  double mean_Pz; // MeV/c
  double mean_K;  // average kinetic energy in MeV
  double mean_E;  // average total energy in MeV
  double sigma_X; // mm
  double sigma_Y; // mm
  double sigma_Z; // mm
  double sigma_Px; // MeV/c
  double sigma_Py; // MeV/c
  double sigma_Pz; // MeV/c
  double sigma_XPx; // mm*MeV/c
  double sigma_YPy; // mm*MeV/c
  double sigma_ZPz; // mm*MeV/c
  double sigma_E;  // kinetic-energy spread in MeV
  double emitt_x; // mm.mrad normalised emittance
  double emitt_y; // mm.mrad normalised emittance
  double emitt_z; // mm.keV that is, emitt_z / mm.keV = sigmaz / mm * sigma_P / keV
  double emitt_4d; // mm.mrad, 4d normalised emittance
  double alpha_x; 
  double alpha_y;
  double alpha_z;
  double beta_x; // mm/mrad
  double beta_y; // mm/mrad
  double beta_z; // mm/permille
  double rmax; // mm
  double transmission; // percent
};

#endif /* bunch6dt_info_hh */
