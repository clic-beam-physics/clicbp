/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef particle_key_hh
#define particle_key_hh

// for hash tables (std::unordered_map)

struct ParticleKey {
  double mass; ///< [MeV/c/c]
  double Q; ///< [e+] charge of each particle
};

struct ParticleKeyHasher { // generates an unique key, 1024.0 should be enough as Q should never exceed it....
  size_t operator()(const ParticleKey &key ) const {
    return key.mass * 1024.0 + key.Q;
  }
};

struct ParticleKeyEquals {
  bool operator()(const ParticleKey &key1, const ParticleKey &key2 ) const {
    return key1.mass == key2.mass && key1.Q == key2.Q;
  }
};


#endif /* particle_key_hh */
