/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef bunch6d_twiss_hh
#define bunch6d_twiss_hh

struct Bunch6d_twiss {
  double Pc; // MeV
  double emitt_x; // mm.mrad normalised emittance
  double emitt_y; // mm.mrad normalised emittance
  double emitt_z; // mm.keV that is, emitt_z / mm.keV = sigmaz / mm * sigma_P / keV
  double alpha_x; 
  double alpha_y;
  double alpha_z;
  double beta_x;  // mm/mrad
  double beta_y;  // mm/mrad
  double beta_z;  // mm/keV
  double disp_x;  // m
  double disp_xp; // rad
  double disp_y;  // m
  double disp_yp; // rad
  double disp_z;  // m
  Bunch6d_twiss() : alpha_x(0.0), alpha_y(0.0), alpha_z(0.0),
		    disp_x(0.0), disp_xp(0.0),
		    disp_y(0.0), disp_yp(0.0),
		    disp_z(0.0) {}
};

#endif /* bunch6d_twiss_hh */
