/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef bunch6d_info_hh
#define bunch6d_info_hh

struct Bunch6d_info {
  double S; // m
  double mean_x; // mm
  double mean_y; // mm
  double mean_t; // mm/c
  double mean_xp; // mrad
  double mean_yp; // mrad
  double mean_P;  // average momentum in MeV
  double mean_K;  // average kinetic energy in MeV
  double mean_E;  // average total energy in MeV
  double sigma_x; // mm
  double sigma_y; // mm
  double sigma_z; // mm
  double sigma_t; // mm/c
  double sigma_xp; // mrad
  double sigma_yp; // mrad
  double sigma_xxp; // mm*mrad
  double sigma_yyp; // mm*mrad
  double sigma_zP; // mm*MeV
  double sigma_E;  // kinetic-energy spread in MeV
  double sigma_P;  // momentum spread in MeV
  double emitt_x; // mm.mrad normalised emittance
  double emitt_y; // mm.mrad normalised emittance
  double emitt_z; // mm.keV that is, emitt_z / mm.keV = sigmaz / mm * sigma_P / keV
  double emitt_4d; // mm.mrad, 4d normalised emittance
  double alpha_x; 
  double alpha_y;
  double alpha_z;
  double beta_x; // mm/mrad
  double beta_y; // mm/mrad
  double beta_z; // mm/permille
  double rmax; // mm
  double transmission; // percent
};

#endif /* bunch6d_info_hh */
