/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef space_charge_pic_long_cylinder_hh
#define space_charge_pic_long_cylinder_hh

#include "space_charge_pic.hh"
#include "greens_functions/coulomb_long_cylinder.hh"

class SpaceCharge_PIC_LongCylinder : public SpaceCharge_PIC<GreensFunction::IntegratedRetardedCoulomb_LongCylinder> {
public:
  
  SpaceCharge_PIC_LongCylinder(size_t Nx_=1, size_t Ny_=1, size_t Nz_=1, double a = 1.0 /* m */ ) : SpaceCharge_PIC<GreensFunction::IntegratedRetardedCoulomb_LongCylinder>(Nx_, Ny_, Nz_) { greens_function.a = a*1e3; }

  void set_aperture(double a /* m */ ) { greens_function.a = a*1e3; }
  double get_aperture() const { return greens_function.a/1e3; } // m
  
};

#endif /* space_charge_pic_long_cylinder_hh */
