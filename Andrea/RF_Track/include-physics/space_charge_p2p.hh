/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef space_charge_p2p_hh
#define space_charge_p2p_hh

#include "space_charge.hh"

class SpaceCharge_P2P : public SpaceCharge {

  // particle-particle method
  std::vector<std::vector<CumulativeKahanSum<StaticVector<3>>>> F_all; // Nthreads x particles x CumulativeKahanSum of StaticVector<3>

  template <typename BUNCH_TYPE>
  void compute_force_(MatrixNd &force, const BUNCH_TYPE &bunch, const ParticleSelector &selector );

public:

  // compute the force [MeV/m] and return it as a Nx3 matrix
  void compute_force(MatrixNd &force, const Bunch6d  &bunch, const ParticleSelector &selector = ParticleSelector()) override { compute_force_(force, bunch, selector); }
  void compute_force(MatrixNd &force, const Bunch6dT &bunch, const ParticleSelector &selector = ParticleSelector()) override { compute_force_(force, bunch, selector); }

};

#endif /* space_charge_p2p_hh */
