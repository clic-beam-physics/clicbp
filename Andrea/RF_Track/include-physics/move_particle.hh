/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef move_particle_hh
#define move_particle_hh

#include "static_vector.hh"
#include "rotation.hh"
#include "particle.hh"
#include "relativistic_velocity_addition.hh"
#include "sign.hh"

inline void move_particle_through_Efield(Particle &particle, const StaticVector<3> &Efield /* V/m */, const double dS_mm )
{
  const auto &Etotal = particle.get_total_energy(); // MeV
  const auto &P = particle.get_Px_Py_Pz(); // MeV/c
  const auto &v = P / Etotal; // c
  if (Efield[0]==0.0 && Efield[1]==0.0 && Efield[2]==0.0) {
    particle.x += particle.xp * dS_mm / 1e3; // mm
    particle.y += particle.yp * dS_mm / 1e3; // mm
    particle.t += dS_mm / v[2]; // mm/c
    return;
  }  
  const auto &F = particle.Q * Efield / 1e9; // MeV/mm
  const auto &a = (F - dot(v,F)*v) / Etotal; // c^2/mm
  double dt_mm = dS_mm / v[2]; // mm/c
  if (a[2]>2*std::numeric_limits<double>::epsilon()*v[2]*dt_mm)
    dt_mm = (sqrt(v[2]*v[2] + 2*a[2]*dS_mm) - v[2]) / a[2]; // mm/c

  particle.x += (v[0] + 0.5 * a[0] * dt_mm) * dt_mm; // mm
  particle.y += (v[1] + 0.5 * a[1] * dt_mm) * dt_mm; // mm
  particle.t += dt_mm; // mm/c

  const auto &P_new = P + F * dt_mm; // MeV/c
  const auto &inv_Pz = 1e3 / P_new[2]; // 1e3/(MeV/c)
  particle.xp = P_new[0] * inv_Pz; // mrad
  particle.yp = P_new[1] * inv_Pz; // mrad
  particle.Pc = norm(P_new); // MeV/c
}

inline void move_particle_through_Efield(ParticleT &particle, const StaticVector<3> &Efield /* V/m */, const double dt_mm )
{
  const auto &v = particle.get_Vx_Vy_Vz(); // c
  if (Efield[0]==0.0 && Efield[1]==0.0 && Efield[2]==0.0) {
    const auto &dX = v * dt_mm; // mm
    particle.X += dX[0]; // mm
    particle.Y += dX[1]; // mm
    particle.S += dX[2]; // mm
    return;
  }    
  const auto &Etotal = particle.get_total_energy(); // MeV
  const auto &F = particle.Q * Efield / 1e9; // MeV/mm
  const auto &a = (F - dot(v,F)*v) / Etotal; // c^2/mm

  const auto &dX = (v + 0.5 * a * dt_mm) * dt_mm; // mm
  particle.X += dX[0]; // mm
  particle.Y += dX[1]; // mm
  particle.S += dX[2]; // mm

  const auto &dP = F * dt_mm; // MeV/c
  particle.Px += dP[0]; // MeV/c
  particle.Py += dP[1]; // MeV/c
  particle.Pz += dP[2]; // MeV/c
}

inline void move_particle_through_Bfield(Particle &particle, const StaticVector<3> &Bfield /* T */, const double dS_mm )
{
  const auto &v = particle.get_Vx_Vy_Vz(); // c
  if (Bfield[0]==0.0 && Bfield[1]==0.0 && Bfield[2]==0.0) {
    particle.x += particle.xp * dS_mm / 1e3; // mm
    particle.y += particle.yp * dS_mm / 1e3; // mm
    particle.t += dS_mm / v[2]; // mm/c
    return;
  }
  const auto &v_para = dot(Bfield,v) * Bfield / dot(Bfield,Bfield); // c, component of velocity parallel to Bfield
  const auto &dt_mm = dS_mm / v[2]; // mm/c
  const auto &Etotal = particle.get_total_energy(); // MeV
  const auto &omega = particle.Q * Bfield / Etotal * (C_LIGHT / 1e9); // rad / (mm/c), angular velocity
  const auto &rotation = Rotation(-omega * dt_mm); // rotation by the integrated angle, see https://cwzx.wordpress.com/2013/12/16/numerical-integration-for-rotational-dynamics
  const auto &r = cross(omega,v) / dot(omega,omega); // mm, Larmor radius in vector form, it's a vector from the guiding center to the particle
  
  const auto &dX = rotation * r - r + v_para * dt_mm; // mm
  particle.x += dX[0]; // mm
  particle.y += dX[1]; // mm
  particle.t += dt_mm; // mm/c
  
  const auto &P_new = rotation * particle.get_Px_Py_Pz(); // MeV/c
  const double inv_Pz = 1e3 / P_new[2]; // 1e3/(MeV/c)
  particle.xp = P_new[0] * inv_Pz; // mrad
  particle.yp = P_new[1] * inv_Pz; // mrad
}

inline void move_particle_through_Bfield(ParticleT &particle, const StaticVector<3> &Bfield /* T */, const double dt_mm )
{
  const auto &v = particle.get_Vx_Vy_Vz(); // c
  if (Bfield[0]==0.0 && Bfield[1]==0.0 && Bfield[2]==0.0) {
    const auto &dX = v * dt_mm; // mm
    particle.X += dX[0]; // mm
    particle.Y += dX[1]; // mm
    particle.S += dX[2]; // mm
    return;
  }    
  const auto &Etotal = particle.get_total_energy(); // MeV
  const auto &omega = particle.Q * Bfield / Etotal * (C_LIGHT / 1e9); // rad / (mm/c), angular velocity
  const auto &rotation = Rotation(-omega * dt_mm); // rotation by the integrated angle, see https://cwzx.wordpress.com/2013/12/16/numerical-integration-for-rotational-dynamics
  const auto &r = cross(omega,v) / dot(omega,omega); // mm, Larmor radius in vector form, it's a vector from the guiding center to the particle
  const auto &v_para = dot(Bfield,v) * Bfield / dot(Bfield,Bfield); // c, component of velocity parallel to Bfield
  
  const auto &dX = rotation * r - r + v_para * dt_mm; // mm
  particle.X += dX[0]; // mm
  particle.Y += dX[1]; // mm
  particle.S += dX[2]; // mm
  
  const auto &P_new = rotation * particle.get_Px_Py_Pz(); // MeV/c
  particle.Px = P_new[0]; // MeV/c
  particle.Py = P_new[1]; // MeV/c
  particle.Pz = P_new[2]; // MeV/c
}

inline void move_particle_through_EBfield(Particle &particle,
					  const StaticVector<3> &Efield /* V/m */,
					  const StaticVector<3> &Bfield /* T   */, const double dS_mm )
{
  if (Bfield[0]==0.0 && Bfield[1]==0.0 && Bfield[2]==0.0) {
    move_particle_through_Efield(particle, Efield, dS_mm);
    return;
  }
  const auto &Ef = Efield / 1e9; // MV/mm
  const auto &Bf = Bfield * C_LIGHT / 1e9; // MV/mm
  const auto &v = particle.get_Vx_Vy_Vz(); // c
  const auto &Etotal = particle.get_total_energy(); // MeV
  const auto &Bf_norm_sqr = dot(Bf,Bf); // norm of the B field squared
  if (dot(Ef,Ef)<=Bf_norm_sqr) {
    const auto &v_para = dot(Bf,v)*Bf / Bf_norm_sqr; // c, component of velocity parallel to Bfield
    const auto &dt_mm = dS_mm / v[2]; // mm/c
    const auto &omega = particle.Q * Bf / Etotal; // rad / (mm/c), angular velocity
    const auto &rotation = Rotation(-omega * dt_mm); // rotation by the integrated angle
    // the only force acting parallel to the Bfield is that due to electric field
    const auto &E_para = dot(Bf,Ef)*Bf / Bf_norm_sqr; // MV/mm, component of Efield parallel to Bfield
    const auto &F_para = particle.Q * E_para; // MeV/mm
    const auto &a_para = (F_para - dot(v,F_para)*v) / Etotal; // c^2/mm    
    const auto &vE = cross(Ef,Bf) / Bf_norm_sqr; // c, guiding center drift, E^B / B^2
    const auto &r = cross(omega, (v-vE)) / dot(omega,omega); // mm, Larmor radius in vector form, it's a vector from the guiding center to the particle
    const auto &dX = rotation * r - r + vE * dt_mm + (v_para + 0.5 * a_para * dt_mm) * dt_mm; // mm
    particle.x += dX[0]; // mm
    particle.y += dX[1]; // mm
    particle.t += dt_mm; // mm/c
    // MeV/c, rotation due to the magnetic field + acceleration due to the component of the Efield parallel to the Bfield
    const auto &P_new = Etotal * (vE + rotation * (v-vE)) + F_para * dt_mm; // MeV/c
    const double inv_Pz = 1e3 / P_new[2]; // 1e3/(MeV/c)
    particle.xp = P_new[0] * inv_Pz; // mrad
    particle.yp = P_new[1] * inv_Pz; // mrad
    particle.Pc = norm(P_new); // MeV/c
  } else {
    // this was obtained doing a taylor expansion for small angles (i.e. small B field)
    const auto &dt_mm = dS_mm / v[2]; // mm/c
    const auto &omega = particle.Q * Bf / Etotal; // rad / (mm/c), angular velocity
    const auto &E_para = dot(Bf,Ef)*Bf / Bf_norm_sqr; // MV/mm, component of Efield parallel to Bfield
    const auto &F = particle.Q * Ef; // MeV/mm
    const auto &a = (F - dot(v,F)*v) / Etotal; // c^2/mm
    const auto &dX = (v + 0.5 * (a + cross(v,omega)) * dt_mm) * dt_mm; // mm
    particle.x += dX[0]; // mm
    particle.y += dX[1]; // mm
    particle.t += dt_mm; // mm/c
    const auto &vE = cross(Ef,Bf) / Bf_norm_sqr; // c, guiding center drift, E^B / B^2
    //const auto &dP = (Etotal * (cross(v-vE,omega) - 0.5 * (v-vE) * (omega*omega) * dt_mm) + particle.Q * E_para) * dt_mm; // MeV/c
    const auto &dP = particle.Q * (cross(v-vE,Bf) - 0.5 * (v*Bf_norm_sqr - cross(Ef,Bf)) * (particle.Q/Etotal) * dt_mm + E_para) * dt_mm; // MeV/c
    const auto &P_new = particle.get_Px_Py_Pz() + dP; // MeV/c
    const double inv_Pz = 1e3 / P_new[2]; // 1e3/(MeV/c)
    particle.xp = P_new[0] * inv_Pz; // mrad
    particle.yp = P_new[1] * inv_Pz; // mrad
    particle.Pc = sign(P_new[2]) * norm(P_new); // MeV/c
  }
}

inline void move_particle_through_EBfield(ParticleT &particle,
					  const StaticVector<3> &Efield /* V/m */,
					  const StaticVector<3> &Bfield /* T   */, const double dt_mm )
{
  if (Bfield[0]==0.0 && Bfield[1]==0.0 && Bfield[2]==0.0) {
    move_particle_through_Efield(particle, Efield, dt_mm);
    return;
  }
  const auto &Ef = Efield / 1e9; // MV/mm
  const auto &Bf = Bfield * C_LIGHT / 1e9; // MV/mm
  const auto &v = particle.get_Vx_Vy_Vz(); // c
  const auto &Etotal = particle.get_total_energy(); // MeV
  const auto &Bf_norm_sqr = dot(Bf,Bf); // norm of the B field squared
  if (dot(Ef,Ef)<=Bf_norm_sqr) {
    const auto &omega = particle.Q * Bf / Etotal; // rad / (mm/c), angular velocity
    const auto &rotation = Rotation(-omega * dt_mm); // rotation by the integrated angle
    // the only force acting parallel to the Bfield is that due to electric field
    const auto &E_para = dot(Bf,Ef) * Bf / Bf_norm_sqr; // MV/mm, component of Efield parallel to Bfield
    const auto &v_para = dot(Bf,v) * Bf / Bf_norm_sqr; // c, component of velocity parallel to Bfield
    const auto &F_para = particle.Q * E_para; // MeV/mm
    const auto &a_para = (F_para - dot(v,F_para)*v) / Etotal; // c^2/mm    
    const auto &vE = cross(Ef,Bf) / Bf_norm_sqr; // c, guiding center drift, E^B / B^2
    const auto &r = cross(omega,v-vE) / dot(omega,omega); // mm, Larmor radius in vector form, it's a vector from the guiding center to the particle
    const auto &dX = rotation * r - r + vE * dt_mm + (v_para + 0.5 * a_para * dt_mm) * dt_mm; // mm
    particle.X += dX[0]; // mm
    particle.Y += dX[1]; // mm
    particle.S += dX[2]; // mm
    // MeV/c, rotation due to the magnetic field + acceleration due to the component of the Efield parallel to the Bfield
    const auto &P_new = Etotal * (vE + rotation * (v-vE)) + F_para * dt_mm; // MeV/c
    particle.Px = P_new[0]; // MeV/c
    particle.Py = P_new[1]; // MeV/c
    particle.Pz = P_new[2]; // MeV/c
  } else {
    // this was obtained doing a taylor expansion for small angles (i.e. small B field)
    const auto &omega = particle.Q * Bf / Etotal; // rad / (mm/c), angular velocity
    const auto &E_para = dot(Bf,Ef) * Bf / Bf_norm_sqr; // MV/mm, component of Efield parallel to Bfield
    const auto &F = particle.Q * Ef; // MeV/mm
    const auto &a = (F - dot(v,F)*v) / Etotal; // c^2/mm
    const auto &dX = (v + 0.5 * (a + cross(v,omega)) * dt_mm) * dt_mm; // mm
    particle.X += dX[0]; // mm
    particle.Y += dX[1]; // mm
    particle.S += dX[2]; // mm
    const auto &vE = cross(Ef,Bf) / Bf_norm_sqr; // c, guiding center drift, E^B / B^2
    //const auto &dP = (Etotal * (((v-vE)^omega) - 0.5 * (v-vE) * (omega*omega) * dt_mm) + particle.Q * E_para) * dt_mm; // MeV/c
    const auto &dP = particle.Q * (cross(v-vE,Bf) - 0.5 * (v*Bf_norm_sqr - cross(Ef,Bf)) * (particle.Q/Etotal) * dt_mm + E_para) * dt_mm; // MeV/c
    particle.Px += dP[0]; // MeV/c
    particle.Py += dP[1]; // MeV/c
    particle.Pz += dP[2]; // MeV/c
  }
}

inline void apply_force_through_Bfield(Particle &particle,
				       const StaticVector<3> &F /* MeV/m */,
				       const StaticVector<3> &Bfield /* T */, const double dS_mm )
{
  move_particle_through_EBfield(particle, F * 1e6 / particle.Q, Bfield, dS_mm);
}

inline void apply_force_through_Bfield(ParticleT &particle,
				       const StaticVector<3> &F /* MeV/m */,
				       const StaticVector<3> &Bfield /* T */, const double dt_mm )
{
  move_particle_through_EBfield(particle, F * 1e6 / particle.Q, Bfield, dt_mm);
}

#endif /* move_particle_hh */
