/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef relativistic_velocity_addition_hh
#define relativistic_velocity_addition_hh

#include "static_vector.hh"

namespace {
  // Relativistic addition of velocities, in two forms vec+vec and (0,0,v)+vec
  // usage for a change of reference system:
  // relativistic_velocity_addition(-u,v) returns 'v' in a reference frame moving with velocity 'u'
  inline StaticVector<3> relativistic_velocity_addition(const StaticVector<3> &u, const StaticVector<3> &v )
  {
    const double dot_uu = dot(u,u);
    if (dot_uu == 0.0)
      return v;
    const double dot_uv = dot(u,v);
    const double alpha_u = sqrt(1.0 - dot_uu);
    return (dot_uu*alpha_u*v + (dot_uu+dot_uv*(1-alpha_u))*u)/(dot_uu*dot_uv+dot_uu);
  }
  // relativistic_velocity_addition(-uz,v) returns 'v' in a reference frame moving with velocity 'uz'
  inline StaticVector<3> relativistic_velocity_addition(const double &uz, const StaticVector<3> &v )
  {
    if (uz == 0.0)
      return v;
    const double alpha_u = sqrt(1.0 - uz*uz);
    const double dot_uv_p1 = uz*v[2]+1.0;
    return StaticVector<3>(alpha_u*v[0], alpha_u*v[1], uz+v[2]) / dot_uv_p1;
  }
}

#endif /* relativistic_velocity_addition_hh */
