/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef space_charge_pic_hh
#define space_charge_pic_hh

#include "space_charge.hh"
#include "mesh3d.hh"
#include "static_field.hh"
#include "fftw_mesh3d.hh"
#include "greens_functions/coulomb.hh"

template <typename GREENS_FUNCTION>
class SpaceCharge_PIC : public SpaceCharge {
protected:
  
  // particle-in-cell method
  size_t Nx, Ny, Nz;
  
  fftwMesh3d mesh_rho; // charge density
  fftwMesh3d mesh_G;   // Green's function

  fftwComplexMesh3d mesh_rho_hat; // charge density
  fftwComplexMesh3d mesh_G_hat;   // Green's function

  Mesh3d_CINT mesh_Phi;
  Mesh3d_CINT mesh_Ax;
  Mesh3d_CINT mesh_Ay;
  Mesh3d_CINT mesh_Az;
  struct {
    double x0, y0, z0;
    double hx, hy, hz;
  } mesh;
  
  fftw_plan p1, p2, p3;

  bool init_pic(size_t Nx, size_t Ny, size_t Nz );

  GREENS_FUNCTION greens_function;
  
  template <typename BUNCH_TYPE>
  void compute_force_(MatrixNd &force, const BUNCH_TYPE &bunch, const ParticleSelector &selector );

public:

  SpaceCharge_PIC(size_t Nx_=16, size_t Ny_=16, size_t Nz_=16 ) { init_pic(Nx_, Ny_, Nz_ ); }
  SpaceCharge_PIC(const SpaceCharge_PIC &sc ) { init_pic(sc.Nx, sc.Ny, sc.Nz); }
  ~SpaceCharge_PIC();

  SpaceCharge_PIC &operator = (const SpaceCharge_PIC &sc );

  // compute the force [MeV/m] and return it as a Nx3 matrix
  void compute_force(MatrixNd &force, const Bunch6d  &bunch, const ParticleSelector &selector = ParticleSelector()) override { compute_force_(force, bunch, selector); }
  void compute_force(MatrixNd &force, const Bunch6dT &bunch, const ParticleSelector &selector = ParticleSelector()) override { compute_force_(force, bunch, selector); }

  StaticField get_field() const;
  
};

// template specialization
typedef SpaceCharge_PIC<GreensFunction::IntegratedRetardedCoulomb> SpaceCharge_PIC_FreeSpace;

#endif /* space_charge_pic_hh */
