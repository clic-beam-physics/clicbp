/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef aperture_hh
#define aperture_hh

#include "particle.hh"

class Aperture {
public:
  enum Shape { NONE, RECTANGULAR, CIRCULAR };
private:
  double rx; // mm, half width
  double ry; // mm, half height
  Shape shape;
public:
  Aperture(double ax = -1.0, double ay = -1.0, Shape shape_ = NONE ) : rx(ax), ry(ay), shape(shape_) {} 
  inline bool is_point_inside_aperture(double x /* mm */, double y /* mm */ ) const
  {
    if (shape == Aperture::NONE || rx == -1.0 || ry == -1.0)
      return true;
    if (shape == Aperture::RECTANGULAR)
      return fabs(x) < rx && fabs(y) < ry;
    const double rx2 = rx*rx;
    const double ry2 = ry*ry;
    return rx2*y*y + ry2*x*x < rx2*ry2;
  }
  inline bool is_particle_inside_aperture(const ParticleT &particle ) const { return is_point_inside_aperture(particle.X, particle.Y); }
  inline bool is_particle_inside_aperture(const Particle &particle ) const  { return is_point_inside_aperture(particle.x, particle.y); }
  double get_aperture_area() const /* [m**2] */ { const double area = rx * ry / 1e6; return shape == Aperture::RECTANGULAR ? 4*area : M_PI*area; }
  double get_aperture_x() const { return rx / 1e3; } // m
  double get_aperture_y() const { return ry / 1e3; } // m
  const char *get_aperture_shape() const;
  void set_aperture_x(double ax /* m */ ) { rx = ax*1e3; }
  void set_aperture_y(double ay /* m */ ) { ry = ay*1e3; }
  void set_aperture(double ax /* m */, double ay = -1.0 /* m */, const char *type = "circular" );
  void set_aperture_shape(const char *shape );
};

#endif /* apeture_hh */
