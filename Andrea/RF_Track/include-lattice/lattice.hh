/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef lattice_hh
#define lattice_hh

#include <memory>
#include <vector>
#include <array>
#include <list>

#include "element.hh"
#include "matrixnd.hh"
#include "bunch6dt.hh"
#include "space_charge.hh"
#include "tracking_options.hh"
#include "parallel_ode_solver.hh"

class Drift;
class RF_Field;
class Quadrupole;

class TransportTable {
protected:
  std::list<Bunch6d_info> transport_table;
  std::list<Bunch6dT_info> transport_tableT;
  void append_bunch_info(const Bunch6d_info &info ) { transport_table.push_back(info); }
  void append_bunch_info(const Bunch6dT_info &info ) { transport_tableT.push_back(info); }
public:
  MatrixNd get_transport_table(const char *fmt = "%S %mean_x %mean_y %emitt_x %emitt_y" ) const;
};

class Lattice : public Parallel_ODE_Solver, public TransportTable {
  std::vector<std::shared_ptr<Element>> elements;
  std::list<std::array<double,3>> bpm_readings;
  friend class Element;
  void append_bpm_readings(double s, double x, double y ) { bpm_readings.push_back(std::array<double,3>{{s, x, y}}); }
public:
  Lattice() = default;
  Lattice(const Lattice &lattice );
  ~Lattice();
  Lattice &operator=(const Lattice &lattice );
  std::shared_ptr<Element> operator[] (size_t i ) { return elements[i]; }
  const std::shared_ptr<Element> operator[] (size_t i ) const { return elements[i]; }
  Lattice *clone() const { return new Lattice(*this); }
  void append(const Lattice &l ) { const size_t l_size = l.elements.size(); for (size_t i=0; i<l_size; i++) append(l.elements[i]); }
  void append(Element *element_ptr ) { Element *ptr = element_ptr->clone(); elements.push_back(std::shared_ptr<Element>(ptr)); }
  void append(std::shared_ptr<Element> element_ptr ) { Element *ptr = element_ptr->clone(); elements.push_back(std::shared_ptr<Element>(ptr)); }
  void append_ref(Element *element_ptr ) { elements.push_back(std::shared_ptr<Element>(element_ptr)); }
  void append_ref(std::shared_ptr<Element> element_ptr ) { elements.push_back(std::shared_ptr<Element>(element_ptr)); }
  size_t size() const { return elements.size(); }
  Bunch6d track(Bunch6d bunch );
  Bunch6dT track(Bunch6dT bunch, const TrackingOpts &T = TrackingOpts());
  double get_length() const;
  MatrixNd get_bpm_readings() const;
  std::vector<Drift*> get_drifts();
  std::vector<RF_Field*> get_rf_fields();
  std::vector<Quadrupole*> get_quadrupoles();
};

#endif /* lattice_hh */
