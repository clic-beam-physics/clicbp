/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef particle_tracker_hh
#define particle_tracker_hh

#include <string>
#include <limits>

#include "bunch6d.hh"

// forward declarations
class Lattice;
struct Bunch6dT;

struct ParticleTracker {
  std::string odeint_algorithm = "leapfrog"; // built for speed 
  double dt = 1; // mm/c, time step
  size_t nsteps = 0; // max number of steps, if '0' tracks all particles all the way to the end
  bool backtrack_at_entrance = false; // at start, backtrack the beam such that the foremost particle is exactly at S=0
  int verbose = 1; // verbose level, 0 = silent
  double Smin = -std::numeric_limits<double>::infinity();
  struct SC {
    size_t nsteps = 0; // every space_charge_nsteps steps applies one step of space-charge
    bool strict_boundaries = false; // if true applies space-charge only to particle with particle.S>=0 and particle.S<L_beamline
  } sc;
  struct WP {
    size_t nsteps = 0; // every watch_point_nsteps saves the beam on a file
    std::string basename = "watch_beam"; // saves the beam distributions with name watch_beam.0000n.txt
    bool gzip = false; // gzips files
  } wp;

  Bunch6d track(const Lattice &lattice, const Bunch6dT &bunch ) { return Bunch6d(bunch); }

};


#endif /* particle_tracker_hh */
