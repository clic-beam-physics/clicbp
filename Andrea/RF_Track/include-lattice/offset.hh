/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef offset_hh
#define offset_hh

struct Offset {
  double x = 0.0; // mm
  double y = 0.0; // mm
  double xp = 0.0; // mrad
  double yp = 0.0; // mrad
  double roll = 0.0; // mrad
};

#endif /* offset_hh */
