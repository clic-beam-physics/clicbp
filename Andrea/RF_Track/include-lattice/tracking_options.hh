/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef tracking_options_hh
#define tracking_options_hh

#include "bunch6d.hh"

class Tracking {
protected:
  mutable size_t nsteps; /// number of tracking steps
  size_t sc_nsteps; /// number of space-charge steps (default 0)
  virtual void track0(Particle &particle, double S, size_t start_step, size_t end_step, size_t thread = 0 ) const = 0;
  virtual void track0_initialize(Bunch6d &bunch ) {} // initalize
  virtual void track0_finalize() {} // finalize
public:
  Tracking(size_t nsteps_ = 1 ) : nsteps(nsteps_), sc_nsteps(0) {}
  virtual ~Tracking() = default;
  size_t get_nsteps() const { return nsteps; }
  size_t get_sc_nsteps() const { return sc_nsteps; }
  void set_sc_nsteps(size_t sc_nsteps_ ) { sc_nsteps = sc_nsteps_; }
  virtual void set_nsteps(size_t nsteps_ ) { nsteps = nsteps_; }
  virtual void track_in(Bunch6d &bunch ) = 0; // entrance misalignment
  virtual void track_out(Bunch6d &bunch ) = 0; // exit misalignment
  virtual std::list<Bunch6d_info> track(Bunch6d &bunch ) = 0;
};

struct TrackingOpts {
  std::string odeint_algorithm = "leapfrog"; // built for speed 
  double dt_mm = 1; // mm/c, time step
  size_t nsteps = 0; // max number of steps, if '0' tracks all particles all the way to the end
  size_t sc_nsteps = 0; // every space_charge_nsteps steps applies one step of space-charge
  bool sc_strict_boundaries = false; // if true applies space-charge only to particle with particle.S>=0 and particle.S<L_beamline
  bool backtrack_at_entrance = false; // at start, backtrack the beam such that the foremost particle is exactly at S=0
  size_t wp_nsteps = 0; // every watch_point_nsteps saves the beam on a file
  std::string wp_basename = "watch_beam"; // saves the beam distributions with name watch_beam.0000n.txt
  bool wp_gzip = false; // gzips files
  int verbose = 1; // verbose level, 0 = silent
};

#endif /* tracking_options_hh */
