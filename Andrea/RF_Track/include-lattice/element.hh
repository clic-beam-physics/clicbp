/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef element_hh
#define element_hh

#include <utility>
#include <limits>
#include <cmath>
#include <list>

#include "tracking_options.hh"
#include "static_matrix.hh"
#include "aperture.hh"
#include "matrixnd.hh"
#include "bunch6d.hh"
#include "offset.hh"

// class Lattice;
class Element : public Aperture, public Offset, public Tracking {
  // friend class Lattice;
public:
  Element(size_t nsteps_ = 1 ) : Tracking(nsteps_) {}
  virtual ~Element() = default;
  virtual Element *clone() = 0;
  virtual double get_length() const = 0;
  virtual std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t ) { return std::pair<StaticVector<3>, StaticVector<3>>(StaticVector<3>(0.0), StaticVector<3>(0.0)); } // x,y,z [mm] ; t [mm/c]
  virtual std::pair<StaticMatrix<3,3>, StaticMatrix<3,3>> get_field_jacobian(double x, double y, double z, double t ) { return std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>>(StaticMatrix<3,3>(0.0), StaticMatrix<3,3>(0.0)); } // x,y,z [mm] t [mm/c], returns V/m/mm, and T/mm
  void track_in(Bunch6d &bunch ); // entrance misalignment
  void track_out(Bunch6d &bunch ); // exit misalignment
  std::list<Bunch6d_info> track(Bunch6d &bunch );
};

#endif /* element_hh */
