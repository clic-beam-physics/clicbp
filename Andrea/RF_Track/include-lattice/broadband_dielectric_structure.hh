/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef broadband_dielectric_structure_hh
#define broadband_dielectric_structure_hh

#include <vector>
#include "generic_field.hh"
#include "mesh3d.hh"

class BroadbandDielectricStructure : public GenericField {
  double x0, y0; // mm
  double hx, hy, hz; // mm
  //  double length; // m
  double z0, z1; // mm
  double t0, t1, dt; // mm/c
  double P_map = 1.0; // W
  double P_actual = 1.0; // W
  
  struct FIELD_SLICE {
    Mesh3d_LINT Ex, Ey, Ez; // V/m
    Mesh3d_LINT Bx, By, Bz; // T
  };

  std::vector<FIELD_SLICE> field_slices;

  double scale_factor = 1.0;
  
  StaticVector<3> static_Efield; // [V/m] static electric field embedding the Drift (e.g. Wein filter)
  StaticVector<3> static_Bfield; // [T] static magnetic field embedding the Drift (e.g. solenoid)

private:

  std::vector<double> ta; // mm/c time axis for time lookup
  
public:
  BroadbandDielectricStructure() = delete;
  BroadbandDielectricStructure(double x0, double y0, // m
			       double hx, double hy, double hz, // m
			       double length, // m
			       double t0, double t1, // mm/c
			       size_t N, // number of time slices
			       double P_map = 1.0, // W
			       double P_actual = 1.0 );
  BroadbandDielectricStructure *clone() override { return new BroadbandDielectricStructure(*this); }
  const StaticVector<3> &get_static_Efield() const { return static_Efield; } // V/m
  const StaticVector<3> &get_static_Bfield() const { return static_Bfield; } // T
  void resize(size_t N ); // set the number of timesteps (use 0 to free memory)
  void set_field_n(size_t n, // set the n-th field (starts from 0)
		   const Mesh3d &Ex, const Mesh3d &Ey, const Mesh3d &Ez, // V/m
		   const Mesh3d &Bx, const Mesh3d &By, const Mesh3d &Bz ) // T
  {
    if (n<field_slices.size()) {
      field_slices[n] = FIELD_SLICE{ Ex, Ey, Ez, Bx, By, Bz };
    } else {
       std::cerr << "error: first you need to call the method 'resize(N)', where 'N' is the\n       total number of time slices needed\n";
    }
  }
  double get_length() const override { return (z1-z0)/1e3; } // return m
  double get_x0() const { return x0/1e3; } // returns m
  double get_y0() const { return y0/1e3; } // returns m
  double get_z0() const { return z0/1e3; } // return m
  double get_z1() const { return z1/1e3; } // return m
  double get_hx() const { return hx/1e3; } // returns m
  double get_hy() const { return hy/1e3; } // returns m
  double get_hz() const { return hz/1e3; } // returns m
  double get_t0() const { return t0; } // mm/c
  double get_t1() const { return t1; } // mm/c
  double get_dt() const { return dt; } // mm/c
  double get_P_map() const { return P_map; }
  double get_P_actual() const { return P_actual; }
  void set_x0(double x0_ ) { x0 = x0_*1e3; } // accepts m
  void set_y0(double y0_ ) { y0 = y0_*1e3; } // accepts m
  void set_z0(double z0_ ) { z0 = z0_*1e3; } // accepts m
  void set_z1(double z1_ ) { z1 = z1_*1e3; } // accepts m
  void set_hx(double hx_ ) { hx = hx_*1e3; } // accepts m
  void set_hy(double hy_ ) { hy = hy_*1e3; } // accepts m
  void set_hz(double hz_ ) { hz = hz_*1e3; } // accepts m
  void set_t0(double t0_ ) { t0 = t0_; } // accepts mm/c
  void set_dt(double dt_ ) { dt = dt_; } // accepts mm/c
  void set_length(double length_ )
  {
    const double z1_max = (field_slices.size()>0) ? (field_slices[0].Ex.size3()-1)*hz : 0.0;
    if (length_<0.0) {
      z0 = 0.0;
      z1 = z1_max;
    } else {
      z1 = z0 + length_*1e3;
      if (z1>z1_max) z1 = z1_max;
    }
  }
  // accepts m
  void set_P_map(double P ) { P_map = P; scale_factor = sqrt(P_actual / P_map); }
  void set_P_actual(double P ) { P_actual = P; scale_factor = sqrt(P_actual / P_map); }
  void set_static_Efield(double Ex, double Ey, double Ez ) { static_Efield = StaticVector<3>(Ex, Ey, Ez); }
  void set_static_Bfield(double Bx, double By, double Bz ) { static_Bfield = StaticVector<3>(Bx, By, Bz); }
  std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t ) override; // x,y,z [mm] ; t [mm/c]

};

#endif /* broadband_dielectric_structure_hh */
