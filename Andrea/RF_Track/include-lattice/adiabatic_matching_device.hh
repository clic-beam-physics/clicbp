/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef adiabatic_matching_device_hh
#define adiabatic_matching_device_hh

#include "generic_field.hh"

class AdiabaticMatchingDevice : public GenericField {
  double length, B0, fMu;
  StaticVector<3> static_Efield; // [V/m] static electric field embedding the Drift (e.g. Wein filter)
  StaticVector<3> static_Bfield; // [T] static magnetic field embedding the Drift (e.g. solenoid)
  double r1, r2; // mm, entrance and exit apertures
public:
  AdiabaticMatchingDevice(double length_ = 0.0, /* m */
			  double B0_ = 0.0, /* T */
			  double fMu_ = 0.0 /* 1/m */ ) : length(length_), B0(B0_), fMu(fMu_), static_Efield{{{ 0.0, 0.0, 0.0 }}}, static_Bfield{{{ 0.0, 0.0, 0.0 }}}, r1(-1.0), r2(-1.0) {}
  AdiabaticMatchingDevice *clone() { return new AdiabaticMatchingDevice(*this); }
  const StaticVector<3> &get_static_Efield() const { return static_Efield; }
  const StaticVector<3> &get_static_Bfield() const { return static_Bfield; } // T
  double get_length() const { return length; } // m
  double get_mu() const { return fMu; } // 1/m
  double get_B0() const { return B0; } // T
  void set_static_Efield(double Ex, double Ey, double Ez ) { static_Efield = StaticVector<3>(Ex, Ey, Ez); }
  void set_static_Bfield(double Bx, double By, double Bz ) { static_Bfield = StaticVector<3>(Bx, By, Bz); }
  void set_length(double length_ ) { length = length_; } // m
  void set_mu(double fMu_ ) { fMu = fMu_; } // 1/m
  void set_B0(double B0_ ) { B0 = B0_; } // T
  double get_entrance_aperture() const { return r1 / 1e3; } // m
  double get_exit_aperture() const { return r2 / 1e3; } // m
  void set_entrance_aperture(double r1_ /* m */ ) { r1 = r1_*1e3; }
  void set_exit_aperture(double r2_ /* m */ ) { r2 = r2_*1e3; }
  std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t ); // x,y,z [mm] ; t [mm/c]

};

#endif /* adiabatic_matching_device_hh */
