/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef generic_field_hh
#define generic_field_hh

#include <complex>
#include <mutex>
#include <array>

#include "element.hh"
#include "parallel_ode_solver.hh"

class GenericField : public Element, public Parallel_ODE_Solver {
  // auxiliary variables for GSL ODE integration
  std::vector<gsl_odeiv2_system> sys;
  mutable bool gsl_error;
protected:
  virtual void track0_initialize(Bunch6d &bunch ); // initialize
  virtual void track0_finalize(); // finalize
  virtual void track0(Particle &particle, double S, size_t start_step, size_t end_step, size_t thread = 0 ) const;
public:
  GenericField() = default;
  virtual ~GenericField() = default;
  virtual GenericField *clone() = 0;
  virtual double get_length() const = 0;
  std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t ) = 0;

};

#endif /* generic_field_hh */
