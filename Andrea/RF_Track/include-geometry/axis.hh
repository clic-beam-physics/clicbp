/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef axis_hh
#define axis_hh

#include <iostream>
#include <valarray>
#include <cmath>

#include "static_vector.hh"

#define Axis_X Axis(M_PI / 2., 0.)
#define Axis_Y Axis(M_PI / 2., M_PI / 2.)
#define Axis_Z Axis(0., 0.)

struct Axis {

  double theta;
  double phi;
	
  explicit Axis(const Vector3d &v )
  {
    const double rho = norm(v);
    theta = rho != 0.0 ? acos(v[2]/rho) : 0.0;
    phi = atan2(v[1], v[0]);
  }
  Axis(double _theta, double _phi ) : theta(_theta), phi(_phi) {}
  Axis() = default;
  
  double x() const { return sin(theta) * cos(phi); } 			  
  double y() const { return sin(theta) * sin(phi); } 			  
  double z() const { return cos(theta); }		  			  
  
  operator Vector3d() const 
  {
    double s_t = sin(theta);
    double c_t = cos(theta);
    double s_p = sin(phi);
    double c_p = cos(phi);
    return Vector3d(s_t * c_p, s_t * s_p, c_t);
  }  
 
  friend Axis operator - (const Axis &a ) { return Axis(fmod(M_PI - a.theta, M_PI), fmod(a.phi + M_PI, 2.*M_PI)); }
  
  friend double operator * (const Axis &a, const Axis &b )
  {
    double theta_m = b.theta - a.theta;
    double theta_p = b.theta + a.theta;
    double phi_m = b.phi - a.phi;
    double c_tm = cos(theta_m);
    double c_tp = cos(theta_p);
    double c_pm = cos(phi_m);
    return (-c_pm*c_tp+c_tp+c_pm*c_tm+c_tm) / 2;
  }
  
  friend Axis cross(const Axis &a, const Axis &b )
  {
    double s_t1 = sin(a.theta);
    double c_t1 = cos(a.theta);
    double s_p1 = sin(a.phi);
    double c_p1 = cos(a.phi);
    double s_t2 = sin(b.theta);
    double c_t2 = cos(b.theta);
    double s_p2 = sin(b.phi);
    double c_p2 = cos(b.phi);
    return Axis(Vector3d (s_p1 * s_t1 * c_t2 - s_p2 * c_t1 * s_t2,
			  c_p2 * c_t1 * s_t2 - c_p1 * s_t1 * c_t2,
			  s_t1 * s_t2 * (c_p1 * s_p2 - s_p1 * c_p2)));
  }
  
  friend Vector3d operator * (double x, const Axis &a )	{ return ::polar(x, a.theta, a.phi); }
  friend Vector3d operator * (const Axis &a, double x )	{ return ::polar(x, a.theta, a.phi); }
  
  friend std::ostream &operator << (std::ostream &stream, const Axis &a );
  friend std::istream &operator >> (std::istream &stream, Axis &a );
};

#endif /* axis_hh */
