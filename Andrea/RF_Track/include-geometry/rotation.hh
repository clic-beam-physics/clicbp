/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef rotation_hh
#define rotation_hh

#include <iostream>

#include "axis.hh"
#include "quaternion.hh"
#include "fftw_complex.hh"
#include "static_matrix.hh"

class Rotation : public Quaternion {
  enum { PHI_X = 0, THETA_Z = 1, PHI_Z = 2 };
public:
  explicit Rotation(const Quaternion &q ) : Quaternion(q) {}
  Rotation(const Vector3d &angle ) : Quaternion(exp(Quaternion(0.0, angle/2))) {}
  Rotation(double angle, const Vector3d &axis ) : Quaternion(cos(angle/2), sin(angle/2) * normalize(axis)) {}
  Rotation(double phi_x, double theta_z, double phi_z ) : Quaternion(Rotation(phi_z, Axis_Z) * Rotation(theta_z, Axis_Y) * Rotation(phi_x, Axis_Z)) {}
  Rotation() : Quaternion(1.0, 0.0, 0.0, 0.0) {}
  Vector3d operator * (const Vector3d &v ) const { const Quaternion &q = *this; return (q * v * conj(q)).v; }
  StaticVector<3,fftwComplex> operator * (const StaticVector<3,fftwComplex> &v ) const
  {
    const Quaternion &q = *this;
    Vector3d
      v_real(v[0].real, v[1].real, v[2].real),
      v_imag(v[0].imag, v[1].imag, v[2].imag);    
    v_real = (q * v_real * conj(q)).v;
    v_imag = (q * v_imag * conj(q)).v;
    return StaticVector<3,fftwComplex>(fftwComplex(v_real[0], v_imag[0]),
				       fftwComplex(v_real[1], v_imag[1]),
				       fftwComplex(v_real[2], v_imag[2]));
  }
  Rotation operator * (const Rotation &r ) const { const Quaternion &q1 = *this, q2 = r; return Rotation(q1*q2); }
  StaticMatrix<3,3> get_rotation_matrix() const
  {
    return StaticMatrix<3,3>(1 - 2 * (v[1] * v[1] + v[2] * v[2]),   2 * (v[0] * v[1] - s * v[2]),	   2 * (v[0] * v[2] + s * v[1]),
			     2 * (v[0] * v[1] + s * v[2]), 	1 - 2 * (v[0] * v[0] + v[2] * v[2]),       2 * (v[1] * v[2] - s * v[0]),
			     2 * (v[0] * v[2] - s * v[1]),	    2 * (v[1] * v[2] + s * v[0]),      1 - 2 * (v[0] * v[0] + v[1] * v[1]));
  }

  void set_euler_angles(const std::valarray<double> &angles )
  {
    *this =
      Rotation(angles[Rotation::PHI_Z], Axis_Z) *
      Rotation(angles[Rotation::THETA_Z], Axis_Y) *
      Rotation(angles[Rotation::PHI_X], Axis_Z);
  }
  
  std::array<double,3> get_euler_angles() const 
  {
    const Axis z = Axis(operator*(Vector3d(Axis_Z)));
    const Axis x = Axis(Rotation(Rotation(-z.theta, Axis_Y) * Rotation(-z.phi, Axis_Z) * (*this)) * Vector3d(Axis_X));
    std::array<double,3> result;
    result[THETA_Z] = z.theta;
    result[PHI_Z] = z.phi;
    result[PHI_X] = x.phi;
    return result;
  }

  friend Rotation inverse(const Rotation &a ) { return Rotation(conj(a)); }

};

#endif /* rotation_hh */
