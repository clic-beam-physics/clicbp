#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass beamer
\begin_preamble
\usepackage{color}
\usepackage{listings}
\definecolor{grey}{rgb}{0.4,0.4,0.4}
\definecolor{darkgreen}{rgb}{0,0.4,0}
\definecolor{comment}{rgb}{0.1,0.50,0.56}
\definecolor{strings}{rgb}{0.25,0.44,0.63}
\lstset{numberstyle=\color{grey}\tiny,          % Numbers style..
basicstyle=\scriptsize\ttfamily,
columns=fullflexible,
tabsize=2,                  % Tabs size
breaklines=flse,            % Break long lines
keywordstyle=\color{darkgreen}\bfseries,
stringstyle=\color{strings}\ttfamily, % String color
commentstyle=\it\color{comment}\ttfamily,
resetmargins=false,
xleftmargin=0pt,
framesep=0pt,
framexleftmargin=0pt,
framexrightmargin=0pt,
framexbottommargin=0pt,
framextopmargin=0pt,
showstringspaces=false,language=Octave}
\setbeamersize{text margin left=0.2cm}
\setbeamersize{text margin right=0.5cm}
\setbeamertemplate{footline}{\vspace{-8mm}
{\color{gray}\tiny{{\hspace{1mm}\insertframenumber/\inserttotalframenumber}} A.~Latina - RF-Track, features and capabilities}\vspace{1mm}}
\date{~\\[2cm]\color{gray}\small{AWAKE Technical Board, 24 April 2017}}
\end_preamble
\options simple
\use_default_options true
\maintain_unincluded_children false
\language british
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "cmbr" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 90 100
\font_tt_scale 90 100
\graphics default
\default_output_format default
\output_sync 1
\bibtex_command default
\index_command default
\paperfontsize 10
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 2
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
\begin_inset VSpace bigskip
\end_inset


\begin_inset Newline newline
\end_inset

RF-Track:
\begin_inset Newline newline
\end_inset

a minimalistic multipurpose
\begin_inset Newline newline
\end_inset

 tracking code featuring space-charge
\end_layout

\begin_layout Author
Andrea Latina
\begin_inset Newline newline
\end_inset


\size footnotesize
(BE-ABP-LAT)
\end_layout

\begin_layout Institute
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
thispagestyle{empty}
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Table of Contents
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Section
Overview
\end_layout

\begin_layout Subsection
Motivations
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Motivations for RF-Track: The TULIP Project
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
High-gradient proton/carbon-ion linac for hadron therapy
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename figures/TULIP.pdf
	width 95col%

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Motivations for RF-Track
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize

\size small
\bar under
TULIP Project
\bar default
: design a linac based on a new 
\bar under
high-gradient
\bar default
 (50 MV/m) S-band 
\bar under
backward travelling-wave structures
\bar default
 (bwTW), with initial 
\begin_inset Formula $\underline{\beta=0.38}$
\end_inset

, dictated the need to develop a new code capable of tracking particles
 through such a linac
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize

\size small
We needed to track protons as well as carbon ions
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize

\size small
We needed 6d tracking in 3d EM field maps of the bwTW structures
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize

\size small
We needed the possibility to maximise the transmission tuning 
\series bold
any
\series default
 parameters: RF input power, quads strengths, quads positions, input distributio
n, etc.
 
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize

\size small
We needed something flexible and fast
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize

\size small
We decided to develop RF-Track
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Subsection
Technicalities
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
RF-Track Highlights
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize

\size footnotesize
Fast, parallel, C++ core, usable from Octave and Python
\end_layout

\begin_deeper
\begin_layout Itemize

\size footnotesize
minimalistic: only the physics it's implemented; for numerical algorithms
 it uses well-established open-source libraries like 
\bar under
GSL
\bar default
, "Gnu Scientific Library", and 
\bar under
FFTW
\bar default
, "Fastest Fourier Transform in the West" 
\begin_inset VSpace defskip
\end_inset


\end_layout

\end_deeper
\begin_layout Itemize

\size footnotesize
Can handle complex 3d field maps of Electric + Magnetic fields: static fields,
 as well as field maps of RF backward/forward travelling waves
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Itemize

\size footnotesize
It's fully relativistic
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize

\size footnotesize
no approximations like 
\begin_inset Formula $\beta\ll1$
\end_inset

 or 
\begin_inset Formula $\gamma\gg1$
\end_inset


\end_layout

\begin_layout Itemize

\size footnotesize
successfully tested with: electrons, positrons, protons, antiprotons, ions,
 at various energies
\begin_inset VSpace defskip
\end_inset


\end_layout

\end_deeper
\begin_layout Itemize

\size footnotesize
Can track mixed-species beams
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Itemize

\size footnotesize
Implements high-order integration algorithms
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Itemize

\size footnotesize
Implements space-charge
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Itemize

\size footnotesize
It's flexible and fast
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Section
Tracking capabilities
\end_layout

\begin_layout Subsection
Beam models
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Tracking: RF-Track implements two beam models
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Enumerate

\size footnotesize
\bar under
Beam moving in space
\bar default
:
\size default

\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize

\size footnotesize
All particles have the same 
\begin_inset Formula $S$
\end_inset

 position
\end_layout

\begin_layout Itemize

\size footnotesize
Each particle's phase space is
\begin_inset Formula 
\[
\left(x\text{ {\color{cyan}[mm]}},\ x^{\prime}\text{ {\color{cyan}[mrad]}},\ y\text{ {\color{cyan}[mm]}},\ y^{\prime}\text{ {\color{cyan}[mrad]}},\ t\text{\text{ {\color{cyan}[mm/c]}}},\ P_{z}\text{{\color{cyan}\text{ [MeV/c]}}}\right)
\]

\end_inset

where 
\begin_inset Formula $t$
\end_inset

 is the proper time of each particle at 
\begin_inset Formula $S$
\end_inset


\end_layout

\begin_layout Itemize

\size footnotesize
Tracking is performed integrating the equations of motion in 
\begin_inset Formula $dS$
\end_inset

:
\begin_inset Formula 
\[
S\to S+dS
\]

\end_inset


\end_layout

\end_deeper
\begin_layout Enumerate

\size footnotesize
\bar under
Beam moving in time
\bar default
:
\size default

\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize

\size footnotesize
All particles are taken at same time 
\begin_inset Formula $t$
\end_inset


\end_layout

\begin_layout Itemize

\size footnotesize
Each particle's phase space is
\begin_inset Formula 
\[
\left(X\text{ {\color{cyan}[mm]}},\ Y\text{ {\color{cyan}[mm]}},\ S\text{ {\color{cyan}[mm]}},\ P_{x}{\color{cyan}\text{ [MeV/c]}},\ P_{y}{\color{cyan}\text{ [MeV/c]}},\ P_{z}{\color{cyan}\text{ [MeV/c]}}\right)
\]

\end_inset


\end_layout

\begin_layout Itemize

\size footnotesize
\bar under
Notice
\bar default
: can handle particles with 
\begin_inset Formula $P_{z}<0$
\end_inset

 or even 
\begin_inset Formula $P_{z}=0$
\end_inset

 
\end_layout

\begin_layout Itemize

\size footnotesize
Tracking is performed integrating the equations of motion in 
\begin_inset Formula $dt$
\end_inset

:
\begin_inset Formula 
\[
t\to t+dt
\]

\end_inset


\end_layout

\end_deeper
\begin_layout Itemize

\size footnotesize
Each particle also stores
\begin_inset Formula 
\[
m:\ \mathsf{mass\ [MeV/c^{2}]},\qquad Q:\ \mathrm{\text{ charge }}[e^{+}],\qquad N:\ \text{nb of particles / macroparticle}
\]

\end_inset

so that 
\bar under
RF-Track can simulate mixed-species beams
\end_layout

\end_deeper
\begin_layout FragileFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Tracking: inquiring the phase space
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard

\size small
One can retrieve the phase space with great flexibility: 
\size default
e.g.
\family typewriter
\size small

\begin_inset listings
inline false
status open

\begin_layout Plain Layout
\align center

P1 = B1.get_phase_space(%x %Px %y %Py %deg@750 %K);
\end_layout

\end_inset


\family default

\begin_inset VSpace 0cm
\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename figures/table_beam.pdf
	width 80col%

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Subsection
Beam line elements
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Tracking: Beam line elements
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Minimal set of elements:
\end_layout

\begin_layout Itemize
RF Field maps
\end_layout

\begin_layout Itemize
Quadrupoles
\end_layout

\begin_layout Itemize
Drift
\end_layout

\begin_layout Itemize
...Field maps and drifts can embed a static B field
\size footnotesize

\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Itemize
Each element:
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
can be tracked in several steps, to capture phase space evolution along
 the element
\end_layout

\begin_layout Itemize
can have an aperture, which is checked at 
\bar under
any
\bar default
 integration step
\size footnotesize

\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
If a particle hits the aperture, it is flagged as lost
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_layout Itemize
User can retrieve its full phase space components, as well as
\begin_inset Newline newline
\end_inset

its 3d position and time at which is lost
\size footnotesize

\begin_inset VSpace defskip
\end_inset


\end_layout

\end_deeper
\begin_layout Itemize
The user can identify what particles are lost in the initial distribution
\end_layout

\end_deeper
\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout FragileFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Tracking: Field maps
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="1" columns="2">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "50col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Itemize

\size footnotesize
Complex EM maps of RF fields
\size default

\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize

\size footnotesize
forward traveling / backward traveling / static fields
\end_layout

\begin_layout Itemize

\size footnotesize
tricubic interpolation
\begin_inset VSpace defskip
\end_inset


\end_layout

\end_deeper
\begin_layout Itemize

\size footnotesize
Accepts quarter/half field maps
\size default

\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize

\size footnotesize
automatic 
\bar under
mirroring
\bar default
 of the fields
\end_layout

\begin_layout Itemize

\size footnotesize
accepts 
\bar under
cartesian
\bar default
 and 
\bar under
cylindrical
\bar default
 maps
\begin_inset VSpace defskip
\end_inset


\end_layout

\end_deeper
\begin_layout Itemize

\size footnotesize
Can change dynamically input power
\size default

\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize

\size footnotesize
Provide 
\begin_inset Formula $P_{\text{map}}$
\end_inset

, set 
\begin_inset Formula $P_{\text{actual}}$
\end_inset


\begin_inset VSpace defskip
\end_inset


\end_layout

\end_deeper
\begin_layout Itemize

\size footnotesize
Not-a-Numbers are considered as walls
\size default

\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize

\size footnotesize
allows to precisely track losses in the 3d volume
\end_layout

\end_deeper
\begin_layout Itemize

\size footnotesize
One can retrieve the fields at any point: e.g.
\size default

\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Plain Layout

\size footnotesize
\begin_inset space \qquad{}
\end_inset


\family typewriter
[E,B] = RFQ.get_field(x,y,z,t);
\end_layout

\end_deeper
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
\begin_inset VSpace defskip
\end_inset


\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "40col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout

\size footnotesize
Example:
\begin_inset listings
inline false
status open

\begin_layout Plain Layout

load 'field.dat.gz';
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

RFQ = RF_Field( ...
\end_layout

\begin_layout Plain Layout

    field.Ex, ...
 % Efield [V/m]
\end_layout

\begin_layout Plain Layout

    field.Ey, ...
\end_layout

\begin_layout Plain Layout

    field.Ez, ...
\end_layout

\begin_layout Plain Layout

    field.Bx, ...
 % Bfield [T]
\end_layout

\begin_layout Plain Layout

    field.By, ...
\end_layout

\begin_layout Plain Layout

    field.Bz, ...
\end_layout

\begin_layout Plain Layout

    field.xa(1), ...
 % x0,y0 [m]
\end_layout

\begin_layout Plain Layout

    field.ya(1), ...
\end_layout

\begin_layout Plain Layout

    field.hx, ...
 % mesh size [m]
\end_layout

\begin_layout Plain Layout

    field.hy, ...
\end_layout

\begin_layout Plain Layout

    field.hz, ...
\end_layout

\begin_layout Plain Layout

    field.za(end), ...
 % length [m]
\end_layout

\begin_layout Plain Layout

    field.frequency, ...
 % [Hz]
\end_layout

\begin_layout Plain Layout

    field.direction, ...
 % +1, -1, 0
\end_layout

\begin_layout Plain Layout

    field.P_map, ...
 % [W]
\end_layout

\begin_layout Plain Layout

    field.P_actual);
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\size scriptsize
\begin_inset VSpace defskip
\end_inset


\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Subsection
Integration algorithms
\end_layout

\begin_layout FragileFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Tracking: Integration algorithms
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard

\size footnotesize
By default, RF-Track uses:
\end_layout

\begin_layout Itemize

\size footnotesize
"
\bar under
leapfrog
\bar default
" integration algorithm: fast, second-order, symplectic
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Standard

\size footnotesize
In some cases, leapfrog might not be accurate enough.
 RF-Track offers the following GSL algorithms as an alternative:
\end_layout

\begin_layout Itemize

\size scriptsize
\bar under
Explicit
\bar default
 algorithms:
\begin_inset Newline newline
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="4" columns="2">
<features tabularvalignment="middle">
<column alignment="left" valignment="top">
<column alignment="left" valignment="top">
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
\begin_inset Formula $\star$
\end_inset

"rk2" Runge-Kutta (2, 3)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
\begin_inset Formula $\star$
\end_inset

"rkck" Runge-Kutta Cash-Karp (4, 5) 
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
\begin_inset Formula $\star$
\end_inset

"rk4" 4th order (classical) Runge-Kutta
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
\begin_inset Formula $\star$
\end_inset

"rk8pd" Runge-Kutta Prince-Dormand (8, 9)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
\begin_inset Formula $\star$
\end_inset

"rkf45" Runge-Kutta-Fehlberg (4, 5)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
\begin_inset Formula $\star$
\end_inset

"msadams" multistep Adams in Nordsieck form;
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
\begin_inset space \qquad{}
\end_inset

order varies dynamically between 1 and 12
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset

 
\end_layout

\begin_layout Itemize

\size scriptsize
\bar under
Implicit
\bar default
 algorithms:
\begin_inset Newline newline
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="3" columns="1">
<features tabularvalignment="middle">
<column alignment="left" valignment="top">
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
\begin_inset Formula $\star$
\end_inset

"rk1imp", "rk2imp", "rk4imp" implicit Runge-Kutta
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
\begin_inset Formula $\star$
\end_inset

"bsimp" Bulirsch-Stoer method of Bader and Deuflhard
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
\begin_inset Formula $\star$
\end_inset

"msbdf" multistep backward differentiation formula (BDF) method in Nordsieck
 form
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset VSpace smallskip
\end_inset


\end_layout

\begin_layout Itemize

\size scriptsize
\bar under
Exact
\bar default
 algorithm:
\begin_inset Newline newline
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="1" columns="1">
<features tabularvalignment="middle">
<column alignment="left" valignment="top">
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\size scriptsize
\begin_inset Formula $\star$
\end_inset

"analytic" integration of the equations of motion in a locally constant
 EM field
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard

\size footnotesize
Example:
\family typewriter
\size small

\begin_inset listings
inline false
status open

\begin_layout Plain Layout

L = Lattice();
\end_layout

\begin_layout Plain Layout

L.append(RFQ);
\end_layout

\begin_layout Plain Layout

L.set_odeint_algorithm("msadams");
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

B1 = L.track(B0, 1.0); % tracks in time, using integration step dt = 1 mm/c
\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Subsection
Collective effects
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Physics: Space-charge interaction
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard

\size footnotesize
RF-Track solves the basic differential laws of magneto and electro-dynamics
 for 
\bar under
any
\bar default
 distribution of particles.
 Full 3d solver, with two optional methods:
\end_layout

\begin_layout Standard

\size footnotesize
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Enumerate

\size footnotesize
\bar under
particle-2-particle
\bar default
: 
\begin_inset Formula $O\left(n_{\text{particles}}^{2}\ /n_{\text{cpus}}\right)$
\end_inset

 computations
\size default

\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize

\size footnotesize
computes the electromagnetic interaction between each pair of particles
\end_layout

\begin_layout Itemize

\size footnotesize
numerically-stable summation of the forces (Kahan summation)
\end_layout

\begin_layout Itemize

\size footnotesize
fully parallel
\begin_inset VSpace medskip
\end_inset


\end_layout

\end_deeper
\begin_layout Enumerate

\size footnotesize
\bar under
cloud-in-cell
\bar default
: 
\begin_inset Formula $O\left(n_{\text{particles}}\cdot n_{\text{grid}}\cdot\log n_{\text{grid}}\ /n_{\text{cpus}}\right)$
\end_inset

 computations 
\begin_inset Formula $\to$
\end_inset

 much faster
\size default

\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard

\size footnotesize
\begin_inset Formula 
\[
\begin{array}{rcc}
{\displaystyle \vec{A}\left(x\right)=\frac{\mu_{0}}{4\pi}\iiint\frac{\vec{j}\left(x^{\prime}\right)}{\left|x-x^{\prime}\right|}d^{3}x^{\prime}} & \Rightarrow & \vec{B}=\nabla\wedge\vec{A}\\
{\displaystyle \phi\left(x\right)=\frac{1}{4\pi\epsilon_{0}}\iiint\frac{\rho\left(x^{\prime}\right)}{\left|x-x^{\prime}\right|}d^{3}x^{\prime}} & \Rightarrow & \vec{E}=\nabla\phi
\end{array}
\]

\end_inset


\end_layout

\begin_layout Itemize

\size footnotesize
uses 3d integrated Green functions
\end_layout

\begin_layout Itemize

\size footnotesize
computes the 
\begin_inset Formula $E$
\end_inset

 and 
\begin_inset Formula $B$
\end_inset

 fields directly from 
\begin_inset Formula $\phi$
\end_inset

 and 
\begin_inset Formula $\vec{A}$
\end_inset

, this ensures 
\begin_inset Formula $\nabla\cdot\vec{B}=0$
\end_inset


\end_layout

\begin_layout Itemize

\size footnotesize
can save 
\begin_inset Formula $E$
\end_inset

 and 
\begin_inset Formula $B$
\end_inset

 field maps on disk, and use them for fast tracking
\end_layout

\begin_layout Itemize

\size footnotesize
implements continuous beams
\end_layout

\begin_layout Itemize

\size footnotesize
fully parallel
\begin_inset VSpace defskip
\end_inset


\end_layout

\end_deeper
\begin_layout Itemize

\size footnotesize
\bar under
No approximations
\bar default
 such as "small velocities", or 
\begin_inset Formula $\vec{B}\ll\vec{E}$
\end_inset

, or gaussian bunch, are made.
\end_layout

\begin_layout Itemize

\size footnotesize
Can simulate beam-beam forces
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Section
Examples
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Examples: ELENA Transfer Line
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard

\size small
(Courtesy of Javier R.
 López, University of Liverpool)
\size default

\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="2" columns="2">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Graphics
	filename figures/ELENA/plot_nominal_xxp.png
	lyxscale 10
	width 45col%

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Graphics
	filename figures/ELENA/plot_nominal_yyp.png
	lyxscale 10
	width 45col%

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Graphics
	filename figures/ELENA/plot_nominal_zpt.png
	lyxscale 10
	width 45col%

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Box Frameless
position "b"
hor_pos "c"
has_inner_box 1
inner_pos "s"
use_parbox 0
use_makebox 0
width "40col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Itemize

\size footnotesize
Antiprotons with kinetic energy 
\begin_inset Formula $E_{\text{kinetic}}=100$
\end_inset

 keV (
\begin_inset Formula $\beta_{\text{rel}}\approx0.015$
\end_inset

)
\end_layout

\begin_layout Itemize

\size footnotesize
Transfer line with 6 FODO cells
\end_layout

\begin_layout Itemize

\size footnotesize
Particle-to-particle comparison with PTC.
\begin_inset VSpace 1.5cm
\end_inset


\end_layout

\end_inset


\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Examples: X-band Injector Gun
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\align left

\size small
(Courtesy of Avni Aksoy, University of Ankara)
\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename figures/plot_Xband_Inj_zK.png
	lyxscale 40
	width 50col%

\end_inset


\end_layout

\begin_layout Standard

\size small
X-band photo injector gun
\end_layout

\begin_layout Itemize

\size small
Electron photo-injector accelerating 
\begin_inset Formula $e^{-}$
\end_inset

 from 
\begin_inset Formula $E_{\text{kinetic}}=0.05$
\end_inset

 eV (
\begin_inset Formula $\beta_{\text{rel}}\text{\approx0.0004}$
\end_inset

) to ~7.5 MeV, using a 
\begin_inset Formula $5\pi/6$
\end_inset

 structure with 200 MV/m gradient
\end_layout

\begin_layout Itemize

\size small
high-order integration required
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Examples: RFQ
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard

\size footnotesize
(Thanks to Alessandra Lombrardi, Veliko Dimov)
\size default

\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_layout Standard
\align center

\size footnotesize
\begin_inset Graphics
	filename figures/plot_RFQ.pdf
	width 100col%

\end_inset


\end_layout

\begin_layout Standard

\size footnotesize
Simulated the tracking in the RFQ.
 Veliko kindly provided the field maps and the input distribution.
\end_layout

\begin_layout Itemize

\size footnotesize
Accelerates proton from 
\begin_inset Formula $E_{\text{kinetic}}=40$
\end_inset

 keV to 
\begin_inset Formula $E_{\text{kinetic}}=5$
\end_inset

 MeV
\end_layout

\begin_layout Itemize

\size footnotesize
Expected transmission confirmed = ~25%
\end_layout

\begin_layout Itemize

\size footnotesize
Field map: 
\begin_inset Formula $27\times27\times10'000$
\end_inset


\end_layout

\begin_layout Itemize

\size footnotesize
Tracking 100'000 particles takes less than 1 minute on a modern PC
\end_layout

\begin_layout Itemize

\size footnotesize
3D tracking of losses
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Examples: Beam-beam force (1/2)
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard

\size footnotesize
(Many thanks to Elias Métral for reviewing these results and for answering
 all my questions)
\end_layout

\begin_layout Standard

\size footnotesize
As RF-Track solves the full set of Maxwell equations for 
\begin_inset Formula $\vec{E}$
\end_inset

 and 
\begin_inset Formula $\vec{B}$
\end_inset

.
 With two bunches going in opposite directions, it is possible to simulate
 beam-beam effects.
 
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Standard

\size footnotesize
Benchmarks against analytical model
\end_layout

\begin_layout Itemize

\size footnotesize
Beam-beam force for a gaussian beam: 
\begin_inset Formula 
\[
F_{r}\left(r\right)=\frac{Nq_{1}q_{2}}{2\pi\varepsilon_{0}l_{b}}\left(1+\beta_{\text{rel}}^{2}\right)\frac{1-\exp\left(-\frac{r^{2}}{2\sigma^{2}}\right)}{r}
\]

\end_inset


\end_layout

\begin_layout Itemize

\size footnotesize
Simulation setup
\size default

\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard

\size footnotesize
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "80col%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Itemize

\size footnotesize
bunch with 20 million particles
\end_layout

\begin_layout Itemize

\size footnotesize
profile: gaussian transversely / uniform longitudinally
\end_layout

\begin_layout Itemize

\size footnotesize
mesh 256
\begin_inset Formula $\times$
\end_inset

256
\begin_inset Formula $\times$
\end_inset

128
\size default

\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Plain Layout

\size footnotesize
\begin_inset Formula $\sigma_{z}\gg\sigma_{x},\sigma_{y}$
\end_inset


\end_layout

\end_deeper
\begin_layout Itemize

\size footnotesize
transverse velocities = 0 (to match the analytical assumption)
\end_layout

\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset VSpace defskip
\end_inset

computational time : 
\begin_inset Formula $t_{\text{cpu}}\approx30$
\end_inset

 s on my laptop
\end_layout

\end_deeper
\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Examples: Beam-beam force (2/2)
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard

\size footnotesize
LHC-like parameters:
\end_layout

\begin_layout Itemize

\size footnotesize
head-on collision
\end_layout

\begin_layout Itemize

\size footnotesize
\begin_inset Formula $P_{z}=7$
\end_inset

 TeV / c; 
\end_layout

\begin_layout Itemize

\size footnotesize
\begin_inset Formula $P_{x}=P_{y}=0$
\end_inset


\end_layout

\begin_layout Itemize

\size footnotesize
\begin_inset Formula $\sigma_{z}$
\end_inset

 = 75.5 mm 
\end_layout

\begin_layout Itemize

\size footnotesize
\begin_inset Formula $\beta^{\star}=0.55$
\end_inset

 m 
\end_layout

\begin_layout Itemize

\size footnotesize
normalised emittance = 3.75 mm
\begin_inset Formula $\cdot$
\end_inset

mrad;
\end_layout

\begin_layout Standard

\size footnotesize
Force is calculated in the range 
\begin_inset Formula $\left[-10\sigma,\ +10\sigma\right]$
\end_inset


\size default

\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename figures/beambeam_force.pdf
	width 80text%
	BoundingBox 0bp 30bp 360bp 220bp

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Last example: Electron Cooling at LEIR
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\align left

\size small
Simulation of the cooling process at LEIR
\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename figures/plot_emitt_LEIR.png
	width 80col%

\end_inset


\end_layout

\begin_layout Standard

\size small
[VIDEO!]
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\begin_layout Section
Conclusions
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Summary and future steps
\end_layout

\end_inset


\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
RF-Track:
\end_layout

\begin_layout Itemize
A new code has been developed: minimalistic, parallel, fast
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
lot of experience went into it: C++ programming, numerical algorithms, particle
 tracking 
\begin_inset VSpace defskip
\end_inset


\end_layout

\end_deeper
\begin_layout Itemize
Flexible: can track any particles at any energy, all together
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Itemize
It implements direct space-charge (and beam-beam)
\end_layout

\begin_layout Itemize
It is being documented, but it's simple enough to be already usable
\begin_inset Separator latexpar
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
you are welcome to use it!
\begin_inset VSpace defskip
\end_inset


\end_layout

\end_deeper
\begin_layout Standard
Possible improvements:
\end_layout

\begin_layout Itemize
Implement indirect space-charge?
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Standard
Availability: a pre-compiled version is on lxplus, with simple clarifying
 examples
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator parbreak
\end_inset


\end_layout

\end_body
\end_document
