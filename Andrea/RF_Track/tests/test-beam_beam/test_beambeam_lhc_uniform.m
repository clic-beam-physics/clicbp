#! /opt/local/bin/octave -q --persist

setenv("GNUTERM","X11");
addpath('../../');

%% Load RF_Track
RF_Track;

%% charge in bunch
Q_proton = 1.15e11;

P_z = 7e6; %  MeV
beta_gamma = P_z / RF_Track.protonmass;
emitt_norm = 3.75; % micron*rad
emitt_geom = emitt_norm/beta_gamma; % micron*rad

beta_star = 0.55; % m
sigma_x  = sqrt(emitt_geom*beta_star); % mm
sigma_y  = sqrt(emitt_geom*beta_star); % mm
sigma_xp = sqrt(emitt_geom/beta_star) / 1e3; % rad
sigma_yp = sqrt(emitt_geom/beta_star) / 1e3; % rad
sigma_z = 75.5; % mm

%% define bunch
P_z = -7e6; %  MeV
N_proton = 20e6;
B_proton = [
	    randn(N_proton,1) * sigma_x, ...	      % x
	    0*randn(N_proton,1) * sigma_xp * P_z, ... % Px
	    randn(N_proton,1) * sigma_y, ...          % y
	    0*randn(N_proton,1) * sigma_yp * P_z, ... % Px
	    %%randn_3sigma(N_proton,1) * sigma_z, ... % z
	    rand(N_proton,1) * sigma_z, ...           % z
	    ones(N_proton,1) * P_z, ...               % Pz
	    ones(N_proton,1) * RF_Track.protonmass, ... % mass
	    ones(N_proton,1), ...                   % Q
	    ones(N_proton,1) * Q_proton / N_proton  % N
];

N_test  = 2;
N_sigma = 6;
X_test = N_sigma * sigma_x * linspace(-1, 1, N_test)';
Y_test = N_sigma * sigma_y * linspace(-1, 1, N_test)';
N_ones = ones(N_test,1);
N_zero = zeros(N_test,1);

B_test_x = [ X_test N_zero N_zero N_zero N_zero N_zero RF_Track.protonmass*N_ones N_ones N_zero ];
B_test_y = [ N_zero N_zero Y_test N_zero N_zero N_zero RF_Track.protonmass*N_ones N_ones N_zero ];

B0 = Bunch6dT( [ B_test_x ; B_test_y ; B_proton ] );

clear B_proton

SC = SpaceCharge_PIC_FreeSpace(512,512,16);
tic
SC.compute_force(B0);
toc

global F
F = SC.get_field();
F.set_hz(0.5 * F.get_hz()); % each particle sees the field for a time that is length / 2c (i.e. half length)
F.set_length(0.5 * F.get_length());
F.set_aperture(F.get_x1(), F.get_y1(), "rectangular");

clear B0 SC

%%

L = Lattice();
L.append(F);
L.set_odeint_algorithm("rkf45");

dX = 0.1 * sigma_x;
dY = 0.1 * sigma_y;

N_test = 40;
N_sigma = 5;
N_sectors = 7;

footprint_dQx = zeros(N_test, N_sectors);
footprint_dQy = zeros(N_test, N_sectors);

clf
grid on
hold on

delete('footprint_r_uniform.txt');
delete('footprint_t_uniform.txt');

index=1;
for phid=linspace(0, 90, N_sectors)

  R_test = N_sigma * sigma_x * linspace(0, 1, N_test)';
  X_test = R_test * cosd(phid);
  Y_test = R_test * sind(phid);
  N_ones = ones(N_test,1);
  N_zero = zeros(N_test,1);

  P_z = 7e6; %  MeV
  B_test_x_pp = [ X_test+dX N_zero Y_test+dY N_zero N_zero P_z*N_ones RF_Track.protonmass*N_ones N_ones N_zero ];
  B_test_x_pm = [ X_test+dX N_zero Y_test-dY N_zero N_zero P_z*N_ones RF_Track.protonmass*N_ones N_ones N_zero ];
  B_test_x_mp = [ X_test-dX N_zero Y_test+dY N_zero N_zero P_z*N_ones RF_Track.protonmass*N_ones N_ones N_zero ];
  B_test_x_mm = [ X_test-dX N_zero Y_test-dY N_zero N_zero P_z*N_ones RF_Track.protonmass*N_ones N_ones N_zero ];

  B0_pp = Bunch6dT(B_test_x_pp);
  B0_pm = Bunch6dT(B_test_x_pm);
  B0_mp = Bunch6dT(B_test_x_mp);
  B0_mm = Bunch6dT(B_test_x_mm);

  Tr = TrackingOpts();
  Tr.dS_mm = 0.1;
  B1_pp = L.track(B0_pp, Tr);
  B1_pm = L.track(B0_pm, Tr);
  B1_mp = L.track(B0_mp, Tr);
  B1_mm = L.track(B0_mm, Tr);

  T1_pp = B1_pp.get_phase_space("%xp %yp");
  T1_pm = B1_pm.get_phase_space("%xp %yp");
  T1_mp = B1_mp.get_phase_space("%xp %yp");
  T1_mm = B1_mm.get_phase_space("%xp %yp");

  dKx = ((T1_pp(:,1) - T1_mp(:,1)) + (T1_pm(:,1) - T1_mm(:,1))) / (4 * dX);
  dKy = ((T1_pp(:,2) - T1_pm(:,2)) + (T1_mp(:,2) - T1_mm(:,2))) / (4 * dY);

  dmux = -0.5 * beta_star * dKx;
  dmuy = -0.5 * beta_star * dKy;
  dQx = dmux / 2 / pi;
  dQy = dmuy / 2 / pi;

  printf('index = %d:\ndQx(0) = %g\ndQy(0) = %g\n', index, dQx(1), dQy(1));

  footprint_dQx(:,index) = dQx';
  footprint_dQy(:,index) = dQy';

  plot(footprint_dQx(:,index), footprint_dQy(:,index), '-')

  T = [ footprint_dQx(:,index) footprint_dQy(:,index) ];
  
  save -text -append footprint_r_uniform.txt T;

  index++;

end

if true
t = int32(N_test/N_sigma);
for j=1:N_test
  if mod(j,t) == 1
    plot(footprint_dQx(j,:), footprint_dQy(j,:), '-')
    T = [ footprint_dQx(j,:) ; footprint_dQy(j,:) ]';
    save -text -append footprint_t_uniform.txt T;
  end
end
plot(footprint_dQx(end,:), footprint_dQy(end,:), '-')
T = [ footprint_dQx(end,:) ; footprint_dQy(end,:) ]';
save -text -append footprint_t_uniform.txt T;
end
