#! /opt/local/bin/octave -q --persist

setenv("GNUTERM","X11");
addpath('../..');

%% Load RF_Track
RF_Track;

global sigma_z

%% charge in bunch
Q_proton = 1.15e11;

P_z = 7e6; %  MeV
gamma = sqrt(RF_Track.protonmass**2 + P_z**2) / RF_Track.protonmass;
emitt_norm = 3.75; % micron*rad
emitt_geom = emitt_norm/gamma;% micron*rad

beta_star = 0.55; % m
sigma_x  = sqrt(emitt_geom*beta_star); % mm
sigma_y  = sqrt(emitt_geom*beta_star); % mm
sigma_xp = sqrt(emitt_geom/beta_star) / 1e3; % rad
sigma_yp = sqrt(emitt_geom/beta_star) / 1e3; % rad
sigma_z = 75.5; % mm

%% define bunch
P_z = -7e6; %  MeV
N_proton = 20e6;
B_proton = [
	    randn(N_proton,1) * sigma_x, ...	    % x
	    randn(N_proton,1) * sigma_xp * P_z, ... % Px
	    randn(N_proton,1) * sigma_y, ...        % y
	    randn(N_proton,1) * sigma_yp * P_z, ... % Px
	    rand(N_proton,1) * sigma_z, ...         % z
	    ones(N_proton,1) * P_z, ...             % Pz
	    ones(N_proton,1) * RF_Track.protonmass, ...  % mass
	    ones(N_proton,1), ...                        % Q
	    ones(N_proton,1) * Q_proton / N_proton % N
];

N_test  = 2;
N_sigma = 11;
X_test =   N_sigma*sigma_x*linspace(-1, 1, N_test)';
Y_test =   N_sigma*sigma_y*linspace(-1, 1, N_test)';
Z_test = 0*N_sigma*sigma_z*linspace(-1, 1, N_test)';
N_ones = ones(N_test,1);
N_zero = zeros(N_test,1);

B_test_x = [ X_test N_zero N_zero N_zero N_zero N_zero RF_Track.protonmass*N_ones N_ones N_zero ];
B_test_y = [ N_zero N_zero Y_test N_zero N_zero N_zero RF_Track.protonmass*N_ones N_ones N_zero ];
B_test_z = [ N_zero N_zero N_zero N_zero Z_test N_zero RF_Track.protonmass*N_ones N_ones N_zero ];

B0 = Bunch6dT( [ B_test_x ; B_test_y ; B_test_z ; B_proton ] );

clear B_proton

SC = SpaceCharge_PIC_FreeSpace(1024,1024,16);
SC.compute_force(B0);

global F
F = SC.get_field();

clear B0 SC

function FF = get_force(X,Y)
  global F sigma_z
  RF_Track
  Z = sigma_z * 0.5;
  P_z = 7e6; %  MeV
  V_z = P_z / sqrt(RF_Track.protonmass**2 + P_z**2); 
  [E,B] = F.get_field(X, Y, Z, 0.0);
  FF(1) = E(1) - V_z * RF_Track.clight * B(2);
  FF(2) = E(2) + V_z * RF_Track.clight * B(1);
  FF(3) = E(3);
  FF /= 1e6; % eV/m -> MeV/m
end

P_z = 7e6;
dX = 0.1 * sigma_x;
dY = 0.1 * sigma_y;

N_test = 80;
N_sigma = 8;
N_sectors = 2;

footprint_dQx = zeros(N_test, N_sectors);
footprint_dQy = zeros(N_test, N_sectors);

clf
grid on
hold on
daspect([ 1 1 ])

index=1;
for phid=linspace(0, 90, N_sectors)

  R_test = N_sigma * sigma_x * linspace(0, 1, N_test);

  Fx_p = Fx_m = Fy_p = Fy_m = [];
  for R=R_test
    X = R * cosd(phid);
    Y = R * sind(phid);
    Fx_p = [ Fx_p ; get_force(X + dX, Y)(1) ];
    Fx_m = [ Fx_m ; get_force(X - dX, Y)(1) ];
    Fy_p = [ Fy_p ; get_force(X, Y + dY)(2) ];
    Fy_m = [ Fy_m ; get_force(X, Y - dY)(2) ];
  end

  dxp_p = Fx_p * sigma_z / 2 / P_z;
  dxp_m = Fx_m * sigma_z / 2 / P_z;
  dyp_p = Fy_p * sigma_z / 2 / P_z;
  dyp_m = Fy_m * sigma_z / 2 / P_z;
  
  dKx = (dxp_p - dxp_m) / 2 / dX;
  dKy = (dyp_p - dyp_m) / 2 / dY;

  dmux = -0.5 * beta_star * dKx;
  dmuy = -0.5 * beta_star * dKy;  
  dQx = dmux / 2 / pi;
  dQy = dmuy / 2 / pi;

  printf('index = %d:\ndQx(0) = %g\ndQy(0) = %g\n', index, dQx(1), dQy(1));

  footprint_dQx(:,index) = dQx';
  footprint_dQy(:,index) = dQy';

  plot(footprint_dQx(:,index), footprint_dQy(:,index), '-')

  index++;

end

if true
  t = int32(N_test/N_sigma);
  text(footprint_dQx(1,1), footprint_dQy(1,1), '    (0,0)');
  for j=1:N_test
    if mod(j,t) == 0
      plot(footprint_dQx(j,:), footprint_dQy(j,:), 'o')
      text(footprint_dQx(j,1), footprint_dQy(j,1), sprintf('    (%d,0)', j/t));
      text(footprint_dQx(j,end), footprint_dQy(j,end), sprintf('  (0,%d)', j/t));
    end
  end
  plot(footprint_dQx(end,:), footprint_dQy(end,:), 'o')
  text(footprint_dQx(end,1), footprint_dQy(end,1), sprintf('    (%d,0)', j/t));
  text(footprint_dQx(end,end), footprint_dQy(end,end), sprintf('    (0,%d)', j/t));
end

xlabel('dQx')
ylabel('dQy')

print -depslatexstandalone -F:8 beambeam_footprint.eps
system('pdflatex beambeam_footprint.tex');

%% save tables

N_sigma = 10;

T1 = T2 = [];
X_test = N_sigma * sigma_x * linspace(-1, 1, N_test);
for X=X_test
  Y = 0.0;
  F_ = get_force(X, Y);
  T1 = [ T1 ; X X/sigma_x F_(1) F_(2) ];

  Fx_p = get_force(X + dX, 0.0)(1);
  Fx_m = get_force(X - dX, 0.0)(1);
  Fy_p = get_force(X, Y + dY)(2);
  Fy_m = get_force(X, Y - dY)(2);

  dxp_p = Fx_p * sigma_z / 2 / P_z;
  dxp_m = Fx_m * sigma_z / 2 / P_z;
  dyp_p = Fy_p * sigma_z / 2 / P_z;
  dyp_m = Fy_m * sigma_z / 2 / P_z;
  
  dKx = (dxp_p - dxp_m) / 2 / dX;
  dKy = (dyp_p - dyp_m) / 2 / dY;

  dmux = -0.5 * beta_star * dKx;
  dmuy = -0.5 * beta_star * dKy;  
  dQx = dmux / 2 / pi;
  dQy = dmuy / 2 / pi;

  T2 = [ T2 ; X X/sigma_x dQx dQy ];
  
end

save -text footprint_force.txt T1;
save -text footprint_table.txt T2;

system("gnuplot -persist plot_force.p");
