%% load RF-Track

addpath('../../')

RF_Track;

%% load the field and create the lattice
quality = 1;

P0c = 100; % MeV/c
Q = 10;

% FODO cell with Lq = 24 cm, Ld = 76 cm (Lcell = 200 cm), mu = 90 degress
%
Lcell = 2; % m
Lquad = 0.24; % m
mu = 90; % deg

k1L = sind(mu/2) / (Lcell/4); % 1/m
strength = k1L * P0c / Q; % MeV/m
gradient = strength / Lquad * 1e6 / RF_Track.clight; % T/m = V/m/m/c

% double Bx = gradient / C_LIGHT T/m = V/m/m/c
% double By = gradient / C_LIGHT T/m = V/m/m/c

Nx = 91;
Ny = 91;
hx = 0.40 / (Nx-1); % m
hy = 0.40 / (Ny-1); % m
hz = 0.010; % m (1 cm)

Ex = Ey = Ez = zeros(Nx, Ny, 13); % number of cells (41 cm x 41 cm x 200 cm)
Bx = By = Bz = zeros(Nx, Ny, 13); % number of cells (41 cm x 41 cm x 200 cm)

for k=1:13 % hz
    for i=1:Nx % hx
        x = (i - (bitshift(Nx,-1)+1)) * hy; % m
        for j=1:Ny % hy
            y = (j - (bitshift(Ny,-1)+1)) * hy; % m
            Bx(i,j,k) = gradient * y; % T
            By(i,j,k) = gradient * x; % T
        end
    end
end

Qf = RF_Field(Ex, Ey, Ez, ...
              Bx, By, Bz, ...
              -0.20, ...
              -0.20, ...
              hx, ...
              hy, ...
              hz, ...
              Lquad/2, 0, 0);
Qf.set_odeint_algorithm("rk2");
Qf.set_nsteps(100);
%Qf = Quadrupole(Lquad/2, strength/2);

Qd = RF_Field( Ex,  Ey, Ez, ...
              -Bx, -By, Bz, ...
              -0.20, ...
              -0.20, ...
              hx, ...
              hy, ...
              hz, ...
              Lquad/2, 0, 0);
Qd.set_odeint_algorithm("rk2");
Qd.set_nsteps(100);
Qd = Quadrupole(Lquad/2, -strength/2);

D = Drift(Lcell/2 - Lquad);

Bunch.sigmaz = 1; % mm
Bunch.P_spread = 0;
Bunch.mass = RF_Track.protonmass;

N_particles = 1000;

BunchT = Bunch6d_twiss();
BunchT.Pc = P0c; % MeV
BunchT.emitt_x = 10; % mm.mrad
BunchT.emitt_y = 10; % mm.mrad
BunchT.emitt_z = 0.0; % mm.mrad
BunchT.alpha_x = 0;
BunchT.alpha_y = 0;
BunchT.alpha_z = 0;
BunchT.beta_x = Lcell * (1 + sind(mu/2)) / sind(mu);
BunchT.beta_y = Lcell * (1 - sind(mu/2)) / sind(mu);
BunchT.beta_z = 1; % m
B0_ = Bunch6d(Bunch.mass, 0.0, Q, BunchT, N_particles, -1);
%M = B0_.get_phase_space();
%M(:,6) = P0c * hypot(1e3, M(:,2), M(:,4)) / 1e3;
%B0_.set_phase_space(M);

L = Lattice();
L.append(Qf);
L.append(D);
L.append(Qd);
L.append(Qd);
L.append(D);
L.append(Qf);

T = [];
B0 = B0_;
for i=1:50000
    B0 = L.track(B0);
    E = P0c / Bunch.mass * det(cov(B0.get_phase_space("%x %xp %y %yp")))**0.25;
    T = [ T ; L.get_transport_table("%S %emitt_x %emitt_y")(1,:) E ];
end

figure
plot(T(:,1), T(:,2), 'b-', T(:,1), T(:,3), 'r-', T(:,1), T(:,4), 'g-');

figure
hold on
scatter(B0_.get_phase_space("%x"), B0_.get_phase_space("%Py"));
scatter(B0.get_phase_space("%x"), B0.get_phase_space("%Py"), 'r')