#! /opt/local/bin/octave -q --persist

setenv("GNUTERM","X11");
addpath('../../');

%% Load RF_Track

RF_Track;

%% initialise the bunch parameters
global Bunch
Bunch.mass = 938.272046; % MeV/c/c
Bunch.population = 1e9; % nb. of particles per bunch
Bunch.charge = -1; % units of e+
Bunch.ekinetic = 0.1; % MeV
Bunch.etotal = Bunch.ekinetic + Bunch.mass; % MeV
Bunch.Pc = sqrt(Bunch.etotal**2 - Bunch.mass**2); % MeV
Bunch.beta = Bunch.Pc / Bunch.etotal;
Bunch.gamma = Bunch.etotal / Bunch.mass;

%% load the beam
T = load('inrays_nominal.csv');
T (:, [ 1 2 3 4 ]) *= 1e3; % mm / mrad
B0 = Bunch6d(Bunch.mass, ...
             Bunch.population, ...
             Bunch.charge, T);

%% load the lattice
L = Lattice();
source('ll.m');

%% perform tracking
% nominal beam
tic
B1 = L.track(B0);
toc
T = B1.get_phase_space("%x %xp %y %yp %z %pt") / 1e3;
save -text out_nominal.dat T

% time-tracking
L.set_odeint_algorithm("rkf45");
B0t = Bunch6dT(B0);
tic
B1t = L.track(B0t, 10);
toc
T = B1t.get_phase_space("%x %xp %y %yp %z %pt") / 1e3;
save -text out_nominal_T.dat T
