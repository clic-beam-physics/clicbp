#! /opt/local/bin/octave -q --persist

setenv("GNUTERM","X11");

%%%%%%%%%%%%%%%% field maps

ExEyEz_file='Efield76.fld';
HxHyHz_file='Hfield76.fld';

%%%%%%%%%%%%%%%% global variables

global Bunch Structure

%%%%%%%%%%%%%%%% bunch parameters

Bunch.mass0 = 0.938272046; % GeV/c/c
Bunch.ekinetic0 = 70e-3; % GeV
Bunch.etotal0 = Bunch.ekinetic0 + Bunch.mass0; % GeV
Bunch.pc0 = sqrt(Bunch.etotal0**2 - Bunch.mass0**2); % GeV
Bunch.beta0 = Bunch.pc0 / Bunch.etotal0

%%%%%%%%%%%%%%%% structure parameters

Structure.frequency = 2.9985e9; % Hz
Structure.direction = 1.0; % backward travelling structure
Structure.P_map = 1; % W design input power
Structure.P_actual = 1; % W actual input power
Structure.beta = Bunch.beta0;
%Structure.beam_loading = @(z) 1.0;
beam_loading = @(z) 1.0;

%%%%%%%%%%%%%%%% some constants

mu0 = 1.2566371e-06; % 4 pi 1e-7 H/m
clight = 299792458; % m/s

%%%%%%%%%%%%%%%% load files and eliminate Nan's

TE=load(ExEyEz_file);
TH=load(HxHyHz_file);
TE(isnan(TE))=0.0;
TH(isnan(TH))=0.0;

%%%%%%%%%%%%%%%%

S=[ 1 1 ];
Structure.xa=unique(TE(:,1));
Structure.ya=unique(TE(:,2));
Structure.za=unique(TE(:,3));
Nx=length(Structure.xa);
Ny=length(Structure.ya);
Nz=length(Structure.za);
Structure.Nx=Nx;
Structure.Ny=Ny;
Structure.Nz=Nz;
Structure.Ex = zeros(Nx,Ny,Nz);
Structure.Ey = zeros(Nx,Ny,Nz);
Structure.Ez = zeros(Nx,Ny,Nz);
Structure.Bx = zeros(Nx,Ny,Nz);
Structure.By = zeros(Nx,Ny,Nz);
Structure.Bz = zeros(Nx,Ny,Nz);

%%%%%%%%%%%%%%%% prepare the output field maps

for n=1:rows(TE)
    i = find(Structure.xa==TE(n,1));
    j = find(Structure.ya==TE(n,2));
    k = find(Structure.za==TE(n,3));
    _beam_loading = beam_loading(Structure.za(k));
    Structure.Ex(i,j,k) = complex(TE(n,4),TE(n,5)) * _beam_loading;
    Structure.Ey(i,j,k) = complex(TE(n,6),TE(n,7)) * _beam_loading;
    Structure.Ez(i,j,k) = complex(TE(n,8),TE(n,9)) * _beam_loading;
    Structure.Bx(i,j,k) = mu0 * complex(TH(n,4),TH(n,5)) * _beam_loading;
    Structure.By(i,j,k) = mu0 * complex(TH(n,6),TH(n,7)) * _beam_loading;
    Structure.Bz(i,j,k) = mu0 * complex(TH(n,8),TH(n,9)) * _beam_loading;
end

EN = 20;

X_axis = Structure.xa;
Y_axis = Structure.ya;
Z_axis = Structure.za;

X=zeros(length(X_axis)*length(Y_axis)*length(Z_axis),1);
Y=zeros(length(X_axis)*length(Y_axis)*length(Z_axis),1);
Z=zeros(length(X_axis)*length(Y_axis)*length(Z_axis),1);
L=zeros(length(X_axis)*length(Y_axis)*length(Z_axis),1);
M=zeros(length(X_axis)*length(Y_axis)*length(Z_axis),1);
N=zeros(length(X_axis)*length(Y_axis)*length(Z_axis),1);
S=zeros(length(X_axis)*length(Y_axis)*length(Z_axis),1);
T=zeros(length(X_axis)*length(Y_axis)*length(Z_axis),1);
U=zeros(length(X_axis)*length(Y_axis)*length(Z_axis),1);

n=1;
for i=1:length(X_axis)
    for j=1:length(Y_axis)
        for k=15 % length(Z_axis)
            E(1) = Structure.Ex(i,j,k);
            E(2) = Structure.Ey(i,j,k);
            E(3) = Structure.Ez(i,j,k);
            B(1) = Structure.Bx(i,j,k);
            B(2) = Structure.By(i,j,k);
            B(3) = Structure.Bz(i,j,k);
            X(n) = X_axis(i);
            Y(n) = Y_axis(j);
            Z(n) = Z_axis(k);
            L(n) = real(E(1));
            M(n) = real(E(2));
            N(n) = real(E(3));
            S(n) = real(B(1));
            T(n) = real(B(2));
            U(n) = real(B(3));
            n++;
        end
    end
end

figure(1)
quiver3(X,Y,Z, L,M,N, 10)
title('E electric field');

figure(2)
quiver3(X,Y,Z, S,T,U, 10)
title('B magnetic field');

