#! /opt/local/bin/octave -q --persist

setenv("GNUTERM","X11");
addpath('../../');

%% Load RF_Track

RF_Track;

% init constants
clight = RF_Track.clight; % m/s
frequency = 2.9985e9;

% initialise the bunch
Bunch.mass = 938.272046; % MeV/c/c
Bunch.population = 2e9; % nb. of particles per bunch
Bunch.charge = +1; % units of e+
Bunch.ekinetic = 70; % MeV
Bunch.etotal = Bunch.ekinetic + Bunch.mass; % MeV
Bunch.Pc = sqrt(Bunch.etotal**2 - Bunch.mass**2); % MeV
Bunch.beta = Bunch.Pc / Bunch.etotal;
Bunch.gamma = Bunch.etotal / Bunch.mass;
Bunch.espread = 0.01; % percent
Bunch.sigmaz = 0.1; % mm

% create the bunch
A = load('OutTULIP70.txt');
B = zeros(rows(A), 6);
B(:,1) =  A(:,1);   % x  [mm]
B(:,2) =  A(:,2);   % x' [mrad]
B(:,3) =  A(:,3);   % y  [mm]
B(:,4) =  A(:,4);   % y' [mrad]
Etotal =  A(:,9) .+ Bunch.mass; % MeV
Pc = sqrt(Etotal.**2 .- Bunch.mass**2); % Pc [MeV]
%Beta = Pc ./ Etotal;
%B(:,5) = -A(:,7) .* Beta * 1000 * clight / frequency / 360.0; % z [mm]
B(:,5) = A(:,8) * 1000 * clight; % t [mm/c]
B(:,6) = Pc; % [MeV]

%% conversione da %dt a gradi
% B1.get_phase_space("%dt") * 1e-3 / clight * frequency * 360.0

B0 = Bunch6d(Bunch.mass, ...
             Bunch.population, ...
             Bunch.charge, ...
             B);

% initialise the structure(s)
load '../Fields/Structure_76.dat.gz'; Structure76 = Structure;
load '../Fields/Structure_84.dat.gz'; Structure84 = Structure;
load '../Fields/Structure_93.dat.gz'; Structure93 = Structure;

global F1 F2 F3
F1 = RF_Field(complex(Structure76.Ex), ...
	      complex(Structure76.Ey), ...
	      complex(Structure76.Ez), ...
              complex(Structure76.Bx), ...
	      complex(Structure76.By), ...
	      complex(Structure76.Bz), ...
              Structure76.xa(1), Structure76.ya(1), ...
              Structure76.xa(2) - Structure76.xa(1), ...
              Structure76.ya(2) - Structure76.ya(1), ...
              Structure76.za(2) - Structure76.za(1), ...
              Structure76.za(end), ...
              Structure76.frequency, ...
              Structure76.direction, ...
              Structure76.P_map, ...
              Structure76.P_actual);

F2 = RF_Field(complex(Structure84.Ex), ...
	      complex(Structure84.Ey), ...
	      complex(Structure84.Ez), ...
	      complex(Structure84.Bx), ...
	      complex(Structure84.By), ...
	      complex(Structure84.Bz), ...
              Structure84.xa(1), Structure84.ya(1), ...
              Structure84.xa(2) - Structure84.xa(1), ...
              Structure84.ya(2) - Structure84.ya(1), ...
              Structure84.za(2) - Structure84.za(1), ...
              Structure84.za(end), ...
              Structure84.frequency, ...
              Structure84.direction, ...
              Structure84.P_map, ...
              Structure84.P_actual);

F3 = RF_Field(complex(Structure93.Ex), ...
	      complex(Structure93.Ey), ...
	      complex(Structure93.Ez), ...
              complex(Structure93.Bx), ...
	      complex(Structure93.By), ...
	      complex(Structure93.Bz), ...
              Structure93.xa(1), Structure93.ya(1), ...
              Structure93.xa(2) - Structure93.xa(1), ...
              Structure93.ya(2) - Structure93.ya(1), ...
              Structure93.za(2) - Structure93.za(1), ...
              Structure93.za(end), ...
              Structure93.frequency, ...
              Structure93.direction, ...
              Structure93.P_map, ...
              Structure93.P_actual);

F1.set_aperture(3.5e-3);
F2.set_aperture(3.5e-3);
F3.set_aperture(3.5e-3);

F1.set_t0(118.50);
F2.set_t0(1116.7);
F3.set_t0(2091.5);

% setup quadrupoles

L_quad = 0.033;

SA = -1548.2781;  % MeV/m = c * 156.5 T/m * 33 mm
SB = 1697.6647;   % MeV/m = c * -171.6  T/m * 33 mm
SC = -1634.3486;  % MeV/m = c * 165.2 T/m * 33 mm
SD = 1655.1242;   % MeV/m = c * -167.3 T/m * 33 mm

QA = Quadrupole(L_quad/2, SA/2);
QB = Quadrupole(L_quad/2, SB/2);
QC = Quadrupole(L_quad/2, SC/2);
QD = Quadrupole(L_quad/2, SD/2);

QA.set_nsteps(5);
QB.set_nsteps(5);
QC.set_nsteps(5);
QD.set_nsteps(5);

QA.set_aperture(3.5e-3);
QB.set_aperture(3.5e-3);
QC.set_aperture(3.5e-3);
QD.set_aperture(3.5e-3);

% setup drifts

DA_2 = Drift(28.50*1e-3);
DB_1 = Drift(31.13*1e-3);
DB_2 = Drift(31.13*1e-3);
DC_1 = Drift(28.50*1e-3); 
DC_2 = Drift(28.50*1e-3); 
DD_1 = Drift(28.50*1e-3);

DA_2.set_aperture(3.5e-3);
DB_1.set_aperture(3.5e-3);
DB_2.set_aperture(3.5e-3);
DC_1.set_aperture(3.5e-3);
DC_2.set_aperture(3.5e-3);
DD_1.set_aperture(3.5e-3);

% setup lattice
global L1 L2 L3
L1 = Lattice();
L2 = Lattice();
L3 = Lattice();
%
L1.append(QA);
L1.append(DA_2);
L1.append(F1);
L1.append(DB_1);
L1.append(QB);
%
L2.append(QB);
L2.append(DB_2);
L2.append(F2);
L2.append(DC_1);
L2.append(QC);
%
L3.append(QC);
L3.append(DC_2);
L3.append(F3);
L3.append(DD_1);
L3.append(QD);
%

% tracking

tic
B1 = L1.track(B0); 
B2 = L2.track(B1); 
B3 = L3.track(B2);
toc

% save data
A0 = B0.get_phase_space("%X %xp %Y %yp %deg@2998.5 %K");
A1 = B1.get_phase_space("%X %xp %Y %yp %deg@2998.5 %K");

save -text "OutTULIP69.dat" A0
save -text "OutTULIP76.dat" A1

%B0.save_as_dst_file("OutTULIP69.dst", 2998.5);
%B1.save_as_dst_file("OutTULIP76.dst", 2998.5);

