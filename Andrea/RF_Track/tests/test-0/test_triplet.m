#! /opt/local/bin/octave -q --persist

setenv("GNUTERM","X11");
addpath('../..');

clear all

%% Load RF_Track
RF_Track;

% RF_Track.number_of_threads = 16;

%%%%%%%%%%%%%%

Ion.mass = 207.2 * RF_Track.protonmass; % Lead ion, A = 207.2
Ion.Np = 5e9; % total number of ions in a bunch
Ion.K = 4.2 * 207.2; % MeV, total kinetic energy
Ion.E = Ion.mass + Ion.K; % MeV, total energy
Ion.Pc = sqrt(Ion.E**2 - Ion.mass**2); % MeV, total momentum * c
Ion.V = Ion.Pc / Ion.E; % ion velocity in units of c
Ion.P_spread = 0.4 % momentum spread, percent
Ion.gamma = 1 / sqrt(1 - Ion.V**2); % relativistic gamma
Ion.sigmaz = 1; % mm
Ion.Q = 54; % ion charge 54+

% define the beam paramters
BunchT = Bunch6d_twiss();
BunchT.Pc = Ion.Pc; % MeV/c
BunchT.emitt_x = 40; % normalized horizontal emittance mm.mrad == micron
BunchT.emitt_y = 40; % normalized vertical emittance mm.mrad == micron
BunchT.emitt_z = Ion.V * Ion.gamma * Ion.sigmaz * (Ion.P_spread / 100) * 1e3; % normalized longitudinal emittance mm.permille 
BunchT.alpha_x = 0;
BunchT.alpha_y = 0;
BunchT.alpha_z = 0;
BunchT.beta_x = 3.707366501; % m
BunchT.beta_y = 14.41031353; % m
BunchT.beta_z = Ion.sigmaz ** 2 / BunchT.emitt_z * Ion.V * Ion.gamma; % m

N_particles = 10000; % number of macroparticles in a bunch

% create the beam

B{1} = Bunch6d(Ion.mass, Ion.Np, Ion.Q, BunchT, N_particles);

% define the elements
Qd = Quadrupole(0.2);
Qd.set_K1L(Ion.Pc / Ion.Q, -0.3566981773);

Qf = Quadrupole(0.4);
Qf.set_K1L(Ion.Pc / Ion.Q, 0.402181838);

d0 = Drift(0.6);
d1 = Drift(1.3);

E = Drift(3);

% build the lattice
L = Lattice();
L.append(Qd);
L.append(d0);
L.append(Qf);
L.append(d1);
L.append(E);
L.append(d1);
L.append(Qf);
L.append(d0);
L.append(Qd);

% 10 turns
for i=1:10
  B{i+1} = L.track(B{i});
end

% read the phase space
M0 = B{1}.get_phase_space();
M1 = B{end}.get_phase_space();

figure(1);
clf
hold on
scatter3(B{1}.get_phase_space("%x"), B{1}.get_phase_space("%y"), B{1}.get_phase_space("%Pz"))
scatter3(B{end}.get_phase_space("%x"), B{end}.get_phase_space("%y"), B{end}.get_phase_space("%Pz"), 'r')
xlabel("x [mm]");
ylabel("y [mm]");
zlabel("Pz [MeV/c]");

figure(2)
clf;
hold on
scatter(B{1}.get_phase_space("%x"), B{1}.get_phase_space("%xp"))
scatter(B{end}.get_phase_space("%x"), B{end}.get_phase_space("%xp"), 'r')
xlabel("x [mm]");
ylabel("x' [mm]");

figure(3)
clf;
hold on
scatter(B{1}.get_phase_space("%y"), B{1}.get_phase_space("%yp"))
scatter(B{end}.get_phase_space("%y"), B{end}.get_phase_space("%yp"), 'r')
xlabel("y [mm]");
ylabel("y' [mm]");

