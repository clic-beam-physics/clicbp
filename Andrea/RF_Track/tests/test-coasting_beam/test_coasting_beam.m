#! /opt/local/bin/octave -q --persist

setenv("GNUTERM","X11");
addpath('../../');

%% Load RF_Track
RF_Track;

%% define bunch
Qb = 1e12;
Ne = 10000;
Lb = 6; % bunch length [mm]

randn("seed", 1234);
rand ("seed", 5678);

Pz = 0; % try with 1

B_pos = [ 0.1 * randn(Ne,1), ... % x
	  0.0 * randn(Ne,1), ... % Px
	  0.1 * randn(Ne,1), ... % y
	  0.0 * randn(Ne,1), ... % Py
	  (rand(Ne,1)-0.5)*Lb, ... % S
	  Pz * ones(Ne,1),  ... % Pz
	  RF_Track.electronmass * ones(Ne,1), ... % mass
	  -1 * ones(Ne,1), ...   % Q
	  Qb / Ne * ones(Ne,1) ]; % N

% example 1
SC = SpaceCharge_PIC_FreeSpace(32,32,32);

% example 2
% SC = SpaceCharge_PIC_LongCylinder(32,32,32);
% SC.set_aperture(10e-3); % 10 mm

% example 3
% SC = SpaceCharge_PIC_HorizontalPlates(32,32,32);
% SC.set_half_gap(10e-3); % 10 mm

B0 = Bunch6dT(B_pos);
B0.set_coasting(Lb);

figure(1)
xlabel('S [mm]');
ylabel('X [mm]');
title('Coasting beam');  

B0_ = Bunch6dT(B_pos);
figure(2)
xlabel('S [mm]');
ylabel('X [mm]');
title('Isolated bunch');

disp('Press any key to continue...');
pause

time_per_frame = 0.0;
dt = 0.15;
N_frames = 30;
for i=1:N_frames
  T0 = B0.get_phase_space("%X %Y %S %Q");
  S = mean(T0(:,3));
  figure(1);
  scatter(T0(:,3), T0(:,2));
  axis([ S-20 S+20 -5 5 ]);
  xlabel('S [mm]');
  ylabel('Y [mm]');
  title('Coasting beam');
  drawnow
  tic
  F = SC.compute_force(B0);
  B0.apply_force(F, dt);
  time_per_frame += toc;

  T0_ = B0_.get_phase_space("%X %Y %S %Q");
  figure(2);
  S_ = mean(T0_(:,3));
  scatter(T0_(:,3), T0_(:,2), 'r');
  axis([ S_-20 S_+20 -5 5 ]);
  xlabel('S [mm]');
  ylabel('Y [mm]');
  title('Isolated bunch');
  drawnow
  F = SC.compute_force(B0_);
  B0_.apply_force(F, dt);
    
end
time_per_frame /= N_frames

figure(3)
scatter3(T0(:,3), T0(:,1), T0(:,2));
axis([ S-20 S+20 -5 5 -5 5 ]);
xlabel('S [mm]');
ylabel('X [mm]');
zlabel('Y [mm]');

figure(4)
scatter3(T0_(:,3), T0_(:,1), T0_(:,2), 'r');
axis([ S_-20 S_+20 -5 5 -5 5 ]);
xlabel('S [mm]');
ylabel('X [mm]');
zlabel('Y [mm]');

range_S_isol = range(T0_(:,3))
std_X_isol = std(T0_(:,1))
std_Y_isol = std(T0_(:,2))

range_S_coas = range(T0(:,3))
std_X_coas = std(T0(:,1))
std_Y_coas = std(T0(:,2))

