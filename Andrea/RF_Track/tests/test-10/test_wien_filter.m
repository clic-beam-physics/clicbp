#! /opt/local/bin/octave -q --persist

setenv("GNUTERM","X11");
addpath('../../');

%% Load RF_Track

RF_Track;

Proton.Np = 5e9; % total number of protons
Proton.Q = +1; % proton charge
Proton.P = 308.6; % MeV/c
Proton.E = sqrt(RF_Track.protonmass**2 + Proton.P**2);
Proton.V = Proton.P / Proton.E;
Proton.P_spread = 10 % 10 percent

N_particles = 10000;

sigma_x = 0; % mm
sigma_xp = 0; % mrad

%%
x = (1 - 2*rand(N_particles,1)) * sigma_x;
y = (1 - 2*rand(N_particles,1)) * sigma_x;
t = zeros(N_particles,1);

Pz = Proton.P * (100 + Proton.P_spread * randn(N_particles,1)) / 100.0; % MeV/c
xp = randn(N_particles,1) * sigma_xp; % mrad
yp = randn(N_particles,1) * sigma_xp; % mrad

B0 = Bunch6d(RF_Track.protonmass, Proton.Np, Proton.Q, [ x xp y yp t Pz ]);
B0t = Bunch6dT(B0);

Bf = 0.01; % T
Ef = Bf * RF_Track.clight * Proton.V; % V/m

D = Drift(1.0);
D.set_static_Efield(Ef, 0, 0);
D.set_static_Bfield(0, Bf, 0);
D.set_odeint_algorithm("analytic");

L = Lattice();
L.append(D);

T0 = B0t.get_phase_space("%X %Y %S %Vx %Vy %Vz");

L.set_odeint_algorithm("analytic");
tic; B1a = L.track(B0t, 10); toc
T1a = B1a.get_phase_space("%X %Y %S %Vx %Vy %Vz");

L.set_odeint_algorithm("rk4imp");
tic; B1r = L.track(B0t, 10); toc
T1r = B1r.get_phase_space("%X %Y %S %Vx %Vy %Vz");

figure(1); clf; hold on;
plot(T1a(:,1), T1a(:,4), 'r.');
plot(T1r(:,1), T1r(:,4), 'b.');
xlabel('x [mm]');
ylabel('Vx [c]');

figure(2); clf; hold on;
plot(T1a(:,2), T1a(:,5), 'r.');
plot(T1r(:,2), T1r(:,5), 'b.');
xlabel('y [mm]');
ylabel('Vy [c]');

figure(3); clf; hold on;
scatter3(T0(:,1),   T0(:,3)/1e3, sqrt(T0(:,4).**2 .+ T0(:,5).**2 + T0(:,6).**2), 'k');
scatter3(T1a(:,1), T1a(:,3)/1e3, sqrt(T1a(:,4).**2 .+ T1a(:,5).**2 + T1a(:,6).**2), 'r');
scatter3(T1r(:,1), T1r(:,3)/1e3, sqrt(T1r(:,4).**2 .+ T1r(:,5).**2 + T1r(:,6).**2), 'b');
xlabel('x [mm]');
xlabel('S [m]');
zlabel('|V| [c]');

save -text output_beam_analuytic.txt T1a
save -text output_beam_rk4imp.txt T1r
