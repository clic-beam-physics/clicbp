Q_ion = Zp;
Q_ele = Ze;

dL = 2; % mm

zi_min = 0;
zi_max = dL;

ze = dL/2;
re = b; % mm, impact parameter

V_ion = B0.get_phase_space("%Vz %Vy %Vx")(1,:);
V_ele = B0.get_phase_space("%Vz %Vy %Vx")(2,:);

V_ion_l = V_ion(3);
t_min = zi_min / V_ion_l;
t_max = zi_max / V_ion_l;

dt = t_max - t_min;

gamma_ele = 1/sqrt(1-dot(V_ele,V_ele));
gamma_ion = 1/sqrt(1-dot(V_ion,V_ion));

E_ele = Me * gamma_ele;
E_ion = Mp * gamma_ion;

P_ele = E_ele * V_ele;
P_ion = E_ion * V_ion;

dr0 = [0,re,ze-zi_min]; %%%% 
dV = V_ele - V_ion;

A = dot(dV,dV);
B = 2*dot(dr0,dV);
C = dot(dr0,dr0);
D = B*B-4*A*C;

sqr = @(x) x.^2;
func_I1 = @(t) -(2*(B+2*t*A))/(sqrt(C+t*(B+t*A))*D);
func_It = @(t) 2*(2*C+t*B)/(sqrt(C+t*(B+t*A))*D);

I1 = func_I1(t_max) - func_I1(t_min); % 1/mm^2
It = func_It(t_max) - func_It(t_min); % 1/mm

K = (Q_ion * Q_ele) / 694461572063.7623; % (e*e/4/pi/epsilon0/mm) MeV

dP = K * (dr0*I1 + It*dV); % MeV * 1/mm * mm/c == MeV/c

dPx = K * ((V_ele(3)*V_ion(1)*dV(3)+V_ele(2)*V_ion(1)*dV(2)-V_ele(3)*V_ion(3)*dV(1)-V_ele(2)*V_ion(2)*dV(1)+dV(1)) * It +
(-dr0(1)*V_ele(3)*V_ion(3)-dr0(1)*V_ele(2)*V_ion(2)+dr0(3)*V_ele(3)*V_ion(1)+dr0(2)*V_ele(2)*V_ion(1)+dr0(1)) * I1); % MeV * 1/mm * mm/c == MeV/c

dPy = K * ((V_ele(3)*V_ion(2)*dV(3)-V_ele(3)*V_ion(3)*dV(2)-V_ele(1)*V_ion(1)*dV(2)+dV(2)+V_ele(1)*V_ion(2)*dV(1)) * It +
(-dr0(2)*V_ele(3)*V_ion(3)+dr0(3)*V_ele(3)*V_ion(2)+dr0(1)*V_ele(1)*V_ion(2)-dr0(2)*V_ele(1)*V_ion(1)+dr0(2)) * I1); % MeV * 1/mm * mm/c == MeV/c

dPz = K * ((-V_ele(2)*V_ion(2)*dV(3)-V_ele(1)*V_ion(1)*dV(3)+dV(3)+V_ele(2)*V_ion(3)*dV(2)+V_ele(1)*V_ion(3)*dV(1)) * It +
(dr0(2)*V_ele(2)*V_ion(3)+dr0(1)*V_ele(1)*V_ion(3)-dr0(3)*V_ele(2)*V_ion(2)-dr0(3)*V_ele(1)*V_ion(1)+dr0(3)) * I1); % MeV * 1/mm * mm/c == MeV/c

dP = [ dPx, dPy, dPz ]

K_ele = dot(dP,dP) / 2 / Me

E_ele_i = hypot(Me, norm(P_ele));
E_ele_f = hypot(Me, norm(P_ele+dP));

U_i = K / norm(dr0);
U_f = K / norm(dr0 + dV * dt);

dE_ion = E_ele_i - E_ele_f + U_i - U_f

%{
P_total = P_ion+P_ele

T = (dot(dP,dP) - dot(dP,P_total))/(E_ele+E_ion)
q = [ T dP ]

p_ele = [ Me*gamma_ele P_ele ];
p_ion = [ Mp*gamma_ion P_ion ];

p_ele_f = p_ele + q;
p_ion_f = p_ion - q;

P    = [ (E_ele + E_ion) P_total ];
P_f  = p_ele_f + p_ion_f;

dot_4d = @(P1,P2) P1(1)*P2(1)-dot(P1(2:4),P2(2:4));
norm_4d = @(P) dot_4d(P,P);

E_ele_f = p_ele_f(1);
P_ele_f = p_ele_f(2:4);
E_ion_f = p_ion_f(1);
P_ion_f = p_ion_f(2:4);
P_total_f = P_ion_f+P_ele_f

X_prime = P_total / (E_ion + E_ele)
X_prime_f = P_total_f / (E_ion_f + E_ele_f)

gamma_prime = 1 / sqrt(1 - dot(X_prime,X_prime));

L = lorentz_boost_matrix(X_prime);

P_ele_cm = L * [ E_ele P_ele ]';
P_ion_cm = L * [ E_ion P_ion ]';

P_cm = P_ele_cm + P_ion_cm

norm_4d(P_cm)
norm_4d(P)
norm_4d(P_f)

norm_4d(q)

%}


%{
disp("################")

V_ele -= X_prime;
V_ion -= X_prime;

gamma_ele = 1/sqrt(1-dot(V_ele,V_ele));
gamma_ion = 1/sqrt(1-dot(V_ion,V_ion));

E_ele = Me * gamma_ele;
E_ion = Mp * gamma_ion;

P_ele = E_ele * V_ele;
P_ion = E_ion * V_ion;

V_ion_l = V_ion(3);
t_min = zi_min / V_ion_l;
t_max = zi_max / V_ion_l;

dr0 = [0,re,ze-zi_min]; 
dV = V_ele - V_ion;

A = dot(dV,dV);
B = 2*dot(dr0,dV);
C = dot(dr0,dr0);
D = B*B-4*A*C;

sqr = @(x) x.^2;
func_I1 = @(t) -(2*(B+2*t*A))/(sqrt(C+t*(B+t*A))*D);
func_It = @(t) 2*(2*C+t*B)/(sqrt(C+t*(B+t*A))*D);

I1 = func_I1(t_max) - func_I1(t_min); % 1/mm^2
It = func_It(t_max) - func_It(t_min); % 1/mm

K = (Q_ion * Q_ele) / 694461572063.7623; % (e*e/4/pi/epsilon0/mm) MeV

dPx = K * ((V_ele(3)*V_ion(1)*dV(3)+V_ele(2)*V_ion(1)*dV(2)-V_ele(3)*V_ion(3)*dV(1)-V_ele(2)*V_ion(2)*dV(1)+dV(1)) * It +
(-dr0(1)*V_ele(3)*V_ion(3)-dr0(1)*V_ele(2)*V_ion(2)+dr0(3)*V_ele(3)*V_ion(1)+dr0(2)*V_ele(2)*V_ion(1)+dr0(1)) * I1); % MeV * 1/mm * mm/c == MeV/c

dPy = K * ((V_ele(3)*V_ion(2)*dV(3)-V_ele(3)*V_ion(3)*dV(2)-V_ele(1)*V_ion(1)*dV(2)+dV(2)+V_ele(1)*V_ion(2)*dV(1)) * It +
(-dr0(2)*V_ele(3)*V_ion(3)+dr0(3)*V_ele(3)*V_ion(2)+dr0(1)*V_ele(1)*V_ion(2)-dr0(2)*V_ele(1)*V_ion(1)+dr0(2)) * I1); % MeV * 1/mm * mm/c == MeV/c

dPz = K * ((-V_ele(2)*V_ion(2)*dV(3)-V_ele(1)*V_ion(1)*dV(3)+dV(3)+V_ele(2)*V_ion(3)*dV(2)+V_ele(1)*V_ion(3)*dV(1)) * It +
(dr0(2)*V_ele(2)*V_ion(3)+dr0(1)*V_ele(1)*V_ion(3)-dr0(3)*V_ele(2)*V_ion(2)-dr0(3)*V_ele(1)*V_ion(1)+dr0(3)) * I1); % MeV * 1/mm * mm/c == MeV/c

dP = [ dPx, dPy, dPz ]

P_total = P_ion+P_ele

T = (dot(dP,dP) - dot(dP,P_total))/(E_ele+E_ion)
q = [ T dP ]

p_ele = [ Me*gamma_ele P_ele ];
p_ion = [ Mp*gamma_ion P_ion ];

p_ele_f = p_ele + q;
p_ion_f = p_ion - q;

P    = [ (E_ele + E_ion) P_total ];
P_f  = p_ele_f + p_ion_f;

E_ele_f = p_ele_f(1);
P_ele_f = p_ele_f(2:4);
E_ion_f = p_ion_f(1);
P_ion_f = p_ion_f(2:4);
P_total_f = P_ion_f+P_ele_f

X_prime = P_total / (E_ion + E_ele)
X_prime_f = P_total_f / (E_ion_f + E_ele_f)

gamma_prime = 1 / sqrt(1 - dot(X_prime,X_prime));

L = lorentz_boost_matrix(X_prime);

P_ele_cm = L * [ E_ele P_ele ]';
P_ion_cm = L * [ E_ion P_ion ]';

P_cm = P_ele_cm + P_ion_cm

norm_4d(P_cm)
norm_4d(P)
norm_4d(P_f)
%}
