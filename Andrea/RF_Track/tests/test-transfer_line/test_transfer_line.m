addpath('../../')
RF_Track;

N_particles = 10000; % number of macroparticles per overlap

Ions.A = 207.2; % Lead
Ions.Q = 54; % ion charge
Ions.mass = Ions.A * RF_Track.protonmass;
Ions.Np = 5e8; % total number of ions per simulated bunch
Ions.sigmaz = 1; %%%% 25 * pi * 1e3; % LEIR circumference mm
Ions.K0 = 4.2 * Ions.A; % MeV
Ions.E0 = Ions.mass + Ions.K0; % MeV
Ions.P0 = sqrt(Ions.E0**2 - Ions.mass**2);
Ions.beta0 = Ions.P0 / Ions.E0; % c
Ions.gamma0 = 1 / sqrt(1 - Ions.beta0**2); %
Ions.P_spread = 0.01; % 

Lcirc_mm = 25*pi*1e3; % mm

BunchT = Bunch6d_twiss();
BunchT.Pc = Ions.P0; % MeV
BunchT.emitt_x = Ions.beta0 * Ions.gamma0 * 14; % mm.mrad == micron
BunchT.emitt_y = Ions.beta0 * Ions.gamma0 * 14; % mm.mrad == micron
BunchT.emitt_z = Ions.beta0 * Ions.gamma0 * Ions.sigmaz * (Ions.P_spread / 100) * 1e3; % mm.mrad
BunchT.alpha_x = 0;
BunchT.alpha_y = 0;
BunchT.alpha_z = 0;
BunchT.beta_x = 3.690910796; % m
BunchT.beta_y = 14.3080807; % m
BunchT.beta_z = Ions.sigmaz ** 2 / BunchT.emitt_z * Ions.beta0 * Ions.gamma0; % m
B0 = Bunch6d(Ions.mass, Ions.Np, Ions.Q, BunchT, N_particles, -1);

load ('twiss_noEC.txt'); % this is T
RING = T;

% chromaticity
DQx = -0.5; % * 2pi
DQy = -1.0; % * 2pi

% momentum compaction
LEIR_momentum_compaction = 0.124;

RING(1,2) = RING(end,2) = 3.690910796;
RING(1,3) = RING(end,3) = 0.0;
RING(1,5) = RING(end,5) = 14.3080807;
RING(1,6) = RING(end,6) = 0.0;

%RR = TransferLine('twiss_noEC.tfs', Ions.P0);
MR = TransferLine(RING, DQx, DQy, LEIR_momentum_compaction, Ions.P0);

L = Lattice(); % Folds Nfold turns in one lattice
L.append(MR);
B1 = L.track(B0);

B = L.get_transport_table("%S %mean_x %mean_y %beta_x %beta_y");

figure(1)
clf
plot(RING(:,1), RING(:,2))
hold on
plot(B(:,1), B(:,4));
xlabel('S [m]')
ylabel('beta_x [m]')
legend('MAD-X Twiss', 'Tracking');

figure(2)
clf
plot(RING(:,1), RING(:,5))
hold on
plot(B(:,1), B(:,5));
xlabel('S [m]')
ylabel('beta_y [m]')
legend('MAD-X Twiss', 'Tracking');

