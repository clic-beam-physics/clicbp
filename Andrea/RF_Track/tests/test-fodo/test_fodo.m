%% load RF-Track

addpath('../../')

RF_Track;

%% Define reference particle, and rigidity
Part.mass = RF_Track.protonmass; % MeV/c^2
Part.P0c = 100; % MeV/c
Part.Q = 1; % e+
B_rho = Part.P0c / Part.Q; % MV/c, reference rigidity

%% FODO cell with Lcell = 2 m, Lq = 24 cm, Ld = 76 cm, mu = 90 degress
Lcell = 2; % m
Lquad = 0.24; % m
mu = 90; % deg

% focusing params
k1L = sind(mu/2) / (Lcell/4); % 1/m
strength = k1L * B_rho; % MeV/m
gradient = strength / Lquad * 1e6 / RF_Track.clight; % T/m = V/m/m/c

%% Lattice
Qf = Quadrupole(Lquad/2,  strength/2);
Qd = Quadrupole(Lquad/2, -strength/2);
D  = Drift(Lcell/2 - Lquad);

FODOcell = Lattice();
FODOcell.append(Qf);
FODOcell.append(D);
FODOcell.append(Qd);
FODOcell.append(Qd);
FODOcell.append(D);
FODOcell.append(Qf);

%% Bunch
Nparticles = 10000;
Twiss = Bunch6d_twiss();
Twiss.Pc = Part.P0c; % MeV
Twiss.emitt_x = 10; % mm.mrad normalised emittance
Twiss.emitt_y = 10; % mm.mrad
Twiss.emitt_z = 0.0; % mm.mrad
Twiss.beta_x = Lcell * (1 + sind(mu/2)) / sind(mu); % m
Twiss.beta_y = Lcell * (1 - sind(mu/2)) / sind(mu); % m
Twiss.beta_z = 1; % m
Twiss.alpha_x = 0;
Twiss.alpha_y = 0;
Twiss.alpha_z = 0;
B0 = Bunch6d(Part.mass, 0.0, Part.Q, Twiss, Nparticles, -1);

%% Tracking
Ncells = 100;

T = [];
B = [];
B1 = B0;
tic
for cell=1:Ncells
  B1 = FODOcell.track(B1);
  emitt_4d = 1e3 * det(cov(B1.get_phase_space('%x %Px %y %Py')))^0.25 / Part.mass; % 4D normalised emittance mm.mrad
  T = [ T ; cell FODOcell.get_transport_table('%emitt_x %emitt_y')(1,:) emitt_4d ];
  B = [ B ; (cell*Lcell + FODOcell.get_transport_table('%S')) FODOcell.get_transport_table('%beta_x %beta_y') ];
end
toc

%% Plots
figure(1)
clf
plot(T(:,1), T(:,2), 'b*-', T(:,1), T(:,3), 'r*-', T(:,1), T(:,4), 'g*-');
legend('emitt x', 'emitt y', 'emitt 4d');
xlabel('cell [#]');
ylabel('emitt [mm.mrad]');

figure(2)
clf
plot(B(:,1), B(:,2), 'b*-', B(:,1), B(:,3), 'r*-');
legend('beta x', 'beta y');
xlabel('S [m]');
ylabel('beta [m]');

figure(3)
clf
hold on
scatter(B0.get_phase_space('%x'), B0.get_phase_space('%Px'));
scatter(B1.get_phase_space('%x'), B1.get_phase_space('%Px'), 'r');
title('initial and final horizontal phase spaces');
xlabel('X [mm]');
ylabel('Px [MeV/c]');

figure(4)
clf
hold on
scatter(B0.get_phase_space('%y'), B0.get_phase_space('%Py'));
scatter(B1.get_phase_space('%y'), B1.get_phase_space('%Py'), 'r');
title('initial and final vertical phase spaces');
xlabel('X [mm]');
ylabel('Px [MeV/c]');
