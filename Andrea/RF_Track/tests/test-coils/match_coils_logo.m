addpath('../..');

RF_Track;

function [M,B1] = merit(Bz)
    global B0 M0
    RF_Track;
    C = TestCoils(0.4, ... % m, length
                  Bz, ... % T, on-axis field
                  0.1); % m, radius
    C.set_nsteps(10);
    C.set_odeint_algorithm("rk2");
    L = Lattice();
    %for i=1:1
        L.append(C);
        L.append(C);
        L.append(C);
        L.append(C);
    %end
    B1 = L.track(B0);
    M1 = B1.get_phase_space();
    M = sum(sum(abs(M1(:,[ 2 4 ]))))
    
    figure(1)
    clf; hold on
    scatter(M0(:,1), M0(:,3), 'r')
    scatter(M1(:,1), M1(:,3))
    drawnow

    figure(2)
    clf; hold on
    scatter(M0(:,1), M0(:,2), 'r')
    scatter(M1(:,1), M1(:,2))
    drawnow
		
    figure(3)
    clf; hold on
    scatter(M0(:,2), M0(:,4), 'r')
    scatter(M1(:,2), M1(:,4))
    drawnow
    
endfunction

%% beam
I = imread('CERN_logo.png');
[r,c] = find(I(:,:,1)>0);

I = imread('cat.png');
[r,c] = find(I(:,:,1)<250);


O_ = zeros(size(r,1),1);
Pz_ = 64 * ones(size(r,1),1);




global B0 M0
B0 = Bunch6d(RF_Track.electronmass, 1, -1, [ (c-64)/64 O_ (64-r)/64 O_ O_ Pz_ ]);
M0 = B0.get_phase_space();


T = [];
for Bz = linspace(0, 3.1, 30)
    [M,B1] = merit(Bz);
    T = [ T ; Bz M ];
		figure(4)
		plot(T(:,1), T(:,2));
		drawnow
end
figure(3)
plot(T(:,1), T(:,2));


format long
O = optimset('TolFun', 1e-16);
Bz = fminbnd("merit", 2.8, 3.2, O)
%Bz =  0.915557297544145;

[M,B1] = merit(Bz);
M = B1.get_phase_space();
figure(1)
clf; hold on
scatter(M0(:,1), M0(:,3), 'r')
scatter(M(:,1), M(:,3))

figure(2)
clf; hold on
scatter(M0(:,2), M0(:,4), 'r')
scatter(M(:,2), M(:,4))
