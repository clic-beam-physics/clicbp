#! /opt/local/bin/octave -q --persist

setenv("GNUTERM","X11");
addpath('../..');

clear all

%% Load RF_Track
RF_Track;
RF_Track.number_of_threads = 8;

randn("seed", 1234);
rand("seed", 5678);

%%%%%%%%%%%%%%

Cooler.L = 3; % m
Cooler.B = 0.07; % T
Cooler.r0 = 200; % mm (should be 25)
Cooler.beta = 5; % m

Ion.mass = 207.2 * RF_Track.protonmass;
Ion.Np = 5e9; % total number of protons
Ion.K = 4.2 * 207.2; % MeV
Ion.E = Ion.mass + Ion.K; % MeV
Ion.P = sqrt(Ion.E**2 - Ion.mass**2);
Ion.V = Ion.P / Ion.E;
Ion.P_spread = 0.4; % 0.4 percent
Ion.gamma = 1 / sqrt(1 - Ion.V**2); %
Ion.sigmaz = 1; % mm
Ion.Q = 54;

Electron.Ne = 8.27e14; % electron number density #/m^3
Electron.V = Ion.V * 0.9999; % 2% less than the proton speed
Electron.gamma = 1 / sqrt(1 - Electron.V**2); %
Electron.P = RF_Track.electronmass * Electron.V * Electron.gamma;

BunchT = Bunch6d_twiss();
BunchT.Pc = Ion.P; % MeV
BunchT.emitt_x = Ion.V * Ion.gamma * 15; % mm.mrad == micron
BunchT.emitt_y = Ion.V * Ion.gamma * 15; % mm.mrad == micron
BunchT.emitt_z = Ion.V * Ion.gamma * Ion.sigmaz * (Ion.P_spread / 100) * 1e3; % mm.mrad
BunchT.alpha_x = 0;
BunchT.alpha_y = 0;
BunchT.alpha_z = 0;
BunchT.beta_x = 3.690910796;
BunchT.beta_y = 14.3080807;
BunchT.beta_z = Ion.sigmaz ** 2 / BunchT.emitt_z * Ion.V * Ion.gamma; % m

N_particles = 1000;

B0 = Bunch6d(Ion.mass, Ion.Np, Ion.Q, BunchT, N_particles);

Vz = B0.get_phase_space("%Vz");
Electron.V = max(Vz) * 1.001

B{1} = B0;

Brho = Ion.P / Ion.Q;

Nx = 16;
Ny = 16;
Nz = 32;

Qd = Quadrupole(0.2);
Qd.set_K1L(Brho, -0.3566981773);
%Qd.set_K1L(Brho, -1.77972 * 0.2);

Qf = Quadrupole(0.4);
Qf.set_K1L(Brho, 0.402181838);
%Qf.set_K1L(Brho,  1.00313 * 0.4);

d0 = Drift(0.6);
d1 = Drift(1.3);

E = ElectronCooler(Cooler.L, Cooler.r0/1e3, Cooler.r0/1e3);
E.set_Q(-1);
E.set_nsteps(Nz);
E.set_electron_mesh_template(Electron.Ne, 0, 0, Electron.V, Nx, Ny);
E.set_static_Bfield(0.0, 0.0, Cooler.B);

%E = Drift(Cooler.L);

L = Lattice();
L.append(Qd);
L.append(d0);
L.append(Qf);
L.append(d1);
L.append(E);
L.append(d1);
L.append(Qf);
L.append(d0);
L.append(Qd);

if exist('LEIR_emitt.dat', 'file')
  load 'LEIR_emitt.dat';
else
  I = B{1}.get_info();
  M = B{1}.get_phase_space("%x %xp %y %yp %t %Pc %Vx %Vy %Vz %Px %Py %Pz");
  save('-text','-z', 'LEIR_beam_1.dat.gz', 'M');
  emitt_x = Ion.V * Ion.gamma * det(cov(M(:,1:2)))**0.5;
  emitt_y = Ion.V * Ion.gamma * det(cov(M(:,3:4)))**0.5;
  emitt4d = Ion.V * Ion.gamma * det(cov(M(:,1:4)))**0.25;
  V = B{1}.get_phase_space("%Vx %Vy %Vz");
  T = [ emitt_x emitt_y emitt4d mean(V) std(V) ];
end

i0 = size(T,1);
if exist('LEIR_beam_last.dat', 'file')
  load 'LEIR_beam_last.dat';
  B{i0} = Bunch6d(Ion.mass, Ion.Np, Ion.Q, BunchT, N_particles);
  M(:,2) .*= Ion.P ./ M(:,12);
  M(:,4) .*= Ion.P ./ M(:,12);
  B{i0}.set_phase_space(M(:,1:6));
end

for i=i0:10000
  dP = mean(B{1}.get_phase_space("%Pc")) - mean(B{i}.get_phase_space("%Pc"))
  %M = B{i}.get_phase_space();
  %M(:,6) += dP;
  %B{i}.set_phase_space(M);
  B{i+1} = L.track(B{i});
  B{i+1}.get_ngood()
  I = B{i+1}.get_info();
  M = B{i+1}.get_phase_space("%x %xp %y %yp %t %Pc %Vx %Vy %Vz %Px %Py %Pz");
  emitt_x = Ion.V * Ion.gamma * det(cov(M(:,1:2)))**0.5;
  emitt_y = Ion.V * Ion.gamma * det(cov(M(:,3:4)))**0.5;
  emitt4d = Ion.V * Ion.gamma * det(cov(M(:,1:4)))**0.25;
  V = B{i+1}.get_phase_space("%Vx %Vy %Vz");
  T = [ T; emitt_x emitt_y emitt4d mean(V) std(V) ];
  save -text LEIR_emitt.dat T
  save -text LEIR_beam_last.dat M;
  %if mod(i,100) == 0
    save('-text', '-z', sprintf('LEIR_beam_%d.dat.gz', i+1), 'M');
  %endif
end

printf("K_initial = %.16g\n", mean(B{1}.get_phase_space("%K")));
printf("K_final   = %.16g\n", mean(B{end}.get_phase_space("%K")));

if false
figure(1);
clf
hold on
scatter3(B{1}.get_phase_space("%x"), B{1}.get_phase_space("%y"), B{1}.get_phase_space("%Pz"))
scatter3(B{end}.get_phase_space("%x"), B{end}.get_phase_space("%y"), B{end}.get_phase_space("%Pz"), 'r')

figure(2)
clf;
hold on
scatter(B{1}.get_phase_space("%x"), B{1}.get_phase_space("%xp"))
scatter(B{end}.get_phase_space("%x"), B{end}.get_phase_space("%xp"), 'r')

figure(3)
clf;
hold on
scatter(B{1}.get_phase_space("%y"), B{1}.get_phase_space("%yp"))
scatter(B{end}.get_phase_space("%y"), B{end}.get_phase_space("%yp"), 'r')
end
