addpath('../../');

%% Load RF_Track

RF_Track;

setenv("DEBUG"); % triggers RF-Track debug information

%%%%%%%%%%%%%%

V_ele = 0.09;

Cooler.L = 3; % m
Cooler.B = 0.07; % T
Cooler.r0 = 25; % mm, electron beam radius

%% Electron beam parameters
Electrons.Ne = 8.27e13; % electron number density #/m^3
Electrons.V = V_ele; % 0.2% less than the ion speed
Electrons.gamma = 1 / sqrt(1 - Electrons.V**2); %
Electrons.P = RF_Track.electronmass * Electrons.V * Electrons.gamma;

Nx = 51; % plasma mesh size
Ny = 51;
Nz = 51;

% mesh grid points
Xa = linspace(-Cooler.r0, Cooler.r0, Nx); % axes
Ya = linspace(-Cooler.r0, Cooler.r0, Ny);
Za = linspace(0, Cooler.L*1e3, Nz);
[X,Y,Z] = ndgrid(Xa, Xa, Za); % mesh grids

%% particles' density distribution
De = Electrons.Ne * ones(Nx,Ny,Nz);
% localized distributions
sigmaX = 10;  %mm
sigmaY = 10; % mm
sigmaZ = 500; % mm
% Centered, uniform
%De = Electrons.Ne * rectpulse(X/sigmaX) .* rectpulse(Y/sigmaY) .* rectpulse((Z-1500)/sigmaZ);
% Centered, Gaussian
%De = Electrons.Ne * normpdf(X/sigmaX) .* normpdf(Y/sigmaY) .* normpdf((Z-1500)/sigmaZ);

%% particles' velocities
Vx = zeros(Nx,Ny,Nz);
Vy = zeros(Nx,Ny,Nz);
Vz = Electrons.V*ones(Nx,Ny,Nz);

%% define plasma (ok, electron cooler in this case)
Ec = ElectronCooler(Cooler.L, Cooler.r0/1e3, Cooler.r0/1e3);
Ec.set_mass(RF_Track.electronmass);
Ec.set_Q(-1);
Ec.set_plasma_mesh(De,Vx,Vy,Vz);
Ec.set_static_Bfield(0.0, 0.0, Cooler.B);

%% play with plasma
F = Ec.get_self_fields();

Ex = zeros(Nx,Nz);
Ey = zeros(Nx,Nz);
Ez = zeros(Nx,Nz);
Bx = zeros(Nx,Nz);
By = zeros(Nx,Nz);
Bz = zeros(Nx,Nz);

D  = zeros(Nx,Nz);
Vx = zeros(Nx,Nz);
Vy = zeros(Nx,Nz);
Vz = zeros(Nx,Nz);

for i=1:Nx
    for j=floor(Ny/2)
        for k=1:Nz
            [E,B]   = F.get_field    (X(i,j,k), Y(i,j,k), Z(i,j,k), 0);
            D(i,k)  = Ec.get_density (X(i,j,k), Y(i,j,k), Z(i,j,k));
            V       = Ec.get_velocity(X(i,j,k), Y(i,j,k), Z(i,j,k));
            Vx(i,k) = V(1);
            Vy(i,k) = V(2);
            Vz(i,k) = V(3);
            Ex(i,k) = E(1);
            Ey(i,k) = E(2);
            Ez(i,k) = E(3);
            Bx(i,k) = B(1);
            By(i,k) = B(2);
            Bz(i,k) = B(3);
        end
    end
end

figure(1)
surf(Za,Xa,Ex);
shading flat
xlabel('Z [mm]')
ylabel('X [mm]')
title('Ex [V/m]')

figure(2)
surf(Za,Xa,Ey);
shading flat
xlabel('Z [mm]')
ylabel('X [mm]')
title('Ey [V/m]')

figure(3)
surf(Za,Xa,Ez);
shading flat
xlabel('Z [mm]')
ylabel('X [mm]')
title('Ez [V/m]')

figure(4)
surf(Za,Xa,Bx);
shading flat
xlabel('Z [mm]')
ylabel('X [mm]')
title('Bx [T]')

figure(5)
surf(Za,Xa,By);
shading flat
xlabel('Z [mm]')
ylabel('X [mm]')
title('By [T]')

figure(6)
surf(Za,Xa,Bz);
shading flat
xlabel('Z [mm]')
ylabel('X [mm]')
title('Bz [T]')

figure(7)
surf(Za,Xa,D)
shading flat
xlabel('Z [mm]')
ylabel('X [mm]')
title(sprintf('density = %g [#/m^3] ; Vz = %g [c]', Electrons.Ne, Electrons.V))
