/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <cstring>

#include "parallel_ode_solver.hh"

void Parallel_ODE_Solver::init_gsl_drivers(const std::vector<gsl_odeiv2_system> &sys )
{
  if (use_gsl()) {
    drivers.resize(sys.size());
    const gsl_odeiv2_step_type *type = [&] () {
      if (algorithm == gsl_rk2)
	return gsl_odeiv2_step_rk2;
      if (algorithm == gsl_rk4)
	return gsl_odeiv2_step_rk4;
      if (algorithm == gsl_rkf45)
	return gsl_odeiv2_step_rkf45;
      if (algorithm == gsl_rkck)
	return gsl_odeiv2_step_rkck;
      if (algorithm == gsl_rk8pd)
	return gsl_odeiv2_step_rk8pd;
      if (algorithm == gsl_rk1imp)
	return gsl_odeiv2_step_rk1imp;
      if (algorithm == gsl_rk2imp)
	return gsl_odeiv2_step_rk2imp;
      if (algorithm == gsl_rk4imp)
	return gsl_odeiv2_step_rk4imp;
      if (algorithm == gsl_bsimp)
	return gsl_odeiv2_step_bsimp;
      if (algorithm == gsl_msadams)
	return gsl_odeiv2_step_msadams;
      /* if (algorithm == gsl_msbdf) */
      return gsl_odeiv2_step_msbdf;
    } ();
    for (size_t i=0; i<sys.size(); i++) {
      if (!(drivers[i] = gsl_odeiv2_driver_alloc_y_new(&sys[i], type, 1e-6, 1e-6, 0.0))) {
	std::cerr << "error: cannot allocate gsl ODE integrator\n";
	exit(0);
      }
    }
  }
}

void Parallel_ODE_Solver::free_gsl_drivers()
{
  for (auto d: drivers)
    gsl_odeiv2_driver_free(d);
  drivers.clear();
}

const char *Parallel_ODE_Solver::get_odeint_algorithm() const
{
  if (algorithm == analytic)
    return "analytic";
  if (algorithm == leapfrog)
    return "leapfrog";
  if (algorithm == gsl_rk2)
    return "rk2";
  if (algorithm == gsl_rk4)
    return "rk4";
  if (algorithm == gsl_rkf45)
    return "rkf45";
  if (algorithm == gsl_rkck)
    return "rkck";
  if (algorithm == gsl_rk8pd)
    return "rk8pd";
  if (algorithm == gsl_rk1imp)
    return "rk1imp";
  if (algorithm == gsl_rk2imp)
    return "rk2imp";
  if (algorithm == gsl_rk4imp)
    return "rk4imp";
  if (algorithm == gsl_bsimp)
    return "bsimp";
  if (algorithm == gsl_msadams)
    return "msadams";
  /* if (algorithm == gsl_msbdf) */
  return "msbdf";
}

void Parallel_ODE_Solver::set_odeint_algorithm(const char *name )
{
  if (strcmp(name, "analytic")==0)
    algorithm = analytic;
  else if (strcmp(name, "leapfrog")==0)
    algorithm = leapfrog;
  else if (strcmp(name, "rk2")==0)
    algorithm = gsl_rk2;
  else if (strcmp(name, "rk4")==0)
    algorithm = gsl_rk4;
  else if (strcmp(name, "rkf45")==0)
    algorithm = gsl_rkf45;
  else if (strcmp(name, "rkck")==0)
    algorithm = gsl_rkck;
  else if (strcmp(name, "rk8pd")==0)
    algorithm = gsl_rk8pd;
  else if (strcmp(name, "rk1imp")==0)
    algorithm = gsl_rk1imp;
  else if (strcmp(name, "rk2imp")==0)
    algorithm = gsl_rk2imp;
  else if (strcmp(name, "rk4imp")==0)
    algorithm = gsl_rk4imp;
  else if (strcmp(name, "bsimp")==0)
    algorithm = gsl_bsimp;
  else if (strcmp(name, "msadams")==0)
    algorithm = gsl_msadams;
  else if (strcmp(name, "msbdf")==0)
    algorithm = gsl_msbdf;
  else
    std::cerr << "error: unknown ODE integration algorithm '" << name << "'\n";
}
