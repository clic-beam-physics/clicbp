/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <vector>
#include "RF_Track.hh"
#include "numtools.hh"
#include "for_all.hh"
#include "relativistic_velocity_addition.hh"

void SpaceCharge::change_reference_frame(const Bunch6d &bunch )
{
  const size_t Nparticles = bunch.size();
    
  // manage pre-allocated memory
  position.resize(Nparticles);
  velocity.resize(Nparticles);

  // change the reference frame
  const double t0 = bunch.get_t(); // time such that <Z> = 0.0
  for (size_t i=0; i<Nparticles; i++) {
    const auto &P = bunch.particles[i];
    if (P) {
      velocity[i] = P.get_Vx_Vy_Vz();
      position[i] = StaticVector<3>(P.x, P.y, (t0 - P.t) * velocity[i][2]);
    }
  }
  if (bunch.coasting()) {
    for (size_t i=0; i<Nparticles; i++) {
      const auto &P = bunch.particles[i];
      if (P) {
	position[i][2] = fmod(position[i][2] + bunch.coasting()/2, bunch.coasting());
	if (position[i][2]<0.0)
	  position[i][2] += bunch.coasting();
	position[i][2] -= bunch.coasting()/2;
      }
    }
  }
}

void SpaceCharge::change_reference_frame(const Bunch6dT &bunch )
{
  const size_t Nparticles = bunch.size();
    
  // manage pre-allocated memory
  position.resize(Nparticles);
  velocity.resize(Nparticles);
    
  // change the reference frame
  for (size_t i=0; i<Nparticles; i++) {
    const auto &P = bunch.particles[i];
    if (P) {
      velocity[i] = P.get_Vx_Vy_Vz();
      position[i] = StaticVector<3>(P.X, P.Y, P.S); // position in lab's frame
    }
  }
  if (bunch.coasting()) {
    for (size_t i=0; i<Nparticles; i++) {
      const auto &P = bunch.particles[i];
      if (P) {
	position[i][2] = fmod(position[i][2] + bunch.coasting()/2, bunch.coasting());
	if (position[i][2]<0.0)
	  position[i][2] += bunch.coasting();
	position[i][2] -= bunch.coasting()/2;
      }
    }
  }
}

// move to a moving reference frame
void SpaceCharge::change_reference_frame(const Bunch6d &bunch, const StaticVector<3> &V_ref ) // same thing, but in a reference frame moving with velocity v_ref
{
  const size_t Nparticles = bunch.size();
    
  // manage pre-allocated memory
  position.resize(Nparticles);
  velocity.resize(Nparticles);

  // change the reference frame
  const auto n = normalize(V_ref);
  const double gamma_ref = 1.0 / sqrt(1.0 - dot(V_ref,V_ref));
  const double t0 = bunch.get_t(); // time such that <Z> = 0.0
  for (size_t i=0; i<Nparticles; i++) {
    const auto &P = bunch.particles[i];
    if (P) {
      velocity[i] = P.get_Vx_Vy_Vz();
      const auto r = StaticVector<3>(P.x, P.y, 0.0) + (t0 - P.t) * velocity[i];
      position[i] = r + (gamma_ref - 1.0) * n*dot(r,n); // stretches the longitudinal direction
    }
  }
}

void SpaceCharge::change_reference_frame(const Bunch6dT &bunch, const StaticVector<3> &V_ref ) // same thing, but in a reference frame moving with velocity v_ref
{
  const size_t Nparticles = bunch.size();
    
  // manage pre-allocated memory
  position.resize(Nparticles);
  velocity.resize(Nparticles);
    
  // change the reference frame
  const auto n = normalize(V_ref);
  const double gamma_ref = 1.0 / sqrt(1.0 - dot(V_ref,V_ref));
  for (size_t i=0; i<Nparticles; i++) {
    const auto &P = bunch.particles[i];
    if (P) {
      const auto r = StaticVector<3>(P.X, P.Y, P.S); // position in lab's frame
      position[i] = r + (gamma_ref - 1.0) * n*dot(r,n); // stretches the longitudinal direction
      velocity[i] = P.get_Vx_Vy_Vz();
    }
  }
}
