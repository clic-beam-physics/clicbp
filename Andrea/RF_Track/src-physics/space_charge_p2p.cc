/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include "RF_Track.hh"

#include "relativistic_velocity_addition.hh"
#include "lorentz_boost.hh"
#include "numtools.hh"
#include "space_charge_p2p.hh"

template <typename BUNCH_TYPE>
void SpaceCharge_P2P::compute_force_(MatrixNd &force, const BUNCH_TYPE &bunch, const ParticleSelector &selector )
{
  const size_t Nthreads = RF_Track_Globals::number_of_threads;
  const size_t Nparticles = bunch.size();

  // compute coordinates in bunch's reference frame
  change_reference_frame(bunch);

  // compute forces
  auto compute_p2p_force_parallel = [&](size_t thread, size_t start, size_t end ) {
    auto &cumulative_F = F_all[thread];
    cumulative_F.resize(Nparticles);
    for (size_t i=0; i<Nparticles; i++) {
      if (bunch.particles[i]) {
	cumulative_F[i] = StaticVector<3>(0.0);
      }
    }
    for (size_t i=0, k=0; i<Nparticles-1; i++) {
      for (size_t j=i+1; j<Nparticles; j++, k++) {
	if (k>=start) {
	  if (k==end) goto end;
	  const auto &P_i = bunch.particles[i];
	  const auto &P_j = bunch.particles[j];
	  if (selector(P_i) && selector(P_j)) {
	    const auto Q_ij = P_i.Q * P_j.Q;
	    if (Q_ij!=0.0) {
	      if (P_i.N!=0.0 || P_j.N!=0.0) {
	        const auto r_ij_lab = position[i] - position[j];
	        // compute the force in the two-particle frame, then go back to lab frame
	        if (dot(r_ij_lab, r_ij_lab) != 0.0) {
		  const auto &v_i_lab = velocity[i];
		  const auto &v_j_lab = velocity[j];
		  const auto v_mean = 0.5 * (v_i_lab + v_j_lab);
		  const auto v_i = relativistic_velocity_addition(-v_mean, v_i_lab);
		  const auto v_j = relativistic_velocity_addition(-v_mean, v_j_lab);
		  const double gamma = 1.0 / sqrt(1.0 - dot(v_mean, v_mean));
		  const auto n = normalize(v_mean);
		  const auto r_ij = r_ij_lab + (dot(r_ij_lab, n) * (gamma - 1.0)) * n; // relativistic stretch
		  const double norm_r_ij = norm(r_ij);
		  const auto F_ij = Q_ij * (r_ij + cross(v_i, cross(v_j, r_ij))) / (norm_r_ij * norm_r_ij * norm_r_ij); // Coulomb force + Biot and Savart law
	    	  const auto F_i_4d =  StaticVector<4>(dot(F_ij, v_i), F_ij[0], F_ij[1], F_ij[2]) / sqrt(1.0 - dot(v_i, v_i)); // 4-force in the particle frame
	    	  const auto F_j_4d = -StaticVector<4>(dot(F_ij, v_j), F_ij[0], F_ij[1], F_ij[2]) / sqrt(1.0 - dot(v_j, v_j)); // 4-force in the particle frame
		  const auto F_i_lab_4d = lorentz_boost(-v_mean, F_i_4d); // 4-force in the Lab frame
	    	  const auto F_j_lab_4d = lorentz_boost(-v_mean, F_j_4d); // 4-force in the Lab frame
		  const auto F_i_lab = StaticVector<3>(F_i_lab_4d[1], F_i_lab_4d[2], F_i_lab_4d[3]) * sqrt(1.0 - dot(v_i_lab, v_i_lab));
		  const auto F_j_lab = StaticVector<3>(F_j_lab_4d[1], F_j_lab_4d[2], F_j_lab_4d[3]) * sqrt(1.0 - dot(v_j_lab, v_j_lab));
		  // cumulate forces for particles i and j
		  cumulative_F[i] += F_i_lab * P_j.N;
		  cumulative_F[j] += F_j_lab * P_i.N;
		}
	      }
	    }
	  }
	}
      }
    }
  end:;
  };
  F_all.resize(Nthreads);
  const size_t Nindex_pairs = (Nparticles*(Nparticles-1))/2;
  const size_t effective_Nthreads = for_all(Nthreads, Nindex_pairs, compute_p2p_force_parallel);

  // merge the data from different threads and store the result in F_all[0]
  for (size_t thread=1; thread<effective_Nthreads; thread++) {
    const auto &F_thread = F_all[thread];
    for (size_t i=0; i<Nparticles; i++) {
      F_all[0][i] += F_thread[i];
    }
  }

  // convert force into MeV/m
  // You have: e*e / 4 / pi / epsilon0 / mm / mm
  // You want: MeV/m
  // e*e / 4 / pi / epsilon0 / mm / mm = 1.43996448504452e-09 MeV/m
  // e*e / 4 / pi / epsilon0 / mm / mm = (1 / 694461572.063762) MeV/m
  force.resize(Nparticles, 3);
  for (size_t i=0; i<Nparticles; i++) {
    if (selector(bunch.particles[i])) {
      StaticVector<3> F = F_all[0][i];
      // convert to MeV/m
      F /= 694461572.063762; // MeV/m
      force[i][0] = F[0];
      force[i][1] = F[1];
      force[i][2] = F[2];
    } else {
      force[i][0] = 0.0;
      force[i][1] = 0.0;
      force[i][2] = 0.0;
    }
  }
}

// template specializations

template void SpaceCharge_P2P::compute_force_(MatrixNd &force, const Bunch6d &bunch, SpaceCharge::ParticleSelector const& );
template void SpaceCharge_P2P::compute_force_(MatrixNd &force, const Bunch6dT &bunch, SpaceCharge::ParticleSelector const& );
