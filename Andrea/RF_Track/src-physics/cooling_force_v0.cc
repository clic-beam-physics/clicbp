/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <fstream>
#include "RF_Track.hh"
#include "cooling_force.hh"

// mesh sizes
#define Nr 80
#define Nl 160
#define N_rnd 100000

static inline void force_unmagnetized(double &ret_x, double &ret_y, double &ret_z, double Vx, double Vy, double Vz )
{
  // \vec{V} / norm(V)^3
  if (Vx==0.0 && Vy==0.0 && Vz==0.0) {
    ret_x = 0.0;
    ret_y = 0.0;
    ret_z = 0.0;
    return;
  }
  const double V3 = hypot(Vx,Vy,Vz)*(Vx*Vx+Vy*Vy+Vz*Vz);
  ret_x = Vx / V3;
  ret_y = Vy / V3;
  ret_z = Vz / V3;
}

static inline void force_magnetized(double &ret_x, double &ret_y, double &ret_z, double Vx, double Vy, double Vz )
{
  if (Vx==0.0 && Vy==0.0) {
    ret_x = 0.0;
    ret_y = 0.0;
    ret_z = 0.0;
    return;
  }
  const double Vr = hypot(Vx,Vy);
  const double V  = hypot(Vr,Vz);
  const double V2 = V*V;
  const double V5 = V2*V2*V;
  const double f1 = Vr*Vr/V5;
  const double f2 = (Vr+Vz)*(Vr-Vz)/(2*V5);
  ret_x = f2 * Vx;
  ret_y = f2 * Vy;
  ret_z = f1 * Vz;
}

void CoolingForce::init_cooling_force(double D_ele_r_ /* c */, double D_ele_l_ /* c */ )
{
  D_ele_r = D_ele_r_;
  D_ele_l = D_ele_l_;
  const auto Ur_axis = linspace( 0.,         10. *D_ele_r, Nr);
  const auto Ul_axis = linspace(-10*D_ele_r, 10. *D_ele_r, Nl);
  const auto Mr_axis = linspace( 0.,          1.5*D_ele_r, Nr);
  const auto Ml_axis = linspace(-1.5*D_ele_r, 1.5*D_ele_r, Nl);
  Ur_min = Ur_axis[0];
  Ul_min = Ul_axis[0];
  Ur_max = Ur_axis[Nr-1];
  Ul_max = Ul_axis[Nl-1];
  Ur_step = Ur_axis[1] - Ur_axis[0];
  Ul_step = Ul_axis[1] - Ul_axis[0];
  f_unmagnetized_r.resize(Nr,Nl);
  f_unmagnetized_l.resize(Nr,Nl);
  Mr_min = Mr_axis[0];
  Ml_min = Ml_axis[0];
  Mr_max = Mr_axis[Nr-1];
  Ml_max = Ml_axis[Nl-1];
  Mr_step = Mr_axis[1] - Mr_axis[0];
  Ml_step = Ml_axis[1] - Ml_axis[0];
  f_magnetized_r.resize(Nr,Nl);
  f_magnetized_l.resize(Nr,Nl);
  std::cerr << "info: pre-computing the cooling force... ";
  const std::vector<double> &rnd_numbers = [] () {
    std::vector<double> retval(3*N_rnd);
    auto ran_gaussian_3sigma = [] () { double t; do t = gsl_ran_gaussian(RF_Track_Globals::rng, 1.0); while (fabs(t)>3.0); return t; };
    for (size_t n=0; n<N_rnd; n++) {
      retval[3*n+0] = ran_gaussian_3sigma();
      retval[3*n+1] = ran_gaussian_3sigma();
      retval[3*n+2] = ran_gaussian_3sigma();
    }
    return retval;
  } ();
  auto compute_force_parallel = [&] (size_t thread, size_t start, size_t end ) {
    const double threshold = 1.0/(D_ele_l*D_ele_l);
    for (size_t i=start; i<end; i++) {
      const double Ur = Ur_axis[i], Mr = Mr_axis[i];
      for (size_t j=0; j<Nl; j++) {
	const double Ul = Ul_axis[j], Ml = Ml_axis[j];
	CumulativeKahanSum<double> sum_U[2], sum_M[2];
	size_t Ncount_U = 0, Ncount_M = 0;
	for (size_t n=0; n<N_rnd; n++) {
	  const double
	    Ve_x = rnd_numbers[3*n+0]*D_ele_r,
	    Ve_y = rnd_numbers[3*n+1]*D_ele_r,
	    Ve_z = rnd_numbers[3*n+2]*D_ele_l;
	  double ret_x, ret_y, ret_z;
	  force_unmagnetized(ret_x, ret_y, ret_z, Ur - Ve_x, 0.0 - Ve_y, Ul - Ve_z);
	  if (fabs(ret_x)<threshold && fabs(ret_y)<threshold && fabs(ret_z)<threshold) {
	    sum_U[0] += ret_x;
	    sum_U[1] += ret_z;
	    Ncount_U++;
	  }
	  force_magnetized(ret_x, ret_y, ret_z, Mr, 0.0, Ml - Ve_z);
	  if (fabs(ret_x)<threshold && fabs(ret_y)<threshold && fabs(ret_z)<threshold) {
	    sum_M[0] += ret_x;
	    sum_M[1] += ret_z;
	    Ncount_M++;
	  }
	}
	f_unmagnetized_r.elem(i,j) = sum_U[0] / Ncount_U;
	f_unmagnetized_l.elem(i,j) = sum_U[1] / Ncount_U;
	f_magnetized_r.elem(i,j)   = sum_M[0] / Ncount_M;
	f_magnetized_l.elem(i,j)   = sum_M[1] / Ncount_M;
      }
    }
  };
  for_all(RF_Track_Globals::number_of_threads, Nr, compute_force_parallel);
  std::cerr << "done!\n";
  if (getenv("DEBUG")) {
    std::cerr << "info: saving the cooling force on file 'cooling_force_[un]magnetized.txt'\n";
    std::ofstream file("cooling_force_unmagnetized.txt");
    if (file) {
      for (double Ur : Ur_axis) {
	for (double Ul : Ul_axis) {
	  double Fr, Fl; // integrated
	  double Ar, At, Al; // asymptotic
	  cooling_force_unmagnetized(Fr, Fl, Ur, Ul);
	  force_unmagnetized(Ar, At, Al, Ur, 0.0, Ul);
	  file << (Ur/D_ele_r) << ' ' << (Ul/D_ele_r) << ' '
	       << Fr << ' ' << Fl << ' '
	       << Ar << ' ' << Al << ' '
	       << (Fr!=0.0 ? Ar/Fr : 0.0) << ' '
	       << (Fl!=0.0 ? Al/Fl : 0.0) << std::endl;
	}
      }
      file.close();
    }
    file.open("cooling_force_magnetized.txt");
    if (file) {
      for (double Mr : Mr_axis) {
	for (double Ml : Ml_axis) {
	  double Fr, Fl; // integrated
	  double Ar, At, Al; // asymptotic
	  cooling_force_magnetized(Fr, Fl, Mr, Ml);
	  force_magnetized(Ar, At, Al, Mr, 0.0, Ml);
	  file << (Mr/D_ele_r) << ' ' << (Ml/D_ele_r) << ' '
	       << Fr << ' ' << Fl << ' '
	       << Ar << ' ' << Al << ' '
	       << (Fr!=0.0 ? Ar/Fr : 0.0) << ' ' << (Fl!=0.0 ? Al/Fl : 0.0) << std::endl;
	}
      }
      file.close();
    }
  }
}

void CoolingForce::cooling_force_unmagnetized(double &Fr, double &Fl, double Ur, double Ul )
{
  if (fabs(Ur)>Ur_max || Ul<Ul_min || Ul>Ul_max) {
    double ret_x, ret_y, ret_z;
    force_unmagnetized(ret_x, ret_y, ret_z, Ur, 0.0, Ul);
    Fr = ret_x;
    Fl = ret_z;
  } else {
    const double f1 = (Ur - Ur_min) / Ur_step;
    const double f2 = (Ul - Ul_min) / Ul_step;
    Fr = f_unmagnetized_r(f1,f2);
    Fl = f_unmagnetized_l(f1,f2);
  }
}
  
void CoolingForce::cooling_force_magnetized(double &Fr, double &Fl, double Mr, double Ml )
{
  if (fabs(Mr)>Mr_max || Ml<Ml_min || Ml>Ml_max) {
    double ret_x, ret_y, ret_z;
    force_magnetized(ret_x, ret_y, ret_z, Mr, 0.0, Ml);
    Fr = ret_x;
    Fl = ret_z;
  } else {
    const double f1 = (Mr - Mr_min) / Mr_step;
    const double f2 = (Ml - Ml_min) / Ml_step;
    Fr = f_magnetized_r(f1,f2);
    Fl = f_magnetized_l(f1,f2);
  }
}
