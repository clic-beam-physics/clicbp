/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <functional>

#include "RF_Track.hh"

#include "relativistic_velocity_addition.hh"
#include "lorentz_boost.hh"
#include "numtools.hh"
#include "space_charge_pic.hh"

#include "greens_function.hh"
#include "greens_functions/coulomb_long_cylinder.hh"
#include "greens_functions/coulomb_horizontal_plates.hh"

template <typename GREENS_FUNCTION>
SpaceCharge_PIC<GREENS_FUNCTION>::~SpaceCharge_PIC()
{
  if (p1) fftw_destroy_plan(p1);
  if (p2) fftw_destroy_plan(p2);
  if (p3) fftw_destroy_plan(p3);
}

template <typename GREENS_FUNCTION>
SpaceCharge_PIC<GREENS_FUNCTION> &SpaceCharge_PIC<GREENS_FUNCTION>::operator = (const SpaceCharge_PIC &sc )
{
  if (this != &sc) {
    if (p1) fftw_destroy_plan(p1);
    if (p2) fftw_destroy_plan(p2);
    if (p3) fftw_destroy_plan(p3);
  }
  init_pic(sc.Nx, sc.Ny, sc.Nz);
  return *this;
}

template <typename GREENS_FUNCTION>
bool SpaceCharge_PIC<GREENS_FUNCTION>::init_pic(size_t Nx_, size_t Ny_, size_t Nz_ )
{
  Nx = Nx_ ? Nx_ : 16;
  Ny = Ny_ ? Ny_ : 16;
  Nz = Nz_ ? Nz_ : 16;

  p1 = p2 = p3 = nullptr;

  // for padding
  size_t Nxx = Nx<<1;
  size_t Nyy = Ny<<1;
  size_t Nzz = Nz<<1;

  // preallocate meshes
  mesh_rho = fftwMesh3d(Nxx, Nyy, Nzz); // charge density, and also Jx, Jy, Jz
  mesh_G   = fftwMesh3d(Nxx, Nyy, Nzz); // Green's function

  mesh_rho_hat = fftwComplexMesh3d(Nxx, Nyy, Nz+1); // charge density, and also Jx, Jy, Jz
  mesh_G_hat   = fftwComplexMesh3d(Nxx, Nyy, Nz+1); // Green's function

  mesh_Phi = Mesh3d(Nx, Ny, Nz);
  mesh_Ax  = Mesh3d(Nx, Ny, Nz);
  mesh_Ay  = Mesh3d(Nx, Ny, Nz);
  mesh_Az  = Mesh3d(Nx, Ny, Nz);

  const size_t Nthreads = RF_Track_Globals::number_of_threads;
  fftw_plan_with_nthreads(Nthreads);

  // create fftw plans
  if ((p1 = fftw_plan_dft_r2c_3d(Nxx, Nyy, Nzz, mesh_G.data(), (fftw_complex *)mesh_G_hat.data(), FFTW_ESTIMATE))) {
    if ((p2 = fftw_plan_dft_r2c_3d(Nxx, Nyy, Nzz, mesh_rho.data(), (fftw_complex *)mesh_rho_hat.data(), FFTW_ESTIMATE))) {
      if ((p3 = fftw_plan_dft_c2r_3d(Nxx, Nyy, Nzz, (fftw_complex *)mesh_rho_hat.data(), mesh_rho.data(), FFTW_ESTIMATE))) {
	return true;
      } fftw_destroy_plan(p2); p2=nullptr;
    } fftw_destroy_plan(p1); p1=nullptr;
  }
  return false;
}

template <typename GREENS_FUNCTION>
template <typename BUNCH_TYPE>
void SpaceCharge_PIC<GREENS_FUNCTION>::compute_force_(MatrixNd &force, const BUNCH_TYPE &bunch, const ParticleSelector &selector )
{
  const size_t Nparticles = bunch.size();
  force.resize(Nparticles, 3);
  if (Nparticles == 0)
    return;

  const size_t Nthreads = RF_Track_Globals::number_of_threads;
  const size_t Nxx = Nx<<1;
  const size_t Nyy = Ny<<1;

  // compute coordinates in bunch's reference frame
  const double gammaz_mean = [&bunch,&selector] () {
    CumulativeKahanSum<double> gammaz_sum = 0.0;
    CumulativeKahanSum<double> Nsum = 0.0;
    for (const auto &particle : bunch.particles) {
      if (selector(particle)) {
        const double Vz = particle.get_Vx_Vy_Vz()[2];
        const double gammaz = 1.0 / sqrt((1.0-Vz)*(1.0+Vz));
        gammaz_sum += particle.N * gammaz;
        Nsum += particle.N;
      }
    }
    return gammaz_sum / Nsum;
  } ();
  change_reference_frame(bunch);

  // find bounding box to compute Green's function
  auto min_ =  StaticVector<3>(std::numeric_limits<double>::max());
  auto max_ = -StaticVector<3>(std::numeric_limits<double>::max());
  {
    size_t good_particles = 0;
    for (size_t i=0; i<Nparticles; i++) {
      const auto &particle = bunch.particles[i];
      if (selector(particle)) {
	if (position[i][0] < min_[0]) min_[0] = position[i][0];
	if (position[i][1] < min_[1]) min_[1] = position[i][1];
	if (position[i][2] < min_[2]) min_[2] = position[i][2];
	if (position[i][0] > max_[0]) max_[0] = position[i][0];
	if (position[i][1] > max_[1]) max_[1] = position[i][1];
	if (position[i][2] > max_[2]) max_[2] = position[i][2];
	good_particles++;
      }
    }
    if (good_particles == 0 || min_ == max_) {
      for (size_t i=0; i<Nparticles; i++) {
	force[i][0] = 0.0;
	force[i][1] = 0.0;
	force[i][2] = 0.0;
      }
      return;
    }
  }
  
  // return if the box has zero size
  if (min_ == max_) {
    for (size_t i=0; i<Nparticles; i++) {
      force[i][0] = 0.0;
      force[i][1] = 0.0;
      force[i][2] = 0.0;
    }
    return;
  }
  
  // compute Green's function if bounding box has changed
  auto dim = max_ - min_;
  mesh.x0 = min_[0];
  mesh.y0 = min_[1];
  mesh.z0 = min_[2];
  mesh.hx = dim[0] / double(Nx-1);
  mesh.hy = dim[1] / double(Ny-1);
  mesh.hz = dim[2] / double(Nz-1);
  greens_function.g = gammaz_mean;
  GreensFunction::compute_mesh(greens_function, mesh_G, mesh.hx, mesh.hy, mesh.hz, bunch.coasting()); // 1/mm
  fftw_execute(p1); // 1/mm

  auto _H = StaticVector<3>(double(Nx-1) / dim[0],
			    double(Ny-1) / dim[1],
			    double(Nz-1) / dim[2]);

  // assign functions (four times to compute rho, Jx, Jy, Jz)

  std::vector<std::function<double(double,int)>> assign_functions(4);
  assign_functions[0] = []  (double charge, int i ) { return charge; };
  assign_functions[1] = [&] (double charge, int i ) { return charge * velocity[i][0]; };
  assign_functions[2] = [&] (double charge, int i ) { return charge * velocity[i][1]; };
  assign_functions[3] = [&] (double charge, int i ) { return charge * velocity[i][2]; };

  for (size_t func_id=0; func_id<4; func_id++) { // func_id:= 0: rho; 1: Jx; 2: Jy; 3: Jz
    mesh_rho = 0.0;
    const auto &assign_function = assign_functions[func_id];
    for (size_t i=0; i<Nparticles; i++) {
      const auto &particle = bunch.particles[i];
      if (selector(particle)) {
        if (particle.Q!=0.0 && particle.N!=0.0) {
	  const auto charge = particle.Q * particle.N;
	  const auto assigned_charge = assign_function(charge, i);
	  const auto r = position[i] - min_;
	  mesh_rho.assign_value(r[0]*_H[0], r[1]*_H[1], r[2]*_H[2], assigned_charge);
	}
      }
    }
    fftw_execute(p2);

    // convolution
    for (size_t i=0; i<Nxx*Nyy*(Nz+1); i++) { mesh_rho_hat.data()[i] *= mesh_G_hat.data()[i]; } // 1/mm

    // inverse Fourier Transform to find the potential
    fftw_execute(p3);
    mesh_rho /= 8.0*double(Nx*Ny*Nz);

    auto copy_mesh = [&] (const fftwMesh3d &from, TMesh3d<double> &dest ) {
      for (size_t i=0; i<Nx; i++)
	for (size_t j=0; j<Ny; j++)
	  for (size_t k=0; k<Nz; k++)
	    dest.elem(i,j,k) = from.elem(i,j,k);
    };

    if      (func_id == 0) copy_mesh(mesh_rho,mesh_Phi);
    else if (func_id == 1) copy_mesh(mesh_rho,mesh_Ax);
    else if (func_id == 2) copy_mesh(mesh_rho,mesh_Ay);
    else                   copy_mesh(mesh_rho,mesh_Az);
  }     

  // compute force for each particle
  // convert force into MeV/m
  //	e*e / epsilon0 / mm / mm = 1.809512739058424e-08 MeV/m
  //	e*e / epsilon0 / mm / mm = (1 / 55263495.99065812) MeV/m
  if (gammaz_mean>1000) _H[2] = 0.0;
  auto compute_force_parallel = [&] (size_t thread, size_t start, size_t end ) {
    for (size_t i=start; i<end; i++) {
      const auto &particle = bunch.particles[i];
      if (selector(particle)) {
	auto  q = particle.Q;
	auto  r = position[i] - min_;
	auto &v = velocity[i];
	r[0] *= _H[0];
	r[1] *= _H[1];
	r[2] *= _H[2];
	// Lorentz force
	const auto dPhi_dx = mesh_Phi.deriv_x(r[0], r[1], r[2])*_H[0]; // 1/mm^2
	const auto dPhi_dy = mesh_Phi.deriv_y(r[0], r[1], r[2])*_H[1];
	const auto dPhi_dz = mesh_Phi.deriv_z(r[0], r[1], r[2])*_H[2];
	const auto dAx_dy = mesh_Ax.deriv_y(r[0], r[1], r[2])*_H[1];
	const auto dAx_dz = mesh_Ax.deriv_z(r[0], r[1], r[2])*_H[2];
	const auto dAy_dx = mesh_Ay.deriv_x(r[0], r[1], r[2])*_H[0];
	const auto dAy_dz = mesh_Ay.deriv_z(r[0], r[1], r[2])*_H[2];
	const auto dAz_dx = mesh_Az.deriv_x(r[0], r[1], r[2])*_H[0];
	const auto dAz_dy = mesh_Az.deriv_y(r[0], r[1], r[2])*_H[1];
	const auto E = -StaticVector<3>(dPhi_dx, dPhi_dy, dPhi_dz); // 1/mm^2
	const auto B =  StaticVector<3>(dAz_dy-dAy_dz, dAx_dz-dAz_dx, dAy_dx-dAx_dy); // 1/mm^2
	auto F = q * (E + cross(v, B)); // 1/mm^2
	// convert to MeV/m
	F /= 55263495.99065812; // MeV/m
	force[i][0] = F[0];
	force[i][1] = F[1];
	force[i][2] = F[2];
      } else {
	force[i][0] = 0.0;
	force[i][1] = 0.0;
	force[i][2] = 0.0;
      }
    }
  };
  for_all(Nthreads, Nparticles, compute_force_parallel);
}

template <typename GREENS_FUNCTION>
StaticField SpaceCharge_PIC<GREENS_FUNCTION>::get_field() const
{
  // e / epsilon0 / mm = (1 / 55.2634959906581) V*mm/m
  // e / epsilon0 / mm / c = (1 / 16567579300.7125) T*mm
  Mesh3d Phi = mesh_Phi / 55.2634959906581; // V*mm/m
  Mesh3d Ax = mesh_Ax / 16567579300.7125; // T*mm
  Mesh3d Ay = mesh_Ay / 16567579300.7125; // T*mm
  Mesh3d Az = mesh_Az / 16567579300.7125; // T*mm
  return StaticField(Phi, Ax, Ay, Az,
		     mesh.x0/1e3, mesh.y0/1e3, // m
		     mesh.hx/1e3, mesh.hy/1e3, mesh.hz/1e3, // m
		     (mesh_Phi.size3()-1) * mesh.hz/1e3); // length m
}

// template specializations

template class SpaceCharge_PIC<GreensFunction::IntegratedRetardedCoulomb>;
template class SpaceCharge_PIC<GreensFunction::IntegratedRetardedCoulomb_LongCylinder>;
template class SpaceCharge_PIC<GreensFunction::IntegratedRetardedCoulomb_HorizontalPlates>;

/*
  template void SpaceCharge_PIC<GreensFunction::IntegratedRetardedCoulomb>::compute_force(MatrixNd &force, const Bunch6d &bunch, bool (*)(Particle const&) );
  template void SpaceCharge_PIC<GreensFunction::IntegratedRetardedCoulomb>::compute_force(MatrixNd &force, const Bunch6dT &bunch, bool (*)(ParticleT const&) );

  template void SpaceCharge_PIC<GreensFunction::IntegratedRetardedCoulomb_LongCylinder>::compute_force(MatrixNd &force, const Bunch6d &bunch, bool (*)(Particle const&) );
  template void SpaceCharge_PIC<GreensFunction::IntegratedRetardedCoulomb_LongCylinder>::compute_force(MatrixNd &force, const Bunch6dT &bunch, bool (*)(ParticleT const&) );

  template void SpaceCharge_PIC<GreensFunction::IntegratedRetardedCoulomb_HorizontalPlates>::compute_force(MatrixNd &force, const Bunch6d &bunch, bool (*)(Particle const&) );
  template void SpaceCharge_PIC<GreensFunction::IntegratedRetardedCoulomb_HorizontalPlates>::compute_force(MatrixNd &force, const Bunch6dT &bunch, bool (*)(ParticleT const&) );
*/
