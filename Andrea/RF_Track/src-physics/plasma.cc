/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <functional>
#include <limits>
#include <utility>
#include <functional>

// #include <umfpack.h> // sparse matrix package
#include "for_all.hh"
#include "numtools.hh"
#include "rotation.hh"
#include "plasma.hh"
#include "stats.hh"
#include "sinc.hh"
#include "move_particle.hh"
#include "greens_function.hh"
#include "greens_functions/yukawa.hh"
#include "greens_functions/coulomb.hh"
#include "vector_field_div_free.hh"
#include "static_field.hh"

//#define DEBUG

Plasma::Cell Plasma::null_cell = Plasma::Cell(0.0);

double Plasma::Cell::get_debye_length(double mass, double kbT /* eV */ ) const // m
{
  const double N_ele = get_number_density(); /* #/m^3 */
  
  if (N_ele == 0.0)
    return std::numeric_limits<double>::infinity();
  
  const StaticVector<3> &V_ele = get_velocity(mass); /* c */
  const double inv_gamma = sqrt(1.0 - dot(V_ele,V_ele));
  
  // sqrt(epsilon0 * eV * m^3 /e/e) = 7433.942156800665 m
  return inv_gamma != 0.0 ? 7433.942156800665 * sqrt(kbT / (N_ele * inv_gamma)) : std::numeric_limits<double>::max(); // m
}

Plasma::Plasma(double length_ /* m */, double rx_ /* m */, double ry_ /* m */, double N_ele /* #/m^3 */, double Vz /* c */, double kb_T_r /* eV */, double kb_T_l /* eV */ ) : mass(RF_Track_Globals::electronmass), Q(-1.0), rx(rx_*1e3), ry(ry_*1e3), length(length_*1e3), static_Bfield(0.0)
{
  set_temperature(kb_T_r, kb_T_l);
  set_plasma_mesh(1, 1, 1, N_ele, 0.0, 0.0, Vz);
}

void Plasma::set_temperature(double kb_T_r /* eV */, double kb_T_l /* eV */ ) // set transverse and longitudinal temperatures
{
  D_ele_r = sqrt(kb_T_r / get_mass()) / 1e3; // c, sqrt(eV / MeV) / 1e3 = 1
  D_ele_l = sqrt(kb_T_l / get_mass()) / 1e3; // c, sqrt(eV / MeV) / 1e3 = 1  
}

void Plasma::set_plasma_mesh(const Mesh3d &density /* #/m^3 */, const Mesh3d &Vx /* c */, const Mesh3d &Vy /* c */, const Mesh3d &Vz /* c */ )
{
  if (density.size1() != Vx.size1() ||
      density.size1() != Vy.size1() ||
      density.size1() != Vz.size1() ||
      density.size2() != Vx.size2() ||
      density.size2() != Vy.size2() ||
      density.size2() != Vz.size2() ||
      density.size3() != Vx.size3() ||
      density.size3() != Vy.size3() ||
      density.size3() != Vz.size3()) {
    std::cerr << "error: the input 3d meshes for Plasma::set_plasma_mesh() must have equal size\n";
    return;
  }

  // resize meshes
  size_t
    Nx = density.size1(),
    Ny = density.size2(),
    Nz = density.size3();
  
  plasma.resize(Nx, Ny, Nz);
  
  // init mesh_electron
  for (size_t i=0; i<Nx; i++) {
    for (size_t j=0; j<Ny; j++) {
      for (size_t k=0; k<Nz; k++) {
	const auto &n = density.elem(i,j,k); // number density
	const auto &V = StaticVector<3>(Vx.elem(i,j,k), Vy.elem(i,j,k), Vz.elem(i,j,k));
	const auto &E = mass / sqrt(1.0 - dot(V,V));
	const auto &P = E * V;
	const auto &cell = Cell(n, P);
	plasma.elem(i,j,k) = cell;
      }
    }
  }
}

void Plasma::set_plasma_mesh(size_t Nz, const MatrixNd &density /* #/m^3 */, const MatrixNd &Vx /* c */, const MatrixNd &Vy /* c */, const MatrixNd &Vz /* c */ )
{
  if (density.size1() != Vx.size1() ||
      density.size1() != Vy.size1() ||
      density.size1() != Vz.size1() ||
      density.size2() != Vx.size2() ||
      density.size2() != Vy.size2() ||
      density.size2() != Vz.size2()) {
    std::cerr << "error: the input matrices for Plasma::set_plasma_mesh() must have equal size\n";
    return;
  }

  // resize meshes
  size_t
    Nx = density.size1(),
    Ny = density.size2();
  
  plasma.resize(Nx, Ny, Nz);
  
  // init mesh_electron
  for (size_t i=0; i<Nx; i++) {
    for (size_t j=0; j<Ny; j++) {
      const auto &n = density[i][j]; // number density
      const auto &V = StaticVector<3>(Vx[i][j], Vy[i][j], Vz[i][j]);
      const auto &E = mass / sqrt(1.0 - dot(V,V));
      const auto &P = E * V;
      const auto &cell = Cell(n, P);
      for (size_t k=0; k<Nz; k++) {
	plasma.elem(i,j,k) = cell;
      }
    }
  }  
}

void Plasma::set_plasma_mesh(size_t Nx, size_t Ny, size_t Nz, double density /* #/m^3 */, double Vx /* c */, double Vy /* c */, double Vz /* c */ )
{
  // init mesh
  const auto &n = density; // number density
  const auto &V = StaticVector<3>(Vx, Vy, Vz); // c
  const auto &E = mass / sqrt(1.0 - dot(V,V)); // MeV
  const auto &P = E * V; // MeV/c
  const auto &cell = Cell(n, P);

  // resize mesh
  plasma.resize(Nx, Ny, Nz);
  
  // init mesh_electron
  for (size_t i=0; i<Nx; i++) {
    for (size_t j=0; j<Ny; j++) {
      for (size_t k=0; k<Nz; k++) {
	plasma.elem(i,j,k) = cell;
      }
    }
  }
}

void Plasma::set_nsteps(size_t nsteps )
{
  const size_t
    Nx = plasma.size1(),
    Ny = plasma.size2();

  MatrixNd density(Nx,Ny); /* #/m^3 */
  MatrixNd Vx(Nx,Ny); /* c */
  MatrixNd Vy(Nx,Ny); /* c */
  MatrixNd Vz(Nx,Ny); /* c */
  
  for (size_t i=0; i<Nx; i++) {
    for (size_t j=0; j<Ny; j++) {
      const auto &cell = plasma.elem(i,j,0);
      const auto &V = cell.get_velocity(mass);
      density[i][j] = cell.get_number_density();
      Vx[i][j] = V[0];
      Vy[i][j] = V[1];
      Vz[i][j] = V[2];
    }
  }
  
  set_plasma_mesh(nsteps, density, Vx, Vy, Vz);
}

StaticVector<3> Plasma::get_mesh_coordinates(double x, double y, double z ) const
{
  // mesh has the longitudinal extension of the element and the trasnverse extension of the aperture
  StaticVector<3> min_(-rx, -ry, 0); // mm
  StaticVector<3> max_(+rx, +ry, length); // mm

  // working box [mm]
  const auto dim = max_ - min_;

  // resize meshes
  size_t
    Nx = plasma.size1(),
    Ny = plasma.size2(),
    Nz = plasma.size3();
  
  const double inv_hx = double(Nx-1) / dim[0]; // 1/mm
  const double inv_hy = double(Ny-1) / dim[1]; // 1/mm
  const double inv_hz = double(Nz-1) / dim[2]; // 1/mm

  auto r_mesh = StaticVector<3>(x,y,z) - min_;
  r_mesh[0] *= inv_hx;
  r_mesh[1] *= inv_hy;
  r_mesh[2] *= inv_hz;
  
  return r_mesh;
}

Mesh3d Plasma::get_density_mesh() const
{
  size_t
    Nx = plasma.size1(),
    Ny = plasma.size2(),
    Nz = plasma.size3();
  
  Mesh3d d(Nx, Ny, Nz);
  for (size_t i=0; i<Nx; i++) {
    for (size_t j=0; j<Ny; j++) {
      for (size_t k=0; k<Nz; k++) {
	d.elem(i,j,k) = plasma.elem(i,j,k).get_number_density();
      }
    }
  }

  return d;
}

Plasma::Cell Plasma::get_state(double x /* mm */, double y /* mm */, double z /* mm */ ) const
{
  const auto &r_mesh = get_mesh_coordinates(x, y, z);
  return plasma(r_mesh[0], r_mesh[1], r_mesh[2]);
}

Plasma::Cell Plasma::get_state_bnd(double x /* mm */, double y /* mm */, double z /* mm */ ) const
{
  const auto &r_mesh = get_mesh_coordinates(x, y, z);
  if (r_mesh[0]<0.0 ||
      r_mesh[1]<0.0 ||
      r_mesh[0]>=double(plasma.size1()) ||
      r_mesh[1]>=double(plasma.size2()) ||
      r_mesh[2]>=double(plasma.size3()))
    return null_cell;
  if (r_mesh[2]<0.0)
    plasma(r_mesh[0], r_mesh[1], 0.0);
  return plasma(r_mesh[0], r_mesh[1], r_mesh[2]);
}

StaticVector<3> Plasma::get_average_velocity() const
{
  size_t
    Nx = plasma.size1(),
    Ny = plasma.size2(),
    Nz = plasma.size3();

  StaticVector<3> Vsum(0.0);
  double Ncount = 0.0;
  for (size_t i=0; i<Nx; i++) {
    for (size_t j=0; j<Ny; j++) {
      for (size_t k=0; k<Nz; k++) {
	const auto &cell = plasma.elem(i,j,k);
	const auto &V = cell.get_velocity(mass); // c
	const double ne = cell.get_number_density(); // #/m^3
	Vsum   += ne*V; // c
	Ncount += ne;
      }
    }
  }
  return Ncount != 0.0 ? Vsum / Ncount : StaticVector<3>(0.0); // c

}

double Plasma::get_average_plasma_parameter() const
{
  size_t
    Nx = plasma.size1(),
    Ny = plasma.size2(),
    Nz = plasma.size3();

  const double kbT = get_temperature(); // eV

  double Nsum = 0.0;
  double Ncount = 0.0;
  for (size_t i=0; i<Nx; i++) {
    for (size_t j=0; j<Ny; j++) {
      for (size_t k=0; k<Nz; k++) {
	const auto &cell = plasma.elem(i,j,k);
	const auto &N = cell.get_plasma_parameter(mass, kbT); // #
	const double ne = cell.get_number_density(); // #/m^3
	Nsum   += ne*N; // #
	Ncount += ne;
      }
    }
  }
  return Ncount != 0.0 ? Nsum / Ncount : 0.0; // #
}

double Plasma::get_average_debye_length() const
{
  size_t
    Nx = plasma.size1(),
    Ny = plasma.size2(),
    Nz = plasma.size3();

  const double kbT = get_temperature(); // eV

  double Lsum = 0.0;
  double Ncount = 0.0;
  for (size_t i=0; i<Nx; i++) {
    for (size_t j=0; j<Ny; j++) {
      for (size_t k=0; k<Nz; k++) {
	const auto &cell = plasma.elem(i,j,k);
	const auto &L = cell.get_debye_length(mass, kbT); // m
	const double ne = cell.get_number_density(); // #/m^3
	Lsum   += ne*L; // #
	Ncount += ne;
      }
    }
  }
  return Ncount != 0.0 ? Lsum / Ncount : 0.0; // m
}

void Plasma::project() // makes the velocity field divergence-free
{
  const size_t Nthreads = RF_Track_Globals::number_of_threads;

  size_t
    Nx = plasma.size1(),
    Ny = plasma.size2(),
    Nz = plasma.size3();
  
  // mesh has the longitudinal extension of the element and the trasnverse extension of the aperture
  StaticVector<3> min_(-rx, -ry, 0); // mm
  StaticVector<3> max_(+rx, +ry, length); // mm
  
  // working box [mm]
  const auto dim = max_ - min_;
  
  // size of the electron mesh cell [mm]
  const double hx = dim[0] / double(Nx-1); // mm
  const double hy = dim[1] / double(Ny-1); // mm
  const double hz = dim[2] / double(Nz-1); // mm
  
  // start
  Mesh3d Vx(Nx,Ny,Nz), Vy(Nx,Ny,Nz), Vz(Nx,Ny,Nz);
  for (size_t i=0; i<Nx; i++) {
    for (size_t j=0; j<Ny; j++) {
      for (size_t k=0; k<Nz; k++) {
	const auto &V = plasma.elem(i,j,k).get_velocity(mass); // c
	Vx.elem(i,j,k) = V[0];
	Vy.elem(i,j,k) = V[1];
	Vz.elem(i,j,k) = V[2];
      }
    }
  }
  
  const VectorField_divFree &Vfield = VectorField_divFree(Vx, Vy, Vz, -rx, -ry, hx, hy, hz);
  
  auto project_parallel = [&] (size_t thread, size_t start, size_t end ) -> void {
    for (size_t i=start; i<end; i++) {
      const double x = -rx + i*hx;
      for (size_t j=0; j<Ny; j++) {
	const double y = -ry + j*hy;
	for (size_t k=0; k<Nz; k++) {
	  const double z = k*hz;
	  const auto &V_new = Vfield(x,y,z); // the B field is divergence-free
	  plasma.elem(i,j,k).set_velocity(mass, V_new);
	}
      }
    }
  };
  for_all(Nthreads, Nx, project_parallel); 
}

void Plasma::advect(double dt_mm ) // dt_mm [mm/c]
{
  const size_t Nthreads = RF_Track_Globals::number_of_threads;

  // resize meshes
  size_t
    Nx = plasma.size1(),
    Ny = plasma.size2(),
    Nz = plasma.size3();

  // mesh has the longitudinal extension of the element and the trasnverse extension of the aperture
  StaticVector<3> min_(-rx, -ry, 0); // mm
  StaticVector<3> max_(+rx, +ry, length); // mm

  // working box [mm]
  const auto dim = max_ - min_;

  // size of the electron mesh cell [mm]
  const double hx = dim[0] / double(Nx-1); // mm
  const double hy = dim[1] / double(Ny-1); // mm
  const double hz = dim[2] / double(Nz-1); // mm

  // const double inv_hx = double(Nx-1) / dim[0]; // 1/mm
  // const double inv_hy = double(Ny-1) / dim[1]; // 1/mm
  // const double inv_hz = double(Nz-1) / dim[2]; // 1/mm

  // start
  bool success = true;
  auto advect_parallel = [&] (size_t thread, size_t start, size_t end ) -> void {
    const size_t Niter = 32; // max number of iterations
    for (size_t i=start; i<end; i++) {
      const double x = -rx + i*hx;
      for (size_t j=0; j<Ny; j++) {
	const double y = -ry + j*hy;
	for (size_t k=0; k<Nz; k++) {
	  const double z = k*hz;

	  // need to handle boundary conditions
	  StaticVector<3> xm(x,y,z);
	  StaticVector<3> dm(0, 0, 0); // initialize better

	  size_t n;
	  for (n=0; n<Niter; n++) {
	    const auto &xm_new = xm - dm;
	    const auto &dm_new = dt_mm * get_velocity(xm_new[0], xm_new[1], xm_new[2]);
	    const auto &dm_dlt = dm_new - dm;
	    dm = dm_new;
	    if (dot(dm_dlt,dm_dlt)<1e-12)
	      break;
	  }
	  xm -= 2*dm;

	  if (n==Niter) {
	    success = false;
	  }

	}
      }
    }
  };
  for_all(Nthreads, Nx, advect_parallel); 

  if (!success)
    std::cerr << "warning: Plasma::advect() didn't reach convergence\n";
  
}

StaticField Plasma::get_self_fields() const
{
  const size_t Nthreads = RF_Track_Globals::number_of_threads;
  
  // mesh has the longitudinal extension of the element and the trasnverse extension of the aperture
  StaticVector<3> min_(-rx, -ry, 0); // mm
  StaticVector<3> max_(+rx, +ry, length); // mm
  
  // working box [mm]
  const auto dim = max_ - min_;
  
  // resize meshes
  const int
    Nx = plasma.size1(),
    Ny = plasma.size2(),
    Nz = plasma.size3();

  // size of the electron mesh cell [mm]
  const double hx = dim[0] / double(Nx-1); // mm
  const double hy = dim[1] / double(Ny-1); // mm
  const double hz = dim[2] / double(Nz-1); // mm

  // self-generated electromagnetic field

  Mesh3d_CINT mesh_Phi = Mesh3d(Nx, Ny, Nz); // V*mm/m
  Mesh3d_CINT mesh_Ax  = Mesh3d(Nx, Ny, Nz); // T*mm
  Mesh3d_CINT mesh_Ay  = Mesh3d(Nx, Ny, Nz); // T*mm
  Mesh3d_CINT mesh_Az  = Mesh3d(Nx, Ny, Nz); // T*mm

  bool success = false;
  {
    // for padding
    const int Nxx = Nx<<1;
    const int Nyy = Ny<<1;
    const int Nzz = Nz<<1;

    fftwMesh3d mesh_rho = fftwMesh3d(Nxx, Nyy, Nzz); // charge density
    fftwMesh3d mesh_G   = fftwMesh3d(Nxx, Nyy, Nzz); // Green's function
    
    fftwComplexMesh3d mesh_rho_hat = fftwComplexMesh3d(Nxx, Nyy, Nz+1); // charge density, and also Jx, Jy, Jz
    fftwComplexMesh3d mesh_G_hat   = fftwComplexMesh3d(Nxx, Nyy, Nz+1); // Green's function
    
    // create fftw plans
    fftw_plan_with_nthreads(Nthreads);

    // Green's function
    if (fftw_plan p1 = fftw_plan_dft_r2c_3d(Nxx, Nyy, Nzz, mesh_G.data(), (fftw_complex *)mesh_G_hat.data(), FFTW_ESTIMATE)) {
      const double N  = get_average_plasma_parameter(); // #
      const double Vz = get_average_velocity()[2]; // c
      const double gamma = 1.0/sqrt(1.0-Vz*Vz); // average gamma
      if (N>1.0) { // Yukawa potential
	if (getenv("DEBUG")) {
	  std::cerr << "info: using Yukawa potential\n";
	}
	GreensFunction::RetardedYukawa yukawa;
	yukawa.r0 = get_average_debye_length()*1e3; // mm
	yukawa.g = gamma; // average gamma
	GreensFunction::compute_mesh(yukawa, mesh_G, hx, hy, hz); // 1/mm
      } else { // Coulomb potential
	if (getenv("DEBUG")) {
	  std::cerr << "info: using Coulomb potential\n";
	}
	GreensFunction::IntegratedRetardedCoulomb coulomb;
	coulomb.g = gamma; // average gamma
	GreensFunction::compute_mesh(coulomb, mesh_G, hx, hy, hz); // 1/mm
      }
      fftw_execute(p1);
      fftw_destroy_plan(p1);
    }
    
    if (fftw_plan p2 = fftw_plan_dft_r2c_3d(Nxx, Nyy, Nzz, mesh_rho.data(), (fftw_complex *)mesh_rho_hat.data(), FFTW_ESTIMATE)) {
      if (fftw_plan p3 = fftw_plan_dft_c2r_3d(Nxx, Nyy, Nzz, (fftw_complex *)mesh_rho_hat.data(), mesh_rho.data(), FFTW_ESTIMATE)) {
	
	// done with all allocations
	success = true;
	
	std::vector<std::function<double(double,double,double,double)>> assign_functions(4);
	assign_functions[0] = [] (double charge, double Vx, double Vy, double Vz ) { return charge; }; // charge density
	assign_functions[1] = [] (double charge, double Vx, double Vy, double Vz ) { return charge * Vx; }; // current density
	assign_functions[2] = [] (double charge, double Vx, double Vy, double Vz ) { return charge * Vy; }; // current density
	assign_functions[3] = [] (double charge, double Vx, double Vy, double Vz ) { return charge * Vz; }; // current density
	
	for (size_t id=0; id<4; id++) { // id: 0: rho; 1: Jx; 2: Jy; 3: Jz
	  mesh_rho = 0.0;
	  for (int i=0; i<Nx; i++) {
	    for (int j=0; j<Ny; j++) {
	      for (int k=0; k<Nz; k++) {
		const auto &cell = plasma.elem(i,j,k);
		const double N = cell.get_number_density() * hx*hy*hz / 1e9; // #/mm^3, number of particles in mesh cell
		const auto  &V = cell.get_velocity(mass);
		mesh_rho.elem(i,j,k) = assign_functions[id](Q*N, V[0], V[1], V[2]);
	      }
	    }
	  }
	  fftw_execute(p2);
	  
	  // convolution between (1) charge distribution (2) greens function (3) rectangular function transform rect(x/hx)
	  for (int i=0; i<Nxx; i++) {
	    double wx = double((i<=Nx) ? i : (i-Nxx)) / Nxx; // rad
	    for (int j=0; j<Nyy; j++) {
	      double wy = double((j<=Ny) ? j : (j-Nyy)) / Nyy; // rad
	      for (int k=0; k<Nz+1; k++) {
		double wz = double((k<=Nz) ? k : (k-Nzz)) / Nzz; // rad
		mesh_rho_hat.elem(i,j,k) *= mesh_G_hat.elem(i,j,k) * sinc(wx) * sinc(wy) * sinc(wz); // convolve both with the green function and with a uniform distribution over one cell (see rectpulse_fft.lyx)
	      }
	    }
	  }
	  
	  // inverse Fourier Transform to find the potential
	  fftw_execute(p3);
	  mesh_rho /= 8.0*double(Nx*Ny*Nz);
	  
	  auto copy_mesh = [&Nx,&Ny,&Nz] (const fftwMesh3d &from, TMesh3d<double> &to ) {
	    for (int i=0; i<Nx; i++)
	      for (int j=0; j<Ny; j++)
		for (int k=0; k<Nz; k++)
		  to.elem(i,j,k) = from.elem(i,j,k);
	  };
	  
	  if      (id==0) copy_mesh(mesh_rho,mesh_Phi);
	  else if (id==1) copy_mesh(mesh_rho,mesh_Ax);
	  else if (id==2) copy_mesh(mesh_rho,mesh_Ay);
	  else            copy_mesh(mesh_rho,mesh_Az);
	}
	
	// e / epsilon0 / mm = (1 / 55.2634959906581) V*mm/m
	// e / epsilon0 / mm / c = (1 / 16567579300.7125) T*mm
	mesh_Phi /= 55.2634959906581; // V*mm/m
	mesh_Ax /= 16567579300.7125; // T*mm
	mesh_Ay /= 16567579300.7125; // T*mm
	mesh_Az /= 16567579300.7125; // T*mm

	fftw_destroy_plan(p3);
      }
      fftw_destroy_plan(p2);
    }
  }
  
  if (!success) {
    std::cerr << "error: failed to compute Plasma's self fields\n";
    exit(1);
  }
  
  return StaticField(mesh_Phi,
		     mesh_Ax,
		     mesh_Ay,
		     mesh_Az,
		     -rx/1e3, // m
		     -ry/1e3, // m
		     hx/1e3, // m
		     hy/1e3, // m
		     hz/1e3, // m
		     length/1e3); // length m
}

void Plasma::apply_momentum_through_dS(const TMesh3d<StaticVector<3>> &dP, double dS_mm ) // dt_mm [mm/c]; dP [MeV/c] mesh
{

}

void Plasma::apply_momentum_through_dt(const TMesh3d<StaticVector<3>> &dP, double dt_mm ) // dt_mm [mm/c]; dP [MeV/c] mesh
{
  const size_t Nthreads = RF_Track_Globals::number_of_threads;

  // mesh has the longitudinal extension of the element and the trasnverse extension of the aperture
  StaticVector<3> min_(-rx, -ry, 0); // mm
  StaticVector<3> max_(+rx, +ry, length); // mm

  // working box [mm]
  const auto dim = max_ - min_;

  // resize meshes
  size_t
    Nx = plasma.size1(),
    Ny = plasma.size2(),
    Nz = plasma.size3();

  // size of the electron mesh cell [mm]
  const double hx = dim[0] / double(Nx-1); // mm
  const double hy = dim[1] / double(Ny-1); // mm
  const double hz = dim[2] / double(Nz-1); // mm

  // const double inv_hx = double(Nx-1) / dim[0]; // 1/mm
  // const double inv_hy = double(Ny-1) / dim[1]; // 1/mm
  // const double inv_hz = double(Nz-1) / dim[2]; // 1/mm

  // volume of a cell, in m^3
  const double Vol_m3 = hx*hy*hz / 1e9; // m^3
  
  // apply half external force
  auto apply_external_force_parallel = [&] (size_t thread, size_t start, size_t end ) -> void {
    for (int i=(int)start; i<(int)end; i++) {
      for (int j=0; j<(int)Ny; j++) {
	for (int k=0; k<(int)Nz; k++) {
	  const auto &dP_ext = 0*dP.elem(i,j,k); // MeV/c  <<<<<<<<<<<<<<<<<<< PROBLEM: with *any* external force, the algorithm diverges...
	  if (gsl_isnan(dP_ext[0]) ||
	      gsl_isnan(dP_ext[1]) ||
	      gsl_isnan(dP_ext[2])) {
	    std::cerr << "error: Nan in Electron Coooling computation (mesh) @ S = " << k*hz << " mm\n";
	    exit(1); 		
	  }
	  auto &state_curr = plasma.elem(i,j,k);
	  auto &state_next = plasma_next.elem(i,j,k);
	  const auto &P = state_curr.get_momentum(); // MeV/c
	  const auto &Ne = state_curr.get_number_density(); ///< [#] number of paricles per mesh m**3
	  ParticleT particle_fwd;
	  particle_fwd.mass = mass; ///< [MeV/c/c]
	  particle_fwd.Q = Q; ///< [e+] charge of each particle
	  particle_fwd.N = Vol_m3 * Ne; ///< [#] number of paricles per mesh cell
	  particle_fwd.X = 0.0; ///< [mm]
	  particle_fwd.Y = 0.0; ///< [mm]
	  particle_fwd.S = 0.0; ///< [mm]
	  particle_fwd.Px = P[0]; ///< [MeV/c]
	  particle_fwd.Py = P[1]; ///< [MeV/c]
	  particle_fwd.Pz = P[2]; ///< [MeV/c]
	  //ParticleT particle_bwd = particle_fwd;
	  const auto &F_ext = dP_ext * 1e3 / dt_mm; // MeV/m
	  apply_force_through_Bfield(particle_fwd, F_ext, static_Bfield,  dt_mm);
	  //apply_force_through_Bfield(particle_bwd, F_ext, static_Bfield, -dt_mm);
	  // forward, the new momentum
	  const auto &P_new = particle_fwd.get_Px_Py_Pz(); // MeV/c
	  state_curr[1] = P_new[0];
	  state_curr[2] = P_new[1];
	  state_curr[3] = P_new[2];
	  // backward, the old position
	  state_next[1] = -particle_fwd.X; // -dX
	  state_next[2] = -particle_fwd.Y;
	  state_next[3] = -particle_fwd.S;
	}
      }
    }
  };
  
  // apply half force and project
  for_all(Nthreads, Nx, apply_external_force_parallel); 
  // project();
  //check_divergence();

  auto advect_parallel = [&] (size_t thread, size_t start, size_t end ) -> void {
    for (int i=(int)start; i<(int)end; i++) {
      for (int j=0; j<(int)Ny; j++) {
	for (int k=0; k<(int)Nz; k++) {
	  auto &state_next = plasma_next.elem(i,j,k); // contains the -dX
	  const auto &dX = StaticVector<3>(state_next[1], // this is not really momentum... it's -dX (see above).
					   state_next[2],
					   state_next[3]);
	  auto X_prev = StaticVector<3>(i*hx+hx/2, j*hy+hy/2, k*hz+hz/2) + dX; // backtrack
	  const auto &state_prev = get_state_bnd(X_prev[0], X_prev[1], X_prev[2]);
	  state_next = state_prev;
	}
      }
    }
  };
  // advance by one dt_mm
  for_all(Nthreads, Nx, advect_parallel);
  std::swap(plasma, plasma_next);

  // drift by dt_mm
  // advect(dt_mm);

  // apply half force and project
  // for_all(Nthreads, Nx, apply_half_external_force_parallel); 
  //////// project();
  //check_divergence();
}
