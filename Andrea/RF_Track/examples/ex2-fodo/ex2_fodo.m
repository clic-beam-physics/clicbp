#! /opt/local/bin/octave -q --persist

setenv("GNUTERM","X11");
addpath('tools')
addpath('../..')

% Load TULIP Code

RF_Track;

% initialise the bunch
Bunch.mass = 938.272046; % MeV/c/c
Bunch.population = 2e9; % nb. of particles per bunch
Bunch.charge = +1; % units of e+
Bunch.ekinetic = 76; % MeV
Bunch.etotal = Bunch.ekinetic + Bunch.mass; % MeV
Bunch.Pc = sqrt(Bunch.etotal**2 - Bunch.mass**2); % MeV
Bunch.beta = Bunch.Pc / Bunch.etotal;
Bunch.gamma = Bunch.etotal / Bunch.mass;
Bunch.espread = 0.01; % percent
Bunch.sigmaz = 0.1; % mm

% prepares the twiss parameters structure (for a bunch with 100
% micron trasnverse size in x and y)
BunchT = Bunch6d_twiss();
BunchT.Pc = Bunch.Pc; % MeV
BunchT.emitt_x = 0.00051282051; % mm.mrad
BunchT.emitt_y = 0.0010869565; % mm.mrad
BunchT.emitt_z = Bunch.beta * Bunch.gamma * Bunch.sigmaz * (Bunch.Pc * Bunch.espread / 100) * 1e3; % mm.keV
BunchT.alpha_x = 0.0;
BunchT.alpha_y = 0.0;
BunchT.alpha_z = 0.0;
BunchT.beta_x = 7.84311; % mm/pi.mrad
BunchT.beta_y = 3.67714; % mm/pi.mrad
BunchT.beta_z = Bunch.sigmaz ** 2 / BunchT.emitt_z * Bunch.beta * Bunch.gamma; % mm/keV

B0 = Bunch6d(Bunch.mass, ...
             Bunch.population, ...
             Bunch.charge, ...
             BunchT, 50000);

% setup the elements
D = Drift(1.6);
D.set_aperture_x(2.5e-3);
D.set_aperture_y(2.5e-3);

Qf = Quadrupole(0.40 * 0.5,  0.4 * Bunch.Pc * 0.5);
Qd = Quadrupole(0.40 * 0.5, -0.4 * Bunch.Pc * 0.5);

% setup the lattice
FODO = Lattice();
FODO.append(Qf);
FODO.append(D);
FODO.append(Qd);
FODO.append(Qd);
FODO.append(D);
FODO.append(Qf);

L = Lattice();
L.append(FODO);
L.append(FODO);
L.append(FODO);
L.append(FODO);

% track
B1 = L.track(B0);
P1 = B1.get_average_particle();

T = L.get_transport_table("%S %beta_x %beta_y %alpha_x %alpha_y");

% do some plots
clf
hold on
plot(T(:,1), T(:,2), 'b-+');
plot(T(:,1), T(:,3), 'r-+');
xlabel('s [m]');
ylabel('beta_{x|y} [m]');


% do some other plots

figure

U = L.get_transport_table("%S %sigma_x %sigma_y");

hold on
plot(U(:,1), U(:,2), 'b-+');
plot(U(:,1), U(:,3), 'r-+');
xlabel('s [m]');
ylabel('sigma_{x|y} [mm]');

figure

V = L.get_transport_table("%S %emitt_x %emitt_y");

hold on
plot(V(:,1), V(:,2), 'b-+');
plot(V(:,1), V(:,3), 'r-+');
xlabel('s [m]');
ylabel('emitt_{x|y} [mm.mrad]');

figure

B = B1.get_phase_space("%S %Z %dt %d %E");

hold on
plot(B(:,2), B(:,4) * 100, 'r.');
xlabel('z [mm]');
ylabel('dP/P [%]');

figure 

% placet-like phase space
plot(B(:,3), B(:,5), 'r.');
xlabel('dt [mm/c]');
ylabel('E [MeV]');
