/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef conversion_hh
#define conversion_hh

#include "constants.hh"

static inline double deg2rad(const double &x )
{
  return x*M_PI/180.0;
}

static inline double rad2deg(const double &x )
{
  return x*180.0/M_PI;
}

static inline double freq2lambda(const double &x ) // GHz to meters
{
  return C_LIGHT/(x*1e9);
}

static inline double lambda2freq(const double &x ) // meters to GHz
{
  return C_LIGHT/(x*1e9);
}

#endif /* conversion_hh */
