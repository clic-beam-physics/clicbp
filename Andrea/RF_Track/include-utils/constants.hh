/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef constants_hh
#define constants_hh

#include <cmath>

/** physical constants from http://physics.nist.gov/cuu/Constants/index.html */
#define EMASS 0.5109989461 /** electron mass [MeV/c/c] */
#define PMASS 938.2720813 /** proton mass [MeV/c/c] */
#define C_LIGHT 299792458.0 /** velocity of light [m/s] */
#define RE 2.8179403227e-15 /** classical electron radius [m] */
#define ECHARGE 1.6021766208e-19 /** electron charge [C] */
#define Z0 (C_LIGHT*4e-7*M_PI) /** characteristic impedance [Ohm] */
#define ALPHA_EM (1.0/137.035999139) /** fine structure constant */
#define HBAR 6.58211974283871e-25 /** reduced Planck constant [GeV s] */
#define ENERGY_LOSS 1.40792839259847e-05 /** e * e / 6 / pi / epsilon0 / m / ((electronmass * c * c / GeV)**4) in GeV */

#endif /* constants_hh */
