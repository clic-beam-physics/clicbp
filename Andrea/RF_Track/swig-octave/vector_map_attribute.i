/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#if defined(SWIGOCTAVE)
%typemap(in, numinputs=0) std::vector<std::map<std::string,Attribute> > & (std::vector<std::map<std::string,Attribute> > temp ) {
  $1 = &temp;
}

%typemap(argout) std::vector<std::map<std::string,Attribute> > & {
  Cell res($1->size(),1);
  for (size_t i=0; i<$1->size(); i++) {
    Octave_map table;
    const std::map<std::string,Attribute> &attributes = (*$1)[i];
    for (std::map<std::string,Attribute>::const_iterator itr=attributes.begin(); itr!=attributes.end(); ++itr) {
      const std::string &key = (*itr).first;
      const Attribute &attribute = (*itr).second;
      switch(attribute.type()) {
      case OPT_INT: table.assign(key, int(attribute)); break;
      case OPT_UINT: table.assign(key, size_t(attribute)); break;
      case OPT_BOOL: table.assign(key, bool(attribute) ? 1: 0); break;
      case OPT_DOUBLE: table.assign(key, double(attribute)); break;
      case OPT_STRING: table.assign(key, std::string(attribute)); break;
      case OPT_COMPLEX: table.assign(key, attribute.get_complex()); break;
      }
    }
    res(i,0)=table;
  }
  $result->append(octave_value(res));
}

#endif
