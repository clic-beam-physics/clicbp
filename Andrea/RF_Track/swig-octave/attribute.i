/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

/* attribute.i */

%{
#include "attribute.hh"
%}

#if defined(SWIGOCTAVE)

// Octave array or cell -> C++ Attribute
%typemap(in) const Attribute & {
  $1 = new Attribute;
  if ($input.is_string()) {
    *$1 = Attribute($input.string_value());
  } else if ($input.is_integer_type()) {
    const Array<int> &array = $input.int_vector_value();
    *$1 = Attribute(int(array(0)));
  } else if ($input.is_bool_type()) {
    const boolMatrix &array = $input.bool_matrix_value();
    *$1 = Attribute(array(0));
  } else if ($input.is_real_type()) {
    const Matrix &array = $input.matrix_value();
    size_t size1 = array.rows();
    size_t size2 = array.columns();
    if (size1==1&&size2==1) {
      *$1 = Attribute(array(0));
    } else {
      MatrixNd matrix(size1, size2);
      for (size_t i=0; i<size1; i++)
	for (size_t j=0; j<size2; j++)
	  matrix[i][j] = array(i,j);
      *$1 = Attribute(matrix);
    }
  } else if ($input.is_complex_type()) {
    const ComplexMatrix &array = $input.complex_matrix_value();
    *$1 = Attribute(array(0));
  } else {
    error("Unknown type in attributes list");
  }
}

%typemap(freearg) const Attribute & {
   if ($1) delete $1;
}

%typemap(typecheck) const Attribute & {
  octave_value obj = $input;
  $1 = obj.is_string() ||
    obj.is_integer_type() ||
    obj.is_bool_type() ||
    obj.is_real_type() ||
    obj.is_complex_type() ? 1 : 0;
}

%typemap(argout,noblock=1) const Attribute & {
}

// C++ Attribute -> Octave array (as an output argument)
%typemap(in, numinputs=0) Attribute & (Attribute temp ) {
  $1 = &temp;
}

%typemap(argout) Attribute & {
  Attribute &attribute = *$1;
  octave_value ret;
  switch(attributes.type()) {
  case OPT_INT: ret = int(attributes[i]); break;
  case OPT_UINT: ret = size_t(attributes[i]); break;
  case OPT_BOOL: ret = bool(attributes[i]); break;
  case OPT_DOUBLE: ret = double(attributes[i]); break;
  case OPT_MATRIX: {
    const MatrixNd &matrix = attributes[i].get_matrix();
    Matrix _matrix(matrix.size1(), matrix.size2());
    for (size_t i=0; i<matrix.size1(); i++)
      for (size_t j=0; j<matrix.size2(); j++)
	_matrix(i,j) = matrix[i][j];
    ret = octave_value(_matrix);
  } break;
  case OPT_STRING: ret = std::string(attributes[i]); break;
  case OPT_COMPLEX: ret = attributes[i].get_complex(); break;
  }
  $result->append(ret); 
}

%typemap(freearg,noblock=1) Attribute & {
}

#endif
