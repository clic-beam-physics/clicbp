/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#if defined(SWIGOCTAVE)

// Vector of strings as input argument
%typemap(in) const std::vector<std::string> & {
  if ($input.is_string()) {
    if (($1 = new std::vector<std::string>(1))) {
      (*$1)[0] = $input.string_value();
    }
  } else if ($input.is_cell()) {
    if (($1 = new std::vector<std::string>($input.length()))) {
      string_vector array = $input.all_strings();
      for (int i=0; i<array.length(); i++)
        (*$1)[i] = array(i);
    }
  }
}

%typemap(freearg) const std::vector<std::string> & {
  if ($1) delete $1;
}

%typemap(typecheck) const std::vector<std::string> & {
  octave_value obj = $input;
  $1 = obj.is_string() || obj.is_cell() ? 1 : 0;
}

%typemap(argout,noblock=1) const std::vector<std::string> & {
}

// Vector of strings as output argument
%typemap(in, numinputs=0) std::vector<std::string> & (std::vector<std::string> temp ) {
  $1 = &temp;
}

%typemap(argout) std::vector<std::string> & {
  std::vector<std::string> &strings = *$1;
  string_vector ret(strings.size()); 
  for (size_t i=0; i<strings.size(); i++)
    ret(i) = strings[i];
  $result->append(ret);
}

%typemap(freearg,noblock=1) std::vector<std::string> & {
}

#endif
