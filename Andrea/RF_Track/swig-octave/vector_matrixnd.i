/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

/* matrixnd.i */

#if defined(SWIGOCTAVE)

// T_ijk as an output argument
%typemap(in, numinputs=0) std::vector<MatrixNd> & (std::vector<MatrixNd> temp ) {
  $1 = &temp;
}

%typemap(argout) std::vector<MatrixNd> & {
  dim_vector dv3;
  dv3.resize(3);
  dv3.elem(0)=6;              
  dv3.elem(1)=6;
  dv3.elem(2)=6;
  NDArray T(dv3);
  for (int i=0; i<6; i++) {
    for (int j=0; j<6; j++) {
      for (int k=0; k<6; k++) {
        T(i,j,k) = (*$1)[i][j][k];
      }
    }
  }
  $result->append(T);
}

#endif
