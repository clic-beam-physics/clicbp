/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <cmath>
#include <assert.h>
#include <cstdio>
#include <sstream>
#include <limits>
#include <unordered_map>

#include "RF_Track.hh"
#include "stats.hh"
#include "bunch6d.hh"
#include "bunch6dt.hh"
#include "for_all.hh"
#include "matrixnd.hh"
#include "numtools.hh"
#include "get_token.hh"
#include "conversion.hh"
#include "particle_key.hh"
#include "relativistic_velocity_addition.hh"

Bunch6d::Bunch6d(double mass_, double population_, double charge_, const MatrixNd &X ) : S(0.0)
{
  if (X.columns()==6) {
    set_phase_space(X);
    const double particles_per_macroparticle = population_ / X.rows();
    for (auto &particle: particles) {
      particle.mass = mass_;
      particle.Q = charge_;
      particle.N = particles_per_macroparticle;
    }
  } else {
    throw "Bunch6d() requires a 6-column matrix for its initialization\n";
  }
}

Bunch6d::Bunch6d(double mass_, double population_, double charge_, const Bunch6d_twiss &T, size_t N, double sigma_cut ) : S(0.0), particles(N)
{
  // creates M, the beam matrix in normalized coordinates
  std::vector<MatrixNd> M(3, MatrixNd(2,N));
  {
    const double beta_gamma = T.Pc / mass_;
    const double sqrt_emitt[3] = { // sqrt(geometric emittances)
      sqrt(T.emitt_x / beta_gamma), // sqrt(mm.mrad)
      sqrt(T.emitt_y / beta_gamma), // sqrt(mm.mrad)
      sqrt(T.emitt_z / beta_gamma)  // sqrt(mm.permille)
    };
    auto ran_gaussian_n_sigma = [] (double sigma, double sigma_cut ) {
      if (sigma_cut==-1.0)
	return gsl_ran_gaussian(RF_Track_Globals::rng,sigma);
      double t;
      while(fabs(t=gsl_ran_gaussian(RF_Track_Globals::rng, 1.0))>sigma_cut) {}
      return t*sigma;
    };
    for (int n=0; n<3; n++) {
      M[n][0][0] = 0.0;
      M[n][1][0] = 0.0;
      CumulativeKahanSum<double> sum[2] = { 0.0 };
      for (size_t i = 1; i < N; i++) {
	sum[0] += (M[n][0][i] = ran_gaussian_n_sigma(sqrt_emitt[n],sigma_cut));
	sum[1] += (M[n][1][i] = ran_gaussian_n_sigma(sqrt_emitt[n],sigma_cut));
      }
      for (size_t i = 1; i < N; i++) {
	M[n][0][i] = (M[n][0][i] * (N-1) - sum[0]) / (N-1);
	M[n][1][i] = (M[n][1][i] * (N-1) - sum[1]) / (N-1);
      }
    }
  }
  // creates A, the transformation matrices from normalized to standard coordinates
  {
    std::vector<MatrixNd> A(3, MatrixNd(2,2));
    {
      const double beta[3] = {
	T.beta_x, // m/rad
	T.beta_y, // m/rad
	T.beta_z  // m
      };
      const double alpha[3] = {
	T.alpha_x,
	T.alpha_y,
	T.alpha_z
      };
      for (int n=0; n<3; n++) {
	A[n][0][0] = sqrt(beta[n]); // sqrt(m/rad)
	A[n][0][1] = 0.0;
	if (fabs(A[n][0][0]) > std::numeric_limits<double>::epsilon()) {
	  A[n][1][0] = -alpha[n]/A[n][0][0];
	  A[n][1][1] = 1.0/A[n][0][0];
	} else {
	  A[n][1][0] = 0.0;
	  A[n][1][1] = 0.0;
	}
      }
    }
    // transforms the beam
    for (int n=0; n<3; n++) {
      M[n] = A[n] * M[n];
    }
  }
  // copies the particles to the beam structure
  const double particles_per_macroparticle = population_ / N;
  for (size_t i=0; i<N; i++) {
    const double
      &x  = M[0][0][i], // mm
      &xp = M[0][1][i], // mrad
      &y  = M[1][0][i], // mm
      &yp = M[1][1][i], // mrad
      &d  = M[2][1][i], // 1000
      &z  = M[2][0][i] + T.disp_z * d, // mm
      &Pc = T.Pc * (1e3 + d) / 1e3; // MeV/c
    const double &Pz = Pc * 1e3 / hypot(1e3, xp, yp); // MeV/c
    const double &Vz = Pz / hypot(mass_, Pc); // c
    Particle &particle = particles[i];
    // transport each particle to the plane S=0 (i.e. via a drift by -z);
    particle.x  = x - xp * z / 1e3 + T.disp_x * d;
    particle.xp = xp + T.disp_xp * d;
    particle.y  = y - yp * z / 1e3 + T.disp_y * d;
    particle.yp = yp + T.disp_yp * d;
    particle.t  = -z / Vz;
    particle.Pc = Pc;
    particle.mass = mass_;
    particle.Q = charge_;
    particle.N = particles_per_macroparticle;
  }
}

Bunch6d::Bunch6d(const Bunch6dT &b, const char *select )
{
  const bool all = strncmp(select, "all", 4) == 0;
  S = b.get_S(); // m
  const double S_mm = S*1e3;
  if (all) {
    particles.resize(b.size());
  } else {
    size_t count = 0;
    for (size_t i=0; i<b.size(); i++) {
      if (b.t >= b.get_particle(i).t0)
	count++;
    }
    particles.resize(count);
  }
  size_t i = 0;
  for (const auto &particleT : b.particles) {
    if (all || b.t>=particleT.t0) {
      const double
	dS = particleT.S - S_mm, // mm
	E  = particleT.get_total_energy(), // MeV
	xp = particleT.Px / particleT.Pz, // rad
	yp = particleT.Py / particleT.Pz, // rad
	Vz = particleT.Pz / E, // c
	x  = particleT.X - dS * xp, // mm
	y  = particleT.Y - dS * yp, // mm
	dt = -dS / Vz; // mm/c
      Particle &particle = particles[i++];
      particle.x  = x;
      particle.y  = y;
      particle.xp = xp * 1e3;
      particle.yp = yp * 1e3;
      particle.t  = b.t + dt;
      particle.Pc = particleT.get_Pc();
      particle.mass = particleT.mass;
      particle.Q = particleT.Q;
      particle.N = particleT.N;
      if (!particleT) {
	particle.lost_at(particleT.S/1e3);
      }
    }
  }
}

Bunch6d::Bunch6d(const MatrixNd &X ) : S(0.0)
{
  if (X.columns()==8 || X.columns()==9) {
    bool has_N = X.columns()==9;
    particles.resize(X.rows());
    for (size_t i=0; i<X.rows(); i++) {
      Particle &particle = particles[i];
      const double
	x    = X[i][0], // mm
	xp   = X[i][1], // mrad
	y    = X[i][2], // mm
	yp   = X[i][3], // mrad
	t    = X[i][4], // mm/c
	Pc   = X[i][5], // MeV/c
	mass = X[i][6], // MeV/c/c
	Q    = X[i][7], // e+
	N    = has_N ? X[i][8] : 1.0;
      particle.mass = mass;
      particle.Q  = Q;
      particle.N  = N;
      particle.x  = x;
      particle.xp = xp;
      particle.y  = y;
      particle.yp = yp;
      particle.t  = t;
      particle.Pc = Pc;
    }
  } else {
    throw "Bunch6d::Bunch6d() requires an 8 or 9-column matrix as an input\n";
  }
}

StaticVector<3> Bunch6d::get_center_of_mass_Vx_Vy_Vz() const
{
  StaticVector<3> Psum(0.0);
  double Esum = 0.0;
  for (const auto &particle : particles) {
    if (particle) {
      Psum += particle.N * particle.get_Px_Py_Pz();
      Esum += particle.N * particle.get_total_energy();
    }
  }
  return Esum != 0.0 ? Psum / Esum : StaticVector<3>(0.0);
}

StaticVector<3> Bunch6d::get_average_Vx_Vy_Vz() const
{
  StaticVector<3> Vsum(0.0);
  double Nsum = 0.0;
  for (const auto &particle : particles) {
    if (particle) {
      Vsum += particle.N * particle.get_Vx_Vy_Vz();
      Nsum += particle.N;
    }
  }
  return Nsum != 0.0 ? Vsum / Nsum : StaticVector<3>(0.0);
}

Particle Bunch6d::get_average_particle() const
{
  Particle p(0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
  if (!particles.empty()) {
    CumulativeKahanSum<double> sum[9];
    size_t ngood = 0;
    for (const Particle &particle : particles) {
      if (particle) {
	sum[0] += particle.x;
	sum[1] += particle.xp;
	sum[2] += particle.y;
	sum[3] += particle.yp;
	sum[4] += particle.t;
	sum[5] += particle.Pc;
	sum[6] += particle.mass;
	sum[7] += particle.Q;
	sum[8] += particle.N;
	ngood++;
      }
    }
    if (ngood!=0) {
      p.x  = sum[0] / ngood;
      p.xp = sum[1] / ngood;
      p.y  = sum[2] / ngood;
      p.yp = sum[3] / ngood;
      p.t  = sum[4] / ngood;
      p.Pc = sum[5] / ngood;
      p.mass = sum[6] / ngood;
      p.Q  = sum[7] / ngood;
      p.N  = sum[8] / ngood;
    }
  }
  return p;
}

double Bunch6d::get_reference_momentum() const
{
  if (particles[0])
    return particles[0].Pc; // MeV/c
  std::cerr << "warning: the beam's first particle was lost, using the beam centroid as reference particle.\n";
  return get_average_particle().Pc; // MeV/c
}

Particle Bunch6d::get_average_particle_t0(double dS ) const // returns the average particle at t = t0 (that is when <z> = dS [m] )
{
  Particle p(0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
  if (!particles.empty()) {
    CumulativeKahanSum<double> sum[8];
    const double t0 = get_t(dS);
    size_t ngood = 0;
    for (const Particle &particle : particles) {
      if (particle) {
	const double dt = particle.t - t0;
	auto const &v = particle.get_Vx_Vy_Vz();
	sum[0] += particle.x - dt * v[0];
	sum[1] += particle.xp;
	sum[2] += particle.y - dt * v[1];
	sum[3] += particle.yp;
	sum[4] += particle.Pc;
	sum[5] += particle.mass;
	sum[6] += particle.Q;
	sum[7] += particle.N;
	ngood++;
      }
    }
    if (ngood!=0) {
      p.x  = sum[0] / ngood;
      p.xp = sum[1] / ngood;
      p.y  = sum[2] / ngood;
      p.yp = sum[3] / ngood;
      p.Pc = sum[4] / ngood;
      p.mass = sum[5] / ngood;
      p.Q  = sum[6] / ngood;
      p.N  = sum[7] / ngood;
      p.t  = t0;
    }
  }
  return p;
}

StaticVector<3> Bunch6d::get_bunch_temperature()
{
  // accumulate velocoty variances discriminating by ParticleKey (mass, charge)
  std::unordered_map<ParticleKey,Weighted_incremental_variance,ParticleKeyHasher,ParticleKeyEquals> K_variance;
  {
    for (auto const &particle : particles) {
      if (particle) {
	const auto key = ParticleKey{ particle.mass, particle.Q };
	const auto P = particle.get_Px_Py_Pz();
	const auto K = StaticVector<3>(P[0]*P[0], P[1]*P[1], P[2]*P[2]) / (2*particle.mass);
	K_variance[key].append(K, particle.N);
      }
    }
  }
  
  // sums the temperatures Tx, Ty, Tz, for each ParticleKey
  StaticVector<3> retval(0.0);
  for (auto const &vv : K_variance) {
    retval += vv.second.variance();
  }

  return 1e6*retval; // eV
}

double Bunch6d::get_t(double dS ) const // returns the time such that <Z> = dS [m]
{
  CumulativeKahanSum<double> sum_t_Vz;
  CumulativeKahanSum<double> sum_Vz;
  for (auto const &particle: particles) {
    if (particle) {
      const auto v = particle.get_Vx_Vy_Vz();
      sum_t_Vz += particle.t * v[2] + dS * 1e3;
      sum_Vz += v[2];
    }
  }
  return sum_Vz != 0.0 ? sum_t_Vz / sum_Vz : 0.0;
}

double Bunch6d::get_t_min() const
{
  double t_min = std::numeric_limits<double>::max();
  for (auto const &particle: particles) {
    if (particle) {
      if (particle.t<t_min) {
	t_min = particle.t;
      }
    }
  }
  return t_min;
}

double Bunch6d::get_t_max() const
{
  double t_max = -std::numeric_limits<double>::max();
  for (auto const &particle: particles) {
    if (particle) {
      if (particle.t>t_max) {
	t_max = particle.t;
      }
    }
  }
  return t_max;
}

void Bunch6d::set_phase_space(const MatrixNd &X ) // takes %x %xp %y %yp %t %Pc (larger %t means later arrival)
{
  if (X.columns()==6) {
    size_t N = X.rows();
    particles.resize(N);
    for (size_t i=0; i<N; i++) {
      const double
	&x  = X[i][0], // mm
	&xp = X[i][1], // mrad
	&y  = X[i][2], // mm
	&yp = X[i][3], // mrad
	&t  = X[i][4], // mm/c
	&Pc = X[i][5]; // MeV
      Particle &particle = particles[i];
      particle.x  = x;
      particle.xp = xp;
      particle.y  = y;
      particle.yp = yp;
      particle.t  = t;
      particle.Pc = Pc;
    }
  } else {
    throw "Bunch6d::set_phase_space() requires a 6-column matrix as an input\n";
  }
}

Bunch6d_info Bunch6d::get_info() const
{
  double *w = new double[particles.size()];
  assert(w);
  double r2max = 0.0;
  for (size_t i=0; i<particles.size(); i++) {
    const Particle &particle = particles[i];
    if (particle) {
      w[i] = 1.0;
      double r2 = particle.x*particle.x + particle.y*particle.y;
      if (r2>r2max)
	r2max = r2;
    } else {
      w[i] = 0.0;
    }
  }
  constexpr size_t particle_stride = sizeof(Particle) / sizeof(double);
  const double rmax = sqrt(r2max);
  const Particle P0 = get_average_particle();
  const auto V0 = P0.get_Vx_Vy_Vz();
  const double var_x = gsl_stats_wvariance_m(w, 1, &particles[0].x, particle_stride, particles.size(), P0.x); // mm**2
  const double var_y = gsl_stats_wvariance_m(w, 1, &particles[0].y, particle_stride, particles.size(), P0.y); // mm**2
  const double var_t = gsl_stats_wvariance_m(w, 1, &particles[0].t, particle_stride, particles.size(), P0.t); // [mm/c]**2
  const double var_xp = gsl_stats_wvariance_m(w, 1, &particles[0].xp, particle_stride, particles.size(), P0.xp); // mrad**2
  const double var_yp = gsl_stats_wvariance_m(w, 1, &particles[0].yp, particle_stride, particles.size(), P0.yp); // mrad**2
  const double var_P = gsl_stats_wvariance_m(w, 1, &particles[0].Pc, particle_stride, particles.size(), P0.Pc); // (MeV/c)**2
  const double var_z = var_t * V0[2] * V0[2]; // mm**2
  const double beta0_gamma0 = P0.Pc / P0.mass;
  Bunch6d_info info;
  info.S = S;
  info.mean_x = P0.x; // mm
  info.mean_y = P0.y; // mm
  info.mean_t = P0.t; // mm/c
  info.mean_P = P0.get_Pc();  // average momentum in MeV
  info.mean_E = P0.get_total_energy(); // average total energy in MeV
  info.mean_xp = P0.xp; // mrad
  info.mean_yp = P0.yp; // mrad
  info.mean_K = info.mean_E - P0.mass; // average kinetic energy in MeV
  if (particles.size()>1) {
    info.sigma_x = sqrt(var_x); // mm
    info.sigma_y = sqrt(var_y); // mm
    info.sigma_z = sqrt(var_z); // mm
    info.sigma_t = sqrt(var_t); // mm/c
    info.sigma_xp = sqrt(var_xp); // mrad
    info.sigma_yp = sqrt(var_yp); // mrad
    info.sigma_xxp = stats_wcovariance_m(w, 1, &particles[0].x, particle_stride, &particles[0].xp, particle_stride, particles.size(), P0.x, P0.xp); // mm*mrad
    info.sigma_yyp = stats_wcovariance_m(w, 1, &particles[0].y, particle_stride, &particles[0].yp, particle_stride, particles.size(), P0.y, P0.yp); // mm*mrad
    MatrixNd sigma_4d(4,4);
    sigma_4d[0][0] = var_x; // mm**2
    sigma_4d[0][1] = sigma_4d[1][0] = info.sigma_xxp; // mm*mrad
    sigma_4d[0][2] = sigma_4d[2][0] = stats_wcovariance_m(w, 1, &particles[0].x, particle_stride, &particles[0].y,  particle_stride, particles.size(), P0.x, P0.y); // mm**2
    sigma_4d[0][3] = sigma_4d[3][0] = stats_wcovariance_m(w, 1, &particles[0].x, particle_stride, &particles[0].yp, particle_stride, particles.size(), P0.x, P0.yp); // mm*mrad
    sigma_4d[1][1] = var_xp; // mrad**2
    sigma_4d[2][1] = sigma_4d[1][2] = stats_wcovariance_m(w, 1, &particles[0].y, particle_stride, &particles[0].xp, particle_stride, particles.size(), P0.y, P0.xp); // mm*mrad
    sigma_4d[3][1] = sigma_4d[1][3] = stats_wcovariance_m(w, 1, &particles[0].yp, particle_stride, &particles[0].xp,  particle_stride, particles.size(), P0.yp, P0.xp); // mrad**2
    sigma_4d[2][2] = var_y; // mm*mm
    sigma_4d[2][3] = sigma_4d[3][2] = info.sigma_yyp; // mm*mrad
    sigma_4d[3][3] = var_yp; // mrad**2
    info.emitt_4d = beta0_gamma0 * pow(det(sigma_4d), 0.25); // mm*mrad, 4d normalized emittance
    info.sigma_zP = stats_wcovariance_m(w, 1, &particles[0].t, particle_stride, &particles[0].Pc, particle_stride, particles.size(), P0.t, P0.Pc) * V0[2]; // mm*MeV
    info.sigma_P = gsl_stats_wsd_m(w, 1, &particles[0].Pc, particle_stride, particles.size(), P0.Pc); // momentum spread in MeV
    info.sigma_E = P0.get_Pc() / info.mean_E * info.sigma_P; // kinetic-energy spread in MeV
    info.emitt_x = sqrt(var_x * var_xp - info.sigma_xxp * info.sigma_xxp); // emittance mm * mrad
    info.emitt_y = sqrt(var_y * var_yp - info.sigma_yyp * info.sigma_yyp); // emittance mm * mrad
    info.emitt_z = sqrt(var_z * var_P  - info.sigma_zP  * info.sigma_zP) * 1e3 / P0.Pc; // emittance mm * permille
    info.alpha_x = -info.sigma_xxp / info.emitt_x;
    info.alpha_y = -info.sigma_yyp / info.emitt_y;
    info.alpha_z = -info.sigma_zP / info.emitt_z / P0.Pc / 1e3;
    info.beta_x = var_x / info.emitt_x; // mm/mrad = m
    info.beta_y = var_y / info.emitt_y; // mm/mrad = m
    info.beta_z = var_z / info.emitt_z; // mm/permille = m
    info.emitt_x *= beta0_gamma0; // normalised emittance mm * mrad
    info.emitt_y *= beta0_gamma0; // normalised emittance mm * mrad
    info.emitt_z *= beta0_gamma0; // normalised emittance mm * permille
  }
  info.rmax = rmax;
  info.transmission = get_ngood() * 100.0 / particles.size();
  delete []w;
  return info;
}

MatrixNd Bunch6d::get_phase_space(const char *fmt, const char *select ) const
{
  struct _ref_par {
    double Pc;
    double t;
  } reference_particle = [&] () {
    if (particles[0]) {
      return _ref_par {
	particles[0].Pc,  // MeV/c
	particles[0].t // mm/c
      };
    }
    std::cerr << "warning: the beam's first particle was lost, using the beam centroid as reference particle.\n";
    return _ref_par {
      get_average_particle().Pc, // MeV/c
      get_t_max() // mm/c
    };
  }();
  const bool all = strncmp(select, "all", 4) == 0;
  size_t rows = all ? particles.size() : get_ngood();
  size_t cols = 0; // will be initialized later
  bool error_msg = false;
  size_t i = 0;
  MatrixNd retval;
  for (const Particle &particle : particles) {
    const bool particle_ok = bool(particle);
    if (all || particle_ok) {
      std::vector<double> values;
      for(const char *ptr0 = fmt, *ptr = strchr(ptr0, '%'); ptr; ptr0 = ptr, ptr = strchr(ptr0, '%')) {
	/// print any constants in 'fmt', if present
	{
	  char *endptr;
	  std::string nptr(ptr0, size_t(ptr-ptr0));
	  double value = strtod(nptr.c_str(), &endptr);
	  if (value!=0.0 || endptr!=nptr.c_str()) {
	    values.push_back(value);
	  }
	}
	ptr++;
	double value = 0.0;
	const auto p = particle.get_px_py_pz(reference_particle.Pc);
	const auto P = particle.get_Px_Py_Pz();
	const auto v = particle.get_Vx_Vy_Vz();
	const double K[3] = {
	  hypot(P[0], particle.mass) - particle.mass,
	  hypot(P[1], particle.mass) - particle.mass,
	  hypot(P[2], particle.mass) - particle.mass
	};
	const double dt = particle.t - reference_particle.t; // [mm/c] delay to be at S=S0 [s] (dt < 0: head of the bunch)
	if (get_token(&ptr, "deg@")) {
	  char *endptr;
	  double frequency = strtod(ptr, &endptr);
	  if (endptr == ptr) {
	    if (!error_msg) {
	      std::cerr << "error: modifier %deg requires to specify a frequency, in MHz, e.g. %deg@2998.5\n";
	      error_msg = true;
	    }
	    continue;
	  }
	  value = fmod(particle.t * frequency * 1e3 * 360.0 / C_LIGHT, 360.0); // (t / 1e3 / C_LIGHT = seconds) * (frequency * 1e6 = MHz) * (360 = deg)
	  ptr = endptr;
	}
	else if (get_token(&ptr, "Vx")) value = v[0]; // c
	else if (get_token(&ptr, "Vy")) value = v[1]; // c
	else if (get_token(&ptr, "Vz")) value = v[2]; // c
	else if (get_token(&ptr, "Px")) value = P[0]; // MeV/c
	else if (get_token(&ptr, "Py")) value = P[1]; // MeV/c
	else if (get_token(&ptr, "Pz")) value = P[2]; // MeV/c
	else if (get_token(&ptr, "px")) value = p[0]; // mrad
	else if (get_token(&ptr, "py")) value = p[1]; // mrad
	else if (get_token(&ptr, "pz")) value = p[2]; // mrad
	else if (get_token(&ptr, "Kx")) value = K[0]; // MeV
	else if (get_token(&ptr, "Ky")) value = K[1]; // MeV
	else if (get_token(&ptr, "Kz")) value = K[2]; // MeV
	else if (get_token(&ptr, "Pc")) value = particle.get_Pc(); // MeV
	else if (get_token(&ptr, "pt")) value = particle.get_pt(reference_particle.Pc) * 1e3; // permille
	else if (get_token(&ptr, "dt")) value = dt; // delay to be at S=S0 [mm/c] ; c * (t-reference_particle.t)
	else if (get_token(&ptr, "xp")) value = particle.xp; // mrad
	else if (get_token(&ptr, "yp")) value = particle.yp; // mrad
	else if (get_token(&ptr, "zp")) value = 1e3; // mrad
	else if (get_token(&ptr, 'x')) value = particle.x; // [mm] 'x' 'y' and 't' are the coordinates being tracked
	else if (get_token(&ptr, 'y')) value = particle.y; // [mm] that is, on a plane at S = bunch.S
	else if (get_token(&ptr, 'z')) value = 0.0; // [mm] that is, on a plane at S = bunch.S
	else if (get_token(&ptr, 't')) value = particle.t; // t is the proper time of each particle [mm/c]
	else if (get_token(&ptr, 'X')) value = particle.x - dt * v[0]; // [mm] 'X' 'Y' 'Z' are the particle coordinates
	else if (get_token(&ptr, 'Y')) value = particle.y - dt * v[1]; // [mm] in space at t = reference_particle.t
	else if (get_token(&ptr, 'Z')) value = -dt * v[2]; // [mm]
	else if (get_token(&ptr, 'd')) value = particle.get_delta(reference_particle.Pc) * 1e3; // permille
	else if (get_token(&ptr, 'K')) value = particle.get_kinetic_energy(); // MeV
	else if (get_token(&ptr, 'E')) value = particle.get_total_energy(); // MeV
	else if (get_token(&ptr, 'S')) value = S*1e3; // mm
	else if (get_token(&ptr, 'm')) value = particle.mass; // [MeV/c/c] rest mass
	else if (get_token(&ptr, 'Q')) value = particle.Q; // [e+] charge of each particle
	else if (get_token(&ptr, 'N')) value = particle.N; // number of paricles per macroparticle
	else {
	  if (!error_msg) {
	    const char *next = strpbrk(ptr, "% \n\t");
	    std::string id = next ? std::string(ptr, next-ptr) : ptr;
	    std::cerr << "error: unknown identifier '%" << id << "'\n";
	    error_msg = true;
	  }
	  continue;
	}
	if (!particle_ok)
	  value = GSL_NAN;
	values.push_back(value);
      }
      if (cols == 0) {
	cols = values.size();
	retval.resize(rows, cols);
      }
      std::copy(values.begin(), values.end(), retval[i]);
      i++;
    }
  }
  return retval;
}

MatrixNd Bunch6d::get_lost_particles() const
{
  MatrixNd ret(get_nlost(), 10);
  size_t i = 0;
  for (auto const &particle : particles) {
    if (!particle) {
      ret[i][0] = particle.S_lost;
      ret[i][1] = particle.x;
      ret[i][2] = particle.xp;
      ret[i][3] = particle.y;
      ret[i][4] = particle.yp;
      ret[i][5] = particle.t;
      ret[i][6] = particle.get_Pc();
      ret[i][7] = particle.mass;
      ret[i][8] = particle.Q;
      ret[i][9] = particle.N;
      i++;
    }
  }
  return ret;
}

MatrixNd Bunch6d::get_lost_particles_mask() const
{
  MatrixNd ret(particles.size(), 1);
  size_t i = 0;
  for (auto const &particle : particles) {
    ret[i++][0] = particle ? 0.0 : 1.0;
  }
  return ret;
}

size_t Bunch6d::get_ngood() const
{
  size_t ngood = 0;
  for (auto const &particle : particles) {
    if (particle) {
      ngood++;
    }
  }
  return ngood;
}

size_t Bunch6d::get_nlost() const
{
  return particles.size() - get_ngood();
}

bool Bunch6d::save_as_dst_file(const char *filename, double frequency_MHz ) const
{
  if (frequency_MHz!=0.0) {
    if (FILE *file = fopen(filename, "w")) {
      std::ostringstream fmt;
      fmt << "%x %xp %y %yp %deg@" << frequency_MHz << " %K";
      MatrixNd bunch = Bunch6d::get_phase_space(fmt.str().c_str());
      int Np = bunch.rows();
      double Ib = 0.0; // beam current [mA]
      fputc(0xfd,file);
      fputc(0x50,file);
      fwrite(&Np, sizeof(int), 1, file);
      fwrite(&Ib, sizeof(double), 1, file);
      fwrite(&frequency_MHz, sizeof(double), 1, file);
      fputc(0x54,file);
      for (int i=0; i<Np; i++) {
	double P[6] = {
	  bunch[i][0] / 10.0,    // cm
	  bunch[i][1] / 1000.0,  // rad
	  bunch[i][2] / 10.0,    // cm
	  bunch[i][3] / 1000.0,  // rad
	  deg2rad(bunch[i][4]),  // rad
	  bunch[i][5]            // MeV
	};
	fwrite(P, sizeof(double), 6, file);
      }
      fwrite(&particles[0].mass, sizeof(double), 1, file);
      fclose(file);
      return true;
    } else std::cerr << "error: couldn't open file\n";
  } else std::cerr << "error: frequency must be != 0.0\n";
  return false;
}

void Bunch6d::apply_force(const MatrixNd &force, double dS_mm )
{
  auto apply_force_parallel = [&](size_t thread, size_t start, size_t end ) -> void {
    for (size_t i=start; i<end; i++) {
      auto &particle = particles[i];
      if (particle) {
	const auto F = StaticVector<3>(force[i]); // MeV/m
	const auto v = particle.get_Vx_Vy_Vz(); // c
	const auto a = (F - dot(v,F)*v) / (particle.get_total_energy() * 1e3); // c^2/mm
	// dS = v*dt + 1/2*a*dt^2 : given dS must find dt
	double dt_mm = dS_mm / v[2]; // mm/c
	if (a[2]>2*std::numeric_limits<double>::epsilon()*v[2]*dt_mm)
	  dt_mm = (sqrt(v[2]*v[2] + 2*a[2]*dS_mm) - v[2]) / a[2]; // mm/c
	const auto dP = F * (dt_mm / 1e3); // MeV/c
	const auto P = particle.get_Px_Py_Pz() + dP; // MeV/c
	// update particle
	particle.x += particle.xp * dS_mm / 1e3 + 0.5 * a[0] * dt_mm * dt_mm; // mm
	particle.y += particle.yp * dS_mm / 1e3 + 0.5 * a[1] * dt_mm * dt_mm; // mm
	particle.t += dt_mm; // mm/c
	particle.xp = P[0] / P[2] * 1e3; // mrad
	particle.yp = P[1] / P[2] * 1e3; // mrad
	particle.Pc = norm(P); // MeV
      }
    }
  };
  const size_t Nthreads = RF_Track_Globals::number_of_threads;
  const size_t Nparticles = particles.size();
  for_all(Nthreads, Nparticles, apply_force_parallel);

  // final step
  S += dS_mm / 1e3;
}

void Bunch6d::kick(const MatrixNd &force, double dS_mm )
{
  auto apply_force_parallel = [&](size_t thread, size_t start, size_t end ) -> void {
    for (size_t i=start; i<end; i++) {
      auto &particle = particles[i];
      if (particle) {
	const auto F = StaticVector<3>(force[i]); // MeV/m
	const auto v = particle.get_Vx_Vy_Vz(); // c
	const auto a = (F - dot(v,F)*v) / (particle.get_total_energy() * 1e3); // c^2/mm
	// dS = v*dt + 1/2*a*dt^2 : given dS must find dt
	double dt_mm = dS_mm / v[2]; // mm/c
	if (a[2]>2*std::numeric_limits<double>::epsilon()*v[2]*dt_mm)
	  dt_mm = (sqrt(v[2]*v[2] + 2*a[2]*dS_mm) - v[2]) / a[2]; // mm/c
	const auto dP = F * (dt_mm / 1e3); // MeV/c
	const auto P = particle.get_Px_Py_Pz() + dP; // MeV/c
	// update particle
	particle.xp = P[0] / P[2] * 1e3; // mrad
	particle.yp = P[1] / P[2] * 1e3; // mrad
	particle.Pc = norm(P); // MeV
      }
    }
  };
  const size_t Nthreads = RF_Track_Globals::number_of_threads;
  const size_t Nparticles = particles.size();
  for_all(Nthreads, Nparticles, apply_force_parallel);
}

void Bunch6d::drift(double dS_mm ) // dS is in mm
{
  const double dS = dS_mm / 1e3;
  auto drift_parallel = [&](size_t thread, size_t start, size_t end ) -> void {
    for (size_t i=start; i<end; i++) {
      auto &particle = particles[i];
      if (particle) {
	const auto v = particle.get_Vx_Vy_Vz(); // c
	const double dt_mm = dS_mm / v[2]; // mm/c
	particle.x += particle.xp * dS; // mm
	particle.y += particle.yp * dS; // mm
	particle.t += dt_mm; // mm/c
      }
    }
  };
  const size_t Nthreads = RF_Track_Globals::number_of_threads;
  const size_t Nparticles = particles.size();
  for_all(Nthreads, Nparticles, drift_parallel);

  // final step
  S += dS;
}
