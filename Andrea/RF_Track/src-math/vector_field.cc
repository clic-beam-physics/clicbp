/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <complex>
#include <array>
#include <cmath>
#include <thread>

#include "RF_Track.hh"
#include "scalar_field.hh"
#include "vector_field.hh"

VectorField::VectorField(const Mesh3d &Vx,
			 const Mesh3d &Vy,
			 const Mesh3d &Vz,
			 double x0_, double y0_,  // mm
			 double hx_, double hy_, double hz_ ) : x0(x0_), // mm
								y0(y0_), // mm
								hx(hx_), // mm
								hy(hy_), // mm
								hz(hz_)  // mm
{
  mesh_Vx = Vx;
  mesh_Vy = Vy;
  mesh_Vz = Vz;
}

StaticVector<3> VectorField::operator() (double x /* mm */, double y /* mm */, double z /* mm */  ) const
{
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width_  = mesh_Vx.size1()-1;
  int height_ = mesh_Vx.size2()-1;
  int length_ = mesh_Vx.size3()-1;
  if (x<0.0 || y<0.0 || x>width_ || y>height_ || z<0.0 || z>length_)
    return StaticVector<3>(0.0); 
  return StaticVector<3>(mesh_Vx(x,y,z),
			 mesh_Vy(x,y,z),
			 mesh_Vz(x,y,z)); //
}

StaticMatrix<3,3> VectorField::jacobian(double x, double y, double z ) const // x,y,z [mm]
{
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width_  = mesh_Vx.size1()-1;
  int height_ = mesh_Vx.size2()-1;
  int length_ = mesh_Vx.size3()-1;
  if (x<0.0 || y<0.0 || x>width_ || y>height_ || z<0.0 || z>length_)
    return StaticMatrix<3,3>(0.0);
  return StaticMatrix<3,3>(mesh_Vx.deriv_x(x,y,z)/hx, mesh_Vx.deriv_y(x,y,z)/hy, mesh_Vx.deriv_z(x,y,z)/hz,
			   mesh_Vy.deriv_x(x,y,z)/hx, mesh_Vy.deriv_y(x,y,z)/hy, mesh_Vy.deriv_z(x,y,z)/hz,
			   mesh_Vz.deriv_x(x,y,z)/hx, mesh_Vz.deriv_y(x,y,z)/hy, mesh_Vz.deriv_z(x,y,z)/hz); //
}

StaticVector<3> VectorField::curl(double x /* mm */, double y /* mm */, double z /* mm */  ) const
{
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width_  = mesh_Vx.size1()-1;
  int height_ = mesh_Vx.size2()-1;
  int length_ = mesh_Vx.size3()-1;
  if (x<0.0 || y<0.0 || x>width_ || y>height_ || z<0.0 || z>length_)
    return StaticVector<3>(0.0); 
  const auto dVx_dy = mesh_Vx.deriv_y(x,y,z)/hy; //
  const auto dVx_dz = mesh_Vx.deriv_z(x,y,z)/hz; //
  const auto dVy_dx = mesh_Vy.deriv_x(x,y,z)/hx; //
  const auto dVy_dz = mesh_Vy.deriv_z(x,y,z)/hz; //
  const auto dVz_dx = mesh_Vz.deriv_x(x,y,z)/hx; //
  const auto dVz_dy = mesh_Vz.deriv_y(x,y,z)/hy; //
  return StaticVector<3>(dVz_dy-dVy_dz, dVx_dz-dVz_dx, dVy_dx-dVx_dy); //
}

VectorField VectorField::anti_curl() const
{
  const int
    Nx = mesh_Vx.size1(),
    Ny = mesh_Vx.size2(),
    Nz = mesh_Vx.size3();

  Mesh3d mesh_Ax(Nx,Ny,Nz);
  Mesh3d mesh_Ay(Nx,Ny,Nz);
  Mesh3d mesh_Az(Nx,Ny,Nz);

  const Mesh3d_LINT &Vx_ = mesh_Vx;
  const Mesh3d_LINT &Vy_ = mesh_Vy;
  const Mesh3d_LINT &Vz_ = mesh_Vz;
  
  auto anti_curl_parallel = [&] (size_t thread, size_t start, size_t end ) {
    for (size_t i=start; i<end; i++) {
      for (int j=0; j<Ny; j++) {
	for (int k=0; k<Nz; k++) {
	  const auto func = [&] (double t ) {
	    const double tx = t*i, ty = t*j, tz = t*k;
	    StaticVector<3> V_vec;
	    V_vec[0] = Vx_(tx,ty,tz); //
	    V_vec[1] = Vy_(tx,ty,tz); //
	    V_vec[2] = Vz_(tx,ty,tz); //
	    StaticVector<3> tr_vec;
	    tr_vec[0] = tx*hx; // mm
	    tr_vec[1] = ty*hy; // mm
	    tr_vec[2] = tz*hz; // mm
	    return cross(V_vec, tr_vec); //
	  };
	  const StaticVector<3> &A = [&] () {
	    CumulativeKahanSum<StaticVector<3>> Integral = StaticVector<3>(0.0);
	    const size_t N = std::max(size_t(ceil(sqrt(i*i+j*j+k*k))),size_t(3));
	    for (size_t n = 1; n < N-1; n++) { // trapezoidal rule
	      Integral += 2.0 * func(double(n) / double(N-1));
	    }
	    Integral += func(1.0);
	    return Integral / (2*N);
	  } ();
	  mesh_Ax.elem(i,j,k) = A[0];
	  mesh_Ay.elem(i,j,k) = A[1];
	  mesh_Az.elem(i,j,k) = A[2];
	}
      }
    }
  };
  for_all(RF_Track_Globals::number_of_threads, Nx, anti_curl_parallel);

  return VectorField(mesh_Ax, mesh_Ay, mesh_Az, x0, y0, hx, hy, hz);
}

ScalarField VectorField::anti_grad() const
{
  const int
    Nx = mesh_Vx.size1(),
    Ny = mesh_Vx.size2(),
    Nz = mesh_Vx.size3();

  Mesh3d mesh_Phi(Nx,Ny,Nz);

  const Mesh3d_LINT &Vx_ = mesh_Vx;
  const Mesh3d_LINT &Vy_ = mesh_Vy;
  const Mesh3d_LINT &Vz_ = mesh_Vz;
  
  auto anti_grad_parallel = [&] (size_t thread, size_t start, size_t end ) {
    for (size_t i=start; i<end; i++) {
      for (int j=0; j<Ny; j++) {
	for (int k=0; k<Nz; k++) {
	  const auto func = [&] (double t ) {
	    const double tx = t*i, ty = t*j, tz = t*k;
	    StaticVector<3> V_vec;
	    V_vec[0] = Vx_(tx,ty,tz); //
	    V_vec[1] = Vy_(tx,ty,tz); //
	    V_vec[2] = Vz_(tx,ty,tz); //
	    StaticVector<3> tr_vec;
	    tr_vec[0] = tx*hx; // mm
	    tr_vec[1] = ty*hy; // mm
	    tr_vec[2] = tz*hz; // mm
	    return dot(V_vec, tr_vec); //
	  };
	  const double phi = [&] () {
	    CumulativeKahanSum<double> Integral = 0.0;
	    const size_t N = std::max(size_t(ceil(sqrt(i*i+j*j+k*k))),size_t(3));
	    for (size_t n = 1; n < N-1; n++) { // trapezoidal rule
	      Integral += 2.0 * func(double(n) / double(N-1));
	    }
	    Integral += func(1.0);
	    return Integral / (2*N);
	  } ();
	  mesh_Phi.elem(i,j,k) = phi;
	}
      }
    }
  };
  for_all(RF_Track_Globals::number_of_threads, Nx, anti_grad_parallel);

  return ScalarField(mesh_Phi, x0, y0, hx, hy, hz);
}
