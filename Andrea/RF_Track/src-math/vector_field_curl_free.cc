/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <complex>
#include <array>
#include <cmath>
#include <thread>

#include "RF_Track.hh"
#include "vector_field_curl_free.hh"

VectorField_curlFree::VectorField_curlFree(const Mesh3d &Ex,
					   const Mesh3d &Ey,
					   const Mesh3d &Ez,
					   double x0_, double y0_,  // mm
					   double hx_, double hy_, double hz_ )
{
  set_x0(x0_);
  set_y0(y0_);
  set_hx(hx_);
  set_hy(hy_);
  set_hz(hz_);
  set_Vx_Vy_Vz(Ex,Ey,Ez);
}

void VectorField_curlFree::set_Vx_Vy_Vz(const Mesh3d &Ex, const Mesh3d &Ey, const Mesh3d &Ez )
{
  const int
    Nx = Ex.size1(),
    Ny = Ex.size2(),
    Nz = Ex.size3();

  Mesh3d mesh_Phi(Nx,Ny,Nz);

  const Mesh3d_LINT &Ex_ = Ex;
  const Mesh3d_LINT &Ey_ = Ey;
  const Mesh3d_LINT &Ez_ = Ez;
  
  auto anti_grad_parallel = [&] (size_t thread, size_t start, size_t end ) {
    for (size_t i=start; i<end; i++) {
      for (int j=0; j<Ny; j++) {
	for (int k=0; k<Nz; k++) {
	  const auto func = [&] (double t ) {
	    const double tx = t*i, ty = t*j, tz = t*k;
	    StaticVector<3> E_vec;
	    E_vec[0] = Ex_(tx,ty,tz); // V/m
	    E_vec[1] = Ey_(tx,ty,tz); // V/m
	    E_vec[2] = Ez_(tx,ty,tz); // V/m
	    StaticVector<3> tr_vec;
	    tr_vec[0] = tx*hx; // mm
	    tr_vec[1] = ty*hy; // mm
	    tr_vec[2] = tz*hz; // mm
	    return -dot(E_vec, tr_vec); // V/m*mm
	  };
	  const double phi = [&] () {
	    CumulativeKahanSum<double> Integral = 0.0;
	    const size_t N = std::max(size_t(ceil(sqrt(i*i+j*j+k*k))),size_t(3));
	    for (size_t n = 1; n < N-1; n++) { // trapezoidal rule
	      Integral += 2.0 * func(double(n) / double(N-1));
	    }
	    Integral += func(1.0);
	    return Integral / (2*N);
	  } ();
	  mesh_Phi.elem(i,j,k) = phi;
	}
      }
    }
  };
  for_all(RF_Track_Globals::number_of_threads, Nx, anti_grad_parallel);

  set_Phi(mesh_Phi);
}

StaticMatrix<3,3> VectorField_curlFree::jacobian(double x, double y, double z ) const // x,y,z [mm]
{
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width_  = mesh_Phi.size1()-1;
  int height_ = mesh_Phi.size2()-1;
  int length_ = mesh_Phi.size3()-1;
  if (x<0.0 || y<0.0 || x>width_ || y>height_ || z<0.0 || z>length_)
    return StaticMatrix<3,3>(0.0);
  const double inv_hxhx = 1.0/(hx*hx);
  const double inv_hxhy = 1.0/(hx*hy);
  const double inv_hxhz = 1.0/(hx*hz);
  const double inv_hyhy = 1.0/(hy*hy);
  const double inv_hyhz = 1.0/(hy*hz);
  const double inv_hzhz = 1.0/(hz*hz);
  return StaticMatrix<3,3>(-mesh_Phi.deriv2_x2(x,y,z)*inv_hxhx, -mesh_Phi.deriv2_xy(x,y,z)*inv_hxhy, -mesh_Phi.deriv2_xz(x,y,z)*inv_hxhz,
			   -mesh_Phi.deriv2_xy(x,y,z)*inv_hxhy, -mesh_Phi.deriv2_y2(x,y,z)*inv_hyhy, -mesh_Phi.deriv2_yz(x,y,z)*inv_hyhz,
			   -mesh_Phi.deriv2_xz(x,y,z)*inv_hxhz, -mesh_Phi.deriv2_yz(x,y,z)*inv_hyhz, -mesh_Phi.deriv2_z2(x,y,z)*inv_hzhz); //
}

StaticVector<3> VectorField_curlFree::operator() (double x /* mm */, double y /* mm */, double z /* mm */  ) const
{
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width_  = mesh_Phi.size1()-1;
  int height_ = mesh_Phi.size2()-1;
  int length_ = mesh_Phi.size3()-1;
  if (x<0.0 || y<0.0 || x>width_ || y>height_ || z<0.0 || z>length_)
    return StaticVector<3>(0.0); 
  return -StaticVector<3>(mesh_Phi.deriv_x(x,y,z)/hx,
			  mesh_Phi.deriv_y(x,y,z)/hy,
			  mesh_Phi.deriv_z(x,y,z)/hz); //
}
