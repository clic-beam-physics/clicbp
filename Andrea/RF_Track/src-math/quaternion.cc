/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include "quaternion.hh"

Quaternion normalize(const Quaternion &q )
{
  double s = fabs(q.s);
  double x = fabs(q.v[0]);
  double y = fabs(q.v[1]);
  double z = fabs(q.v[2]);
  double denom = 1.;
  if ( s > x ) {
    if ( s > y ) {
      if ( s > z ) {
	if ( 1. + s > 1. ) {
	  x /= s;
	  y /= s;
	  z /= s;
	  denom = s*sqrt(1.+x*x+y*y+z*z);
	}
      } else {
	if ( 1.+z > 1. ) {
	  x /= z;
	  y /= z;
	  s /= z;
	  denom = z*sqrt(1.+x*x+y*y+s*s);
	}
      }
    } else {
      if ( y > z ) {
	if ( 1.+y > 1. ) {
	  x /= y;
	  s /= y;
	  z /= y;
	  denom = y*sqrt(1.+x*x+s*s+z*z);
	}
      } else {
	if ( 1.+z > 1. ) {
	  x /= z;
	  y /= z;
	  s /= z;
	  denom = z*sqrt(1.+x*x+y*y+s*s);
	}
      }
    }
  } else {	
    if ( x > y ) {
      if ( x > z ) {
	if ( 1.+x > 1. ) {
	  s /= x;
	  y /= x;
	  z /= x;
	  denom = x*sqrt(1.+s*s+y*y+z*z);
	}
      } else {
	if ( 1.+z > 1. ) {
	  x /= z;
	  y /= z;
	  s /= z;
	  denom = z*sqrt(1.+x*x+y*y+s*s);
	}
      }
    } else {
      if ( y > z ) {
	if ( 1.+y > 1. ) {
	  x /= y;
	  s /= y;
	  z /= y;
	  denom = y*sqrt(1.+x*x+s*s+z*z);
	}
      } else {
	if ( 1.+z > 1. ) {
	  x /= z;
	  y /= z;
	  s /= z;
	  denom = z*sqrt(1.+x*x+y*y+s*s);
	}
      }
    }		
  }
  return (1.+s+x+y+z > 1.) ? q / denom : q;
}


