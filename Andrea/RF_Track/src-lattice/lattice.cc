/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <cstdio>
#include <list>
#include <string>
#include <iomanip>

#include "lattice.hh"
#include "get_token.hh"
#include "for_all.hh"
#include "RF_Track.hh"
#include "drift.hh"
#include "rf_field.hh"
#include "quadrupole.hh"
#include "move_particle.hh"

#include <gsl/gsl_interp.h>

MatrixNd TransportTable::get_transport_table(const char *fmt ) const
{
  MatrixNd ret;
  if (!transport_table.empty()) {
    size_t rows = transport_table.size();
    size_t cols = 0; // will be initialized later
    size_t i = 0;
    bool error_msg = false;
    for (auto const &info : transport_table) {
      std::vector<double> values;
      for(const char *ptr0 = fmt, *ptr = strchr(ptr0, '%'); ptr; ptr0 = ptr, ptr = strchr(ptr0, '%')) {
	/// print any constants in 'fmt', if present
	{
	  char *endptr;
	  std::string nptr(ptr0, size_t(ptr-ptr0));
	  double value = strtod(nptr.c_str(), &endptr);
	  if (value!=0.0 || endptr!=nptr.c_str()) {
	    values.push_back(value);
	  }
	}
	ptr++;
	double value = 0.0;
	if      (get_token(&ptr, "sigma_xp")) value = info.sigma_xp;
	else if (get_token(&ptr, "sigma_yp")) value = info.sigma_yp;
	else if (get_token(&ptr, "emitt_4d")) value = info.emitt_4d;
	else if (get_token(&ptr, "sigma_x")) value = info.sigma_x;
	else if (get_token(&ptr, "sigma_y")) value = info.sigma_y;
	else if (get_token(&ptr, "sigma_z")) value = info.sigma_z;
	else if (get_token(&ptr, "sigma_E")) value = info.sigma_E;
	else if (get_token(&ptr, "sigma_P")) value = info.sigma_P;
	else if (get_token(&ptr, "emitt_x")) value = info.emitt_x;
	else if (get_token(&ptr, "emitt_y")) value = info.emitt_y;
	else if (get_token(&ptr, "emitt_z")) value = info.emitt_z;
	else if (get_token(&ptr, "alpha_x")) value = info.alpha_x;
	else if (get_token(&ptr, "alpha_y")) value = info.alpha_y;
	else if (get_token(&ptr, "alpha_z")) value = info.alpha_z;
	else if (get_token(&ptr, "mean_xp")) value = info.mean_xp;
	else if (get_token(&ptr, "mean_yp")) value = info.mean_yp;
	else if (get_token(&ptr, "mean_x")) value = info.mean_x;
	else if (get_token(&ptr, "mean_y")) value = info.mean_y;
	else if (get_token(&ptr, "mean_t")) value = info.mean_t;
	else if (get_token(&ptr, "mean_P")) value = info.mean_P;
	else if (get_token(&ptr, "mean_K")) value = info.mean_K;
	else if (get_token(&ptr, "mean_E")) value = info.mean_E;
	else if (get_token(&ptr, "beta_x")) value = info.beta_x;
	else if (get_token(&ptr, "beta_y")) value = info.beta_y;
	else if (get_token(&ptr, "beta_z")) value = info.beta_z;
	else if (get_token(&ptr, "rmax")) value = info.rmax;
	else if (get_token(&ptr, 'N')) value = info.transmission;
	else if (get_token(&ptr, 'S')) value = info.S;
	else {
	  if (!error_msg) {
	    const char *next = strpbrk(ptr, "% \n\t");
	    std::string id = next ? std::string(ptr, next-ptr) : ptr;
	    std::cerr << "warning: unknown identifier '%" << id << "'\n";
	    error_msg = true;
	  }
	  continue;
	}
	values.push_back(value);
      }
      if (cols == 0) {
	cols = values.size();
	ret.resize(rows, cols);
      }
      for (size_t j = 0; j<cols; j++) {
	ret[i][j] = values[j];
      }
      i++;
    }
  } else if (!transport_tableT.empty()) {
    size_t rows = transport_tableT.size();
    size_t cols = 0; // will be initialized later
    size_t i = 0;
    bool error_msg = false;
    for (auto const &info : transport_tableT) {
      std::vector<double> values;
      for(const char *ptr0 = fmt, *ptr = strchr(ptr0, '%'); ptr; ptr0 = ptr, ptr = strchr(ptr0, '%')) {
	/// print any constants in 'fmt', if present
	{
	  char *endptr;
	  std::string nptr(ptr0, size_t(ptr-ptr0));
	  double value = strtod(nptr.c_str(), &endptr);
	  if (value!=0.0 || endptr!=nptr.c_str()) {
	    values.push_back(value);
	  }
	}
	ptr++;
	double value = 0.0;
	if      (get_token(&ptr, "sigma_Px")) value = info.sigma_Px;
	else if (get_token(&ptr, "sigma_Py")) value = info.sigma_Py;
	else if (get_token(&ptr, "sigma_Pz")) value = info.sigma_Pz;
	else if (get_token(&ptr, "emitt_4d")) value = info.emitt_4d;
	else if (get_token(&ptr, "sigma_X")) value = info.sigma_X;
	else if (get_token(&ptr, "sigma_Y")) value = info.sigma_Y;
	else if (get_token(&ptr, "sigma_Z")) value = info.sigma_Z;
	else if (get_token(&ptr, "sigma_E")) value = info.sigma_E;
	else if (get_token(&ptr, "emitt_x")) value = info.emitt_x;
	else if (get_token(&ptr, "emitt_y")) value = info.emitt_y;
	else if (get_token(&ptr, "emitt_z")) value = info.emitt_z;
	else if (get_token(&ptr, "alpha_x")) value = info.alpha_x;
	else if (get_token(&ptr, "alpha_y")) value = info.alpha_y;
	else if (get_token(&ptr, "alpha_z")) value = info.alpha_z;
	else if (get_token(&ptr, "mean_Px")) value = info.mean_Px;
	else if (get_token(&ptr, "mean_Py")) value = info.mean_Py;
	else if (get_token(&ptr, "mean_Pz")) value = info.mean_Pz;
	else if (get_token(&ptr, "mean_X")) value = info.mean_X;
	else if (get_token(&ptr, "mean_Y")) value = info.mean_Y;
	else if (get_token(&ptr, "mean_S")) value = info.mean_S;
	else if (get_token(&ptr, "mean_K")) value = info.mean_K;
	else if (get_token(&ptr, "mean_E")) value = info.mean_E;
	else if (get_token(&ptr, "beta_x")) value = info.beta_x;
	else if (get_token(&ptr, "beta_y")) value = info.beta_y;
	else if (get_token(&ptr, "beta_z")) value = info.beta_z;
	else if (get_token(&ptr, "rmax")) value = info.rmax;
	else if (get_token(&ptr, 'N')) value = info.transmission;
	else if (get_token(&ptr, 't')) value = info.t;
	else {
	  if (!error_msg) {
	    const char *next = strpbrk(ptr, "% \n\t");
	    std::string id = next ? std::string(ptr, next-ptr) : ptr;
	    std::cerr << "warning: unknown identifier '%" << id << "'\n";
	    error_msg = true;
	  }
	  continue;
	}
	values.push_back(value);
      }
      if (cols == 0) {
	cols = values.size();
	ret.resize(rows, cols);
      }
      for (size_t j = 0; j<cols; j++) {
	ret[i][j] = values[j];
      }
      i++;
    }
  } else {
    std::cerr << "error: no transport table in memory!\n";
  }
  return ret;
}

Lattice::Lattice(const Lattice &lattice ) {
  for (auto &element_ptr : lattice.elements) {
    Element *ptr = element_ptr->clone();
    elements.push_back(std::shared_ptr<Element>(ptr));
  }
}

Lattice::~Lattice()
{}

Lattice &Lattice::operator=(const Lattice &lattice )
{
  elements.clear();
  for (auto &element_ptr : lattice.elements) {
    Element *ptr = element_ptr->clone();
    elements.push_back(std::shared_ptr<Element>(ptr));
  }
  return *this;
}

double Lattice::get_length() const
{
  double length = 0.0;
  for (auto &element_ptr : elements) {
    length += element_ptr->get_length();
  }
  return length;
}

MatrixNd Lattice::get_bpm_readings() const
{
  MatrixNd ret;
  if (!bpm_readings.empty()) {
    size_t rows = bpm_readings.size();
    size_t columns = 3;
    ret.resize(rows, columns);
    // TO DO
  } else {
    std::cerr << "error: no bpm readings in memory!\n";
  }
  return ret;
}

std::vector<RF_Field*> Lattice::get_rf_fields()
{
  std::vector<RF_Field*> ret_val;
  for (auto &e_ptr : elements) {
    if (RF_Field *s_ptr=dynamic_cast<RF_Field*>(e_ptr.get())) {
      ret_val.push_back(s_ptr);
    }
  }
  return ret_val;
}

std::vector<Drift*> Lattice::get_drifts()
{
  std::vector<Drift*> ret_val;
  for (auto &e_ptr : elements) {
    if (Drift *d_ptr=dynamic_cast<Drift*>(e_ptr.get())) {
      ret_val.push_back(d_ptr);
    }
  }
  return ret_val;
}

std::vector<Quadrupole*> Lattice::get_quadrupoles()
{
  std::vector<Quadrupole*> ret_val;
  for (auto &e_ptr : elements) {
    if (Quadrupole *q_ptr=dynamic_cast<Quadrupole*>(e_ptr.get())) {
      ret_val.push_back(q_ptr);
    }
  }
  return ret_val;
}

Bunch6d Lattice::track(Bunch6d bunch )
{
  transport_table.clear();
  transport_tableT.clear();
  bpm_readings.clear();
  append_bunch_info(bunch.get_info());
  for (auto &element_ptr : elements) {
    element_ptr->track_in(bunch);
    auto &&transport_table_elem = element_ptr->track(bunch);
    element_ptr->track_out(bunch);
    transport_table.splice(transport_table.end(), transport_table_elem);
    append_bunch_info(bunch.get_info());
  }
  return bunch;
}

struct Params {
  double mass;
  double charge;
  const std::vector<double> *S_array_mm; // array element ends in mm
  const std::vector<std::shared_ptr<Element>> *elements; // from lattice
};

static int func(double t_mm, const double Y[], double dY[], void *params_ )
{
  // init parameters
  const Params &params = *(const Params *)(params_);
  const double mass = params.mass;
  const double q = params.charge;
  const auto &S_array_mm = *params.S_array_mm;
  const auto &elements = *params.elements;

  // system of equations for GSL
  // X_prime = V[0]; // c
  // Y_prime = V[1]; // c
  // S_prime = V[2]; // c
  // Px_prime = F[0]/1e3; // MeV/mm
  // Py_prime = F[1]/1e3; // MeV/mm
  // Pz_prime = F[2]/1e3; // MeV/mm

  const auto &P = StaticVector<3>(Y[3], Y[4], Y[5]); // MeV/c
  const auto &v = P / hypot(mass,P); // c

  dY[0] = v[0]; // c
  dY[1] = v[1]; // c
  dY[2] = v[2]; // c

  if (Y[2]<0.0) {
    dY[3] = 0.0; // MeV/mm
    dY[4] = 0.0; // MeV/mm
    dY[5] = 0.0; // MeV/mm
    return GSL_SUCCESS;
  }

  size_t imin = gsl_interp_bsearch(S_array_mm.data(), Y[2], 0, S_array_mm.size());
  if (imin >= elements.size()) {
    dY[3] = 0.0; // MeV/mm
    dY[4] = 0.0; // MeV/mm
    dY[5] = 0.0; // MeV/mm
    return GSL_SUCCESS;
  }

  const auto element_ptr = elements[imin];
  if (!element_ptr->is_point_inside_aperture(Y[0], Y[1]))
    return GSL_EBADFUNC;

  StaticVector<3> F;
  {
    const auto &field = element_ptr->get_field(Y[0], Y[1], Y[2] - S_array_mm[imin], t_mm);
    const auto &E = field.first; // V/m
    if (gsl_isnan(E[0])) { // in a consistent map if one component is Nan then all other components are Nan too
      return GSL_EBADFUNC;
    } else {
      const auto &B = field.second * C_LIGHT; // T*c = V/m
      F = q * (E + cross(v, B)) / 1e9; // MeV/mm
    }
  }
  dY[3] = F[0]; // MeV/mm
  dY[4] = F[1]; // MeV/mm
  dY[5] = F[2]; // MeV/mm
  return GSL_SUCCESS;
}

static int jac(double t_mm, const double Y[], double *dfdy, 
	       double dfdt[], void *params_ )
  
{
  // init parameters
  const Params &params = *(const Params *)(params_);
  const double mass = params.mass;
  const double charge = params.charge;
  const auto &S_array_mm = *params.S_array_mm;
  const auto &elements = *params.elements;

  // auxiliary variables
  const auto &P = StaticVector<3>(Y[3], Y[4], Y[5]); // MeV/c
  const auto &E = hypot(mass,P); // MeV

  // derivative of force with time
  for (int i=3; i<6; i++)
    dfdt[i] = 0.0;

  // jacobian of force with Y
  gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, 6, 6);
  gsl_matrix *m = &dfdy_mat.matrix;
  for (int i=0; i<3; i++) {
    for (int j=0; j<3; j++) {
      gsl_matrix_set (m, i, j, 0.0); // dVi / dj
    }
  }

  const double inv_E3 = 1.0 / (E*E*E); 
  gsl_matrix_set (m, 0, 3, (mass*mass + P[1]*P[1] + P[2]*P[2]) * inv_E3); // dVx / dPx
  gsl_matrix_set (m, 0, 4, -P[0]*P[1] * inv_E3); // dVx / dPy
  gsl_matrix_set (m, 0, 5, -P[0]*P[2] * inv_E3); // dVx / dPz
  gsl_matrix_set (m, 1, 3, -P[1]*P[0] * inv_E3); // dVy / dPx
  gsl_matrix_set (m, 1, 4, (mass*mass + P[2]*P[2] + P[0]*P[0]) * inv_E3); // dVy / dPy
  gsl_matrix_set (m, 1, 5, -P[1]*P[2] * inv_E3); // dVy / dPz
  gsl_matrix_set (m, 2, 3, -P[2]*P[0] * inv_E3); // dVz / dPx
  gsl_matrix_set (m, 2, 4, -P[2]*P[1] * inv_E3); // dVz / dPy
  gsl_matrix_set (m, 2, 5, (mass*mass + P[0]*P[0] + P[1]*P[1]) * inv_E3); // dVz / dPz

  if (Y[2]<0.0) {
    for (int i=0; i<3; i++)
      dfdt[i] = 0.0;
    for (int i=3; i<6; i++) {
      for (int j=0; j<6; j++) {
	gsl_matrix_set (m, i, j, 0.0); // dVi / dj
      }
    }
    return GSL_SUCCESS;
  }

  size_t imin = gsl_interp_bsearch(S_array_mm.data(), Y[2], 0, S_array_mm.size());
  if (imin >= elements.size()) {
    for (int i=0; i<3; i++)
      dfdt[i] = 0.0;
    for (int i=3; i<6; i++) {
      for (int j=0; j<6; j++) {
	gsl_matrix_set (m, i, j, 0.0); // dVi / dj
      }
    }
    return GSL_SUCCESS;
  }

  const auto element_ptr = elements[imin];
  if (!element_ptr->is_point_inside_aperture(Y[0], Y[1]))
    return GSL_EBADFUNC;

  const auto &field = element_ptr->get_field(Y[0], Y[1], Y[2] - S_array_mm[imin], t_mm);
  if (gsl_isnan(field.first[0])) // in a consistent map if one component is Nan then all other components are Nan too
    return GSL_EBADFUNC;

  const auto &Ef = field.first / 1e9; // V/m -> MV/mm
  const auto &Bf = field.second * C_LIGHT / 1e9; // T*c = V/m -> MV/mm
  const auto &v = P / E; // c
  const auto &F = charge * (Ef + cross(v,Bf)); // MeV/mm
  const auto &a = (F - dot(v,F)*v) / E; // c^2/mm
  for (int i=0; i<3; i++)
    dfdt[i] = a[i];
  gsl_matrix_set (m, 3, 3, 0.0); // dFx / dPx
  gsl_matrix_set (m, 3, 4,  charge * Bf[2] * gsl_matrix_get (m, 0, 3)); // dFx / dPy
  gsl_matrix_set (m, 3, 5, -charge * Bf[1] * gsl_matrix_get (m, 2, 5)); // dFx / dPz
  gsl_matrix_set (m, 4, 3, -charge * Bf[2] * gsl_matrix_get (m, 1, 4)); // dFy / dPx
  gsl_matrix_set (m, 4, 4, 0.0); // dFy / dPy
  gsl_matrix_set (m, 4, 5,  charge * Bf[0] * gsl_matrix_get (m, 2, 5)); // dFy / dPz
  gsl_matrix_set (m, 5, 3,  charge * Bf[1] * gsl_matrix_get (m, 0, 3)); // dFz / dPx
  gsl_matrix_set (m, 5, 4, -charge * Bf[0] * gsl_matrix_get (m, 1, 4)); // dFz / dPy
  gsl_matrix_set (m, 5, 5, 0.0); // dFz / dPz

  auto &&jacobian = element_ptr->get_field_jacobian(Y[0], Y[1], Y[2] - S_array_mm[imin], t_mm);
  jacobian.first /= 1e6; // V/m/mm -> MV/m/mm
  jacobian.second *= C_LIGHT / 1e6; // T/mm*c = V/m/mm -> MV/m/mm
  const auto &Ef_jac = jacobian.first; // V/m/mm -> MV/m/mm
  const auto &Bf_jac = jacobian.second; // T/mm*c = V/m/mm -> MV/m/mm
  gsl_matrix_set (m, 3, 0, charge * (Ef_jac[0][0] + v[1] * Bf_jac[2][0] - v[2] * Bf_jac[1][0])); // MeV/m/mm
  gsl_matrix_set (m, 3, 1, charge * (Ef_jac[0][1] + v[1] * Bf_jac[2][1] - v[2] * Bf_jac[1][1])); // MeV/m/mm
  gsl_matrix_set (m, 3, 2, charge * (Ef_jac[0][2] + v[1] * Bf_jac[2][2] - v[2] * Bf_jac[1][2])); // MeV/m/mm
  gsl_matrix_set (m, 4, 0, charge * (Ef_jac[1][0] + v[2] * Bf_jac[0][0] - v[0] * Bf_jac[2][0])); // MeV/m/mm
  gsl_matrix_set (m, 4, 1, charge * (Ef_jac[1][1] + v[2] * Bf_jac[0][1] - v[0] * Bf_jac[2][1])); // MeV/m/mm
  gsl_matrix_set (m, 4, 2, charge * (Ef_jac[1][2] + v[2] * Bf_jac[0][2] - v[0] * Bf_jac[2][2])); // MeV/m/mm
  gsl_matrix_set (m, 5, 0, charge * (Ef_jac[2][0] + v[0] * Bf_jac[1][0] - v[1] * Bf_jac[0][0])); // MeV/m/mm
  gsl_matrix_set (m, 5, 1, charge * (Ef_jac[2][1] + v[0] * Bf_jac[1][1] - v[1] * Bf_jac[0][1])); // MeV/m/mm
  gsl_matrix_set (m, 5, 2, charge * (Ef_jac[2][2] + v[0] * Bf_jac[1][2] - v[1] * Bf_jac[0][2])); // MeV/m/mm

  return GSL_SUCCESS;
}

Bunch6dT Lattice::track(Bunch6dT bunch, const TrackingOpts &tracking_opts )
{
  set_odeint_algorithm(tracking_opts.odeint_algorithm.c_str());
  const size_t sc_nsteps = tracking_opts.sc_nsteps;
  const size_t wp_nsteps = tracking_opts.wp_nsteps;
  const size_t nsteps = tracking_opts.nsteps;
  size_t wp_index = 1;

  transport_table.clear();
  transport_tableT.clear();
  bpm_readings.clear();

  // init integration auxiliary variables
  std::vector<double> S_array_mm;
  {
    double S_mm = 0.0;
    S_array_mm.push_back(S_mm);
    for (const auto &element_ptr : elements) {
      S_array_mm.push_back(S_mm+=element_ptr->get_length()*1e3);
    }
  }
  const double S_end_of_beamline_mm = S_array_mm[S_array_mm.size()-1];

  std::vector<Params> params(RF_Track_Globals::number_of_threads);
  for (auto &params_ : params) {
    // params_.mass = bunch.mass; // initialized later
    // params_.charge = bunch.charge; // initialized later
    params_.elements = &elements; // from lattice
    params_.S_array_mm = &S_array_mm; // position of the elements end
  }

  // init GSL ODE
  std::vector<gsl_odeiv2_system> sys;
  if (use_gsl()) {
    sys.resize(RF_Track_Globals::number_of_threads);
    for (size_t i=0; i<RF_Track_Globals::number_of_threads; i++) {
      sys[i].function = func;
      sys[i].jacobian = jac;
      sys[i].dimension = 6;
      sys[i].params = &params[i];
    }
    init_gsl_drivers(sys);
  }

  // lambda to check if there are particles that need to be tracked
  auto does_particle_need_to_be_tracked = [&S_end_of_beamline_mm] (const ParticleT &particle ) {
    return particle && ((particle.S<S_end_of_beamline_mm && particle.Pz>0.0) || (particle.S>0.0 && particle.Pz<0.0));
  };
  
  auto do_particles_need_to_be_tracked = [&] () { // true: continue tracking; false: end tracking
    for (const auto &particle : bunch.particles) {
      if (does_particle_need_to_be_tracked(particle)) {
	return true;
      }
    }
    return false;
  };

  append_bunch_info(bunch.get_info());

  // tracking starts
  
  // apply a negative drift to bring S_max to 0.0
  if (tracking_opts.backtrack_at_entrance) {
    double dt_max = -std::numeric_limits<double>::max();
    for (size_t i=0; i<bunch.size(); i++) {
      const auto &particle = bunch.particles[i];
      if (does_particle_need_to_be_tracked(particle) && bunch.t>=particle.t0) {
	const double Vz = particle.get_Vx_Vy_Vz()[2]; // c
	const double dt = particle.S / Vz; // mm/c
	if (dt > dt_max)
	  dt_max = dt;
      }
    }
    if (dt_max != -std::numeric_limits<double>::max()) {
      for (auto &particle : bunch.particles) {
	if (bunch.t>=particle.t0) {
	  const auto dX = particle.get_Vx_Vy_Vz() * dt_max; // mm
	  particle.X -= dX[0]; // mm
	  particle.Y -= dX[1]; // mm
	  particle.S -= dX[2]; // mm
	}
      }
      bunch.t -= dt_max; // mm/c
    }
  }

  // main loop
  bool gsl_error = false;
  size_t iterations_cnt = 0;
  size_t sc_iterations_cnt = 0;
  const size_t nloop = [&] () { // number of steps per iteration loop, i.e. between one space-charge step and the next
    if (sc_nsteps && wp_nsteps)
      return std::min(sc_nsteps, wp_nsteps);
    if (sc_nsteps)
      return sc_nsteps;
    if (wp_nsteps)
      return wp_nsteps;
    if (nsteps)
      return nsteps;
    return size_t(100);
  } ();
  MatrixNd sc_force;
  bool loop;
  do {

    struct ParticleSelector_exists : public SpaceCharge::ParticleSelector {
      const double &t;
      bool operator () (const ParticleT &p ) const { return p && t>=p.t0; }
      ParticleSelector_exists(const double &t_ ) : t(t_) {}
    } selector_exists(bunch.t);

    struct ParticleSelector_strict : public ParticleSelector_exists {
      const double &S_end;
      ParticleSelector_strict(const double &t_, const double &S_ ) : ParticleSelector_exists(t_), S_end(S_) {}
      bool operator () (const ParticleT &p ) const { return ParticleSelector_exists::operator()(p) && p.S >= 0.0 && p.S < S_end; }
    } selector_strict(bunch.t, S_end_of_beamline_mm);

    const SpaceCharge::ParticleSelector &particle_selector = tracking_opts.sc_strict_boundaries ?
      static_cast<const SpaceCharge::ParticleSelector &>(selector_strict) :
      static_cast<const SpaceCharge::ParticleSelector &>(selector_exists);
    
    // first step of space charge (half kick)
    if (sc_nsteps && iterations_cnt==0) {
    
      if (tracking_opts.verbose>1)
	std::cout << " applying space-charge " << sc_iterations_cnt++ << std::endl;

      RF_Track_Globals::SC_engine.compute_force(sc_force, bunch, particle_selector);
      const double dt = (nloop * tracking_opts.dt_mm) / 2e3; // applies half step
      for (size_t i=0; i<bunch.size(); i++) {
	auto &particle = bunch.particles[i];
	if (particle_selector(particle)) {
	  const auto dP = StaticVector<3>(sc_force[i]) * dt; // MeV/c
	  particle.Px += dP[0]; // MeV/c
	  particle.Py += dP[1]; // MeV/c
	  particle.Pz += dP[2]; // MeV/c
	}
      }
    }
    
    // do tracking
    auto drift_particle = [] (ParticleT &particle, const double &dt_mm ) {
      const auto &dX = particle.get_Vx_Vy_Vz() * dt_mm; // mm
      particle.X += dX[0]; // mm
      particle.Y += dX[1]; // mm
      particle.S += dX[2]; // mm
    };

    auto track_parallel = [&] (size_t thread, size_t start, size_t end ) {
      if (use_analytic()) { // use analytic
	for (size_t i=start; i<end; i++) {
	  ParticleT &particle = bunch.particles[i];
	  if (particle) {
	    for (size_t iter=0; iter<nloop; iter++) {
	      double t0_mm = bunch.t + iter * tracking_opts.dt_mm; // mm/c
	      double t1_mm = t0_mm + tracking_opts.dt_mm; // mm/c
	      if (particle.t0 > t1_mm) // particle won't come to existance within this time step
		continue;
	      if (particle.t0 > t0_mm) { // particle is created in this time step, drift it to t1_mm
		t0_mm = particle.t0; // mm/c
	      }
	      if (particle.S<0.0) { // particle already exists but it's still upstream of the tracking volume
		drift_particle(particle, t1_mm - t0_mm); // mm
		continue;
	      }
	      const size_t &imin = gsl_interp_bsearch(S_array_mm.data(), particle.S, 0, S_array_mm.size());
	      if (imin >= elements.size()) { // particle is downstream of the tracking volume
		drift_particle(particle, t1_mm - t0_mm); // mm
		continue;
	      }
	      const auto element_ptr = elements[imin]; // particle is inside the tracking volume
	      if (!element_ptr->is_particle_inside_aperture(particle)) { // particle has hit the aperture
		particle.lost_at(t0_mm);
		break;
	      }
	      const auto &em_field = element_ptr->get_field(particle.X, particle.Y, particle.S - S_array_mm[imin], t0_mm);
	      if (gsl_isnan(em_field.first[0])) { // in a consistent map if one component is Nan then all other components are Nan too
		particle.lost_at(t0_mm);
		break;
	      }
	      const auto &Ef = em_field.first;  // E [V/m]
	      const auto &Bf = em_field.second; // B [T]
	      move_particle_through_EBfield(particle, Ef, Bf, t1_mm - t0_mm);
	    }
	  }
	}
      } else if (use_leapfrog()) { // use leapfrog
	for (size_t i=start; i<end; i++) {
	  ParticleT &particle = bunch.particles[i];
	  if (particle) {
	    params[thread].mass = particle.mass;
	    params[thread].charge = particle.Q;
	    double dY[6], Y[6] = {
	      particle.X, // mm
	      particle.Y, // mm
	      particle.S, // mm
	      particle.Px, // MeV/c
	      particle.Py, // MeV/c
	      particle.Pz  // MeV/c
	    };
	    for (size_t iter=0; iter<nloop; iter++) {
	      double t0_mm = bunch.t + iter * tracking_opts.dt_mm; // mm/c
	      double t1_mm = t0_mm + tracking_opts.dt_mm; // mm/c
	      if (particle.t0 > t1_mm) // particle won't come to existance within this time step
		continue;
	      if (particle.t0 > t0_mm) { // particle is created in this time step, drift it to t1_mm
		t0_mm = particle.t0;
	      }
	      if (func(t0_mm, Y, dY, &params[thread]) == GSL_EBADFUNC) { // particle already exists it hits the aperture
		particle.lost_at(t0_mm);
		break;
	      }
	      // particle exists and is inside the tracking volume
	      const double dt_mm = t1_mm - t0_mm;
	      const auto &F = StaticVector<3>(dY[3], dY[4], dY[5]); // MeV/mm
	      const auto &v = StaticVector<3>(dY[0], dY[1], dY[2]); // c
	      const auto &a = (F - dot(v,F)*v) / particle.get_total_energy(); // c^2/mm
	      Y[0] += (dY[0] + 0.5 * a[0] * dt_mm) * dt_mm; // X
	      Y[1] += (dY[1] + 0.5 * a[1] * dt_mm) * dt_mm; // Y
	      Y[2] += (dY[2] + 0.5 * a[2] * dt_mm) * dt_mm; // S
	      Y[3] += dY[3] * dt_mm; // Px
	      Y[4] += dY[4] * dt_mm; // Py
	      Y[5] += dY[5] * dt_mm; // Pz
	    }
	    // update particle
	    particle.X  = Y[0]; // mm
	    particle.Y  = Y[1]; // mm
	    particle.S  = Y[2]; // mm
	    particle.Px = Y[3]; // MeV/c
	    particle.Py = Y[4]; // MeV/c
	    particle.Pz = Y[5]; // MeV/c
	  }
	}
      } else /* if (use_gsl()) */ { // use GSL
	gsl_odeiv2_driver *driver = get_gsl_driver(thread);
	for (size_t i=start; i<end; i++) {
	  ParticleT &particle = bunch.particles[i];
	  if (particle) {
	    params[thread].mass = particle.mass;
	    params[thread].charge = particle.Q;
	    double Y[6] = {
	      particle.X, // mm
	      particle.Y, // mm
	      particle.S, // mm
	      particle.Px, // MeV/c
	      particle.Py, // MeV/c
	      particle.Pz  // MeV/c
	    };
	    gsl_odeiv2_driver_reset(driver);
	    for (size_t iter=0; iter<nloop; iter++) {
	      double t0_mm = bunch.t + iter * tracking_opts.dt_mm; // mm/c
	      double t1_mm = t0_mm + tracking_opts.dt_mm; // mm/c
	      if (particle.t0 > t1_mm) // particle won't come to existance within this time step
		continue;
	      if (particle.t0 > t0_mm) { // particle is created in this time step, drift it to t1_mm
		drift_particle(particle, t1_mm - particle.t0); // mm
		Y[0] = particle.X; // mm
		Y[1] = particle.Y; // mm
		Y[2] = particle.S; // mm
		continue;
	      }
	      const int status = gsl_odeiv2_driver_apply(driver, &t0_mm, t1_mm, Y);
	      if (status == GSL_EBADFUNC) { // particle already exists it hits the aperture
		particle.lost_at(t0_mm);
		break;
	      }
	      if (status != GSL_SUCCESS) { // gsl integrator gave an error
		gsl_error = true;
		break;
	      }
	    }
	    // update particle
	    particle.X  = Y[0]; // mm
	    particle.Y  = Y[1]; // mm
	    particle.S  = Y[2]; // mm
	    particle.Px = Y[3]; // MeV/c
	    particle.Py = Y[4]; // MeV/c
	    particle.Pz = Y[5]; // MeV/c
	  }
	}
      }
    };
    for_all(RF_Track_Globals::number_of_threads, bunch.size(), track_parallel);
    
    bunch.t += nloop * tracking_opts.dt_mm;
    iterations_cnt += nloop;
    
    loop = [&] () { // does tracking need to continue?
      if (nsteps)
	return iterations_cnt < nsteps;
      return do_particles_need_to_be_tracked();
    } ();

    // apply space charge kick (or half if it's the last step)
    if (sc_nsteps != 0 && int(iterations_cnt/sc_nsteps) != int((iterations_cnt-1)/sc_nsteps)) {
      if (tracking_opts.verbose>1)
	std::cout << " applying space-charge " << sc_iterations_cnt++ << std::endl;
      RF_Track_Globals::SC_engine.compute_force(sc_force, bunch, particle_selector);
      const double dt = (loop ? 1.0 : 0.5) * (nloop * tracking_opts.dt_mm) / 1e3; // the very last one applies half step
      for (size_t i=0; i<bunch.size(); i++) {
	auto &particle = bunch.particles[i];
	if (particle_selector(particle)) {
	  const auto dP = StaticVector<3>(sc_force[i]) * dt; // MeV/c
	  particle.Px += dP[0]; // MeV/c
	  particle.Py += dP[1]; // MeV/c
	  particle.Pz += dP[2]; // MeV/c
	}
      }
    }
    
    // watchpoint
    if (wp_nsteps != 0 && int(iterations_cnt/wp_nsteps) != int((iterations_cnt-1)/wp_nsteps)) {
      const bool gzip = tracking_opts.wp_gzip;
      std::stringstream filename;
      if (gzip) filename << std::string("gzip -9 >");
      filename << tracking_opts.wp_basename << '.' << std::setw(6) << std::setfill('0') << wp_index++ << ".txt";
      if (gzip) filename << ".gz";
      if (FILE *file = gzip ? popen(filename.str().c_str(), "w") : fopen(filename.str().c_str(), "w")) {
	fprintf(file, "# t = %g mm/c\n", bunch.t); 
	for (const auto &particle : bunch.particles) {
	  if (particle) {
	    fprintf(file, "%.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g\n",
		    particle.X,    // mm
		    particle.Px,   // MeV/c
		    particle.Y,    // mm
		    particle.Py,   // MeV/c
		    particle.S,    // mm
		    particle.Pz,   // MeV/c
		    particle.mass, // MeV/c^2
		    particle.Q,    // e+
		    particle.N);   //
	  }
	}
	if (gzip)
	  pclose(file);
	else
	  fclose(file);
      } else {
	std::cerr << "error: cannot open output file '" << filename.str() << "'\n";
      }
    }
    
    append_bunch_info(bunch.get_info());
    
  } while(loop);

  // bring back particles to S_min = S_end_of_beamline
  if (nsteps==0) {
    double dt_min = std::numeric_limits<double>::max(); // mm/c
    for (auto const &particle : bunch.particles) {
      if (particle && bunch.t>=particle.t0) {
	const double Vz = particle.get_Vx_Vy_Vz()[2]; // c
        if (particle.S>S_end_of_beamline_mm && Vz>0.0) {
	  const double dt = (particle.S - S_end_of_beamline_mm) / Vz; // mm/c
	  if (dt < dt_min)
	    dt_min = dt;
	} else if (particle.S<0.0 && Vz<0.0) {
	  const double dt = particle.S / Vz; // mm/c
	  if (dt < dt_min)
	    dt_min = dt;
	}
      }
    }
    if (dt_min != std::numeric_limits<double>::max()) {
      for (auto &particle : bunch.particles) {
	if (particle && bunch.t>=particle.t0) {
	  const auto dX = particle.get_Vx_Vy_Vz() * dt_min; // mm
	  particle.X -= dX[0]; // mm
	  particle.Y -= dX[1]; // mm
	  particle.S -= dX[2]; // mm
	}
      }
      bunch.t -= dt_min; // mm/c
    }
  }
  
  // finalize GSL ODE
  if (use_gsl())
    free_gsl_drivers();
  
  if (gsl_error)
    std::cerr << "warning: an error occurred integrating the equations of motion, consider reducing the integration step\n";

  if (tracking_opts.verbose>0)
    std::cout << "total number of integration steps = " << iterations_cnt << std::endl;

  return bunch;
}
