/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <complex>
#include <array>
#include <cmath>
#include <thread>
#include <gsl/gsl_sys.h>

#include "static_field.hh"
#include "RF_Track.hh"
#include "numtools.hh"
#include "constants.hh"
#include "move_particle.hh"
#include "greens_function.hh"

StaticField::StaticField(const Mesh3d &Ex,
			 const Mesh3d &Ey,
			 const Mesh3d &Ez,
			 const Mesh3d &Bx,
			 const Mesh3d &By,
			 const Mesh3d &Bz,
			 double x0_, double y0_, /*, double z0_, */ // m
			 double hx_, double hy_, double hz_, // m
			 double length ) : rbf_interpolate(5,5,5,hx_*1e3,hy_*1e3,hz_*1e3),
					   x0(x0_*1e3), // accepts m, stores mm
					   y0(y0_*1e3), // accepts m, stores mm
					   z0(0.0), // mm
					   hx(hx_*1e3), // accepts m, stores mm
					   hy(hy_*1e3), // accepts m, stores mm
					   hz(hz_*1e3)  // accepts m, stores mm
{
  set_Ex_Ey_Ez(Ex,Ey,Ez);
  set_Bx_By_Bz(Bx,By,Bz);
  set_nsteps(Ex.size3()-1);
  set_length(length);
}

void StaticField::set_Ex_Ey_Ez(const Mesh3d &Ex, const Mesh3d &Ey, const Mesh3d &Ez )
{
  mesh_Ex = Ex;
  mesh_Ey = Ey;
  mesh_Ez = Ez;
}

void StaticField::set_Bx_By_Bz(const Mesh3d &Bx, const Mesh3d &By, const Mesh3d &Bz )
{
  B0 = [&] () {
    StaticVector<3> average;
    CumulativeKahanSum<double> sum;
    sum = 0.0; for (size_t i=0; i<Bx.size(); i++) { sum += Bx.data()[i]; } average[0] = sum / Bx.size();
    sum = 0.0; for (size_t i=0; i<By.size(); i++) { sum += By.data()[i]; } average[1] = sum / By.size();
    sum = 0.0; for (size_t i=0; i<Bz.size(); i++) { sum += Bz.data()[i]; } average[2] = sum / Bz.size();
    return average;
  } ();
  
  const int
    Nx = Bx.size1(),
    Ny = Bx.size2(),
    Nz = Bx.size3();
  
  mesh_Bx.resize(Nx,Ny,Nz);
  mesh_By.resize(Nx,Ny,Nz);
  mesh_Bz.resize(Nx,Ny,Nz);
  
  auto copy_mesh_subtr_average = [&] (const Mesh3d &from, Mesh3d &to, double average ) {
    for (int i=0; i<Nx; i++) {
      for (int j=0; j<Ny; j++) {
	for (int k=0; k<Nz; k++) {
	  to.elem(i,j,k) = from.elem(i,j,k) - average;
	}
      }
    }
  };
  
  copy_mesh_subtr_average(Bx, mesh_Bx, B0[0]);
  copy_mesh_subtr_average(By, mesh_By, B0[1]);
  copy_mesh_subtr_average(Bz, mesh_Bz, B0[2]);
}

void StaticField::set_length(double length )  // accepts m
{
  const double z1_max = (mesh_Ex.size3()-1)*hz;
  if (length<0.0) {
    z1 = z1_max;
  } else {
    z1 = z0 + length*1e3;
    if (z1>z1_max) z1 = z1_max;
  }
}

std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>> StaticField::get_field_jacobian(double x, double y, double z, double /* t */ ) // x,y,z [mm], returns V/m/mm, and T/mm
{
  z += z0;
  if ((z<0.0) || z>(z1-z0))
    return std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>>(StaticMatrix<3,3>(0.0), StaticMatrix<3,3>(0.0));
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  StaticMatrix<3,3> E(0.0), B(0.0);
  std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>> retval(E, B);
  return retval;
}

std::pair<StaticVector<3>,StaticVector<3>> StaticField::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t /* mm/c */ )
{
  z += z0;
  if ((z<0.0) || z>(z1-z0))
    return std::pair<StaticVector<3>,StaticVector<3>>(StaticVector<3>(0.0), StaticVector<3>(0.0));
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  const int
    Nx = mesh_Ex.size1(),
    Ny = mesh_Ex.size2(),
    Nz = mesh_Ex.size3();
  
  if (x<0.0||y<0.0||z<0.0||x>=double(Nx)||y>=double(Nz))
    return std::pair<StaticVector<3>,StaticVector<3>>(StaticVector<3>(0.0), StaticVector<3>(0.0));

  StaticVector<3> E(mesh_Ex(x,y,z), mesh_Ey(x,y,z), mesh_Ez(x,y,z));
  
  double iinteger, jinteger, kinteger;
  (void)modf(x, &iinteger);
  (void)modf(y, &jinteger);
  (void)modf(z, &kinteger);
  const int i = int(iinteger);
  const int j = int(jinteger);
  const int k = int(kinteger);

  const size_t Nd = rbf_interpolate.get_stencil_size();
  double Bx[Nd], By[Nd], Bz[Nd];

  int i0 = [&] () -> int {
    if (i-int(rbf_interpolate.Nx>>1)<0)
      return 0;
    if (i+int(rbf_interpolate.Nx>>1)>Nx-1)
      return Nx-rbf_interpolate.Nx;
    return i-int(rbf_interpolate.Nx>>1);
  } ();

  int j0 = [&] () -> int {
    if (j-int(rbf_interpolate.Ny>>1)<0)
      return 0;
    if (j+int(rbf_interpolate.Ny>>1)>Ny-1)
      return Ny-rbf_interpolate.Ny;
    return j-int(rbf_interpolate.Ny>>1);
  } ();

  int k0 = [&] () -> int {
    if (k<int(rbf_interpolate.Nz>>1))
      return 0;
    if (k+int(rbf_interpolate.Nz>>1)>Nz-1)
      return Nz-rbf_interpolate.Nz;
    return k-int(rbf_interpolate.Nz>>1);
  } ();
  {
    size_t l=0;
    for (int i_=0; i_<rbf_interpolate.Nx; i_++) {
      for (int j_=0; j_<rbf_interpolate.Ny; j_++) {
	for (int k_=0; k_<rbf_interpolate.Nz; k_++) {
	  Bx[l] = mesh_Bx.elem(i0+i_, j0+j_, k0+k_);
	  By[l] = mesh_By.elem(i0+i_, j0+j_, k0+k_);
	  Bz[l] = mesh_Bz.elem(i0+i_, j0+j_, k0+k_);
	  l++;
	}
      }
    }
  }
  const double *c_ptr = rbf_interpolate.prepare(Bx, By, Bz);

  StaticVector<3> B(0.0);
  {
    size_t l=0;
    for (int i_=0; i_<rbf_interpolate.Nx; i_++) {
      for (int j_=0; j_<rbf_interpolate.Ny; j_++) {
	for (int k_=0; k_<rbf_interpolate.Nz; k_++) {
	  StaticVector<3> c(c_ptr[l     ],
			    c_ptr[l+  Nd],
			    c_ptr[l+2*Nd]);
	  B += rbf_interpolate.func((x-double(i0+i_)) * hx,
				    (y-double(j0+j_)) * hy,
				    (z-double(k0+k_)) * hz) * c;
	  l++;
	}
      }
    }
  }
  
  return std::pair<StaticVector<3>,StaticVector<3>>(E, B0 + B);
}

struct Params {
  double mass;
  double charge;
  StaticField *rf_field;
};

static int func(double S /* m */, const double Y[], double dY[], void *params_ )
{
  // init parameters
  const Params *params = (const Params *)(params_);
  StaticField *this_ = params->rf_field;

  // check aperture
  if (!this_->is_point_inside_aperture(Y[0], Y[1]))
    return GSL_EBADFUNC;

  // compute force
  const auto field = this_->get_field(Y[0], Y[1], S*1e3, Y[2]);
  if (gsl_isnan(field.first[0])) // NaNs indicate walls
    return GSL_EBADFUNC;

  // init variables
  const double mass = params->mass;
  const double charge = params->charge;
  const auto &P = StaticVector<3>(Y[3], Y[4], Y[5]); // MeV/c
  const auto &v = P / hypot(mass, P); // c

  const auto &Efield = field.first; // V/m
  const auto &Bfield = field.second * C_LIGHT; // T*c = V/m
  const auto &F = charge * (Efield + cross(v,Bfield)) / 1e6; // MeV/m
  
  // update system of equations for GSL
  const double inv_Pz = 1e3 / P[2]; // 1e3/(MeV/c)
  const double inv_Vz = 1.0 / v[2]; // 1/c
  dY[0] = P[0] * inv_Pz; // mrad
  dY[1] = P[1] * inv_Pz; // mrad
  dY[2] = 1e3  * inv_Vz; // 1e3/c
  dY[3] = F[0] * inv_Vz; // MeV/m/c
  dY[4] = F[1] * inv_Vz; // MeV/m/c
  dY[5] = F[2] * inv_Vz; // MeV/m/c
  return GSL_SUCCESS;
}

static int jac(double S /* m */, const double Y[], double *dfdy, 
	       double dfdt[], void *params_ )
{
  // init parameters
  const Params *params = (const Params *)(params_);
  StaticField *this_ = params->rf_field;

  if (!this_->is_point_inside_aperture(Y[0], Y[1]))
    return GSL_EBADFUNC;

  const double mass = params->mass;
  const double charge = params->charge;

  // init variables
  const auto &P = StaticVector<3>(Y[3], Y[4], Y[5]); // MeV/c
  const auto &E = hypot(mass, P); // MeV
  const auto &v = P / E; // c

  // compute force
  const auto &field = this_->get_field(Y[0], Y[1], S*1e3, Y[2]);
  if (gsl_isnan(field.first[0])) // in a consistent map if one component of E is Nan, then all other components of E and of B are Nan too
    return GSL_EBADFUNC;
  const auto &Ef = field.first / 1e6; // V/m -> MV/m
  const auto &Bf = field.second * C_LIGHT / 1e6; // T*c = V/m -> MV/m
  const auto &F = charge * (Ef + cross(v, Bf)); // MeV/m
  const auto &a = (F - dot(v,F)*v) / E; // c^2/m

  // dfdt
  const double inv_Vz = 1.0 / v[2];
  const double inv_Pz = 1.0 / P[2];
  const double xp = P[0] * inv_Pz; // rad
  const double yp = P[1] * inv_Pz; // rad
  dfdt[0] = 1e3 * (a[0] - xp * a[2]) * inv_Vz * inv_Vz; // mrad
  dfdt[1] = 1e3 * (a[1] - yp * a[2]) * inv_Vz * inv_Vz; // mrad
  dfdt[2] = -a[2] * inv_Vz * inv_Vz * inv_Vz; // 1/c
  dfdt[3] = F[0] * dfdt[2]; // MeV/m/c
  dfdt[4] = F[1] * dfdt[2]; // MeV/m/c
  dfdt[5] = F[2] * dfdt[2]; // MeV/m/c
  dfdt[2] *= 1e3; // 1000/c
  
  // jacobian of force with Y
  gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, 6, 6);
  gsl_matrix *m = &dfdy_mat.matrix;
  for (int i=0; i<3; i++) {
    for (int j=0; j<3; j++) {
      gsl_matrix_set (m, i, j, 0.0); // dVi / dj
    }
  }
  auto &&jacobian = this_->get_field_jacobian(Y[0], Y[1], S*1e3, Y[2]);
  jacobian.first /= 1e6; // V/m/mm -> MV/m/mm
  jacobian.second *= C_LIGHT / 1e6; // T/mm*c = V/m/mm -> MV/m/mm
  const auto &Ef_jac = jacobian.first; // V/m/mm -> MV/m/mm
  const auto &Bf_jac = jacobian.second; // T/mm*c = V/m/mm -> MV/m/mm
  gsl_matrix_set (m, 3, 0, charge * (Ef_jac[0][0] * inv_Vz + yp * Bf_jac[2][0]        - Bf_jac[1][0])); // MeV/m/c/mm
  gsl_matrix_set (m, 3, 1, charge * (Ef_jac[0][1] * inv_Vz + yp * Bf_jac[2][1]        - Bf_jac[1][1])); // MeV/m/c/mm
  gsl_matrix_set (m, 3, 2, charge * (Ef_jac[0][2] +        v[1] * Bf_jac[2][2] - v[2] * Bf_jac[1][2])); // MeV/m/mm
  gsl_matrix_set (m, 4, 0, charge * (Ef_jac[1][0] * inv_Vz + Bf_jac[0][0] -   xp * Bf_jac[2][0])); // MeV/m/c/mm
  gsl_matrix_set (m, 4, 1, charge * (Ef_jac[1][1] * inv_Vz + Bf_jac[0][1] -   xp * Bf_jac[2][1])); // MeV/m/c/mm
  gsl_matrix_set (m, 4, 2, charge * (Ef_jac[1][2] +   v[2] * Bf_jac[0][2] - v[0] * Bf_jac[2][2])); // MeV/m/mm
  gsl_matrix_set (m, 5, 0, charge * (Ef_jac[2][0] * inv_Vz + xp * Bf_jac[1][0] -   yp * Bf_jac[0][0])); // MeV/m/c/mm
  gsl_matrix_set (m, 5, 1, charge * (Ef_jac[2][1] * inv_Vz + xp * Bf_jac[1][1] -   yp * Bf_jac[0][1])); // MeV/m/c/mm
  gsl_matrix_set (m, 5, 2, charge * (Ef_jac[2][2] +        v[0] * Bf_jac[1][2] - v[1] * Bf_jac[0][2])); // MeV/m/mm
  
  const double inv_E = 1.0 / E;
  gsl_matrix_set (m, 0, 3, inv_Pz); // dxp / dj
  gsl_matrix_set (m, 0, 4, 0.0); // dxp / dj
  gsl_matrix_set (m, 0, 5, -xp * inv_Pz); // dxp / dj
  gsl_matrix_set (m, 1, 3, 0.0); // dyp / dj
  gsl_matrix_set (m, 1, 4, inv_Pz); // dyp / dj
  gsl_matrix_set (m, 1, 5, -yp * inv_Pz); // dyp / dj
  gsl_matrix_set (m, 2, 3, xp * inv_E); // dyp / dj
  gsl_matrix_set (m, 2, 4, yp * inv_E); // dyp / dj
  gsl_matrix_set (m, 2, 5, (inv_E - inv_Pz * inv_Vz)); // dyp / dj
  gsl_matrix_set (m, 3, 3, charge * (Ef[0] * gsl_matrix_get(m, 2, 3) + Bf[2] * gsl_matrix_get(m, 1, 3)));
  gsl_matrix_set (m, 3, 4, charge * (Ef[0] * gsl_matrix_get(m, 2, 4) + Bf[2] * gsl_matrix_get(m, 1, 4)));
  gsl_matrix_set (m, 3, 5, charge * (Ef[0] * gsl_matrix_get(m, 2, 5) + Bf[2] * gsl_matrix_get(m, 1, 5)));
  gsl_matrix_set (m, 4, 3, charge * (Ef[1] * gsl_matrix_get(m, 2, 3) - Bf[2] * gsl_matrix_get(m, 0, 3)));
  gsl_matrix_set (m, 4, 4, charge * (Ef[1] * gsl_matrix_get(m, 2, 4) - Bf[2] * gsl_matrix_get(m, 0, 4)));
  gsl_matrix_set (m, 4, 5, charge * (Ef[1] * gsl_matrix_get(m, 2, 5) - Bf[2] * gsl_matrix_get(m, 0, 5)));
  gsl_matrix_set (m, 5, 3, charge * (Ef[2] * gsl_matrix_get(m, 2, 3) + Bf[1] * gsl_matrix_get(m, 0, 3) - Bf[0] * gsl_matrix_get(m, 1, 3)));
  gsl_matrix_set (m, 5, 4, charge * (Ef[2] * gsl_matrix_get(m, 2, 4) + Bf[1] * gsl_matrix_get(m, 0, 4) - Bf[0] * gsl_matrix_get(m, 1, 4)));
  gsl_matrix_set (m, 5, 5, charge * (Ef[2] * gsl_matrix_get(m, 2, 5) + Bf[1] * gsl_matrix_get(m, 0, 5) - Bf[0] * gsl_matrix_get(m, 1, 5)));
  gsl_matrix_set (m, 0, 3, 1e3 * gsl_matrix_get (m, 0, 3));
  gsl_matrix_set (m, 0, 5, 1e3 * gsl_matrix_get (m, 0, 5));
  gsl_matrix_set (m, 1, 4, 1e3 * gsl_matrix_get (m, 1, 4));
  gsl_matrix_set (m, 1, 5, 1e3 * gsl_matrix_get (m, 1, 5));
  gsl_matrix_set (m, 2, 3, 1e3 * gsl_matrix_get (m, 2, 3));
  gsl_matrix_set (m, 2, 4, 1e3 * gsl_matrix_get (m, 2, 4));
  gsl_matrix_set (m, 2, 5, 1e3 * gsl_matrix_get (m, 2, 5));
  return GSL_SUCCESS;
}

void StaticField::track0_initialize(Bunch6d &bunch )
{
  // init GSL ODE
  sys.resize(RF_Track_Globals::number_of_threads);
  for (size_t i=0; i<RF_Track_Globals::number_of_threads; i++) {
    sys[i].function = func;
    sys[i].jacobian = jac;
    sys[i].dimension = 6;
    sys[i].params = new Params({ 0.0, 0.0, this });
  }
  if (use_gsl())
    init_gsl_drivers(sys);
  gsl_error = false;
}

void StaticField::track0_finalize()
{
  if (gsl_error)
    std::cerr << "warning: an error occurred integrating the equations of motion, consider increasing 'nsteps'\n";

  for (auto s: sys) {
    delete (Params*)(s.params);
  }

  if (use_gsl())
    free_gsl_drivers();
}

void StaticField::track0(Particle &particle, double S, size_t start_step, size_t end_step, size_t thread ) const
{
  Params *params = (Params *)(sys[thread].params);
  params->mass = particle.mass;
  params->charge = particle.Q;
  const double dS = get_length() / nsteps; // m
  double S_initial; // m
  int status = GSL_SUCCESS;
  if (use_analytic()) { // use analytic
    for (size_t step = start_step; step<end_step; step++) {
      S_initial = step * dS; // m
      if (!is_particle_inside_aperture(particle)) {
	status = GSL_EBADFUNC;
	break;
      }
      const auto &field = const_cast<StaticField*>(this)->get_field(particle.x, particle.y, S_initial*1e3, particle.t);
      if (gsl_isnan(field.first[0])) { // in a consistent map if one component is Nan then all other components are Nan too
	status = GSL_EBADFUNC;
	break;
      }
      const auto &Ef = field.first; // V/m
      const auto &Bf = field.second; // T
      move_particle_through_EBfield(particle, Ef, Bf, dS*1e3);
    }    
  } else if (use_leapfrog()) { // use leapfrog
    double dY[6], Y[6];
    {
      const auto P = particle.get_Px_Py_Pz(); // momentum
      Y[0] = particle.x; // mm
      Y[1] = particle.y; // mm
      Y[2] = particle.t; // mm/c
      Y[3] = P[0]; // MeV/c
      Y[4] = P[1]; // MeV/c
      Y[5] = P[2]; // MeV/c
    }
    for (size_t step = start_step; step<end_step; step++) {
      S_initial = step * dS;
      if ((status = func(S_initial, Y, dY, sys[thread].params)) == GSL_EBADFUNC) {
	break;
      }
      const double Vz = 1.0 / dY[2]; // c/1e3
      const auto F = StaticVector<3>(dY[3], dY[4], dY[5]) * Vz; // MeV/mm
      const auto v = StaticVector<3>(dY[0] * Vz, dY[1] * Vz, Vz * 1e3); // c
      const auto a = (F - dot(v,F)*v) / particle.get_total_energy(); // c^2/mm
      double dt_mm = dS * dY[2]; // mm/c
      if (a[2]>2*std::numeric_limits<double>::epsilon()*v[2]*dt_mm)
	dt_mm = (sqrt(v[2]*v[2] + 2e3*a[2]*dS) - v[2]) / a[2]; // mm/c
      Y[0] += (v[0] + 0.5 * a[0] * dt_mm) * dt_mm; // X
      Y[1] += (v[1] + 0.5 * a[1] * dt_mm) * dt_mm; // Y
      Y[2] += dt_mm; // S
      Y[3] += dY[3] * dS; // Px
      Y[4] += dY[4] * dS; // Py
      Y[5] += dY[5] * dS; // Pz
    }
    // update particle
    double inv_Pz = 1e3 / Y[5]; // 1e3/(MeV/c)
    particle.x  = Y[0]; // mm
    particle.y  = Y[1]; // mm
    particle.t  = Y[2]; // mm/c
    particle.xp = Y[3] * inv_Pz; // mrad
    particle.yp = Y[4] * inv_Pz; // mrad
    particle.Pc = hypot(Y[3], Y[4], Y[5]); // MeV/c
  } else /* if (use_gsl()) */ { // use GSL
    double Y[6];
    {
      const auto P = particle.get_Px_Py_Pz(); // momentum
      Y[0] = particle.x; // mm
      Y[1] = particle.y; // mm
      Y[2] = particle.t; // mm/c
      Y[3] = P[0]; // MeV/c
      Y[4] = P[1]; // MeV/c
      Y[5] = P[2]; // MeV/c
    }
    S_initial = start_step * dS; // m
    gsl_odeiv2_driver *driver = drivers[thread];
    gsl_odeiv2_driver_reset(driver);
    for (size_t step = start_step+1; step<=end_step; step++) {
      if ((status = gsl_odeiv2_driver_apply(driver, &S_initial, step * dS, Y)) != GSL_SUCCESS)
	break;
    }
    // update particle
    double inv_Pz = 1e3 / Y[5]; // 1e3/(MeV/c)
    particle.x  = Y[0]; // mm
    particle.y  = Y[1]; // mm
    particle.t  = Y[2]; // mm/c
    particle.xp = Y[3] * inv_Pz; // mrad
    particle.yp = Y[4] * inv_Pz; // mrad
    particle.Pc = hypot(Y[3], Y[4], Y[5]); // MeV/c
  }
  if (status == GSL_EBADFUNC) particle.lost_at(S + S_initial);
  else if (status != GSL_SUCCESS) gsl_error = true;
}

double StaticField::get_divB(double x /* mm */, double y /* mm */, double z /* mm */ ) const
{
  auto retval = 0.0;
  return retval;
}
