/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <array>
#include <limits>
#include <numeric>
#include <utility>

#include "for_all.hh"
#include "bunch6d.hh"
#include "bunch6dt.hh"
#include "numtools.hh"
#include "rotation.hh"
#include "lorentz_boost.hh"
#include "electron_cooler.hh"
#include "move_particle.hh"
#include "stats.hh"

// #define DEBUG

ElectronCooler::ElectronCooler(double length /* m */, double rx /* m */, double ry /* m */, double N_ele /* #/m^3 */, double Vz /* c */ ) : Plasma(length, rx, ry, N_ele, Vz, 0.04, 1.7e-4)
{
  // set_aperture(rx, ry, "circular"); // the element aperture does not coincide with the electron beam size
  // const double Kbolt_T_ele_r = 0.04; // eV, see CAS CERN-92-01, pag 157-158
  // const double Kbolt_T_ele_l = 1.7e-4; // eV, see CAS CERN-92-01, pag 157-158
}

// tracking
std::list<Bunch6d_info> ElectronCooler::track(Bunch6d &bunch )
{
  std::list<Bunch6d_info> transport_table;

#ifdef DEBUG
  std::cout << "ElectronCooler::enter tracking " << bunch.get_ngood() << std::endl;
#endif

  const size_t Nthreads = RF_Track_Globals::number_of_threads;
  const size_t Nparticles = bunch.size();

  // resize meshes
  size_t
    Nx = size1(),
    Ny = size2(),
    Nz = nsteps;

  // init mesh external forces [MeV/mm]
  dP_to_electrons_mesh_parallel.resize(Nthreads);
  for (auto &dP_to_electrons_mesh: dP_to_electrons_mesh_parallel)
    dP_to_electrons_mesh.resize(Nx, Ny, Nz);

  const double l_max_mm = sqrt(get_area())*1e3; // mm, transverse dimension of electron beam (approximate)

  // convert the field from tesla to MV/mm
  const auto Bf = get_static_Bfield() * C_LIGHT / 1e9; // MV/mm
  const auto norm_Bf_sqr = dot(Bf,Bf); // (MV/mm)^2

  // start integration loop
  const double dS = get_length() / nsteps;
  const double dS_mm = dS*1e3; // mm
  double S_mm = 0.0; // mm, coordinate along the element
  for (size_t step = 0; step<nsteps; step++) {
    // initialization of the thread's local variables
    std::vector<double> dt_mm_average(Nthreads); // mm/c, average time spent by the beam in one step (used to advance the electron mesh)
    const double l_debye_mm = [&] () { // mm, Debye length at half step
      const double half_dS_mm = 0.5 * dS_mm;
      auto P0 = bunch.get_average_particle();
      move_particle_through_Bfield(P0, get_static_Bfield(), half_dS_mm);
      const auto r_ion = StaticVector<3>(P0.x, P0.y, S_mm + half_dS_mm); // mm
      const auto &state = plasma_get_state_bnd(r_ion);
      const auto r_ion = StaticVector<3>(P0.x, P0.y, S_mm + half_dS_mm); // mm
      const auto &state = plasma_get_state_bnd(r_ion);
      return get_debye_length(state) * 1e3; // mm
    } ();
    auto track_particle_parallel = [&] (size_t thread, size_t start, size_t end ) {
      auto &dP_to_electrons_mesh = dP_to_electrons_mesh_parallel[thread]; // MeV/mm
      auto &dt_mm_avg = dt_mm_average[thread]; // mm/c
      for (size_t i=0; i<Nx*Ny*Nz; i++)
	dP_to_electrons_mesh.data()[i] = StaticVector<3>(0.0);
      dt_mm_avg = 0.0;
      size_t count = 0; // count of good particles (necessary to dt_mm_average);
      for (size_t n=start; n<end; n++) {
	auto &ion = bunch.particles[n];
	if (ion) {
#ifdef DEBUG
	  std::cout << "START" << std::endl;
#endif
	  // ion position halfway through the cell
	  const auto &r_ion = [&] () {
	    auto _ion = ion;
	    const double half_dS_mm = 0.5 * dS_mm;
	    move_particle_through_Bfield(_ion, get_static_Bfield(), half_dS_mm);
	    return StaticVector<3>(_ion.x, _ion.y, S_mm + half_dS_mm); // mm
	  }();
	  
	  if (!is_point_inside_aperture(r_ion[0], r_ion[1])) {
	    ion.lost_at(bunch.S + 0.5*dS);
	    continue;
	  }
	  
	  if (!is_point_inside_area(r_ion[0], r_ion[1])) { // if the ion is outside plasma just drift the ion
	    move_particle_through_Bfield(ion, get_static_Bfield(), dS_mm);
	    continue;
	  }

	  // consider the electrons
	  const auto &state = plasma_get_state(r_ion);
	  const auto N_ele = plasma_get_number_density(state); // #/m^3
	  if (N_ele == 0.0) { // if the ion is in a zero-density region, just drift the ion
	    move_particle_through_Bfield(ion, get_static_Bfield(), dS_mm);
	    continue;
	  }

	  // other auxiliary variables
	  const auto &V_ele = plasma_get_velocity(state); // c
	  const double inv_gamma_ele = sqrt(1.0 - dot(V_ele,V_ele));
	  const double gamma_ele = 1.0 / inv_gamma_ele;
	  const double E_ele = get_mass() * gamma_ele; // MeV, total energy of the electron

	  // radial and longitudinal electron temperatures (times the Boltzmann constant)
	  const double D_ele_r = get_D_ele_r(); // c
	  const double D_ele_l = get_D_ele_l(); // c
	  const double D_ele = hypot(D_ele_r, D_ele_l); // c

	  // prepare for physics
	  const double Q_ion = ion.Q;
	  const double Q_ele = get_Q();

	  //////////
	  // You have: e*e/(4*pi*epsilon0)
	  // You want: MeV*mm
	  // e*e/(4*pi*epsilon0) = 1.439964485044515e-12 MeV*mm
	  // e*e/(4*pi*epsilon0) = (1 / 694461572063.7623) MeV*mm
	  const double K = (Q_ion * Q_ele) / 694461572063.7623; // (e*e/4/pi/epsilon0) MeV*mm
	  const double reduced_mass = ion.mass * get_mass() / (ion.mass + get_mass()); // MeV/c^2

	  // other auxiliary variables
	  const auto omega_ele = Q_ele * Bf / E_ele; // rad / (mm/c), angular velocity
	  const double r_ele_mm = norm_Bf_sqr>0.0 ? norm(cross(omega_ele,V_ele)) / dot(omega_ele,omega_ele) : std::numeric_limits<double>::infinity(); // mm, Larmor radius of the electron

	  const auto &V_ion = ion.get_Vx_Vy_Vz(); // c
	  const auto &dt_mm = dS_mm / V_ion[2]; // mm/c, time spent by the ion in the cell
	  dt_mm_avg += (dt_mm - dt_mm_avg) / ++count; // mm/c, running average of dt_mm (used to advance the electron mesh)
	  const double gamma_ion = ion.get_gamma();
	  
	  // estimate the Coulomb logarithm for the fast collisions
	  auto L0 = [&] (const double &norm_U0 ) {
	    const double norm_U0_sqr = norm_U0*norm_U0; // c^2
	    const double KE = 0.5 * reduced_mass * norm_U0_sqr; // MeV, kinetic energy
	    const double l_min_mm = fabs(K) / KE; // mm, minimum impact distance
	    const double l_scr_mm = std::min(l_max_mm, std::max(l_debye_mm, l_min_mm)); // mm, screening length in the lab frame
	    const double r_min = l_min_mm; // mm
	    const double r_max = std::min(l_scr_mm, std::max(r_ele_mm, l_min_mm)); // mm
	    return log(r_max / r_min);
	  };

	  // estimate the Coulomb logarithm for the adiabatic collisions
	  auto LA = [&] (const double &norm_UA ) {
	    const double norm_UA_sqr = norm_UA*norm_UA; // c^2
	    const double KE = 0.5 * reduced_mass * norm_UA_sqr; // MeV, kinetic energy
	    const double l_min_mm = fabs(K) / KE / Q_ion; // mm, minimum impact distance ((2.10) *not numered* in p235.pdf)
	    const double l_scr_mm = std::min(l_max_mm, std::max(l_debye_mm, l_min_mm)); // mm, screening length in the lab frame
	    const double r_max = std::min(l_scr_mm, std::min(norm_UA * dt_mm, norm_UA / norm(omega_ele))); // mm
	    const double r_min = std::max(r_ele_mm, l_min_mm); // mm
	    return log(r_max / r_min);
	  };

	  // compute the stopping force
	  const StaticVector<3> F0 = [&](){ // MeV/m, stopping force for fast collisions
	    const auto U0 = relativistic_velocity_addition(-V_ele, V_ion); // relativistic velocity addition, the ion velocity in the electron frame
	    const auto &nz = normalize(V_ele); // unit vector running parallel to the electron in the lab's frame
	    const auto &nr = normalize(U0 - nz * dot(U0, nz)); // radial ion velocity, orthogonal to V_ele
	    const double U0_l = dot(U0, nz); // axial component of the velocity
	    const double U0_r = dot(U0, nr); // radial velocity
	    const double norm_U0 = norm(U0);
	    const double N_ele_star = N_ele * inv_gamma_ele; // #/m^3, electron number density in the reference frame of the electrons
	    const auto F_ = -N_ele_star * 2.0*M_PI * (K*K) / (0.5*reduced_mass) / 1e6; // MeV * mm^2 / m^3 / 1e6 = MeV/m
    	    if (norm_U0>D_ele_r)
	      return F_ * L0(norm_U0) * U0 / (norm_U0*norm_U0*norm_U0); // (4.1) p235.pdf //// CAS CERN-92-01, pag 166 (5.4a)
	    double F_r = F_ * sqrt(M_PI/8) * L0(D_ele_r) * U0_r / (D_ele_r*D_ele_r*D_ele_r); // CAS CERN-92-01, pag 166 (5.4b)
	    double F_l = F_ / (D_ele_r*D_ele_r);
	    if (U0_l>D_ele_l)
	      F_l *= (U0_l>0.0 ? 1 : (U0_l<0.0 ? -1 : 0)) * L0(U0_l) - U0_l * sqrt(M_PI/2) * L0(D_ele_r) / D_ele_r; // (4.3) p235.pdf /// CAS CERN-92-01, pag 165 (5.3c)
	    else
	      F_l *= L0(D_ele_l) * U0_l * sqrt(2/M_PI) / D_ele_l; // (4.3) p235.pdf //// CAS CERN-92-01, pag 165 (5.3c)
	    const auto F = nz*F_l + nr*F_r; // MeV/m, stopping force, in the electron frame
	    const auto F_4d = StaticVector<4>(dot(F,U0), F[0], F[1], F[2]) / sqrt(1.0 - dot(U0,U0)); // 4-force in the electrons frame
	    const auto F_lab_4d = lorentz_boost(-V_ele, F_4d); // 4-force in the Lab frame
	    return StaticVector<3>(F_lab_4d[1], F_lab_4d[2], F_lab_4d[3]) / gamma_ion; // stopping force in the Lab frame
	  }();

	  const StaticVector<3> FA = [&](){ // MeV/m, stopping force for adiabatic collisions
	    if (norm_Bf_sqr==0.0)
	      return StaticVector<3>(0.0);
	    const auto &nz = normalize(Bf); // unit vector running parallel Bf
	    const auto V_ele_l = nz * dot(V_ele, nz); // component of the electron velocity parallel to Bf
	    const auto UA = relativistic_velocity_addition(-V_ele_l, V_ion); // relativistic velocity addition, the ion velocity in the electron frame
	    const auto &nr = normalize(UA - nz * dot(UA, nz)); // radial electron velocity, orthogonal to UA
	    const double UA_l = dot(UA, nz); // axial component of the velocity
	    const double UA_r = dot(UA, nr); // radial velocity, will be different in case of scattering angle
	    const double norm_UA = norm(UA);
	    const double N_ele_star = N_ele * sqrt(1.0 - dot(V_ele_l,V_ele_l)); // #/m^3, electron number density in the reference frame of the electrons
	    const auto F_ = -N_ele_star * 2.0*M_PI * (K*K) / (0.5*reduced_mass) / 1e6; // MeV * mm^2 / m^3 / 1e6 = MeV/m
	    double F_r = F_, F_l = F_;
	    if (norm_UA>hypot(D_ele_r,D_ele_l)) {
	      const double LC = LA(norm_UA); // Coulomb logarithm
	      F_r *= 0.5 * LC * (UA_r*UA_r - 2*UA_l*UA_l)*UA_r/pow(norm_UA, 5); // (3.2) p235.pdf
	      F_l *= 1.5 * LC * UA_r*UA_r*UA_l/pow(norm_UA, 5); // (3.3) p235.pdf
	    } else if (fabs(UA_r) > D_ele_r) {
	      F_r *= 0.5 * sqrt(2.0/M_PI) * UA_r * LA(D_ele_l) * log(D_ele_l / UA_r) / (D_ele_l*D_ele_l*D_ele_l); // (3.7) p235.pdf
	      F_l *= 0.5 * sqrt(2.0/M_PI) * UA_l * LA(UA_r) / (D_ele_l*D_ele_l*D_ele_l); // (3.8) p235.pdf
	    } else {
	      F_r = 0.0;
	      F_l *= 0.5 * sqrt(2.0/M_PI) * UA_l * LA(D_ele_r) / (D_ele_l*D_ele_l*D_ele_l); // (3.8) p235.pdf
	    }
	    const auto F = nz*F_l + nr*F_r; // MeV/m, stopping force, in the electron frame
	    const auto F_4d = StaticVector<4>(dot(F,UA), F[0], F[1], F[2]) / sqrt(1.0 - dot(UA,UA));; // 4-force in the electrons frame
	    const auto F_lab_4d = lorentz_boost(-V_ele, F_4d); // 4-force in the Lab frame
	    return StaticVector<3>(F_lab_4d[1], F_lab_4d[2], F_lab_4d[3]) / gamma_ion; // stopping force in the Lab frame
	  }();

	  const auto F = F0 + FA; // MeV/m

	  // particle update
	  if (gsl_isnan(F[0]) || gsl_isnan(F[1]) || gsl_isnan(F[2])) {
	    std::cerr << "error: Nan in Electron Coooling computation (ions) @ S = " << S_mm << " mm\n";
	    exit(1);
	  }

	  // apply the force on the ion, considering the effect of the magnetic field
	  apply_force_through_Bfield(ion, F, get_static_Bfield(), dS_mm);

#ifdef DEBUG
	  std::cout << ">>> F = " << F << std::endl;
#endif

	  // final momentum
	  if (fabs(ion.Pc) < std::numeric_limits<double>::epsilon()) {
	    ion.Pc = 0.0; // MeV/c
	    ion.lost_at(bunch.S + 0.5*dS);
	    continue;
	  }
	}
      }
    };
    /* const auto effective_Nthreads = */ for_all(Nthreads, Nparticles, track_particle_parallel);

#ifdef DEBUG
    std::cout << "END!\n";
#endif

    // merge the forces from different threads and store the result in dP_to_electrons_mesh[0]
    // compute also the average time spent by the beam during the current step, store the result is dt_mm_average[0]
    /*
    for (size_t thread=1; thread<effective_Nthreads; thread++) {
      const auto &dP_to_electrons_mesh = dP_to_electrons_mesh_parallel[thread];
      for (size_t i=0; i<Nx*Ny*Nz; i++) {
	dP_to_electrons_mesh_parallel[0].data()[i] += dP_to_electrons_mesh.data()[i];
      }
      dt_mm_average[0] += dt_mm_average[thread]; // mm/c
    }
    dt_mm_average[0] /= effective_Nthreads;
    */
    // advances the plasma (TO DO!)
    /////////apply_momentum_through_dt(dt_mm_average[0], dP_to_electrons_mesh_parallel[0]);

    S_mm += dS_mm;

    bunch.S += dS;
    transport_table.push_back(bunch.get_info());
  }

  return transport_table;
}
