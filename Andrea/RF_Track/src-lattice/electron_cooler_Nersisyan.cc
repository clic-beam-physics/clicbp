/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <fstream>
#include <array>
#include <limits>
#include <numeric>
#include <utility>

#include "for_all.hh"
#include "bunch6d.hh"
#include "bunch6dt.hh"
#include "numtools.hh"
#include "rotation.hh"
#include "lorentz_boost.hh"
#include "electron_cooler.hh"
#include "move_particle.hh"
#include "stats.hh"

std::map<std::pair<double,double>, CoolingForce> ElectronCooler::cooling_force_table;

ElectronCooler::ElectronCooler(double length /* m */, double rx /* m */, double ry /* m */, double N_ele /* #/m^3 */, double Vz /* c */ ) : Plasma(length, rx, ry, N_ele, Vz), cooling_force_ptr(nullptr)

{
  // set_aperture(rx, ry, "circular"); // the element aperture does not coincide with the electron beam size
  // const double Kbolt_T_ele_r = 0.04; // eV, see CAS CERN-92-01, pag 157-158
  // const double Kbolt_T_ele_l = 1.7e-4; // eV, see CAS CERN-92-01, pag 157-158
  set_temperature(0.04, 1.7e-4);
}

void ElectronCooler::set_temperature(double kb_T_r /* eV */, double kb_T_l /* eV */ ) // set transverse and longitudinal temperatures, in the rest frame of the electrons
{
  Plasma::set_temperature(kb_T_r, kb_T_l);
}

void ElectronCooler::compute_cooling_force()
{
  const auto &key = std::pair<double,double>(Plasma::get_D_ele_r(), Plasma::get_D_ele_l());
  if (cooling_force_table.find(key) == cooling_force_table.end()) {
    cooling_force_table[key] = CoolingForce(Plasma::get_D_ele_r(), Plasma::get_D_ele_l());
  }
  cooling_force_ptr = &cooling_force_table[key];
}

// tracking
std::list<Bunch6d_info> ElectronCooler::track(Bunch6d &bunch )
{
  compute_cooling_force();
  
  std::ofstream file;
  if (getenv("DEBUG")) {
    std::cerr << "info: saving the cooling force on beam on file 'cooling_force_beam.txt'\n";
    file.open("cooling_force_beam.txt");
    if (file.is_open()) {
      file << "# units are MeV/m\n";
      file.precision(15);
    }
  }

  std::list<Bunch6d_info> transport_table;

  const size_t Nthreads = getenv("DEBUG") ? 1 : RF_Track_Globals::number_of_threads;
  const size_t Nparticles = bunch.size();

  // resize meshes
  size_t
    Nx = size1(),
    Ny = size2(),
    Nz = nsteps;

  // init mesh external forces [MeV/mm]
  dP_to_electrons_mesh_parallel.resize(Nthreads);
  for (auto &dP_to_electrons_mesh: dP_to_electrons_mesh_parallel)
    dP_to_electrons_mesh.resize(Nx, Ny, Nz);

  const double l_max_mm = sqrt(get_area())*1e3; // mm, transverse dimension of electron beam (approximate)

  // convert the field from tesla to MV/mm
  const auto Bf = get_static_Bfield() * C_LIGHT / 1e9; // MV/mm
  const auto norm_Bf_sqr = dot(Bf,Bf); // (MV/mm)^2

  // radial and longitudinal electron temperatures (times the Boltzmann constant)
  const double D_ele_r = Plasma::get_D_ele_r(); // c
  const double D_ele_l = Plasma::get_D_ele_l(); // c
  const double D_ele = hypot(sqrt(2.0)*D_ele_r, D_ele_l); // c
  
  // start integration loop
  const double dS = get_length() / nsteps;
  const double dS_mm = dS*1e3; // mm
  double S_mm = 0.0; // mm, coordinate along the element
  for (size_t step = 0; step<nsteps; step++) {
    // initialization of the thread's local variables
    auto track_particle_parallel = [&] (size_t thread, size_t start, size_t end ) {
      auto &dP_to_electrons_mesh = dP_to_electrons_mesh_parallel[thread]; // MeV/mm
      for (size_t i=0; i<Nx*Ny*Nz; i++)
	dP_to_electrons_mesh.data()[i] = StaticVector<3>(0.0);
      for (size_t n=start; n<end; n++) {
	auto &ion = bunch.particles[n];
	if (ion) {
	  // ion position halfway through the cell
	  const auto &r_ion = [&] () {
	    auto _ion = ion;
	    const double half_dS_mm = 0.5 * dS_mm;
	    move_particle_through_Bfield(_ion, get_static_Bfield(), half_dS_mm);
	    return StaticVector<3>(_ion.x, _ion.y, S_mm + half_dS_mm); // mm
	  }();
	  
	  if (!is_point_inside_aperture(r_ion[0], r_ion[1])) {
	    ion.lost_at(bunch.S + 0.5*dS);
	    continue;
	  }
	  
	  if (!is_point_inside_area(r_ion[0], r_ion[1])) { // if the ion is outside plasma just drift the ion
	    move_particle_through_Bfield(ion, get_static_Bfield(), dS_mm);
	    continue;
	  }

	  // consider the electrons
	  const auto &state = get_state(r_ion);
	  const auto N_ele = state.get_number_density(); // #/m^3
	  if (N_ele == 0.0) { // if the ion is in a zero-density region, just drift the ion
	    move_particle_through_Bfield(ion, get_static_Bfield(), dS_mm);
	    continue;
	  }
	  
	  // other auxiliary variables
	  const auto &V_ele = state.get_velocity(get_mass()); // c
	  const double inv_gamma_ele = sqrt(1.0 - dot(V_ele,V_ele));
	  const double gamma_ele = 1.0 / inv_gamma_ele;
	  const double E_ele = get_mass() * gamma_ele; // MeV, total energy of the electron
	  const double N_ele_star = N_ele * inv_gamma_ele; // #/m^3, electron number density in the reference frame of the electrons

	  // prepare for physics
	  const double Q_ion = ion.Q;
	  const double Q_ele = get_Q();

	  //
	  const double l_debye_mm = get_debye_length(state) * 1e3; // mm

	  //////////
	  // You have: e*e/(4*pi*epsilon0)
	  // You want: MeV*mm
	  // e*e/(4*pi*epsilon0) = 1.439964485044515e-12 MeV*mm
	  // e*e/(4*pi*epsilon0) = (1 / 694461572063.7623) MeV*mm
	  const double K = (Q_ion * Q_ele) / 694461572063.7623; // (e*e/4/pi/epsilon0) MeV*mm
	  const double reduced_mass = ion.mass * get_mass() / (ion.mass + get_mass()); // MeV/c^2

	  // other variables
	  const auto &V_ion = ion.get_Vx_Vy_Vz(); // c
	  const auto &dt_mm = dS_mm / V_ion[2]; // mm/c, time spent by the ion in the cell
	  const double gamma_ion = ion.get_gamma();

	  // for fast collisions
	  const auto U0 = relativistic_velocity_addition(-V_ele, V_ion); // relativistic velocity addition, the ion velocity in the electron frame
	  const double norm_U0 = norm(U0); // c
	  const double r_max_mm = std::min(l_max_mm, std::min(norm_U0 * dt_mm, l_debye_mm * hypot(1.0, norm_U0/(D_ele/sqrt(3.0))))); // mm

	  // for magnetized collisions
	  const auto &B_nz = normalize(Bf); // unit vector running parallel Bf
	  const auto V_ele_l = (norm_Bf_sqr != 0.0) ? B_nz * dot(V_ele, B_nz) : StaticVector<3>(0.0); // component of the electron velocity parallel to Bf
	  const auto UA = relativistic_velocity_addition(-V_ele_l, V_ion); // relativistic velocity addition, the ion velocity in the electron frame
	  const double UA_l = dot(UA, B_nz); // longitudinal component of the velocity
	  const auto omega_ele = Q_ele * Bf / E_ele; // rad / (mm/c), electron gyrofrequency
	  const double r_fast_mm = (norm_Bf_sqr == 0.0) ? r_max_mm : std::min(fabs(UA_l) / norm(omega_ele), r_max_mm); // mm, pitch of the electron helix
	  
	  //
	  const auto F_ = -N_ele_star * (4.0*M_PI) * (K*K) / reduced_mass / 1e6; // MeV * mm^2 / m^3 / 1e6 = MeV/m
	  const auto sqr = [] (double x ) { return x*x; };

	  // stopping force for fast collisions
	  const StaticVector<3> F0 = [&] () { // MeV/m
	    const double l_min_mm = fabs(K)/(reduced_mass*(norm_U0*norm_U0)); // mm, minimum impact distance
	    const double LC = 0.5*log1p(sqr(r_fast_mm/l_min_mm));
	    const StaticVector<3> F = [&] () {
	      const auto &nz = normalize(V_ele); // unit vector running parallel to the electron in the lab's frame
	      const double U0_l = dot(U0, nz); // axial component of the velocity
	      const auto nr = normalize(U0 - nz * U0_l); // radial ion velocity, orthogonal to V_ele
	      const double U0_r = dot(U0, nr); // radial velocity
	      double f_r, f_l;
	      cooling_force_ptr->cooling_force_unmagnetized(f_r, f_l, U0_r, U0_l);
	      return F_ * LC * (f_r*nr + f_l*nz); // MeV/m
	    } ();
	    // moves to the lab frame
	    const auto F_4d = StaticVector<4>(dot(F,U0), F[0], F[1], F[2]) / sqrt(1.0 - dot(U0,U0)); // 4-force in the electrons frame
	    const auto F_lab_4d = lorentz_boost(-V_ele, F_4d); // 4-force in the Lab frame
	    return StaticVector<3>(F_lab_4d[1], F_lab_4d[2], F_lab_4d[3]) / gamma_ion; // stopping force in the Lab frame
	  } ();
	  
	  const StaticVector<3> FM = [&] () { // MeV/m, stopping force for magnetised collisions
	    if (norm_Bf_sqr != 0.0 && r_fast_mm != 0.0) { // unmagnetized plasma
	      const double LM = 0.5*log1p(sqr(r_max_mm/r_fast_mm));
	      const StaticVector<3> F = [&] () {
		const auto B_nr = normalize(UA - B_nz * UA_l); // radial ion velocity, orthogonal to B_nz
		const double UA_r = dot(UA, B_nr); // radial component of the velocity
		double f_r, f_l;
		cooling_force_ptr->cooling_force_magnetized(f_r, f_l, UA_r, UA_l);
		return F_ * LM * (f_r*B_nr + f_l*B_nz); // MeV/m
	      } ();
	      // moves to the lab frame
	      const auto F_4d = StaticVector<4>(dot(F,UA), F[0], F[1], F[2]) / sqrt(1.0 - dot(UA,UA)); // 4-force in the electrons frame
	      const auto F_lab_4d = lorentz_boost(-V_ele_l, F_4d); // 4-force in the Lab frame
	      return StaticVector<3>(F_lab_4d[1], F_lab_4d[2], F_lab_4d[3]) / gamma_ion; // stopping force in the Lab frame
	    }
	    return StaticVector<3>(0.0, 0.0, 0.0);
	  } ();
	  
	  const auto F = F0 + FM; // MeV/m

	  if (getenv("DEBUG")) {
	    // std::cerr << "r_max_mm = " << r_max_mm << std::endl;
	    // std::cerr << "r_fast_mm = " << r_fast_mm << std::endl;
	    // std::cerr << "U0 = " << U0 << std::endl;
	    // std::cerr << "UA = " << UA << std::endl;
	    if (file.is_open()) {
	      file << F << std::endl;
	    }
	  }
	  
	  // particle update
	  if (gsl_isnan(F[0]) || gsl_isnan(F[1]) || gsl_isnan(F[2])) {
	    std::cerr << "error: Nan in Electron Coooling computation (ions) @ S = " << S_mm << " mm\n";
	    exit(1);
	  }

	  // apply the force to the ion, considering the effect of the magnetic field
	  apply_force_through_Bfield(ion, F, get_static_Bfield(), dS_mm);

	  // apply the force to the electrons
	  /*{
	    const auto &r_mesh = get_mesh_coordinates(r_ion);
	    dP_to_electrons_mesh.assign_value(r_mesh[0], r_mesh[1], r_mesh[2], ion.N/N_ele * (-F*dt_mm));
	  }*/
	  
	  // final momentum
	  if (fabs(ion.Pc) < std::numeric_limits<double>::epsilon()) {
	    ion.Pc = 0.0; // MeV/c
	    ion.lost_at(bunch.S + 0.5*dS);
	    continue;
	  }
	}
      }
    };
    /* const auto effective_Nthreads = */ for_all(Nthreads, Nparticles, track_particle_parallel);

    // merge the forces from different threads and store the result in dP_to_electrons_mesh[0]
    // compute also the average time spent by the beam during the current step, store the result is dt_mm_average[0]
    /*
    for (size_t thread=1; thread<effective_Nthreads; thread++) {
      const auto &dP_to_electrons_mesh = dP_to_electrons_mesh_parallel[thread];
      for (size_t i=0; i<Nx*Ny*Nz; i++) {
	dP_to_electrons_mesh_parallel[0].data()[i] += dP_to_electrons_mesh.data()[i];
      }
    }
    */
    // advances the plasma (TO DO!)
    /////////apply_momentum_through_dS(dP_to_electrons_mesh_parallel[0], dS_mm);

    S_mm += dS_mm;

    bunch.S += dS;
    transport_table.push_back(bunch.get_info());
  }

  if (getenv("DEBUG")) {
    if (file.is_open())
      file.close();
  }

  return transport_table;
}
