/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <complex>
#include <array>
#include <cmath>
#include <thread>
#include <gsl/gsl_sys.h>

#include "RF_Track.hh"
#include "rf_field.hh"
#include "constants.hh"
#include "move_particle.hh"

RF_Field::RF_Field(const ComplexMesh3d &Ex,
		   const ComplexMesh3d &Ey,
		   const ComplexMesh3d &Ez,
		   const ComplexMesh3d &Bx,
		   const ComplexMesh3d &By,
		   const ComplexMesh3d &Bz,
		   double xa_, double ya_, /*, double za_, */ // m
		   double hx_, double hy_, double hz_, // m
		   double length_,
		   double frequency_,
		   double direction_,
		   double P_map_,
		   double P_actual_ ) :	Efield_x(Ex),
					Efield_y(Ey),
					Efield_z(Ez),
					Bfield_x(Bx),
					Bfield_y(By),
					Bfield_z(Bz),
					xa(xa_*1e3), // accepts m, stores mm
					ya(ya_*1e3), // accepts m, stores mm
					hx(hx_*1e3), // accepts m, stores mm
					hy(hy_*1e3), // accepts m, stores mm
					hz(hz_*1e3), // accepts m, stores mm
					cylindrical(false),
					z0(0.0), // mm
					omega(2.0 * M_PI * (frequency_ / (C_LIGHT*1e3))), // frequency in Hz
					direction(direction_ == 0.0 ? 0 : (direction_ > 0.0 ? +1 : -1)),
					P_map(P_map_), // W
					P_actual(P_actual_), // W
					scale_factor_and_phase(sqrt(P_actual / P_map)),
					phi(0.0),
					t0_is_set(false),
					t0(0.0),
					static_Bfield(0.0, 0.0, 0.0)
{
  set_nsteps(Efield_x.size3()-1);
  set_length(length_);
  init_bounding_box();
}

RF_Field::RF_Field(const ComplexMesh3d &Ex,
		   const ComplexMesh3d &Ey,
		   const ComplexMesh3d &Ez,
		   const ComplexMesh3d &Bx,
		   const ComplexMesh3d &By,
		   const ComplexMesh3d &Bz,
		   const Mesh3d &static_Bx_,
		   const Mesh3d &static_By_,
		   const Mesh3d &static_Bz_,
		   double xa_, double ya_, /*, double za_, */ // m
		   double hx_, double hy_, double hz_, // m
		   double length_,
		   double frequency_,
		   double direction_,
		   double P_map_,
		   double P_actual_ ) :	Efield_x(Ex),
					Efield_y(Ey),
					Efield_z(Ez),
					Bfield_x(Bx),
					Bfield_y(By),
					Bfield_z(Bz),
					xa(xa_*1e3), // accepts m, stores mm
					ya(ya_*1e3), // accepts m, stores mm
					hx(hx_*1e3), // accepts m, stores mm
					hy(hy_*1e3), // accepts m, stores mm
					hz(hz_*1e3), // accepts m, stores mm
					cylindrical(false),
					z0(0.0), // mm
					omega(2.0 * M_PI * (frequency_ / (C_LIGHT*1e3))), // frequency in Hz
					direction(direction_ == 0.0 ? 0 : (direction_ > 0.0 ? +1 : -1)),
					P_map(P_map_), // W
					P_actual(P_actual_), // W
					scale_factor_and_phase(sqrt(P_actual / P_map)),
					phi(0.0),
					t0_is_set(false),
					t0(0.0),
					static_Bfield(0.0, 0.0, 0.0),
					static_Bx(static_Bx_),
					static_By(static_By_),
					static_Bz(static_Bz_)
{
  set_nsteps(Efield_x.size3()-1);
  set_length(length_);
  init_bounding_box();
  use_3D_static_field = true;
}

RF_Field::RF_Field() : Efield_x(DefaultComplexMesh3d),
		       Efield_y(DefaultComplexMesh3d),
		       Efield_z(DefaultComplexMesh3d),
		       Bfield_x(DefaultComplexMesh3d),
		       Bfield_y(DefaultComplexMesh3d),
		       Bfield_z(DefaultComplexMesh3d),
		       xa(0.0), // accepts m, stores mm
		       ya(0.0), // accepts m, stores mm
		       hx(1e3), // accepts m, stores mm
		       hy(1e3), // accepts m, stores mm
		       hz(1e3), // accepts m, stores mm
		       cylindrical(false),
		       z0(0.0), // mm
		       omega(0.0), // rad/(mm/c)
		       direction(0.0),
		       P_map(1.0), // W
		       P_actual(1.0), // W
		       scale_factor_and_phase(1.0),
		       phi(0.0),
		       t0_is_set(false),
		       t0(0.0),
		       static_Bfield(0.0, 0.0, 0.0)
{
  set_nsteps(0);
  set_length(0.0);
  init_bounding_box();
}

void RF_Field::set_length(double length_ )  // accepts m
{
  const double z1_max = (Efield_x.size3()-1)*hz;
  if (length_<0.0) {
    z1 = z1_max;
  } else {
    z1 = z0 + length_*1e3;
    if (z1>z1_max) z1 = z1_max;
  }
}

void RF_Field::init_bounding_box()
{
  if (cylindrical) {
    x0 = std::numeric_limits<double>::max();
    y0 = std::numeric_limits<double>::max();
    rho_max_sqr = -std::numeric_limits<double>::max();
    double x1 = -std::numeric_limits<double>::max();
    double y1 = -std::numeric_limits<double>::max();
    for (size_t i=0; i<Efield_x.size1(); i++) {
      const double rho = xa + i * hx; // mm
      if (rho*rho > rho_max_sqr)
	rho_max_sqr = rho*rho;
      for (size_t j=0; j<Efield_x.size2(); j++) {
	const double theta = (ya + j * hy) / 1e3; // rad
	double x = rho * cos(theta); // mm
	double y = rho * sin(theta); // mm
	if (x < x0) x0 = x;
	if (x > x1) x1 = x;
	if (y < y0) y0 = y;
	if (y > y1) y1 = y;
      }
    }
    width  = x1 - x0; // mm
    height = y1 - y0; // mm
  } else {
    x0 = xa;
    y0 = ya;
    width  = (Efield_x.size1()-1) * hx;
    height = (Efield_x.size2()-1) * hy;
  }
}

void RF_Field::set_cylindrical(bool c )
{
  if (cylindrical != c) {
    cylindrical = c;
    init_bounding_box();
    for (size_t k=0; k<Efield_x.size3(); k++) {
      for (size_t i=0; i<Efield_x.size1(); i++) {
	for (size_t j=0; j<Efield_x.size2(); j++) {
	  const double theta = (cylindrical ? +1 : -1) * (ya + j*hy) / 1e3; // rad
	  double c = cos(theta); // mm
	  double s = sin(theta); // mm
	  const auto Er = Efield_x.elem(i,j,k)* c + Efield_y.elem(i,j,k)*s;
	  const auto Et = Efield_x.elem(i,j,k)*-s + Efield_y.elem(i,j,k)*c;
	  const auto Br = Bfield_x.elem(i,j,k)* c + Bfield_y.elem(i,j,k)*s;
	  const auto Bt = Bfield_x.elem(i,j,k)*-s + Bfield_y.elem(i,j,k)*c;
	  Efield_x.elem(i,j,k) = Er;
	  Efield_y.elem(i,j,k) = Et;
	  Bfield_x.elem(i,j,k) = Br;
	  Bfield_y.elem(i,j,k) = Bt;
	}
      }
    }
  }
}

std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>> RF_Field::get_field_complex(double x /* mm */, double y /* mm */, double z /* mm */, double t  /* mm/c */ )
{
  // returned fields are in V/m and T
  static std::mutex mutex;
  if (!t0_is_set) {
    if (mutex.try_lock()) {
      t0_is_set = true;
      t0 = t;
    } else {
      mutex.lock();
    }
    mutex.unlock();
  }
  z += z0;
  if ((z<0.0) || z>(z1-z0))
    return std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>>(StaticVector<3,fftwComplex>(0.0, 0.0, 0.0),
									       StaticVector<3,fftwComplex>(0.0, 0.0, 0.0));
  x -= x0;
  y -= y0;
  struct { bool x, y; } mirror;
  {
    if ((mirror.x = x<0.0)) x = -x;
    if ((mirror.y = y<0.0)) y = -y;
    if (x >= 2*width)  x = fmod(x, 2*width);
    if (y >= 2*height) y = fmod(y, 2*height);
    if (x > width)  { mirror.x = !mirror.x; x = 2*width  - x; }
    if (y > height) { mirror.y = !mirror.y; y = 2*height - y; }
  }
  std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>> field;
  double _c, _s;
  if (cylindrical) {
    x += x0;
    y += y0;
    double rho_sqr = x*x+y*y; // mm^2
    if (rho_sqr>rho_max_sqr)
      return std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>>(StaticVector<3,fftwComplex>(GSL_NAN, GSL_NAN, GSL_NAN),
										 StaticVector<3,fftwComplex>(GSL_NAN, GSL_NAN, GSL_NAN));
    const double rho = hypot(x,y); // mm
    const double theta = atan2(y,x); // rad
    const double _r = rho!=0.0 ? 1.0/rho : 0.0;
    _c = x*_r;
    _s = y*_r;
    x = rho - xa;
    y = theta*1e3 - ya; // mrad
    x /= hx;
    y /= hy;
    z /= hz;
    // do not change the order of the following lines...
    const double two_pi_hy = round(2e3*M_PI/hy);
    while (y<0.0) y += two_pi_hy; // no negative angles
    while (y>=Efield_x.size2()) y -= two_pi_hy;
    if (y<0.0) y = 0.0;
    const auto Er = Efield_x(x, y, z);
    const auto Et = Efield_y(x, y, z);
    const auto Br = Bfield_x(x, y, z);
    const auto Bt = Bfield_y(x, y, z);
    field.first[0] = Er*_c - Et*_s;
    field.first[1] = Er*_s + Et*_c;
    field.first[2] = Efield_z(x, y, z);
    field.second[0] = Br*_c - Bt*_s;
    field.second[1] = Br*_s + Bt*_c;
    field.second[2] = Bfield_z(x, y, z);
  } else {
    x /= hx;
    y /= hy;
    z /= hz;
    field.first[0] = Efield_x(x, y, z);
    field.first[1] = Efield_y(x, y, z);
    field.first[2] = Efield_z(x, y, z);
    field.second[0] = Bfield_x(x, y, z);
    field.second[1] = Bfield_y(x, y, z);
    field.second[2] = Bfield_z(x, y, z);
  }
  // phase computation
  const fftwComplex &f = [&] () {
    const double phase = direction * omega * (t - t0);
    const double c = cos(phase);
    const double s = sin(phase);
    return scale_factor_and_phase * fftwComplex(c,s); // complex
  } ();
  field.first *= f;
  field.second *= f;
  // mirroring
  if (mirror.x) {
    field.first[0] = -field.first[0];
    field.second[1] = -field.second[1];
  }
  if (mirror.y) {
    field.first[1] = -field.first[1];
    field.second[0] = -field.second[0];
  }
  // add static 3D field, if used
  if (use_3D_static_field) {
    if (cylindrical) {
      const auto static_Br = static_Bx(x, y, z);
      const auto static_Bt = static_By(x, y, z);
      field.second[0] += static_Br*_c - static_Bt*_s;
      field.second[1] += static_Br*_s + static_Bt*_c;
      field.second[2] += static_Bz(x, y, z);
    } else {
      field.second[0] += static_Bx(x, y, z);
      field.second[1] += static_By(x, y, z);
      field.second[2] += static_Bz(x, y, z);
    }
  }
  // add static B field (e.g. solenoid)
  for(size_t i=0; i<3; i++) {
    if (static_Bfield[i]!=0.0) {
      field.second[i] += static_Bfield[i];
    }
  }
return field;
}

std::pair<StaticVector<3>,StaticVector<3>> RF_Field::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t /* mm/c */ )
{
  const auto &field = get_field_complex(x, y, z, t);
  return std::pair<StaticVector<3>, StaticVector<3>>(StaticVector<3>(real(field.first[0]),
								     real(field.first[1]),
								     real(field.first[2])),
						     StaticVector<3>(real(field.second[0]),
								     real(field.second[1]),
								     real(field.second[2])));
}
