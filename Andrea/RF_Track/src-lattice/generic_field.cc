/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <complex>
#include <array>
#include <cmath>
#include <thread>
#include <gsl/gsl_sys.h>

#include "RF_Track.hh"
#include "generic_field.hh"
#include "constants.hh"
#include "move_particle.hh"

struct Params {
  double mass;
  double charge;
  GenericField *field;
};

static int func(double S /* m */, const double Y[], double dY[], void *params_ )
{
  // init parameters
  const Params *params = (const Params *)(params_);
  GenericField *this_ = params->field;

  // check particle
  if (Y[5]<=0.0)
    return GSL_EBADFUNC;
  
  // check aperture
  if (!this_->is_point_inside_aperture(Y[0], Y[1]))
    return GSL_EBADFUNC;

  // compute force
  const auto field = this_->get_field(Y[0], Y[1], S*1e3, Y[2]);
  if (gsl_isnan(field.first[0])) // NaNs indicate walls
    return GSL_EBADFUNC;

  // init variables
  const double mass = params->mass;
  const double charge = params->charge;
  const auto &P = StaticVector<3>(Y[3], Y[4], Y[5]); // MeV/c
  const auto &v = P / hypot(mass,P); // c

  const auto &Efield = field.first; // V/m
  const auto &Bfield = field.second * C_LIGHT; // T*c = V/m
  const auto &F = charge * (Efield + cross(v, Bfield)) / 1e6; // MeV/m
  
  // update system of equations for GSL
  const double inv_Pz = 1e3 / P[2]; // 1e3/(MeV/c)
  const double inv_Vz = 1.0 / v[2]; // 1/c
  dY[0] = P[0] * inv_Pz; // mrad
  dY[1] = P[1] * inv_Pz; // mrad
  dY[2] = 1e3  * inv_Vz; // 1e3/c
  dY[3] = F[0] * inv_Vz; // MeV/m/c
  dY[4] = F[1] * inv_Vz; // MeV/m/c
  dY[5] = F[2] * inv_Vz; // MeV/m/c
  return GSL_SUCCESS;
}

static int jac(double S /* m */, const double Y[], double *dfdy, 
	       double dfdt[], void *params_ )
{
  // init parameters
  const Params *params = (const Params *)(params_);
  GenericField *this_ = params->field;

  if (!this_->is_point_inside_aperture(Y[0], Y[1]))
    return GSL_EBADFUNC;

  const double mass = params->mass;
  const double charge = params->charge;

  // init variables
  const auto &P = StaticVector<3>(Y[3], Y[4], Y[5]); // MeV/c
  const auto &E = hypot(mass,P); // MeV
  const auto &v = P / E; // c

  // compute force
  const auto &field = this_->get_field(Y[0], Y[1], S*1e3, Y[2]);
  if (gsl_isnan(field.first[0])) // in a consistent map if one component of E is Nan, then all other components of E and of B are Nan too
    return GSL_EBADFUNC;
  const auto &Ef = field.first / 1e6; // V/m -> MV/m
  const auto &Bf = field.second * C_LIGHT / 1e6; // T*c = V/m -> MV/m
  const auto &F = charge * (Ef + cross(v, Bf)); // MeV/m
  const auto &a = (F - dot(v,F)*v) / E; // c^2/m

  const double inv_Vz = 1.0 / v[2];
  const double inv_Pz = 1.0 / P[2];
  const double xp = P[0] * inv_Pz; // rad
  const double yp = P[1] * inv_Pz; // rad
  dfdt[0] = 1e3 * (a[0] - xp * a[2]) * inv_Vz * inv_Vz;
  dfdt[1] = 1e3 * (a[1] - yp * a[2]) * inv_Vz * inv_Vz;
  dfdt[2] = -a[2] * inv_Vz * inv_Vz * inv_Vz;
  dfdt[3] = F[0] * dfdt[2];
  dfdt[4] = F[1] * dfdt[2];
  dfdt[5] = F[2] * dfdt[2];
  dfdt[2] *= 1e3;
  
  // jacobian of force with Y
  gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, 6, 6);
  gsl_matrix *m = &dfdy_mat.matrix;
  for (int i=0; i<6; i++) {
    for (int j=0; j<3; j++) {
      gsl_matrix_set (m, i, j, 0.0); // dVi / dj
    }
  }
  const double inv_E = 1.0 / E;
  gsl_matrix_set (m, 0, 3, inv_Pz); // dxp / dj
  gsl_matrix_set (m, 0, 4, 0.0); // dxp / dj
  gsl_matrix_set (m, 0, 5, -xp * inv_Pz); // dxp / dj
  gsl_matrix_set (m, 1, 3, 0.0); // dyp / dj
  gsl_matrix_set (m, 1, 4, inv_Pz); // dyp / dj
  gsl_matrix_set (m, 1, 5, -yp * inv_Pz); // dyp / dj
  gsl_matrix_set (m, 2, 3, xp * inv_E); // dyp / dj
  gsl_matrix_set (m, 2, 4, yp * inv_E); // dyp / dj
  gsl_matrix_set (m, 2, 5, (inv_E - inv_Pz * inv_Vz)); // dyp / dj
  gsl_matrix_set (m, 3, 3, charge * (Ef[0] * gsl_matrix_get(m, 2, 3) + Bf[2] * gsl_matrix_get(m, 1, 3)));
  gsl_matrix_set (m, 3, 4, charge * (Ef[0] * gsl_matrix_get(m, 2, 4) + Bf[2] * gsl_matrix_get(m, 1, 4)));
  gsl_matrix_set (m, 3, 5, charge * (Ef[0] * gsl_matrix_get(m, 2, 5) + Bf[2] * gsl_matrix_get(m, 1, 5)));
  gsl_matrix_set (m, 4, 3, charge * (Ef[1] * gsl_matrix_get(m, 2, 3) - Bf[2] * gsl_matrix_get(m, 0, 3)));
  gsl_matrix_set (m, 4, 4, charge * (Ef[1] * gsl_matrix_get(m, 2, 4) - Bf[2] * gsl_matrix_get(m, 0, 4)));
  gsl_matrix_set (m, 4, 5, charge * (Ef[1] * gsl_matrix_get(m, 2, 5) - Bf[2] * gsl_matrix_get(m, 0, 5)));
  gsl_matrix_set (m, 5, 3, charge * (Ef[2] * gsl_matrix_get(m, 2, 3) + Bf[1] * gsl_matrix_get(m, 0, 3) - Bf[0] * gsl_matrix_get(m, 1, 3)));
  gsl_matrix_set (m, 5, 4, charge * (Ef[2] * gsl_matrix_get(m, 2, 4) + Bf[1] * gsl_matrix_get(m, 0, 4) - Bf[0] * gsl_matrix_get(m, 1, 4)));
  gsl_matrix_set (m, 5, 5, charge * (Ef[2] * gsl_matrix_get(m, 2, 5) + Bf[1] * gsl_matrix_get(m, 0, 5) - Bf[0] * gsl_matrix_get(m, 1, 5)));
  gsl_matrix_set (m, 0, 3, 1e3 * gsl_matrix_get (m, 0, 3));
  gsl_matrix_set (m, 0, 5, 1e3 * gsl_matrix_get (m, 0, 5));
  gsl_matrix_set (m, 1, 4, 1e3 * gsl_matrix_get (m, 1, 4));
  gsl_matrix_set (m, 1, 5, 1e3 * gsl_matrix_get (m, 1, 5));
  gsl_matrix_set (m, 2, 3, 1e3 * gsl_matrix_get (m, 2, 3));
  gsl_matrix_set (m, 2, 4, 1e3 * gsl_matrix_get (m, 2, 4));
  gsl_matrix_set (m, 2, 5, 1e3 * gsl_matrix_get (m, 2, 5));
  return GSL_SUCCESS;
}

void GenericField::track0_initialize(Bunch6d &bunch )
{
  // init GSL ODE
  sys.resize(RF_Track_Globals::number_of_threads);
  for (size_t i=0; i<RF_Track_Globals::number_of_threads; i++) {
    sys[i].function = func;
    sys[i].jacobian = jac;
    sys[i].dimension = 6;
    sys[i].params = new Params({ 0.0, 0.0, this });
  }
  if (use_gsl())
    init_gsl_drivers(sys);
  gsl_error = false;
}

void GenericField::track0_finalize()
{
  if (gsl_error)
    std::cerr << "warning: an error occurred integrating the equations of motion, consider increasing 'nsteps'\n";

  for (auto s: sys) {
    delete (Params*)(s.params);
  }

  if (use_gsl())
    free_gsl_drivers();
}

void GenericField::track0(Particle &particle, double S, size_t start_step, size_t end_step, size_t thread ) const
{
  Params *params = (Params *)(sys[thread].params);
  params->mass = particle.mass;
  params->charge = particle.Q;
  const double dS = get_length() / nsteps; // m
  double S_initial; // m
  int status = GSL_SUCCESS;
  if (use_analytic()) { // use analytic
    for (size_t step = start_step; step<end_step; step++) {
      S_initial = step * dS; // m
      if (!is_particle_inside_aperture(particle)) {
	status = GSL_EBADFUNC;
	break;
      }
      const auto &field = const_cast<GenericField*>(this)->get_field(particle.x, particle.y, S_initial*1e3, particle.t);
      if (gsl_isnan(field.first[0])) { // in a consistent map if one component is Nan then all other components are Nan too
	status = GSL_EBADFUNC;
	break;
      }
      const auto &Ef = field.first; // V/m
      const auto &Bf = field.second; // T
      move_particle_through_EBfield(particle, Ef, Bf, dS*1e3);
    }
  } else if (use_leapfrog()) { // use leapfrog
    double dY[6], Y[6];
    {
      const auto P = particle.get_Px_Py_Pz(); // momentum
      Y[0] = particle.x; // mm
      Y[1] = particle.y; // mm
      Y[2] = particle.t; // mm/c
      Y[3] = P[0]; // MeV/c
      Y[4] = P[1]; // MeV/c
      Y[5] = P[2]; // MeV/c
    }
    for (size_t step = start_step; step<end_step; step++) {
      S_initial = step * dS;
      if ((status = func(S_initial, Y, dY, sys[thread].params)) == GSL_EBADFUNC) {
	break;
      }
      const double Vz = 1.0 / dY[2]; // c/1e3
      const auto F = StaticVector<3>(dY[3], dY[4], dY[5]) * Vz; // MeV/mm
      const auto v = StaticVector<3>(dY[0] * Vz, dY[1] * Vz, Vz * 1e3); // c
      const auto a = (F - dot(v,F)*v) / particle.get_total_energy(); // c^2/mm
      double dt_mm = dS * dY[2]; // mm/c
      if (a[2]>2*std::numeric_limits<double>::epsilon()*v[2]*dt_mm)
	dt_mm = (sqrt(v[2]*v[2] + 2e3*a[2]*dS) - v[2]) / a[2]; // mm/c
      Y[0] += (v[0] + 0.5 * a[0] * dt_mm) * dt_mm; // X
      Y[1] += (v[1] + 0.5 * a[1] * dt_mm) * dt_mm; // Y
      Y[2] += dt_mm; // S
      Y[3] += dY[3] * dS; // Px
      Y[4] += dY[4] * dS; // Py
      Y[5] += dY[5] * dS; // Pz
    }
    // update particle
    double inv_Pz = 1e3 / Y[5]; // 1e3/(MeV/c)
    particle.x  = Y[0]; // mm
    particle.y  = Y[1]; // mm
    particle.t  = Y[2]; // mm/c
    particle.xp = Y[3] * inv_Pz; // mrad
    particle.yp = Y[4] * inv_Pz; // mrad
    particle.Pc = hypot(Y[3], Y[4], Y[5]); // MeV/c
  } else /* if (use_gsl()) */ { // use GSL
    double Y[6];
    {
      const auto P = particle.get_Px_Py_Pz(); // momentum
      Y[0] = particle.x; // mm
      Y[1] = particle.y; // mm
      Y[2] = particle.t; // mm/c
      Y[3] = P[0]; // MeV/c
      Y[4] = P[1]; // MeV/c
      Y[5] = P[2]; // MeV/c
    }
    S_initial = start_step * dS; // m
    gsl_odeiv2_driver *driver = drivers[thread];
    gsl_odeiv2_driver_reset(driver);
    for (size_t step = start_step+1; step<=end_step; step++) {
      if ((status = gsl_odeiv2_driver_apply(driver, &S_initial, step * dS, Y)) != GSL_SUCCESS)
	break;
    }
    // update particle
    double inv_Pz = 1e3 / Y[5]; // 1e3/(MeV/c)
    particle.x  = Y[0]; // mm
    particle.y  = Y[1]; // mm
    particle.t  = Y[2]; // mm/c
    particle.xp = Y[3] * inv_Pz; // mrad
    particle.yp = Y[4] * inv_Pz; // mrad
    particle.Pc = hypot(Y[3], Y[4], Y[5]); // MeV/c
  }
  if (status == GSL_EBADFUNC) particle.lost_at(S + S_initial);
  else if (status != GSL_SUCCESS) gsl_error = true;
}
