/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <complex>
#include <array>
#include <cmath>
#include <thread>
#include <gsl/gsl_sys.h>

#include "RF_Track.hh"
#include "rf_field_1d.hh"
#include "constants.hh"
#include "move_particle.hh"

RF_Field_1d::RF_Field_1d(const ComplexMesh1d &Ez,
			 double hz_,
			 double length_,
			 double frequency_,
			 double direction_,
			 double P_map_,
			 double P_actual_ ) : Efield_z_1d(Ez), // V/m
					      Bfield_z_1d(Ez.size(), 0.0), // 0
					      hz(hz_*1e3), // accepts m, stores mm
					      z0(0.0), // mm
					      omega(2.0 * M_PI * (frequency_ / (C_LIGHT*1e3))), // frequency in Hz, omega in radian/(mm/c)
					      direction(direction_ == 0.0 ? 0 : (direction_ > 0.0 ? +1 : -1)),
					      P_map(P_map_), // W
					      P_actual(P_actual_), // W
					      scale_factor_and_phase(sqrt(P_actual / P_map)),
					      phi0(0.0),
					      t0_is_set(false),
					      t0(0.0),
					      static_Bfield(0.0, 0.0, 0.0)
{
  set_nsteps(Efield_z_1d.size1()-1);
  set_length(length_);
}

RF_Field_1d::RF_Field_1d(const ComplexMesh1d &Ez,
			 const Mesh1d &Bz,
			 double hz_,
			 double length_,
			 double frequency_,
			 double direction_,
			 double P_map_,
			 double P_actual_ ) : Efield_z_1d(Ez),
					      Bfield_z_1d(Bz),
					      hz(hz_*1e3), // accepts m, stores mm
					      z0(0.0), // mm
					      omega(2.0 * M_PI * (frequency_ / (C_LIGHT*1e3))), // frequency in Hz, omega in radian/(mm/c)
					      direction(direction_ == 0.0 ? 0 : (direction_ > 0.0 ? +1 : -1)),
					      P_map(P_map_), // W
					      P_actual(P_actual_), // W
					      scale_factor_and_phase(sqrt(P_actual / P_map)),
					      phi0(0.0),
					      t0_is_set(false),
					      t0(0.0),
					      static_Bfield(0.0, 0.0, 0.0)
{
  if (Ez.size() != Bz.size())
    std::cerr << "error: the electric and the magnetic 1-D field vectors must have the same number of elements\n";
  set_nsteps(Efield_z_1d.size1()-1);
  set_length(length_);
}

void RF_Field_1d::set_length(double length_ )  // accepts m
{
  const double z1_max = (get_nz()-1)*hz;
  if (length_<0.0) {
    z1 = z1_max;
  } else {
    z1 = z0 + length_*1e3;
    if (z1>z1_max) z1 = z1_max;
  }
}

std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>> RF_Field_1d::get_field_complex(double x /* mm */, double y /* mm */, double z /* mm */, double t  /* mm/c */ )
{
  // returned fields are in V/m and T
  static std::mutex mutex;
  if (!t0_is_set) {
    if (mutex.try_lock()) {
      t0_is_set = true;
      t0 = t;
    } else {
      mutex.lock();
    }
    mutex.unlock();
  }
  z += z0;
  if ((z<0.0) || z>(z1-z0))
    return std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>>(StaticVector<3,fftwComplex>(0.0, 0.0, 0.0),
									       StaticVector<3,fftwComplex>(0.0, 0.0, 0.0));
  z /= hz;
  // phase computation
  const fftwComplex phase = [&] () {
    const double ph = direction * omega * (t - t0); // radian
    const double c = cos(ph);
    const double s = sin(ph);
    return scale_factor_and_phase * fftwComplex(c,s); // complex
  } ();
  const auto Ez0 = phase * Efield_z_1d(z); // V/m
  const auto Bz0 = Bfield_z_1d(z); // T
  if (x==0.0 && y==0.0) {
    return std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>>({ 0.0, 0.0, Ez0 }, {
	static_Bfield[0],
	static_Bfield[1],
	Bz0 + static_Bfield[2] });
  }
  const double r2 = x*x + y*y; // mm**2
  // electric field, Efield_z_1d
  const fftwComplex dEz0_dz   = phase * (Efield_z_1d.deriv (z) / hz); // V/m/mm
  const fftwComplex d2Ez0_dz2 = phase * (Efield_z_1d.deriv2(z) / hz / hz); // V/m/mm**2
  const fftwComplex d3Ez0_dz3 = phase * (Efield_z_1d.deriv3(z) / hz / hz / hz); // V/m/mm**3
  const fftwComplex Er_r = -dEz0_dz / 2.0 + r2 * (d3Ez0_dz3 + omega*omega * dEz0_dz) / 16.0; // V/m/mm
  const fftwComplex Ez = Ez0 - r2 * (d2Ez0_dz2 + omega*omega * Ez0) / 4.0; // V/m
  const fftwComplex Ex = Er_r * x; // V/m
  const fftwComplex Ey = Er_r * y; // V/m
  // magnetic field due to the oscillating electric field
  const fftwComplex dEz_dt = Ez * fftwComplex(0.0, direction * omega); // V/m * radian/(mm/c)
  const fftwComplex Bt_r = dEz_dt / C_LIGHT / 2.0; // T/mm, Wangler 2nd Edition, (7.21)
  fftwComplex Bx = -Bt_r * y; // T
  fftwComplex By =  Bt_r * x; // T
  // magnetic field, Bfield_z_1d
  const double dBz0_dz   = Bfield_z_1d.deriv (z) / hz; // T/mm
  const double d2Bz0_dz2 = Bfield_z_1d.deriv2(z) / hz / hz; // T/mm**2
  const double d3Bz0_dz3 = Bfield_z_1d.deriv3(z) / hz / hz / hz; // T/mm**3
  const double Br_r = -dBz0_dz / 2.0 + r2 * d3Bz0_dz3 / 16.0; // T/mm
  const double Bz = Bz0 - r2 * d2Bz0_dz2 / 4.0; // T
  Bx += Br_r * x; // T
  By += Br_r * y; // T
  return std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>>({ Ex, Ey, Ez }, {
      Bx + static_Bfield[0],
      By + static_Bfield[1],
      Bz + static_Bfield[2] });
}

std::pair<StaticVector<3>,StaticVector<3>> RF_Field_1d::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t /* mm/c */ )
{
  const auto &field = get_field_complex(x, y, z, t);
  return std::pair<StaticVector<3>, StaticVector<3>>(StaticVector<3>(real(field.first[0]),
								     real(field.first[1]),
								     real(field.first[2])),
						     StaticVector<3>(real(field.second[0]),
								     real(field.second[1]),
								     real(field.second[2])));
}
