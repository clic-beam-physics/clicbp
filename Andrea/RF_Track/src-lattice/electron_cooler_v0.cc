/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <limits>
#include <utility>

#include <gsl/gsl_roots.h>

#include "for_all.hh"
#include "bunch6d.hh"
#include "bunch6dt.hh"
#include "numtools.hh"
#include "rotation.hh"
#include "lorentz_boost.hh"
#include "electron_cooler.hh"
#include "move_particle.hh"
#include "stats.hh"

#define DEBUG

ElectronCooler::ElectronCooler(double length /* m */, double ax /* m */, double ay /* m */, double N_ele /* #/m^3 */, double Vz /* c */ ) : Plasma(length, ax, ay, N_ele, Vz)
{
  set_aperture(ax, ay, "rectangular");
}

// return the bunch temperature in the moving frame of the plasma
template <typename BUNCH_TYPE>
static std::unordered_map<ParticleKey,StaticVector<3>,ParticleKeyHasher,ParticleKeyEquals> get_bunch_temperature(const BUNCH_TYPE &bunch, const StaticVector<3> &V_ele )
{
  // accumulate velocity variances, discriminating by ParticleKey (mass, charge)
  std::unordered_map<ParticleKey,Weighted_incremental_variance,ParticleKeyHasher,ParticleKeyEquals> velocity_variances;
  {
    const auto mvelocity = -V_ele;
    for (auto const &particle: bunch.particles) {
      if (particle) {
	auto key = ParticleKey{ particle.mass, particle.Q };
	auto V = particle.get_Vx_Vy_Vz();
	auto V_rel = relativistic_velocity_addition(mvelocity, V);
	velocity_variances[key].add(V_rel, particle.N);
      }
    }
  }

  // computes the temperatures Tx, Ty, Tz, for each ParticleKey
  // returns the temperatures in K
  // MeV / kboltzmann = 11604519366.495 K
  std::unordered_map<ParticleKey,StaticVector<3>,ParticleKeyHasher,ParticleKeyEquals> retval;
  const double gamma = 1.0 / sqrt(1.0 - V_ele*V_ele);
  for (auto const &vv: velocity_variances) {
    retval[vv.first] = vv.first.mass * gamma * vv.second.variance() * 11604519366.495;
  }

  return retval;
}

template <typename BUNCH_TYPE>
static double get_debye_length(const BUNCH_TYPE &bunch, double density /* #/m^3 */, const StaticVector<3> &V_ele )
{
  // const double kbT = get_plasma_temperature() / 11604519366.495; // MeV
  StaticVector<3> T(0.0);
  const auto TT = get_bunch_temperature(bunch, V_ele);
  for (auto const &tt: TT) {
    T += tt.second;
  }

  const double kbT = (T[0] + T[1] + T[2]) / 3.0 / 11604519366.495; // MeV
  const double inv_gamma = sqrt(1.0 - V_ele*V_ele);

  // sqrt(epsilon0 * MeV * m**3 /e/e) = 7433942.2 m
  return inv_gamma > std::numeric_limits<double>::epsilon() ? 7433942.2 * sqrt(kbT / (density * inv_gamma)) : std::numeric_limits<double>::max(); // m
}

template <class FUNC>
static inline double Integrate2d(FUNC integral,
				 const double &x0, const double &x1,
				 const double &y0, const double &y1 )
{
  CumulativeKahanSum<double> sum;
  const double x[2] = { x0, x1 };
  const double y[2] = { y0, y1 };
  for (size_t i=0; i<2; i++) { const bool sgn_i = (i==1);
    for (size_t j=0; j<2; j++) { const bool sgn_ij = (j==1) ? sgn_i : !sgn_i;
      if (sgn_ij)
	sum += integral(x[i], y[j]);
      else
	sum -= integral(x[i], y[j]);
    }
  }
  return sum;
}

template <class FUNC>
static inline double Integrate3d(FUNC integral,
				 const double &x0, const double &x1,
				 const double &y0, const double &y1,
				 const double &z0, const double &z1 )
{
  CumulativeKahanSum<double> sum;
  const double x[2] = { x0, x1 };
  const double y[2] = { y0, y1 };
  const double z[2] = { z0, z1 };
  for (size_t i=0; i<2; i++) { const bool sgn_i = (i==1);
    for (size_t j=0; j<2; j++) { const bool sgn_ij = (j==1) ? sgn_i : !sgn_i;
      for (size_t k=0; k<2; k++) { const bool sgn_ijk = (k==1) ? sgn_ij : !sgn_ij;
	if (sgn_ijk)
	  sum += integral(x[i], y[j], z[k]);
	else
	  sum -= integral(x[i], y[j], z[k]);
      }
    }
  }
  return sum;
}

struct my_func_Pf_params { double mass; double Pi; double Ei; double t; };
static double my_func_Pf (double Pf, void * p) // return the 
{
  struct my_func_Pf_params *params = (my_func_Pf_params*)p;
  const double &mass = params->mass;
  const double &Pi = params->Pi;
  const double &Ei = params->Ei;
  const double &t = params->t;
  const double Ef = hypot(mass, Pf); 
  return 2*(Pi*Pf+mass*mass-Ei*Ef)+t;
}

// compute the momentum gained by the electrons (such that P_ion_new = P_ion_old - returned_value)
static StaticVector<3> compute_transferred_momentum_energy_loss(const Particle &ion,
								const double N_ele,            // local density of electrons per m^3
								const double Q_ele,            // electron charge [e+]
								const StaticVector<3> &V_ele,  // electron's velocity in the lab's frame
								const double zi_min,           // zi_min in the lab's frame [mm]
								const double zi_max,           // zi_max in the lab's frame [mm]
								const double ze_min,           // ze_min in the lab's frame [mm]
								const double ze_max,           // ze_max in the lab's frame [mm]
								const double re_min,           // re_min in the lab's frame [mm]
								const double re_max,           // re_max in the lab's frame [mm]
								const double te_min,           // te_min in the lab's frame [rad]
								const double te_max )          // te_max in the lab's frame [rad]
{
  const double Q_ion = ion.Q;
  const StaticVector<4> &P_ion_4d = ion.get_four_momentum(); // MeV/c
  const StaticVector<3> &V_ion = StaticVector<3>(P_ion_4d[1], P_ion_4d[2], P_ion_4d[3]) / P_ion_4d[0]; // ion velocity [c]
  const auto &nz = normalize(V_ion); // unit vector running parallel to the ion in the lab's frame

  // useful variables
  const auto &nz_ele = normalize(V_ele); // unit vector running parallel to the electron in the lab's frame
  const auto gamma_ele = 1.0 / sqrt(1.0 - V_ele*V_ele); // relativistic gamma for the electron
  const auto N_ele_star = N_ele / gamma_ele; // electron density in the electron's frame [#/m^3]

  // time spent by the ion in the cell
  // const auto dt = (zi_max - zi_min) / norm(V_ion); // mm/c
  // const auto dt_star = dt * gamma_ele; // time spent by the ion in the cell in the electron's frame [mm/c]

  // useful variables in the electon's frame
  const auto &P_ion_star_4d = lorentz_boost(V_ele, P_ion_4d); // MeV/c
  const auto &P_ion_star = StaticVector<3>(P_ion_star_4d[1], P_ion_star_4d[2], P_ion_star_4d[3]); // MeV/c
  const auto &V_ion_star = P_ion_star / P_ion_star_4d[0]; // c
  // const auto L_star = norm(V_ion_star) * dt_star; // length of the ion's trajectory in the electron's frame [mm]
  const auto &nz_star = normalize(V_ion_star); // unit vector running parallel to the ion in the electron's frame

  // compute ze_min and ze_max in the electron's frame
  const auto &ze_min_vect = ze_min * nz; // ze_min w.r.t. the ion in the lab's frame
  const auto &ze_max_vect = ze_max * nz; // ze_min w.r.t. the ion in the lab's frame
  const auto &ze_min_star = (ze_min_vect * nz_ele) / gamma_ele; // ze_min w.r.t. the ion in the electrons's frame with space contraction
  const auto &ze_max_star = (ze_max_vect * nz_ele) / gamma_ele; // ze_max w.r.t. the ion in the electrons's frame with space contraction

  // You have: e*e/(4*pi*epsilon0)/c/mm
  // You want: MeV/c
  // e*e/(4*pi*epsilon0)/c = 1.439964485044515e-12 MeV/c * mm
  // e*e/(4*pi*epsilon0)/c = (1 / 694461572063.7623) MeV/c * mm
  const double K = (Q_ion * Q_ele) / norm(V_ion_star) / 694461572063.7623; // (e*e/4/pi/epsilon0/c/mm) MeV/c * mm
  
  // const double dE_star = std::min(K*K / RF_Track_Globals::electronmass * N_ele_star / 1e9 * dtheta * Integrate2d([](const double &re, const double &ze ){ return ze*log(re); }, re_min, re_max, ze_min_star, ze_max_star), K_ion_star); // MeV (cannot be larger than the entire kinetic energy of the ion)

  const auto K_ion_star = P_ion_star_4d[0] - ion.mass; // MeV, ion's kinetic energy in the electron's frame

  // here I need to add the correction due to the fact that we are not integrating over the whole 'z' axis
  const double K_ele_star = std::min(K*K / RF_Track_Globals::electronmass * N_ele_star / 1e9 * (te_max - te_min) * (ze_max_star - ze_min_star) * log(re_max / re_min), K_ion_star); // MeV, acquired kinetic energy by the electrons, in their reference frame

  // t Mandelstam variable (Lorentz invariant)
  const double t = 2 * RF_Track_Globals::electronmass * K_ele_star; // MeV**2

  const double P_ion_initial = ion.get_Pc();
  const double P_ion_final = [&](){
    double retval = P_ion_initial;
    if (gsl_root_fsolver *s = gsl_root_fsolver_alloc(gsl_root_fsolver_brent)) {
      struct my_func_Pf_params params = { ion.mass, P_ion_initial, ion.get_total_energy(), t };
      gsl_function F;
      F.function = &my_func_Pf;
      F.params = &params;
      gsl_root_fsolver_set (s, &F, 0.0, P_ion_initial);
      const size_t max_iter = 100;
      size_t iter = 0;
      do {
	iter++;
	gsl_root_fsolver_iterate (s);
	const double x_lo = gsl_root_fsolver_x_lower (s);
	const double x_hi = gsl_root_fsolver_x_upper (s);
	if (gsl_root_test_interval(x_lo, x_hi, 0, 1e-6) != GSL_CONTINUE)
	  break;
      } while (iter < max_iter);
      retval = gsl_root_fsolver_root (s);
      gsl_root_fsolver_free(s);
    }
    return retval;
  }();

#ifdef DEBUG
  std::cout << "Root Solver: Pf = " << P_ion_final << std::endl;
  std::cout << "Root Solver: dP_ion = " << P_ion_final - P_ion_initial << std::endl;
#endif
  
  const double dE_star = std::min(K*K / RF_Track_Globals::electronmass * N_ele_star / 1e9 * (te_max - te_min) * (ze_max_star - ze_min_star) * log(re_max / re_min), K_ion_star); // MeV (cannot be larger than the entire kinetic energy of the ion)

#ifdef DEBUG
  std::cout << "dE_ion_star = " << -dE_star << " (must be negative!)" << std::endl;
#endif
  
  // compute the final total energy and the final ion momentum given the energy loss dE_star, then it computes the momentum transferred
  const double E_ion_star_final = P_ion_star_4d[0] - dE_star; // MeV
  const auto &P_ion_star_final = sqrt((E_ion_star_final+ion.mass)*(E_ion_star_final-ion.mass)) * nz_star; // MeV/c
  const auto dP_star = P_ion_star - P_ion_star_final; // MeV/c (momentum gained by the electron)

  // four momentum
  const auto &Q_star_4d = StaticVector<4>(dE_star, dP_star[0], dP_star[1], dP_star[2]);

  // go back to the lab's frame
  const auto &Q_4d = lorentz_boost(-V_ele, Q_star_4d); // MeV/c

#ifdef DEBUG
  std::cout << "E_ion_star_initial = " << P_ion_star_4d[0] << std::endl;
  std::cout << "E_ion_star_final = " << E_ion_star_final << std::endl;
  std::cout << "normalize(P_ion_star_final) ==? normalize(P_ion_star) ? " << normalize(P_ion_star_final) << " ==? " << normalize(P_ion_star) << std::endl;
  std::cout << "dP_cell_energy_loss = " << StaticVector<3>(Q_4d[1], Q_4d[2], Q_4d[3]) << std::endl; // MeV/c
#endif

  return ion.N * StaticVector<3>(Q_4d[1], Q_4d[2], Q_4d[3]); // MeV/c
}

// compute the momentum gained by the electrons (such that P_ion_new = P_ion_old - returned_value)
static StaticVector<3> compute_transferred_momentum_cross_section(const Particle &ion,
								  const double N_ele,            // local density of electrons per m^3
								  const double Q_ele,            // electron charge [e+]
								  const StaticVector<3> &V_ele,  // electron's velocity in the lab's frame
								  const double zi_min,           // zi_min in the lab's frame [mm]
								  const double zi_max,           // zi_max in the lab's frame [mm]
								  const double ze_min,           // ze_min in the lab's frame [mm]
								  const double ze_max,           // ze_max in the lab's frame [mm]
								  const double l_min_mm )        // l_min_mm
{
  // useful function
  auto sqr = [] (const double &x ) { return x*x; };

  const double Q_ion = ion.Q;
  const StaticVector<4> &P_ion_4d = ion.get_four_momentum(); // MeV/c
  const StaticVector<3> &V_ion = StaticVector<3>(P_ion_4d[1], P_ion_4d[2], P_ion_4d[3]) / P_ion_4d[0]; // ion velocity [c]
  const auto &nz = normalize(V_ion); // unit vector running parallel to the ion in the lab's frame

  // useful variables
  const auto &nz_ele = normalize(V_ele); // unit vector running parallel to the electron in the lab's frame
  const auto gamma_ele = 1.0 / sqrt(1.0 - V_ele*V_ele); // relativistic gamma for the electron
  const auto N_ele_star = N_ele / gamma_ele; // electron density in the electron's frame [#/m^3]

  // time spent by the ion in the cell, length of its trajectory in the lab's frame, and base of the integration volume
  const auto dt = (zi_max - zi_min) / norm(V_ion); // mm/c
  const auto dt_star = dt * gamma_ele; // time spent by the ion in the cell in the electron's frame [mm/c]

  // useful variables in the electon's frame
  const auto &P_ion_star_4d = lorentz_boost(V_ele, P_ion_4d); // MeV/c
  const auto &P_ion_star = StaticVector<3>(P_ion_star_4d[1], P_ion_star_4d[2], P_ion_star_4d[3]); // MeV/c
  const auto &V_ion_star = P_ion_star / P_ion_star_4d[0]; // c
  const auto L_star = norm(V_ion_star) * dt_star; // length of the ion's trajectory in the electron's frame [mm]
  const auto &nz_star = normalize(V_ion_star); // unit vector running parallel to the ion in the electron's frame

  // compute ze_min and ze_max in the electron's frame
  const auto &ze_min_vect = ze_min * nz; // ze_min w.r.t. the ion in the lab's frame
  const auto &ze_max_vect = ze_max * nz; // ze_min w.r.t. the ion in the lab's frame
  const auto &ze_min_star = (ze_min_vect * nz_ele) / gamma_ele; // ze_min w.r.t. the ion in the electrons's frame with space contraction
  const auto &ze_max_star = (ze_max_vect * nz_ele) / gamma_ele; // ze_max w.r.t. the ion in the electrons's frame with space contraction

  // You have: e*e/(4*pi*epsilon0)/c/mm
  // You want: MeV/c
  // e*e/(4*pi*epsilon0)/c = 1.439964485044515e-12 MeV/c * mm
  // e*e/(4*pi*epsilon0)/c = (1 / 694461572063.7623) MeV/c * mm
  const double K = (Q_ion * Q_ele) / norm(V_ion_star) / 694461572063.7623; // (e*e/4/pi/epsilon0/c/mm) MeV/c * mm

  // use the cross section
  const double ion_mass_sqr = sqr(ion.mass); // MeV**2
  const double beta_ion_star_sqr = V_ion_star*V_ion_star;
  const double gamma_ion_star_sqr = 1.0 / (1.0 - beta_ion_star_sqr);

#ifdef DEBUG
  std::cout << "\ncompute_transferred_momentum_cross_section\n";
  std::cout << "  (we are in the cell the ion is traversing)" << std::endl;
  std::cout << "beta_ion_star_sqr = " << beta_ion_star_sqr << std::endl;
  std::cout << "epsilon = " << std::numeric_limits<double>::epsilon() << std::endl;
#endif
  
  if (beta_ion_star_sqr<=std::numeric_limits<double>::epsilon())
    return StaticVector<3>(0.0);
  
  // compute variables T_min and T_Max
  auto integral_Pr = [](const double &ze_min, const double &ze_max, double r ) {
    // assume(r>0); ratsimp(integrate(r/sqrt(r**2+(ze-zi)**2)**3,ze)), zi=0;
    auto integral = [](double ze, double r ) { return ze / (r * hypot(r, ze)); };
    return integral(ze_max, r) - integral(ze_min, r); // 1/mm
  };

  auto integral_Pl = [](const double &ze_min, const double &ze_max, double r ) {
    // assume(r>0); ratsimp(integrate((ze-zi)/sqrt(r**2+(ze-zi)**2)**3,ze)), zi=0;
    auto integral = [](double ze, double r ) { return -1.0 / hypot(r, ze); };
    return integral(ze_max, r) - integral(ze_min, r); // 1/mm
  };

  const double dPr_star = K * integral_Pr(ze_min_star, ze_max_star, l_min_mm); // MeV/c
  const double dPl_star = K * integral_Pl(ze_min_star, ze_max_star, l_min_mm); // MeV/c
  const double D = (sqr(RF_Track_Globals::electronmass) + 2.0*RF_Track_Globals::electronmass*P_ion_star_4d[0] + ion_mass_sqr)/ion_mass_sqr; // correction term to dE_max (see Jackson, following Eqn. 13.4)
  const double dE_max = 2.0*gamma_ion_star_sqr*beta_ion_star_sqr*RF_Track_Globals::electronmass / D; // head-on collision, MeV (see Jackson Eqn. 13.4)
  const double dE_min = std::min(hypot(RF_Track_Globals::electronmass, dPr_star, dPl_star) - RF_Track_Globals::electronmass, dE_max); // relativistic e- kinetic energy, at r = l_min_mm

#ifdef DEBUG
  std::cout << "dPr_star = " << dPr_star << std::endl;
  std::cout << "dPl_star = " << dPl_star << std::endl;
  std::cout << "dE_min = " << dE_min << std::endl;
  std::cout << "dE_max = " << dE_max << std::endl;
#endif

  if (dE_min<=std::numeric_limits<double>::epsilon())
    return StaticVector<3>(0.0);

  // energy loss for unit of length (see Jackson Eqn. 13.6, and notes)
  // You have: 2pi * e**4/((4 pi epsilon0)**2 electronmass*c*c)/mm**3
  // You want: MeV/mm
  // 2pi * e**4/((4 pi epsilon0)**2 electronmass*c*c)/mm**3 = 2.549549459801919e-23 MeV/mm
  // 2pi * e**4/((4 pi epsilon0)**2 electronmass*c*c)/mm**3 = (1 / 3.922261622167913e+22) MeV/mm
  const double dE_dz = N_ele_star / 1e9 * sqr(Q_ion * Q_ele) *
    (log(dE_max/dE_min) - beta_ion_star_sqr*(1.0 - dE_min/dE_max)) /
    beta_ion_star_sqr / 3.922261622167913e+22; // MeV/mm

  // energy gained by the electrons in the electron's frame
  const double K_ion_star = P_ion_star_4d[0] - ion.mass; // MeV
  const double dE_star = std::min(dE_dz * L_star, K_ion_star); // MeV (cannot be larger than the entire kinetic energy of the ion)

  // compute the final total energy and the final ion momentum given the energy loss dE_star, then it computes the momentum transferred
  const double E_ion_star_final = P_ion_star_4d[0] - dE_star; // MeV
  const auto &P_ion_star_final = sqrt((E_ion_star_final+ion.mass)*(E_ion_star_final-ion.mass)) * nz_star; // MeV/c
  const auto dP_star = P_ion_star - P_ion_star_final; // MeV/c (momentum gained by the electron)

  // four momentum
  const auto &Q_star_4d = StaticVector<4>(dE_star, dP_star[0], dP_star[1], dP_star[2]);

  // go back to the lab's frame
  const auto &Q_4d = lorentz_boost(-V_ele, Q_star_4d); // MeV/c

#ifdef DEBUG
  std::cout << "dE_dz = " << dE_dz << " (must be positive!)" << std::endl;
  std::cout << "dE_ion_star = " << -dE_star << " (must be negative!)" << std::endl;
  std::cout << "E_ion_star_initial = " << P_ion_star_4d[0] << std::endl;
  std::cout << "E_ion_star_final = " << E_ion_star_final << std::endl;
  std::cout << "dP_cell_cross_section = " << StaticVector<3>(Q_4d[1], Q_4d[2], Q_4d[3]) << std::endl; // MeV/c
#endif

  return ion.N * StaticVector<3>(Q_4d[1], Q_4d[2], Q_4d[3]); // MeV/c
}

// compute the momentum gained by the electrons (such that P_ion_new = P_ion_old - returned_value)
static StaticVector<3> compute_transferred_momentum_cylindrical(const Particle &ion,
								const double N_ele,      // local density of electrons per m^3
								const double Q_ele,      // electron charge [e+]
								const double V_ele_r,    // radial component of the electron's velocity w.r.t. the ion direction
								const double V_ele_t,    // angular component of the electron's velocity w.r.t. the ion direction
								const double V_ele_z,    // longitudinal component of the electron's velocity w.r.t. the ion direction
								const double zi_min,     // zi_min in the lab's frame [mm]
								const double zi_max,     // zi_max in the lab's frame [mm]
								const double ze_min,     // ze_min in the lab's frame [mm]
								const double ze_max,     // ze_max in the lab's frame [mm]
								const double re_min,     // re_min in the lab's frame [mm]
								const double re_max,     // re_max in the lab's frame [mm]
								const double te_min,     // te_min in the lab's frame [rad]
								const double te_max )    // te_max in the lab's frame [rad]
{
  const auto &Q_ion = ion.Q;
  const auto &V_ion_z = norm(ion.get_Vx_Vy_Vz());

  // transferred momentum, electrons to ion
  auto integral_Ir = [] (const double &zi, const double &ze, const double &re ) {
    // assume(re>0); ratsimp(integrate(integrate(integrate(re**2/sqrt(re**2+(zi-ze)**2)**3,re),ze),zi));
    auto sqr = [] (const double &x ) { return x*x; };
    const double term_A = [&] () {
      const double abs_zi_ze = fabs(zi-ze);
      if (re<abs_zi_ze)
	return sqr(abs_zi_ze)*asinh(re/abs_zi_ze);
      const double taylor_0_bound = sqrt(std::numeric_limits<double>::epsilon());
      const double taylor_2_bound = sqrt(taylor_0_bound);
      const double taylor_4_bound = sqrt(taylor_2_bound);
      const double abs_X = abs_zi_ze / re;
      if (abs_X >= taylor_4_bound)
	return sqr(abs_zi_ze)*asinh(re/abs_zi_ze);
      const double log_abs_X = log(abs_X);
      double result = log(2.0) - (std::isfinite(log_abs_X) ? log_abs_X : 0.0);
      if (abs_X >= taylor_0_bound) {
	const double X2 = sqr(abs_X);
	result += X2 / 4.0;
	if (abs_X >= taylor_2_bound) {
	  const double X4 = sqr(X2);
	  result -= 3.0 * X4 / 32.0;
	}
      }
      return sqr(abs_zi_ze)*result;
    }();
    const double term_B = [&] () { 
      const double abs_zi = fabs(zi);
      const double abs_ze = fabs(ze);
      const double re_sqr = sqr(re);
      if (re>abs_zi) {
	if (re>abs_ze) {
	  return re_sqr * sqrt(1.0 + sqr(zi/re) + sqr(ze/re) - 2*zi*ze/re_sqr);
	} else {
	  return re * abs_ze * sqrt(1.0 + sqr(zi/ze) + sqr(re/ze) - 2*zi/ze);
	}
      } else {
	if (abs_zi>abs_ze) {
	  return re * abs_zi * sqrt(1.0 + sqr(ze/zi) + sqr(re/zi) - 2*ze/zi);
	} else {
	  return re * abs_ze * sqrt(1.0 + sqr(zi/ze) + sqr(re/ze) - 2*zi/ze);
	}
      }
    }();
    return -(term_A + term_B) / 2; // mm**2
  };

  // transferred momentum, electrons to ion
  auto integral_Iz = [] (const double &zi, const double &ze, const double &re ) {
    // assume(re>0); ratsimp(integrate(integrate(integrate(re*(zi-ze)/sqrt(re**2+(zi-ze)**2)**3,re),ze),zi));
    auto sqr = [] (const double &x ) { return x*x; };
    const double zi_ze = zi - ze;
    const double term_A = [&] () {
      if (fabs(zi_ze)<re)
	return zi_ze>=0 ? sqr(re)*asinh(zi_ze/re) : -sqr(re)*asinh(-zi_ze/re);
      const double taylor_0_bound = sqrt(std::numeric_limits<double>::epsilon());
      const double taylor_2_bound = sqrt(taylor_0_bound);
      const double taylor_4_bound = sqrt(taylor_2_bound);
      const double X = re/zi_ze, abs_X = fabs(X);
      if (abs_X >= taylor_4_bound)
	return zi_ze>0 ? sqr(re)*asinh(zi_ze/re) : -sqr(re)*asinh(-zi_ze/re);
      const double log_abs_X = log(abs_X);
      double result = log(2.0) - (std::isfinite(log_abs_X) ? log_abs_X : 0.0);
      if (abs_X >= taylor_0_bound) {
	const double X2 = sqr(abs_X);
	result += X2 / 4.0;
	if (abs_X >= taylor_2_bound) {
	  const double X4 = sqr(X2);
	  result -= 3.0 * X4 / 32.0;
	}
      }
      return X>0 ? sqr(re)*result : -sqr(re)*result;
    }();
    const double term_B = [&] () {
      const double abs_zi = fabs(zi);
      const double abs_ze = fabs(ze);
      if (re>abs_zi) {
	if (re>abs_ze) {
	  return zi_ze * re * sqrt(1.0 + sqr(zi/re) + sqr(ze/re) - 2*zi*ze/sqr(re));
	} else {
	  return zi_ze * abs_ze * sqrt(1.0 + sqr(zi/ze) + sqr(re/ze) - 2*zi/ze);
	}
      } else {
	if (abs_zi>abs_ze) {
	  return zi_ze * abs_zi * sqrt(1.0 + sqr(ze/zi) + sqr(re/zi) - 2*ze/zi);
	} else {
	  return zi_ze * abs_ze * sqrt(1.0 + sqr(zi/ze) + sqr(re/ze) - 2*zi/ze);
	}
      }
    }();
    return (term_A + term_B) / 2; // mm**2;
  };

  const double Ir = Integrate3d(integral_Ir, zi_min, zi_max, ze_min, ze_max, re_min, re_max); // mm**2
  const double Iz = Integrate3d(integral_Iz, zi_min, zi_max, ze_min, ze_max, re_min, re_max); // mm**2
  const double dtheta = te_max - te_min;

  // You have: e*e/(4*pi*epsilon0)/c/mm
  // You want: MeV/c
  // e*e/(4*pi*epsilon0)/c = 1.439964485044515e-12 MeV/c * mm
  // e*e/(4*pi*epsilon0)/c = (1 / 694461572063.7623) MeV/c * mm
  const double K = (Q_ion * Q_ele) / V_ion_z / 694461572063.7623; // (e*e/4/pi/epsilon0/c/mm) MeV/c * mm

  // transferred momentum (electrons to ion, later will be changed in sign)
  const double K_ = N_ele / 1e9 * K * dtheta; //  mm**-3 * (MeV/c * mm) == MeV/c/mm**2
  const double dPr = K_ * ((V_ion_z * V_ele_z - 1.0) * Ir + V_ion_z * V_ele_r * Iz); // MeV/c/mm**2 * mm**2 == MeV/c
  const double dPt = K_ * V_ion_z * V_ele_t * Iz; // MeV/c/mm**2 * mm**2 == MeV/c
  const double dPl = K_ * Iz; // MeV/c/mm**2 * mm**2 == MeV/c

#ifdef DEBUG
  std::cout << "\ncompute_transferred_momentum\n";
  std::cout << "Iz = " << Iz << '\n';
  std::cout << "Ir = " << Ir << '\n';
  std::cout << "V_ion_z = " << V_ion_z << '\n';
  std::cout << "V_ele_r = " << V_ele_r << '\n';
  std::cout << "V_ele_t = " << V_ele_t << '\n';
  std::cout << "V_ele_z = " << V_ele_z << '\n';
  std::cout << "zi_min = " << zi_min << std::endl;
  std::cout << "zi_max = " << zi_max << std::endl;
  std::cout << "ze_min = " << ze_min << std::endl;
  std::cout << "ze_max = " << ze_max << std::endl;
  std::cout << "re_min = " << re_min << std::endl;
  std::cout << "re_max = " << re_max << std::endl;
  std::cout << "te_min = " << te_min << std::endl;
  std::cout << "te_max = " << te_max << std::endl;
  std::cout << "dtheta_deg = " << (dtheta/M_PI*180.0) << std::endl;
  std::cout << "dPr = " << dPr << "\n";
  std::cout << "dPl = " << dPl << '\n';
  std::cout << "dPt = " << dPt << '\n';
#endif

  // change sign to obtain the transferred momentum ions-to-electrons
  return -ion.N * StaticVector<3>(dPr, dPt, dPl);
}

// tracking
std::list<Bunch6d_info> ElectronCooler::track(Bunch6d &bunch )
{
  std::list<Bunch6d_info> transport_table;

#ifdef DEBUG
  std::cout << "ElectronCooler::enter tracking " << bunch.get_ngood() << std::endl;
#endif
  
  const size_t Nthreads = RF_Track_Globals::number_of_threads;
  const size_t Nparticles = bunch.size();

  // resize meshes
  size_t
    Nx = size1(),
    Ny = size2(),
    Nz = nsteps;

  // init mesh external forces [MeV/mm]
  dP_to_electrons_mesh_parallel.resize(Nthreads);
  for (auto &dP_to_electrons_mesh: dP_to_electrons_mesh_parallel)
    dP_to_electrons_mesh.resize(Nx, Ny, Nz);

  // mesh has the longitudinal extension of the element and the trasnverse extension of the aperture
  StaticVector<3> min_(-aperture.x, -aperture.y, 0); // mm
  StaticVector<3> max_(+aperture.x, +aperture.y, 1e3*get_length()); // mm

  // working box [mm]
  const auto dim = max_ - min_;

  // size of the electron mesh cell [mm]
  const double hx = dim[0] / double(Nx); // mm
  const double hy = dim[1] / double(Ny); // mm
  const double hz = dim[2] / double(Nz); // mm
  const double r_max_mm = hypot(hx, hy, hz); // mm
	
  // (inverse) size of the electron mesh cell [mm]
  const double inv_hx = double(Nx) / dim[0]; // 1/mm
  const double inv_hy = double(Ny) / dim[1]; // 1/mm
  const double inv_hz = double(Nz) / dim[2]; // 1/mm

  // start integration loop
  const double dS = get_length() / nsteps;
  const double dS_mm = get_length() * 1e3 / nsteps; // mm
  double S_mm = 0.0; // mm, coordinate along the element
  for (size_t step = 0; step<nsteps; step++) {
    std::vector<double> dt_mm_average(Nthreads); // mm/c, average time spent by the beam in one step (used to advance the electron mesh)
    auto track_particle_parallel = [&](size_t thread, size_t start, size_t end ) -> void {
      auto &dP_to_electrons_mesh = dP_to_electrons_mesh_parallel[thread]; // MeV/mm
      auto &dt_mm_avg = dt_mm_average[thread]; // mm/c
      for (size_t i=0; i<Nx*Ny*Nz; i++)
	dP_to_electrons_mesh.data()[i] = StaticVector<3>(0.0);
      dt_mm_avg = 0.0;
      size_t count = 0; // count of good particles (necessary to dt_mm_average);
      auto sqr = [] (const double &x ) { return x*x; };
      for (size_t n=start; n<end; n++) {
	auto &ion = bunch.particles[n];
	if (ion && ion.N>0.0) {

	  // ion position halfway through the cell
	  const auto &r_ion = StaticVector<3>(ion.x + ion.xp * 0.5 * dS_mm / 1e3,
					      ion.y + ion.yp * 0.5 * dS_mm / 1e3,
					      S_mm + 0.5 * dS_mm); // mm

	  if (!is_point_inside_aperture(r_ion[0], r_ion[1])) {
	    ion.lost_at(bunch.S + 0.5*dS);
	    continue;
	  }

	  const auto &V_ion = ion.get_Vx_Vy_Vz(); // c
	  const auto &dt_mm = dS_mm / V_ion[2]; // mm/c, time spent by the ions in the cell (used to advance the electron mesh)
	  const auto &dL_ion_mm = norm(V_ion) * dt_mm; // mm, Length of the trajectory of the ion in the integration step
	  const auto &L_ion_mm = dL_ion_mm * Nz; // mm, Length of the total  trajectory of the ion inside the element
	  const auto &zi = dL_ion_mm * step + dL_ion_mm/2; // mm, position of the ion along its trajectory
	  // const double E_ion = ion.get_total_energy(); // MeV
	  // const auto ion_angular_frequency = ion.Q * norm(B_field) / E_ion / 1e9; // electron cyclotron frequency, c/mm
	  
	  CumulativeKahanSum<StaticVector<3>> dP_ions_to_electrons; // total momentum transferred from ion.N ions to all electrons in one step, MeV/c

	  // orthogonal axes with 'z' parallel to the ion's velocity
	  const auto &nz = normalize(V_ion); // unit vector running parallel to the ion in the electron's frame
	  const auto &nx = normalize(StaticVector<3>(0.0, 1.0, 0.0) ^ V_ion); // unit vector perpendicular to nz
	  const auto &ny = nz ^ nx; // unit vector perpendicular to nx and nz

#ifdef DEBUG
	  std::cout << " nx = " << nx << std::endl;
	  std::cout << " ny = " << ny << std::endl;
	  std::cout << " nz = " << nz << std::endl;
#endif

	  // find the ion's coordinates w.r.t. the electron mesh to find the local N_ele, and V_ele
	  auto r = r_ion - min_;
	  r[0] *= inv_hx;
	  r[1] *= inv_hy;
	  r[2] *= inv_hz;

	  // boundary conditions for the integration cell (not optimal but should be ok in most cases)
	  const auto &state = plasma_bnd(r[0], r[1], r[2]);
	  const auto  N_ele = plasma_get_number_density(state); // #/m^3
	  const auto &V_ele = plasma_get_velocity(state); // c
	  const auto gamma_ele = 1.0 / sqrt(1.0 - V_ele*V_ele);
#ifdef DEBUG
	  std::cout << "V_ele = " << V_ele << std::endl;
#endif
	  const auto &l_min_l_scr_mm = [&] () {
	    // compute l_min_mm and l_scr_mm
	    // see Conte pag 248, l_min: mm = 2*e*e / 4/ pi/ epsilon0 / MeV / 347230786031.881 minimum impact distance
	    const auto &V_rel = relativistic_velocity_addition(-V_ele, V_ion); // relative velocity
	    const auto &V_rel_sqr = norm_sqr(V_rel);
	    const double LC = 10.0; // Coulomb logarithm
	    const double dist_ele_mm = N_ele == 0.0 ? std::numeric_limits<double>::max() : 1e3 / pow(N_ele, 1.0 / 3.0); // mm, average electron distance
	    const auto &l_min_mm = std::min(fabs(ion.Q * get_Q()) / get_mass() / V_rel_sqr / 347230786031.881, dist_ele_mm); // mm, minimum impact distance
	    const auto &l_debye_mm = get_debye_length(bunch, N_ele, V_ele) * 1e3; // mm
	    const auto &l_scr_mm = std::max(l_min_mm * exp(LC), l_debye_mm) * gamma_ele; // mm, screening length in the lab frame
#ifdef DEBUG
	    const auto &gamma_rel = 1.0 / sqrt(1.0 - V_rel*V_rel);
	    std::cout << "gamma_ele = " << gamma_ele << std::endl;
	    std::cout << "gamma_rel = " << gamma_rel << std::endl;
	    std::cout << "dist_ele_mm = " << dist_ele_mm << std::endl;
	    std::cout << "l_min_mm = " << l_min_mm << std::endl;
	    std::cout << "l_debye_mm = " << l_debye_mm << std::endl;
	    std::cout << "l_scr_mm = " << l_scr_mm << std::endl;
#endif
	    return std::pair<double,double>(l_min_mm, l_scr_mm);
	  }();
	  const auto &l_min_mm = l_min_l_scr_mm.first; // mm
	  const auto &l_scr_mm = l_min_l_scr_mm.second; // mm
	  
	  // integrate over a cylinder (w.r.t. the ion's position, in the tilted reference frame parallel to the ion)
	  const double zi_min = zi-dL_ion_mm/2;
	  const double zi_max = zi+dL_ion_mm/2;
	  const double ze_min = -l_scr_mm;
	  const double ze_max = std::max(L_ion_mm+l_scr_mm, ze_min);

	  // consider contribution from r=0 (cross section parameter)
	  {
	    const auto &dP_cell = compute_transferred_momentum_cross_section(ion,
									     N_ele,      // local density of electrons per m^3
									     get_Q(),    // electron charge [e+]
									     V_ele,      // electron's velocity in the lab's frame
									     zi_min,     // zi_min in the lab's frame [mm]
									     zi_max,     // zi_max in the lab's frame [mm]
									     ze_min,     // ze_min in the lab's frame [mm]
									     ze_max,     // ze_max in the lab's frame [mm]
									     l_min_mm ); // l_min_mm
	    dP_ions_to_electrons += dP_cell;
	    
	    const auto &dP_to_electrons = [&] () {
	      const double Vol = M_PI * sqr(l_min_mm) * (ze_max - ze_min); // mm**3
	      const double Ne = N_ele * Vol / 1e9; // number of electrons in volume Vol
#ifdef DEBUG
	      std::cout << "Vol = " << Vol << std::endl;
	      std::cout << "dze = " << (ze_max - ze_min) << std::endl;
	      std::cout << "Ne = " << Ne << std::endl;
	      std::cout << "dP_cell_cs = " << dP_cell << std::endl;
#endif
	      return Ne > std::numeric_limits<double>::epsilon() ? dP_cell / Ne : StaticVector<3>(0.0); // MeV/c momentum
	    }();
#ifdef DEBUG
	    std::cout << "dP_to_ele_cross_section = " << dP_to_electrons << std::endl;
#endif 
	    dP_to_electrons_mesh.assign_value(r[0], r[1], r[2], dP_to_electrons); // MeV/c
	    dt_mm_avg += (dt_mm - dt_mm_avg) / ++count; // mm/c, running average of dt (only for particles that actually contributed to the force)
	  }

	  const double re_min = std::min(l_min_mm, r_max_mm);
	  const double re_max = std::min(std::max(l_scr_mm, re_min), r_max_mm);

	    // prepare for integration
	  const size_t nsteps_ze = 1; // number of cylindrical blocks (for now 10, must be properly chosen)
	  const size_t nsteps_re = 10;
	  const size_t nsteps_te = 10;

	  // consider contribution from all the other blocks
	  const double dze = (ze_max - ze_min) / nsteps_ze;
	  const double dre = (re_max - re_min) / nsteps_re;
	  const double dte = 2.0 * M_PI / nsteps_te;
	  for (size_t ii=0; ii<nsteps_ze; ii++) {
	    const double ze = ((nsteps_ze - ii) * ze_min + ii * ze_max) / nsteps_ze;
	    for (size_t jj=0; jj<nsteps_re; jj++) {
	      const double re = ((nsteps_re - jj) * re_min + jj * re_max) / nsteps_re;
	      for (size_t kk=0; kk<nsteps_te; kk++) {
		const double te = kk * 2.0 * M_PI / nsteps_te;
		const auto &rot = Rotation(te+dte/2, nz); // quaternion
		const auto &nr = rot * nx; // radial unit vector
		const auto &nt = rot * ny; // angular unit vector
		const auto &r_ele = r_ion + (re+dre/2) * nr + (ze+dze/2) * nz; // this is the location of the integration cell in the 3d volume

		// find out if r_ele is inside the electron mesh
		auto r = r_ele - min_;
		r[0] *= inv_hx;
		r[1] *= inv_hy;
		r[2] *= inv_hz;

		const auto &state = plasma_bnd(r[0], r[1], r[2]);
		const auto  N_ele = plasma_get_number_density(state); // #/m^3
		const auto &V_ele = plasma_get_velocity(state); // c
		// const auto &V_ele_r = V_ele * nr;
		// const auto &V_ele_t = V_ele * nt;
		// const auto &V_ele_z = V_ele * nz;

#ifdef DEBUG
		std::cout << "re = " << (re+dre/2) << std::endl;
		std::cout << "nz = " << nz << std::endl;
		std::cout << "nr = " << nr << std::endl;
		std::cout << "nt = " << nt << std::endl;
		std::cout << "r_ion = " << r_ion << std::endl;
		std::cout << "r_ele = " << r_ele << std::endl;
		std::cout << "V_ele = " << V_ele << std::endl;
#endif
		const auto &dP_cell_cylind = compute_transferred_momentum_energy_loss(ion,
										      N_ele,        // local density of electrons per m^3
										      get_Q(),      // electron charge [e+]
										      V_ele,        // electron's velocity in the lab's frame
										      zi_min,       // zi_min in the lab's frame [mm]
										      zi_max,       // zi_max in the lab's frame [mm]
										      ze,           // ze_min in the lab's frame [mm]
										      ze+dze,       // ze_max in the lab's frame [mm]
										      re,           // re_min in the lab's frame [mm]
										      re+dre,       // re_max in the lab's frame [mm]
										      te,           // te_min in the lab's frame [rad]
										      te+dte);      // te_max in the lab's frame [rad]

		// const auto &dP_cell_cylind = compute_transferred_momentum_cylindrical(ion,
		// 								      N_ele,        // local density of electrons per m^3
		// 								      get_Q(),      // electron charge [e+]
		// 								      V_ele_r,      // radial component of the electron's velocity w.r.t. the ion direction
		// 								      V_ele_t,      // angular component of the electron's velocity w.r.t. the ion direction
		// 								      V_ele_z,      // longitudinal component of the electron's velocity w.r.t. the ion direction
		// 								      zi_min,       // zi_min in the lab's frame [mm]
		// 								      zi_max,       // zi_max in the lab's frame [mm]
		// 								      ze,           // ze_min in the lab's frame [mm]
		// 								      ze+dze,       // ze_max in the lab's frame [mm]
		// 								      re,           // re_min in the lab's frame [mm]
		// 								      re+dre,       // re_max in the lab's frame [mm]
		// 								      te,           // te_min in the lab's frame [rad]
		// 								      te+dte);      // te_max in the lab's frame [rad]

		// toward the momentum...
		const auto &dP_cell = [&] () {
		  const auto &dPr = dP_cell_cylind[0];
		  const auto &dPt = dP_cell_cylind[1];
		  const auto &dPl = dP_cell_cylind[2];
#ifdef DEBUG
		  std::cout << "dP_ele_r = " << dPr << std::endl;
		  std::cout << "dP_ele_t = " << dPt << std::endl;
		  std::cout << "dP_ele_z = " << dPl << std::endl;
#endif
		  return dPr * nr + dPt * nt + dPl * nz;
		}();
		
		dP_ions_to_electrons += dP_cell;

		const auto &dP_to_electrons = [&] () { 
		  const double Vol = 0.5 * (sqr(re+dre) - sqr(re)) * dze * dte; // mm**3, volume of the integration region
		  const double Ne = N_ele * Vol / 1e9; // number of electrons in volume Vol
		  return Ne > std::numeric_limits<double>::epsilon() ? dP_cell / Ne : StaticVector<3>(0.0); // MeV/c momentum
		}();
		
		dP_to_electrons_mesh.assign_value(r[0], r[1], r[2], dP_to_electrons); // MeV/c
#ifdef DEBUG
		std::cout << "[ze,re,te] = " << ze << '\t' << re << '\t' << te << std::endl;
		std::cout << "V_ele = " << V_ele << std::endl;
		// std::cout << "V_ele_r = " << V_ele_r << '\n';
		// std::cout << "V_ele_t = " << V_ele_t << std::endl;
		// std::cout << "V_ele_z = " << V_ele_z << std::endl;
		std::cout << "dP_cell = " << dP_cell << std::endl;
#endif
		dt_mm_avg += (dt_mm - dt_mm_avg) / ++count; // mm/c, running average of dt_mm (only for particles that actually contributed to the force)
	      }
	    }
	  }

	  // particle update
	  const auto &dP_to_ion = -dP_ions_to_electrons / ion.N;
	  if (gsl_isnan(dP_to_ion[0]) ||
	      gsl_isnan(dP_to_ion[1]) ||
	      gsl_isnan(dP_to_ion[2])) {
	    std::cerr << "error: Nan in Electron Coooling computation (ions) @ S = " << S_mm << " mm\n";
	    exit(1); 		
	  }

#ifdef DEBUG
	  std::cout << ">>> dP_ions_to_electrons = " << dP_ions_to_electrons << std::endl;
	  std::cout << ">>> dP_to_ion = " << dP_to_ion << std::endl;
	  std::cout << ">>> norm(ion.P) / dP = " << norm(ion.get_Px_Py_Pz()) << " / " << dP_to_ion << std::endl;
#endif
	  
	  // Compute the effect of the magnetic field on the ion
	  {
	    const auto &Vz = ion.get_Vx_Vy_Vz()[2]; // c
	    const auto &F = dP_to_ion * 1e3 * Vz / dS_mm; // MeV/m
	    apply_force_through_Bfield(ion, F, get_static_Bfield(), dS_mm);
	  }
	  
	  // final momentum
	  if (ion.Pz <= 0.0) {
	    ion.Pz = 0.0; // MeV/c
	    ion.lost_at(bunch.S + 0.5*dS);
	    goto end;
	  }
	  
	} else {
	  move_particle_through_Bfield(ion, get_static_Bfield(), dS_mm);
	}

      }
    end:; // in case of lost particle
    };
    const auto effective_Nthreads = for_all(Nthreads, Nparticles, track_particle_parallel);

    // merge the forces from different threads and store the result in dP_to_electrons_mesh[0]
    // compute also the average time spent by the beam during the current step, store the result is dt_mm_average[0]
    for (size_t thread=1; thread<effective_Nthreads; thread++) {
      const auto &dP_to_electrons_mesh = dP_to_electrons_mesh_parallel[thread];
      for (size_t i=0; i<Nx*Ny*Nz; i++) {
	dP_to_electrons_mesh_parallel[0].data()[i] += dP_to_electrons_mesh.data()[i];
      }
      dt_mm_average[0] += dt_mm_average[thread]; // mm/c
    }
    dt_mm_average[0] /= effective_Nthreads;

    // advances the plasma
    apply_momentum_through_dt(dt_mm_average[0], dP_to_electrons_mesh_parallel[0]);

    S_mm += dS_mm;

    bunch.S += dS;
    transport_table.push_back(bunch.get_info());
  }

  return transport_table;
}
