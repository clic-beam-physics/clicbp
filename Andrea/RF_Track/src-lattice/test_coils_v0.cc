/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include "test_coils.hh"

std::pair<StaticVector<3>,StaticVector<3>> TestCoils::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t /* mm/c */ )
{
  if (z<0 || z>1e3*length)
    return std::pair<StaticVector<3>, StaticVector<3>>({{ 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 }});
  const double r_ = hypot(x,y); // mm
  const double K = B0*R*R*R; // T*m^3
  // See also CLIC note 465 https://cds.cern.ch/record/492189?ln=en
  // Bz(z) := K/(z^2+R^2)^(3/2)
  // Br(r,z):=-r/2*('diff(B(z),z)-r**2/8*'diff(B(z),z,3)+r**4/192*'diff(B(z),z,5));
  // Bz(r,z):=B(z)-r^2/4*('diff(B(z),z,2)-r^2/16*('diff(B(z),z,4))+r^4/576*('diff(B(z),z,6)))
  auto B =  [&] (double r /* m */, double z /* m */ ) {
    const double R_ = hypot(z, R); // m
    const double R2_ = R_*R_; // m^2
    const double R3_ = R2_*R_; // m^3
    const double Bz0 = K / R3_; // T
    if (r==0.0)
      return std::pair<double,double>(0.0,Bz0);
    const double r2 = r*r; // m^2
    const double z2 = z*z; // m^2
    const double z4 = z2*z2; // m^4
    const double z6 = z4*z2; // m^4
    const double R2 = R*R; // m^2
    const double R4 = R2*R2; // m^4
    const double R6 = R4*R2; // m^4
    const double R4_ = R2_*R2_; // m^4
    const double R6_ = R4_*R2_; // m^6
    const double dBz_dz = -3 * Bz0 * z / R2_; // T/m 
    const double d2Bz_dz2 = 3 * Bz0 * (2*z-R) * (2*z+R) / R4_; // T/m^2
    const double d3Bz_dz3 = 5 * dBz_dz * (4*z2-3*R2) / R4_; // T/m^3
    const double d4Bz_dz4 = 45 * Bz0 * (8*z4-12*R2*z2+R4) / (R4_*R4_); // T/m^4
    const double d5Bz_dz5 = 105 * dBz_dz * (8*z4-20*R2*z2+5*R4) / (R4_*R4_); // T/m^5
    const double d6Bz_dz6 = 315 * Bz0 * (64*z6-240*R2*z4+120*R4*z2-5*R6) / (R6_*R6_); // T/m^6
    const double Br = -r*(dBz_dz-r2*(d3Bz_dz3/8-r2*d5Bz_dz5/192))/2; // T
    const double Bz = Bz0-r2*(d2Bz_dz2-r2*(d4Bz_dz4/16-r2*d6Bz_dz6/576))/4; // T
    return std::pair<double,double>(Br,Bz);
  };
  auto Bp_ = B(r_/1e3, z/1e3 - 0.25*length);
  auto Bm_ = B(r_/1e3, z/1e3 - 0.75*length);
  CumulativeKahanSum<double> Br = Bp_.first; // T
  CumulativeKahanSum<double> Bz = Bp_.second; // T
  Br -= Bm_.first; // T
  Bz -= Bm_.second; // T
  const size_t Ncoils = 128; // 100 coils upstram and downstream
  for (size_t n=1; n<Ncoils; n++) { // Ncoils coils before and after, length is the period
    auto Bpp = B(r_/1e3, z/1e3 - 0.25*length + n*length);
    auto Bpm = B(r_/1e3, z/1e3 - 0.25*length - n*length);
    auto Bmp = B(r_/1e3, z/1e3 - 0.75*length + n*length);
    auto Bmm = B(r_/1e3, z/1e3 - 0.75*length - n*length);
    Br += Bpp.first;
    Br += Bpm.first;
    Br -= Bmp.first;
    Br -= Bmm.first;
    Bz += Bpp.second;
    Bz += Bpm.second;
    Bz -= Bmp.second;
    Bz -= Bmm.second;
  }
  if (r_==0.0)
    return std::pair<StaticVector<3>, StaticVector<3>>(static_Efield, static_Bfield + StaticVector<3>(0.0, 0.0, Bz));
  return std::pair<StaticVector<3>, StaticVector<3>>(static_Efield, static_Bfield + StaticVector<3>(Br*x/r_, Br*y/r_, Bz));
}
