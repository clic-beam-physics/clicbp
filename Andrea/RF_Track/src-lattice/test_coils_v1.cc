/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include "test_coils.hh"
#include <gsl/gsl_sf_ellint.h>

std::pair<StaticVector<3>,StaticVector<3>> TestCoils::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t /* mm/c */ )
{
  const double length_mm = length*1e3; // mm
  if (z<0 || z>length_mm)
    return std::pair<StaticVector<3>, StaticVector<3>>({{ 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 }});
  const double r = hypot(x,y); // mm
  const double alpha = r/(R*1e3); // #
  // See also CLIC note 465 https://cds.cern.ch/record/492189?ln=en
  // Bz(z) := B0*R^3/(z^2+R^2)^(3/2)
  // Br(r,z):=-r/2*('diff(B(z),z)-r**2/8*'diff(B(z),z,3)+r**4/192*'diff(B(z),z,5));
  // Bz(r,z):=B(z)-r^2/4*('diff(B(z),z,2)-r^2/16*('diff(B(z),z,4))+r^4/576*('diff(B(z),z,6)))
  // and https://tiggerntatie.github.io/emagnet/offaxis/iloopoffaxis.htm
  auto B =  [&] (double z_ /* mm */ ) {
    const double beta = z_/(R*1e3); // #
    const double gamma = z_/r; // #
    const double sqrt_Q = hypot(1+alpha, beta); // #
    const double Q = (1+alpha)*(1+alpha) + beta*beta; // #
    const double k = 2*sqrt(alpha) / sqrt_Q;
    const double K = gsl_sf_ellint_Kcomp(k, GSL_PREC_DOUBLE);
    const double E = gsl_sf_ellint_Ecomp(k, GSL_PREC_DOUBLE);
    const double B0_ = B0/M_PI/sqrt_Q; // T
    const double Bz = B0_*      (E * (1-alpha*alpha-beta*beta) / (Q-4*alpha) + K); // T
    const double Br = B0_*gamma*(E * (1+alpha*alpha+beta*beta) / (Q-4*alpha) - K); // T
    return std::pair<double,double>(Br, Bz);
  };
  const auto Bp_ = B(z - 0.25*length_mm);
  const auto Bm_ = B(z - 0.75*length_mm);
  CumulativeKahanSum<double> Br = Bp_.first; // T
  CumulativeKahanSum<double> Bz = Bp_.second; // T
  Br -= Bm_.first; // T
  Bz -= Bm_.second; // T
  const size_t Ncoils = 32; // 100 coils upstram and downstream
  for (size_t n=1; n<Ncoils; n++) { // Ncoils coils before and after, length is the period
    auto Bpp = B(z - 0.25*length_mm + n*length_mm);
    auto Bpm = B(z - 0.25*length_mm - n*length_mm);
    Br += Bpp.first;
    Br += Bpm.first;
    Bz += Bpp.second;
    Bz += Bpm.second;
    auto Bmp = B(z - 0.75*length_mm + n*length_mm);
    auto Bmm = B(z - 0.75*length_mm - n*length_mm);
    Br -= Bmp.first;
    Br -= Bmm.first;
    Bz -= Bmp.second;
    Bz -= Bmm.second;
  }
  if (r==0.0)
    return std::pair<StaticVector<3>, StaticVector<3>>(static_Efield, static_Bfield + StaticVector<3>(0.0, 0.0, Bz));
  return std::pair<StaticVector<3>, StaticVector<3>>(static_Efield, static_Bfield + StaticVector<3>(Br*x/r, Br*y/r, Bz));
}
