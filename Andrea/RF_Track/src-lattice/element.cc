/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <utility>
#include "RF_Track.hh"
#include "element.hh"
#include "for_all.hh"

void Aperture::set_aperture(double ax /* m */, double ay /* m */, const char *shape )
{
  rx = ax*1e3; // mm
  ry = ay == -1.0 ? rx : ay*1e3; // mm
  set_aperture_shape(shape);
}

void Aperture::set_aperture_shape(const char *shape )
{
  if (!strcmp(shape, "circular"))
    Aperture::shape = Aperture::CIRCULAR;
  else if (!strcmp(shape, "rectangular"))
    Aperture::shape = Aperture::RECTANGULAR;
  else if (!strcmp(shape, "none"))
    Aperture::shape = Aperture::NONE;
  else
    std::cerr << "error: unknown aperture shape '"<< shape << "'. Valid options are: 'none', 'rectangular', and 'circular'.\n";
}

const char *Aperture::get_aperture_shape() const
{
  if (Aperture::shape == Aperture::CIRCULAR)
    return "circular";
  if (Aperture::shape == Aperture::RECTANGULAR)
    return "rectangular";
  return "none";
}

void Element::track_in(Bunch6d &bunch ) // entrance misalignment
{
  const double eps = std::numeric_limits<double>::epsilon();
  const double offset_x = 0.5 * get_length() * xp - x;
  if (fabs(offset_x)>eps || fabs(xp)>eps) {
    for (auto &particle : bunch.particles) {
      if (particle) {
	particle.x  += offset_x;
	particle.xp -= xp;
      }
    }
  }
  const double offset_y = 0.5 * get_length() * yp - y;
  if (fabs(offset_y)>eps || fabs(yp)>eps) {
    for (auto &particle : bunch.particles) {
      if (particle) {
	particle.y  += offset_y;
	particle.yp -= yp;
      }
    }
  }
  if (fabs(roll)>eps) {
    const double s = sin(roll/1e3);
    const double c = cos(roll/1e3);
    for (auto &particle : bunch.particles) {
      if (particle) {
	double tmp=c*particle.x-s*particle.y;
	particle.y=s*particle.x+c*particle.y;
	particle.x=tmp;
	tmp=c*particle.xp-s*particle.yp;
	particle.yp=s*particle.xp+c*particle.yp;
	particle.xp=tmp;
      }
    }
  }
}

void Element::track_out(Bunch6d &bunch ) // exit misalignment
{
  const double eps = std::numeric_limits<double>::epsilon();
  if (fabs(roll)>eps) {
    const double s = sin(-roll/1e3);
    const double c = cos(-roll/1e3);
    for (auto &particle : bunch.particles) {
      if (particle) {
	double tmp=c*particle.x-s*particle.y;
	particle.y=s*particle.x+c*particle.y;
	particle.x=tmp;
	tmp=c*particle.xp-s*particle.yp;
	particle.yp=s*particle.xp+c*particle.yp;
	particle.xp=tmp;
      }
    }
  }
  const double offset_x = 0.5 * get_length() * xp + x;
  if (fabs(offset_x)>eps || fabs(xp)>eps) {
    for (auto &particle : bunch.particles) {
      if (particle) {
	particle.x  += offset_x;
	particle.xp += xp;
      }
    }
  }
  const double offset_y = 0.5 * get_length() * yp + y;
  if (fabs(offset_y)>eps || fabs(yp)>eps) {
    for (auto &particle : bunch.particles) {
      if (particle) {
	particle.y  += offset_y;
	particle.yp += yp;
      }
    }
  }
}

std::list<Bunch6d_info> Element::track(Bunch6d &bunch )
{
  std::list<Bunch6d_info> transport_table;
  
  auto kick_particle = [] (Particle &particle, const double *force, double dS ) -> void {
    const double Fx = force[0]; // MeV/m
    const double Fy = force[1]; // MeV/m
    const double Fz = force[2]; // MeV/m
    auto const V = particle.get_Vx_Vy_Vz(); // c
    const double dt = dS / V[2]; // m/c
    auto P = particle.get_Px_Py_Pz(); // MeV/c
    P[0] += Fx * dt; // MeV/c
    P[1] += Fy * dt; // MeV/c
    P[2] += Fz * dt; // MeV/c
    const double xp = P[0] * 1e3 / P[2]; // mrad
    const double yp = P[1] * 1e3 / P[2]; // mrad
    particle.xp = xp; // mrad
    particle.yp = yp; // mrad
    particle.Pc = norm(P); // MeV/c
  };

  track0_initialize(bunch);

  // parallel tracking through the elementm in sc_nsteps space-charge steps...
  size_t nsteps_ = nsteps;
  if (nsteps < sc_nsteps)
    nsteps = sc_nsteps;

  MatrixNd sc_force;

  const double S_element = bunch.S;
  size_t sc_step = 0;
  do {
    size_t step_start;
    size_t step_end;
    if (sc_nsteps>0) {
      step_start = sc_step * nsteps / sc_nsteps;
      step_end = (sc_step+1) * nsteps / sc_nsteps;
      if (sc_step==0) {
	double dS = (step_end - step_start) * get_length() / nsteps * 0.5;
	std::cout << " applying space-charge 0" << '/' << sc_nsteps << '\n';
	RF_Track_Globals::SC_engine.compute_force(sc_force, bunch);
	for (size_t i=0; i<bunch.size(); i++) {
	  auto &particle = bunch.particles[i];
	  if (particle) {
	    kick_particle(particle, sc_force[i], dS);
	  }
	}
      }
    } else {
      step_start = 0;
      step_end = nsteps;
    }

    // parallel tracking
    auto track_parallel = [&](size_t thread, size_t start, size_t end) -> void {
      for (size_t i=start; i<end; i++) {
	Particle &particle = bunch.particles[i];
	if (particle) {
	  track0(particle, S_element, step_start, step_end, thread);
	}
      }
    };
    const size_t Nthreads = RF_Track_Globals::number_of_threads;
    for_all(Nthreads, bunch.size(), track_parallel);

    const double dS_ = (step_end - step_start) * get_length() / nsteps;
    bunch.S += dS_;
    transport_table.push_back(bunch.get_info());

    // apply space-charge
    if (sc_nsteps>0) {
      double dS = (step_end - step_start) * get_length() / nsteps;
      if (sc_step == sc_nsteps-1) // half space charge if last step
	dS *= 0.5;
      std::cout << " applying space-charge " << (sc_step+1) << '/' << sc_nsteps << '\n';
      RF_Track_Globals::SC_engine.compute_force(sc_force, bunch);
      for (size_t i=0; i<bunch.size(); i++) {
	auto &particle = bunch.particles[i];
	if (particle) {
	  kick_particle(particle, sc_force[i], dS);
	}
      }
    }
  } while (++sc_step<sc_nsteps);

  nsteps = nsteps_;

  track0_finalize();

  for (auto &particle: bunch.particles) {
    if (particle) {
      if (!is_particle_inside_aperture(particle)) {
	particle.lost_at(bunch.S);
      }
    }
  }

  return transport_table;
}
