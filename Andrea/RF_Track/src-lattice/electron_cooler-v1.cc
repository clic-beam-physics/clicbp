/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <array>
#include <limits>
#include <numeric>
#include <utility>

#include "for_all.hh"
#include "bunch6d.hh"
#include "bunch6dt.hh"
#include "numtools.hh"
#include "rotation.hh"
#include "lorentz_boost.hh"
#include "electron_cooler.hh"
#include "move_particle.hh"
#include "stats.hh"

// #define DEBUG

ElectronCooler::ElectronCooler(double length /* m */, double ax /* m */, double ay /* m */, double N_ele /* #/m^3 */, double Vz /* c */ ) : Plasma(length, ax, ay, N_ele, Vz)
{
  set_aperture(ax, ay, "circular");
}

// return the bunch temperature in the moving frame of the plasma
template <typename BUNCH_TYPE>
static std::unordered_map<ParticleKey,StaticVector<3>,ParticleKeyHasher,ParticleKeyEquals> get_bunch_temperature(const BUNCH_TYPE &bunch, const StaticVector<3> &V_ele )
{
  // accumulate velocity variances, discriminating by ParticleKey (mass, charge)
  std::unordered_map<ParticleKey,Weighted_incremental_variance,ParticleKeyHasher,ParticleKeyEquals> velocity_variances;
  {
    const auto mvelocity = -V_ele;
    for (auto const &particle: bunch.particles) {
      if (particle) {
	auto key = ParticleKey{ particle.mass, particle.Q };
	auto V = particle.get_Vx_Vy_Vz();
	auto V_rel = relativistic_velocity_addition(mvelocity, V);
	velocity_variances[key].add(V_rel, particle.N);
      }
    }
  }

  // computes the temperatures Tx, Ty, Tz, for each ParticleKey
  // returns the temperatures in K
  // MeV / kboltzmann = 11604519366.495 K
  std::unordered_map<ParticleKey,StaticVector<3>,ParticleKeyHasher,ParticleKeyEquals> retval;
  const double gamma = 1.0 / sqrt(1.0 - V_ele*V_ele);
  for (auto const &vv: velocity_variances) {
    retval[vv.first] = vv.first.mass * gamma * vv.second.variance() * 11604519366.495;
  }

  return retval;
}

template <typename BUNCH_TYPE>
static double get_debye_length(const BUNCH_TYPE &bunch, double density /* #/m^3 */, const StaticVector<3> &V_ele )
{
  if (density == 0.0)
    return 0.0;

  // const double kbT = get_plasma_temperature() / 11604519366.495; // MeV
  StaticVector<3> T(0.0);
  const auto TT = get_bunch_temperature(bunch, V_ele);
  for (auto const &tt: TT) {
    T += tt.second;
  }

#ifdef DEBUG
  std::cout << "bunch temperature = " << T << std::endl;
#endif

  const double kbT = (T[0] + T[1] + T[2]) / 3.0 / 11604519366.495; // MeV
  const double inv_gamma = sqrt(1.0 - V_ele*V_ele);

#ifdef DEBUG
  std::cout << "inv_gamma = " << inv_gamma << std::endl;
  std::cout << "Debye length = " << 7433942.2 * sqrt(kbT / (density * inv_gamma)) << std::endl;
#endif
  
  // sqrt(epsilon0 * MeV * m^3 /e/e) = 7433942.2 m
  return inv_gamma > std::numeric_limits<double>::epsilon() ? 7433942.2 * sqrt(kbT / (density * inv_gamma)) : std::numeric_limits<double>::max(); // m
}

// tracking
std::list<Bunch6d_info> ElectronCooler::track(Bunch6d &bunch )
{
  std::list<Bunch6d_info> transport_table;

#ifdef DEBUG
  std::cout << "ElectronCooler::enter tracking " << bunch.get_ngood() << std::endl;
#endif
  
  const size_t Nthreads = RF_Track_Globals::number_of_threads;
  const size_t Nparticles = bunch.size();

  // resize meshes
  size_t
    Nx = size1(),
    Ny = size2(),
    Nz = nsteps;

  // init mesh external forces [MeV/mm]
  dP_to_electrons_mesh_parallel.resize(Nthreads);
  for (auto &dP_to_electrons_mesh: dP_to_electrons_mesh_parallel)
    dP_to_electrons_mesh.resize(Nx, Ny, Nz);

  // mesh has the longitudinal extension of the element and the trasnverse extension of the aperture
  StaticVector<3> min_(-aperture.x, -aperture.y, 0); // mm
  StaticVector<3> max_(+aperture.x, +aperture.y, 1e3*get_length()); // mm

  // working box [mm]
  const auto dim = max_ - min_;

  // size of the electron mesh cell [mm]
  const double r_max_mm = hypot(dim[0], dim[1], dim[2]); // mm
	
  // (inverse) size of the electron mesh cell [mm]
  const double inv_hx = double(Nx) / dim[0]; // 1/mm
  const double inv_hy = double(Ny) / dim[1]; // 1/mm
  const double inv_hz = double(Nz) / dim[2]; // 1/mm

  // start integration loop
  const double dS = get_length() / nsteps;
  const double dS_mm = get_length() * 1e3 / nsteps; // mm
  double S_mm = 0.0; // mm, coordinate along the element
  for (size_t step = 0; step<nsteps; step++) {
    // initialization of the thread's local variables
    std::vector<double> dt_mm_average(Nthreads); // mm/c, average time spent by the beam in one step (used to advance the electron mesh)
    auto track_particle_parallel = [&](size_t thread, size_t start, size_t end ) -> void {
      auto &dP_to_electrons_mesh = dP_to_electrons_mesh_parallel[thread]; // MeV/mm
      auto &dt_mm_avg = dt_mm_average[thread]; // mm/c
      for (size_t i=0; i<Nx*Ny*Nz; i++)
	dP_to_electrons_mesh.data()[i] = StaticVector<3>(0.0);
      dt_mm_avg = 0.0;
      size_t count = 0; // count of good particles (necessary to dt_mm_average);
      for (size_t n=start; n<end; n++) {
	auto &ion = bunch.particles[n];
	if (ion) {
#ifdef DEBUG
	  std::cout << "START" << std::endl;
#endif
	  // ion position halfway through the cell
	  const auto &r_ion = [&ion,&S_mm,&dS_mm] () {
	    const double half_dS_mm = 0.5 * dS_mm;
	    return StaticVector<3>(ion.x + ion.xp * half_dS_mm / 1e3,
				   ion.y + ion.yp * half_dS_mm / 1e3,
				   S_mm + half_dS_mm); // mm
	  }();

	  if (!is_point_inside_aperture(r_ion[0], r_ion[1])) { ion.lost_at(bunch.S + 0.5*dS); continue; }

	  const double Q_ion = ion.Q;
	  const double Q_ele = get_Q();
	  //////////
	  // You have: e*e/(4*pi*epsilon0)
	  // You want: MeV*mm
	  // e*e/(4*pi*epsilon0) = 1.439964485044515e-12 MeV*mm
	  // e*e/(4*pi*epsilon0) = (1 / 694461572063.7623) MeV*mm
	  const double K = (Q_ion * Q_ele) / 694461572063.7623; // (e*e/4/pi/epsilon0) MeV*mm

	  const auto &V_ion = ion.get_Vx_Vy_Vz(); // c
	  const auto &dt_mm = dS_mm / V_ion[2]; // mm/c, time spent by the ion in the cell
	  dt_mm_avg += (dt_mm - dt_mm_avg) / ++count; // mm/c, running average of dt_mm (used to advance the electron mesh)
	  
	  const auto V_ion_l = norm(V_ion); // axial component of the velocity
	  const auto dL_ion_mm = V_ion_l * dt_mm; // mm, Length of the trajectory of the ion in the integration step
	  const auto L_ion_mm = dL_ion_mm * Nz; // mm, total length of the ion trajectory inside the element
	  const auto zi = step*dL_ion_mm; // mm, position of the ion along its trajectory
	  
	  // orthogonal axes with 'z' parallel to the ion's velocity
	  const auto &nz = normalize(V_ion); // unit vector running parallel to the ion in the lab's frame
	  const auto &nx = normalize(StaticVector<3>(0.0, 1.0, 0.0) ^ V_ion); // unit vector perpendicular to nz

#ifdef DEBUG
	  std::cout << " nx = " << nx << std::endl;
	  std::cout << " nz = " << nz << std::endl;
#endif
	  // compute l_min_mm, l_scr_mm at the current position of the ion
	  const double zi_min = dL_ion_mm*step;
	  const double zi_max = dL_ion_mm*(step+1);
	  const double t_min = (zi_min-zi)/V_ion_l; // mm/c
	  const double t_max = (zi_max-zi)/V_ion_l; // mm/c
	  double re_max;
	  double ze_min;
	  double ze_max;
	  double te_min;
	  double te_max;
	  {
	    // boundary conditions for the integration cell
	    const auto &state = [&] () {
	      auto r = r_ion - min_;
	      r[0] *= inv_hx;
	      r[1] *= inv_hy;
	      r[2] *= inv_hz;
	      return plasma_bnd(r[0], r[1], r[2]);
	    } ();
	    const auto  N_ele = plasma_get_number_density(state); // #/m^3
	    const auto &V_ele = plasma_get_velocity(state); // c
	    // const auto gamma_ele = 1.0 / sqrt(1.0 - dot(V_ele,V_ele));
	    
	    // compute l_min_mm and l_scr_mm
	    // see Conte pag 248, l_min: mm = 2*e*e / 4/ pi/ epsilon0 / MeV / 347230786031.881 minimum impact distance
	    const auto dV = V_ele - V_ion; // relative velocity in the lab frame
	    const auto dV_sqr = dot(dV,dV);
	    const double LC = 10.0; // Coulomb logarithm
	    const double dist_ele_mm = N_ele == 0.0 ? std::numeric_limits<double>::max() : 1e3 / pow(N_ele, 1.0 / 3.0); // mm, average electron distance
	    const double l_debye_mm = get_debye_length(bunch, N_ele, V_ele) * 1e3; // mm
	    const double l_min_mm = std::min(fabs(K) / (0.5 * get_mass() * dV_sqr), dist_ele_mm); // mm, minimum impact distance
	    const double l_scr_mm = std::max(l_min_mm * exp(LC), l_debye_mm); // mm, screening length in the lab frame

	    // integration limits
	    re_max = std::min(l_scr_mm, r_max_mm);
	    ze_min = std::max(0.0, zi_min-l_scr_mm);
	    ze_max = std::min(zi_max+l_scr_mm, L_ion_mm);
	    te_min = 0.0;
	    te_max = 2.0 * M_PI;

#ifdef DEBUG
	    std::cout << "V_ele = " << V_ele << std::endl;
	    std::cout << "V_ion = " << V_ion << std::endl;
	    std::cout << "dV = " << dV << std::endl;
	    std::cout << "dist_ele_mm = " << dist_ele_mm << std::endl;
	    std::cout << "l_min_mm = " << l_min_mm << std::endl;
	    std::cout << "l_debye_mm = " << l_debye_mm << std::endl;
	    std::cout << "l_scr_mm = " << l_scr_mm << std::endl;
	    std::cout << "zi_min = " << zi_min << std::endl;
	    std::cout << "zi_max = " << zi_max << std::endl;
	    std::cout << "ze_min = " << ze_min << std::endl;
	    std::cout << "ze_max = " << ze_max << std::endl;
	    std::cout << "re_max = " << re_max << std::endl;
#endif	  
	  }

	  auto get_state_wrt_ion = [&](double re, double te, double ze ) {
	    const auto rot = Rotation(te, nz); // rotation quaternion
	    const auto nr = rot * nx; // radial unit vector
	    const auto dr0 = re*nr + (ze-zi)*nz; // this is the location of the electron w.r.t. the ion
	    const auto &r_ele = r_ion + dr0; // this is the location of the integration cell in the 3d volume
	    // find state at r_ele position
	    auto r = r_ele - min_;
	    r[0] *= inv_hx;
	    r[1] *= inv_hy;
	    r[2] *= inv_hz;
	    return plasma_bnd(r[0], r[1], r[2]);
	  };
	  
	  auto func_dE = [&](double re, double te, double ze, const StaticVector<4> &state ) { // see notes
	    const auto rot = Rotation(te, nz); // rotation quaternion
	    const auto nr = rot * nx; // radial unit vector
	    const auto dr0 = re*nr + (ze-zi)*nz; // this is the location of the electron w.r.t. the ion
	    // compute the initial parameters of the electrons
	    const auto  N_ele = plasma_get_number_density(state); // #/m^3
	    const auto &V_ele = plasma_get_velocity(state); // c
	    const auto gamma_ele = 1.0 / sqrt(1.0 - dot(V_ele,V_ele)); // relativistic gamma for the electron
	    const auto E_ele_i = get_mass() * gamma_ele; // MeV, electron total energy
	    const auto P_ele_i = E_ele_i * V_ele; // MeV/c, electron momentum
	    //
	    const auto V_com = (ion.get_Px_Py_Pz() + P_ele_i) / (ion.get_total_energy() + E_ele_i); // velocity of center of mass in lab's frame
	    const auto nz_com = normalize(V_com); // c
	    const auto dV = V_ele - V_ion; // velocity difference V_ele - V_ion
#ifdef DEBUG
	    std::cout << "[ze,re,te] = " << ze << '\t' << re << '\t' << te << std::endl;
	    std::cout << "r_ion = " << r_ion << std::endl;
	    std::cout << "dr0 = " << dr0 << std::endl;
	    std::cout << "N_ele = " << N_ele << std::endl;
	    std::cout << "V_ele = " << V_ele << std::endl;
	    std::cout << "V_ion = " << V_ion << std::endl;
	    std::cout << "V_com = " << V_com << std::endl;
#endif
	    const auto &dP = [&] () { // momentum transferred to the electrons
	      if (re==0.0) { // maximum energy transfer: head-on collision, the electron reverses its direction in the center of mass
		const auto &P_ele_i_4d = StaticVector<4>(E_ele_i, P_ele_i[0], P_ele_i[1], P_ele_i[2]);
		const auto &P_ele_i_4d_com = lorentz_boost(V_com, P_ele_i_4d);
		const auto &P_ele_f_4d_com = StaticVector<4>(P_ele_i_4d_com[0], -P_ele_i_4d_com[1], -P_ele_i_4d_com[2], -P_ele_i_4d_com[3]);
		const auto &P_ele_f_4d = lorentz_boost(-V_com, P_ele_f_4d_com);
		const auto &P_ele_f = StaticVector<3>(P_ele_f_4d[1], P_ele_f_4d[2], P_ele_f_4d[3]);
		return P_ele_f - P_ele_i; // MeV/c
	      } else {
		auto sqr = [] (const double &x ) { return x*x; };
		/*
		  load("vect");
		  expandcrossplus : true;
		  expandcrosscross : true;
		  expandcrosscross : false;
		  declare(Ve,nonscalar);
		  declare(Vi,nonscalar);
		  declare(dr0,nonscalar);
		  declare(dV,nonscalar);
		  dr:dr0+dV*t;
		  dP:vectorsimp(dr+(Ve~(Vi~dr)))/((dr.dr)^(3/2));
		  dP_epxr:K*vectorsimp(coeff(num(dP),t) * It + coeff(num(dP),t,0) * I1);
		  ==> which gives: K*(I1*((dr0~Vi)~Ve+dr0)+It*(dV+Ve~Vi~dV))
		  DENOM:denom(dP);
		  EXPR:expand(express(DENOM^(2/3)));
		  A:coeff(EXPR,t^2);
		  B:coeff(EXPR,t);
		  C:coeff(EXPR,t,0);
		  remvalue(A,B,C);
		  func_I1:factor(integrate(1/((A*t^2+B*t+C)^(3/2)),t));
		  func_It:factor(integrate(t/((A*t^2+B*t+C)^(3/2)),t));
		*/
		const auto A = dot(dV,dV); // c^2
		const auto B = 2*dot(dr0,dV); // mm*c
		const auto C = dot(dr0,dr0); // mm^2
		const auto D = B*B-4*A*C; // mm^2 * c^2
		const auto func_I1 = [&A,&B,&C,&D,&sqr](const double &t ) {
		  return D != 0.0 ? -(2*(B+2*t*A))/(sqrt(C+t*(B+t*A))*D) : -(2*sqrt(A))/sqr(B+2*t*A);
		}; // 1/mm^2/c
		const auto func_It = [&A,&B,&C,&D,&sqr](const double &t ) {
		  const auto T1 = sqrt(C+t*(B+t*A)); // mm
		  return D != 0.0 ? 2*(2*C+t*B)/(T1*D) : (sqrt(A)*B*T1-sqr(B)-4*t*A*B-4*sqr(t*A))/((A*sqr(B)+4*t*sqr(A)*B+4*A*sqr(t*A))*T1);
		}; // 1/mm/c^2
		const auto I1 = func_I1(t_max) - func_I1(t_min); // 1/mm^2/c
		const auto It = func_It(t_max) - func_It(t_min); // 1/mm/c^2
		// const auto I1 = D != 0.0 ? (8*A)/(4*sqrt(A*A*A)*C-sqrt(A)*B*B) : 0.0;
		// const auto It = D != 0.0 ? (4*B)/(4*sqrt(A*A*A)*C-sqrt(A)*B*B) : 0.0;
		const auto dP = K*(I1*(dr0+(V_ele^(V_ion^dr0)))+It*(dV+(V_ele^(V_ion^V_ele)))); // MeV*mm * (1/mm^2/c*mm + 1/mm/c^2*c) == MeV/c
		////// takes only the component of dP that is orthogonal to V_com (this is the case in an elastic collision)
		return dP; ///// - dot(dP,nz_com)*nz_com;
	      }
	    }();

	    const double E_ele_f = hypot(get_mass(), norm(P_ele_i+dP)); // MeV, electron final total energy

	    // use t Mandelstam, MeV^2, to find K_ele
	    //  t =  2.0 * (E_ele_i * (E_ele_i - E_ele_f) + dot(P_ele_i, dP)) , in the lab's reference frame
	    //  t = -2.0 * electronmass * K_ele_f , in the reference system of the electron
	    const double K_ele_f = (E_ele_i * (E_ele_f - E_ele_i) - dot(P_ele_i, dP)) / get_mass(); // in the reference system of the electron

	    // use the conservation of energy, E_ele_i + E_ion_i + U_i = E_ele_f + E_ion_f + U_f, to find dE
	    const double norm_dr_i = norm(dr0); // mm 
	    const double norm_dr_f = norm(dr0 + dV*dt_mm); // mm
	    const double U_i = norm_dr_i != 0.0 ? K / norm_dr_i : 0.0; // MeV, initial potential energy
	    const double U_f = norm_dr_f != 0.0 ? K / norm_dr_f : 0.0; // MeV, final potential energy
	    const double dE = N_ele * (E_ele_i - E_ele_f  + U_i - U_f); // dE = N_ele * (E_ion_f - E_ion_i) MeV/m^3
#ifdef DEBUG
	    std::cout << "func_dE = " << dE << (re==0.0 ? " (head on)\n" : "\n");
	    std::cout << "func_dE (K_ele_f) = " << K_ele_f << (re==0.0 ? " (head on)\n" : "\n");
#endif
	    return std::array<double,2>{{ dE, K_ele_f }}; // MeV / m^3, MeV
	  };

	  auto func_dE_dr = [&](double re0, double re1, double te, double ze ) { // see notes
	    const auto &state = get_state_wrt_ion(0.0, 0.0, 0.0); // gets N_ele and V_ele
	    const auto N_ele = plasma_get_number_density(state); // #/m^3
	    const auto &V_ele = plasma_get_velocity(state); // c
	    const auto &dV = V_ele - V_ion; // velocity difference V_ele - V_ion
	    const auto dV_sqr = dot(dV,dV); // c^2
	    const auto T_max = func_dE(re0, te, ze, state)[1]; // MeV, K_ele_f at re = 0.0
	    const auto T_min = func_dE(re1, te, ze, state)[1]; // MeV, K_ele_f at re = re_min
	    if (N_ele == 0.0 || dV_sqr==0.0 || T_max/T_min <= 0.0)
	      return 0.0;
	    const int sign = dV*V_ion < 0 ? -1 : 1; // decelerates if (V_ele - V_ion) goes in the opposte direction of V_ion
	    const double dE = sign * N_ele * (K*K) * log(fabs(T_max / T_min)) / (get_mass()*dV_sqr); // MeV * mm^2 / m^3
#ifdef DEBUG
	    std::cout << "T_max = " << T_max << std::endl;
	    std::cout << "T_min = " << T_min << std::endl;
	    std::cout << "func_dE_dr = " << dE << std::endl;
#endif
	    return dE; // MeV * mm^2 / m^3
	  };
	    
	  const double dE_ion = [&] () {	    
	    // 3d integration
	    // const int coeff[] = { 1, 4, 1 }; // Simpson's rule
	    // const int coeff[] = { 1, 3, 3, 1 }; // Simpson's rule 3/8
	    // const int coeff[] = { 7, 32, 12, 32, 7 }; // Boole's rule
	    // const int coeff[] = { 11, 26, 31, 26, 11 }; // Stable Newton-Cotes formula http://www.holoborodko.com/pavel/numerical-methods/numerical-integration/
	    // const int coeff[] = { 31, 61, 76, 76, 61, 31 }; // O(h^5*f(4))
	    // const int coeff[] = { 268, 933, 786, 646, 786, 933, 268 }; // O(h^7*f(6))
	    // const int coeff[] = { 1657, 5157, 4947, 4079, 4079, 4947, 5157, 1657 }; // O(h^7*f(6))
	    // const int coeff[] = { 309, 869, 904, 779, 713, 779, 904, 869, 309 }; // O(h^7*f(6))
	    const int coeff[] = { 1, 1 };
	    const size_t N_ = sizeof(coeff) / sizeof(*coeff); // number of points
	    const size_t sum_ = std::accumulate(coeff, coeff+N_, 0);
	    // const double re_min = re_max / N_;
	    const double re_min = 0.0; ////////re_max / N_;
	    double re[N_], te[N_], ze[N_];
	    linspace(re_min, re_max, N_, re);
	    linspace(te_min, te_max, N_, te);
	    linspace(ze_min, ze_max, N_, ze);
	    CumulativeKahanSum<double> sum(0.0);
	    for (size_t i=0; i<N_; i++) {
	      for (size_t j=0; j<N_; j++) {
		for (size_t k=0; k<N_-1; k++) {
		  sum += (coeff[i] * coeff[j]) * func_dE_dr(re[k], re[k+1], te[i], ze[j]); // MeV * mm^2 / m^3
		}
		// sum += (coeff[i] * coeff[j]) * func_dE_dr(0.0, re[0], te[i], ze[j]); // MeV * mm^2 / m^3
		// for (size_t k=0; k<N_; k++) {
		//   const auto &state = get_state_wrt_ion(re[k], te[i], ze[j]);
		//   sum += (coeff[i] * coeff[j] * coeff[k]) * re[k] * func_dE(re[k], te[i], ze[j], state)[0] * (re_max - re_min) / sum_; // MeV * mm^2 / m^3
		// }
		// const auto &dP_to_electrons = [&] () { 
		//   const double Vol = 0.5 * (sqr(re+dre) - sqr(re)) * dze * dte; // mm^3, volume of the integration region
		//   const double Ne = N_ele * Vol / 1e9; // number of electrons in volume Vol
		//   return Ne > std::numeric_limits<double>::epsilon() ? dE_cell / Ne : 0.0; // MeV/c momentum
		// }() * ion.N * nr; //// TO BE DONE! 
		// dP_to_electrons_mesh.assign_value(r[0], r[1], r[2], dP_to_electrons); // MeV/c
	      }
	    }
	    const double dE = sum * (te_max - te_min) * (ze_max - ze_min) / (sum_*sum_); // MeV * mm^3 / m^3 = MeV / 1e9
	    return dE / 1e9; // MeV
	  } ();

#ifdef DEBUG
	  std::cout << "dE_ion = " << dE_ion << std::endl;
#endif
	  // particle update
	  if (gsl_isnan(dE_ion)) {
	    std::cerr << "error: Nan in Electron Coooling computation (ions) @ S = " << S_mm << " mm\n";
	    exit(1);
	  }
	  
	  // Compute the effect of the magnetic field on the ion
	  const auto &F = nz * dE_ion * 1e3 / dL_ion_mm; // MeV/m
	  apply_force_through_Bfield(ion, F, get_static_Bfield(), dS_mm);
	  
#ifdef DEBUG
	  std::cout << ">>> F = " << F << std::endl;
#endif
	  
	  // final momentum
	  if (ion.Pc <= 0.0) {
	    ion.Pc = 0.0; // MeV/c
	    ion.lost_at(bunch.S + 0.5*dS);
	    goto end;
	  }
	}
      }
    end:; // in case of lost particle
    };
    const auto effective_Nthreads = for_all(Nthreads, Nparticles, track_particle_parallel);
    
#ifdef DEBUG
    std::cout << "END!\n";
#endif
    
    // merge the forces from different threads and store the result in dP_to_electrons_mesh[0]
    // compute also the average time spent by the beam during the current step, store the result is dt_mm_average[0]
    for (size_t thread=1; thread<effective_Nthreads; thread++) {
      const auto &dP_to_electrons_mesh = dP_to_electrons_mesh_parallel[thread];
      for (size_t i=0; i<Nx*Ny*Nz; i++) {
	dP_to_electrons_mesh_parallel[0].data()[i] += dP_to_electrons_mesh.data()[i];
      }
      dt_mm_average[0] += dt_mm_average[thread]; // mm/c
    }
    dt_mm_average[0] /= effective_Nthreads;
    
    // advances the plasma (TO DO!)
    /////////apply_momentum_through_dt(dt_mm_average[0], dP_to_electrons_mesh_parallel[0]);
    
    S_mm += dS_mm;
    
    bunch.S += dS;
    transport_table.push_back(bunch.get_info());
  }
  
  return transport_table;
}
