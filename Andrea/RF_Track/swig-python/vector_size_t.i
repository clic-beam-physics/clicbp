/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#if defined(SWIGPYTHON)

// Vector of indexes as input argument
%typemap(in) const std::vector<size_t> & {
  if (PyList_Check($input)) {
    int size = PyList_Size($input);
    if (($1 = new std::vector<size_t>(size))) {
      std::vector<size_t> &ret = *$1;
      for (size_t i=0; i<size; i++) {
	ret[i] = (size_t)PyLong_AsUnsignedLong(PyList_GetItem($input, i));
      }
    }
  } else if (PyInt_Check($input)) {
    if (($1 = new std::vector<size_t>(1))) {
      (*$1)[0] = (size_t)PyLong_AsUnsignedLong($input);
    }
  }
 }

%typemap(freearg) const std::vector<size_t> & {
  if ($1) delete $1;
}

%typemap(typecheck) const std::vector<size_t> & {
  $1 = PyList_Check($input) || PyLong_Check($input) ? 1 : 0;
}

%typemap(argout,noblock=1) const std::vector<size_t> & {
}

// Vector of indexes as output argument
%typemap(in, numinputs=0) std::vector<size_t> & (std::vector<size_t> temp ) {
  $1 = &temp;
}

%typemap(argout) std::vector<size_t> & {
  if (PyObject *res = PyList_New($1->size())) {
    for (size_t i=0; i<$1->size(); i++) {
      PyList_SetItem(res, i, PyLong_FromUnsignedLong((*$1)[i]));
    }
    $result = SWIG_Python_AppendOutput($result, res);
  }
}

%typemap(freearg,noblock=1) std::vector<size_t> & {
}

#endif
