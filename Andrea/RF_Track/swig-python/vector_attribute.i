/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

/* attribute.i */

%{
#include <vector>
#include <map>
#include "attribute.hh"
%}

#if defined(SWIGPYTHON)

// NumPy Dictionary -> const C++ Attribute
%typemap(in) const std::vector<Attribute> & {
  if (PyString_Check($input)) {
    if (($1 = new std::vector<Attribute>(1))) {
      std::vector<Attribute> &ret = *$1;
      ret[0] = std::string(PyString_AsString($input));
    }
  } else if (PyList_Check($input)) {
    int size = PyList_Size($input);
    if (($1 = new std::vector<Attribute>(size))) {
      std::vector<Attribute> &ret = *$1;
      for (size_t i=0; i<size; i++) {
	PyObject *object = PyList_GetItem($input, i);
	if (PyInt_Check(object)) {
	  ret[i] = (size_t)PyLong_AsUnsignedLong(object);
	} else if (PyFloat_Check(object)) {
	  ret[i] = (double)PyFloat_AsDouble(object);
	} else if (PyComplex_Check(object)) {
	  Py_complex D = PyComplex_AsCComplex(object);
	  ret[i] = std::complex<double>(D.real, D.imag);
	} else if (PyString_Check(object)) {
	  ret[i] = std::string(PyString_AsString(object));
	} else {
	  std::cerr << "Unknown type in attributes list\n";
	}
      }
    }
  } else {
    if (($1 = new std::vector<Attribute>(1))) {
      std::vector<Attribute> &ret = *$1;
      if (PyString_Check($input)) {
	ret[0] = std::string(PyString_AsString($input));
      } else if (PyInt_Check($input)) {
	ret[0] = (size_t)PyLong_AsUnsignedLong($input);
      } else if (PyFloat_Check($input)) {
	ret[0] = (double)PyFloat_AsDouble($input);
      } else if (PyComplex_Check($input)) {
	Py_complex D = PyComplex_AsCComplex($input);
	ret[0] = std::complex<double>(D.real, D.imag);
      } else {
	std::cerr << "Unknown type in attributes list\n";
      }
    }
  }
}

%typemap(freearg) const std::vector<Attribute> & {
  if ($1) delete $1;
}

%typemap(typecheck) const std::vector<Attribute> & {
  $1 = PyList_Check($input) ||
    PyString_Check($input) ||
    PyInt_Check($input) ||
    PyFloat_Check($input) ||
    PyComplex_Check($input) ? 1 : 0;
}

%typemap(argout,noblock=1) const std::vector<Attribute> & {
}

// C++ Attribute -> Python List (as a return value)
%typemap(in, numinputs=0) std::vector<Attribute> & (std::vector<Attribute> temp ) {
  $1 = &temp;
}

%typemap(argout) std::vector<Attribute> & {
  std::vector<Attribute> &attributes = *$1;
  if (PyObject *res = PyList_New(attributes.size())) {
    for (size_t i=0; i<attributes.size(); i++) {
      switch(attributes[i].type()) {
      case OPT_INT: PyList_SetItem(res, i, PyLong_FromLong(int(attributes[i]))); break;
      case OPT_UINT: PyList_SetItem(res, i, PyLong_FromUnsignedLong(size_t(attributes[i]))); break;
      case OPT_BOOL: PyList_SetItem(res, i, PyInt_FromLong(bool(attributes[i]) ? 1: 0)); break;
      case OPT_DOUBLE: PyList_SetItem(res, i, PyFloat_FromDouble(double(attributes[i]))); break;
      case OPT_STRING: PyList_SetItem(res, i, PyString_FromString(std::string(attributes[i]).c_str())); break;
      case OPT_COMPLEX: PyList_SetItem(res, i, PyComplex_FromDoubles(attributes[i].get_complex().real(), attributes[i].get_complex().imag())); break;
      }
    }
    $result = SWIG_Python_AppendOutput($result, res);
  }
}

%typemap(freearg,noblock=1) std::vector<Attribute> & {
}

#endif
