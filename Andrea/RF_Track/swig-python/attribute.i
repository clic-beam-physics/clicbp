/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2015-2018 CERN, Geneva. All rights not expressly granted
** are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

/* attribute.i */

%{
#include "attribute.hh"
%}

#if defined(SWIGPYTHON)

// Python array or cell -> C++ Attribute
%typemap(in) const Attribute & {
  $1 = new Attribute;
  if (is_array($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_DOUBLE, 1, 2);
    npy_intp strides[2] = { 0, 0 };
    int rows, cols;
    if (PyArray_NDIM(array) == 1) {
      rows = 1;
      cols = PyArray_DIMS(array)[0];
      strides[0] = 0;
      strides[1] = PyArray_STRIDES(array)[0];
    } else {
      rows = PyArray_DIMS(array)[0];
      cols = PyArray_DIMS(array)[1];
      strides[0] = PyArray_STRIDES(array)[0];
      strides[1] = PyArray_STRIDES(array)[1];
    }
    char *data = PyArray_BYTES(array);
    MatrixNd matrix(rows, cols);
    for (int i=0; i<rows; i++)
      for (int j=0; j<cols; j++)
        matrix[i][j] = *(double *)(data + i*strides[0] + j*strides[1]);
    *$1 = matrix;
  } else if (PyString_Check($input)) {
    *$1 = std::string(PyString_AsString($input));
  } else if (PyInt_Check($input)) {
    *$1 = (size_t)PyLong_AsUnsignedLong($input);
  } else if (PyFloat_Check($input)) {
    *$1 = (double)PyFloat_AsDouble($input);
  } else if (PyComplex_Check($input)) {
    Py_complex D = PyComplex_AsCComplex($input);
    *$1 = std::complex<double>(D.real, D.imag);
  } else {
    std::cerr << "Unknown type in attributes list\n";
  }
}

%typemap(freearg) const Attribute & {
   if ($1) delete $1;
}

%typemap(typecheck) const Attribute & {
  $1 = PyString_Check($input) ||
    PyInt_Check($input) ||
    PyFloat_Check($input) ||
    PyComplex_Check($input) ? 1 : 0;
}

%typemap(argout,noblock=1) const Attribute & {
}

// C++ Attribute -> Python array (as an output argument)
%typemap(in, numinputs=0) Attribute & (Attribute temp ) {
  $1 = &temp;
}

%typemap(argout) Attribute & {
  const Attribute &attribute = *$1;
  PyObject *res;
  switch(attribute.type()) {
  case OPT_INT: res = PyLong_FromLong(int(attribute)); break;
  case OPT_UINT: res = PyLong_FromUnsignedLong(size_t(attribute)); break;
  case OPT_BOOL: res = PyInt_FromLong(bool(attribute) ? 1: 0); break;
  case OPT_DOUBLE: res = PyFloat_FromDouble(double(attribute)); break;
  case OPT_MATRIX: {
    const MatrixNd &matrix = attributes[i].get_matrix();
    npy_intp dimensions[2] = { matrix.size1(), matrix.size2() };
    PyArrayObject *_matrix = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
    npy_intp *strides = PyArray_STRIDES(_matrix);
    char *data = PyArray_BYTES(_matrix);
    for (npy_intp i=0; i<dimensions[0]; i++)
      for (npy_intp j=0; j<dimensions[1]; j++)
	*(double*)(data + i*strides[0] + j*strides[1]) = matrix[i][j];
    res = PyArray_Return(_matrix);
  } break;
  case OPT_STRING: res = PyString_FromString(std::string(attribute).c_str()); break;
  case OPT_COMPLEX: res = PyComplex_FromDoubles(attribute.get_complex().real(), attribute.get_complex().imag()); break;
  }
  $result = SWIG_Python_AppendOutput($result, res);
}

%typemap(freearg,noblock=1) Attribute & {
}

#endif
