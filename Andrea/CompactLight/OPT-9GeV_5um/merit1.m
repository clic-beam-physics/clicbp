function M = merit1(X)

    global XLS
    
    X /= 100;
		
    XLS.X0_phase = constrain(X(1), -90, 90);
    XLS.X0_voltage = constrain(X(2), 0, 0.500);
    XLS.K_phase =  constrain(X(3), -180, 180);
    XLS.K_voltage = constrain(X(4), 0, 50e-3);
    XLS.BC1_R56 =  constrain(X(5), 0, 0.050);
    XLS.BC1_P0 = constrain(X(6), 0, 1);
    XLS.BC1_T566 =  constrain(X(7), -0.1, 0.1);

    [B2,B1,B0] = track1();
    
    Z = sort(B2.data(:,1));
    Z_ = linspace(0, 100, length(Z));
    Z_min = spline(Z_, Z, 2);
    Z_max = spline(Z_, Z, 98);
    
    sigmaz_um = Z_max - Z_min;
    energy_GeV = mean(B2.data(:,2));
    skew = skewness(B2.data(:,1));
    kurt = kurtosis(B2.data(:,1));

    M = 0.2 * abs(sigmaz_um - 115) + ...
        1000 * abs(energy_GeV - 0.3) + ...
        10 * abs(skew) + ...
        100 * abs(kurt - 1.8);

    %disp([ M sigmaz_um energy_GeV skew kurt ]);
    
end
