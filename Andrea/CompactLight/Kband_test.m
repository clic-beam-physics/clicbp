#! /opt/local/bin/octave -q --persist

addpath('../XFEL_Code');
addpath('common');

XFEL_Code;

%% Inizialization

global RF

a_div_lambda_K = 0.15; % 1.249 mm

RF = setup_RF(a_div_lambda_K);

function f = constrain(X, a, b)
  X = X / (1+abs(X));
  f = ((b-a)*X+(a+b))/2;
end

function X = iconstrain(f, a, b)
  f = (2*f-b-a)/(b-a);
  X = f / (1-abs(f));
end

Nparticles = 10000;
Z = 130 * randn(Nparticles,1); % um
P = 0.200 * (1 + 0.000001 * randn(Nparticles,1)); % GeV/c

global Beam
Beam.charge = 250; % pC
Beam.sigmaZ = std(Z); % um
Beam.mass = 0.00051099893; % [GeV/c^2]
Beam.B0 = Bunch1d(Beam.mass, Beam.charge * 6241509.343260179, [ Z P ]);  

K0 = Cavity(RF.Kstructure);
K0.set_gradient(0.0);
K0.set_phased  (0.0);

B1 = Beam.B0;
K0.track_z(B1);

scatter(B1.data(:,1), B1.data(:,2));
