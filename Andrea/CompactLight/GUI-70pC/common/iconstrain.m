function X = iconstrain(f, a, b)
  f = (2*f-b-a)/(b-a);
  X = f / (1-abs(f));
