function [B5,B4,B3] = track2()
    
    global XLS RF B2 B5
    
    XFEL_Code;
    
    [track_X1, track_BC2, track_X2] = deal(true);
    if track_X1
        X1 = CavityArray(RF.Xstructure);
        X1.set_voltage(XLS.X1_voltage);
        X1.set_phased (XLS.X1_phase);
        if !XLS.X_wakes
            X1.disable_wakefields();
        end
        
        L = Lattice();
        L.append(X1); 
        L.set_reference_momentum(mean(B2.data(:,2)));
        B3 = L.track_z(B2);
    end
    
    if track_BC2
        XLS.BC2_T566 = 0 * -3 / 2 * XLS.BC2_R56;
        BC2 = Chicane(XLS.BC2_R56, XLS.BC2_T566);
        L = Lattice();
        L.append(BC2);
        P_min = min(B3.data(:,2));
        P_max = max(B3.data(:,2));
        L.set_reference_momentum(P_min + (P_max - P_min) * XLS.BC2_P0);
        B4 = L.track_z(B3);
	D = B4.data;
	D(:,1) -= mean(D(:,1));
	B4.set_data(D);
    end
    
    if track_X2
        X2 = CavityArray(RF.Xstructure);
        X2.set_voltage(XLS.X2_voltage);
        X2.set_phased (XLS.X2_phase);
        if !XLS.X_wakes
            X2.disable_wakefields();
        end
        
        L = Lattice();
        L.append(X2);
        L.set_reference_momentum(mean(B4.data(:,2)));
        B5 = L.track_z(B4);
    end
end
