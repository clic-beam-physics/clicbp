global val0 val1 val2 val3 val4 val5 val6 val7 val8 val9 val10 val11 xband_wakes kband_wakes
global gui_to_octave

set octave_to_gui [lindex $argv 0]
set gui_to_octave [lindex $argv 1]

global init_data
set fl [open $octave_to_gui "r" ]
set init_data [read $fl]
close $fl

eval $init_data

proc reset {} {
    global val0 val1 val2 val3 val4 val5 val6 val7 val8 val9 val10 val11
    global xband_wakes kband_wakes
    global init_data
    eval $init_data
    save_all
}

proc save_all {} {
    global val0 val1 val2 val3 val4 val5 val6 val7 val8 val9 val10 val11
    global xband_wakes kband_wakes
    global gui_to_octave
    puts "Saving configuration to 'Kband_gui-saved.dat'"
    foreach file_name [list Kband_gui-saved.dat $gui_to_octave ] {
	set fl [open $file_name "w" ]
	puts $fl "XLS.X0_phase = $val0;"
	puts $fl "XLS.X0_voltage = ($val1) / 1e3;"
	puts $fl "XLS.K_phase =  $val2;"
	puts $fl "XLS.K_voltage = ($val3) / 1e3;"
	puts $fl "XLS.BC1_R56 =  $val4;"
	puts $fl "XLS.BC1_P0 = $val5;"
	puts $fl "XLS.X1_phase = $val6;"
	puts $fl "XLS.X1_voltage = $val7;"
	puts $fl "XLS.BC2_R56 =  $val8;"
	puts $fl "XLS.BC2_P0 = $val9;"
	puts $fl "XLS.X2_phase =  $val10;"
	puts $fl "XLS.X2_voltage = $val11;"
	puts $fl "XLS.X_wakes = $xband_wakes;"
	puts $fl "XLS.K_wakes = $kband_wakes;"
	close $fl
    }
}

proc save_plot {} {
    global gui_to_octave
    puts "Plotting phase space to 'plot_Kband_gui.png'"
    set fl [open $gui_to_octave "w" ]
    puts $fl "print -dpng tmp.png;"
    puts $fl "system(\"pngtopnm tmp.png | pnmcrop | pnmtopng -compression=9 > plot_Kband_gui.png && rm -f tmp.png\");"
    close $fl
}

proc quit_gui {} {
    global gui_to_octave
    set fl [open $gui_to_octave "w" ]
    puts $fl "QUIT = true;"
    close $fl
    destroy .
}

proc isnumeric { value } {
    if {![catch {expr {abs($value)}}]} {
        return 1
    }
    set value [string trimleft $value 0]
    if {![catch {expr {abs($value)}}]} {
        return 1
    }
    return 0
}

proc action0 { val } { global gui_to_octave ; set fl [open $gui_to_octave "w" ] ; puts $fl "XLS.X0_phase = $val; % deg" ; close $fl }
proc action1 { val } { global gui_to_octave ; set fl [open $gui_to_octave "w" ] ; puts $fl "XLS.X0_voltage = ($val) / 1e3; % GV" ; close $fl }
proc action2 { val } { global gui_to_octave ; set fl [open $gui_to_octave "w" ] ; puts $fl "XLS.K_phase =  $val; % deg" ; close $fl }
proc action3 { val } { global gui_to_octave ; set fl [open $gui_to_octave "w" ] ; puts $fl "XLS.K_voltage = ($val) / 1e3; % GV" ; close $fl }
proc action4 { val } { global gui_to_octave ; set fl [open $gui_to_octave "w" ] ; puts $fl "XLS.BC1_R56 =  $val; % m" ; close $fl }
proc action5 { val } { global gui_to_octave ; set fl [open $gui_to_octave "w" ] ; puts $fl "XLS.BC1_P0 = $val; % 0..1" ; close $fl }
proc action6 { val } { global gui_to_octave ; set fl [open $gui_to_octave "w" ] ; puts $fl "XLS.X1_phase = $val; % deg" ; close $fl }
proc action7 { val } { global gui_to_octave ; set fl [open $gui_to_octave "w" ] ; puts $fl "XLS.X1_voltage = $val; % GV" ; close $fl }
proc action8 { val } { global gui_to_octave ; set fl [open $gui_to_octave "w" ] ; puts $fl "XLS.BC2_R56 =  $val; % m" ; close $fl }
proc action9 { val } { global gui_to_octave ; set fl [open $gui_to_octave "w" ] ; puts $fl "XLS.BC2_P0 = $val; % 0..1" ; close $fl }
proc action10 { val } { global gui_to_octave ; set fl [open $gui_to_octave "w" ] ; puts $fl "XLS.X2_phase =  $val; % deg" ; close $fl }
proc action11 { val } { global gui_to_octave ; set fl [open $gui_to_octave "w" ] ; puts $fl "XLS.X2_voltage = $val; % GV" ; close $fl }

frame .fr
pack .fr -fill both -expand 1

label .fr.l0  -justify left -width 14 -text "X0::Phase"
label .fr.l1  -justify left -width 14 -text "X0::Voltage"
label .fr.l2  -justify left -width 14 -text "K0::Phase" 
label .fr.l3  -justify left -width 14 -text "K0::Voltage"
label .fr.l4  -justify left -width 14 -text "BC1::R56"
label .fr.l5  -justify left -width 14 -text "BC1::P0"
label .fr.l6  -justify left -width 14 -text "X1::Phase"
label .fr.l7  -justify left -width 14 -text "X1::Voltage"
label .fr.l8  -justify left -width 14 -text "BC2::R56"
label .fr.l9  -justify left -width 14 -text "BC2::P0"
label .fr.l10 -justify left -width 14 -text "X2::Phase" 
label .fr.l11 -justify left -width 14 -text "X2::Voltage"

label .fr.u0  -justify left -width 12 -text "deg"
label .fr.u1  -justify left -width 12 -text "MV"
label .fr.u2  -justify left -width 12 -text "deg" 
label .fr.u3  -justify left -width 12 -text "MV"
label .fr.u4  -justify left -width 12 -text "m"
label .fr.u5  -justify left -width 12 -text "Pmin .. Pmax"
label .fr.u6  -justify left -width 12 -text "deg"
label .fr.u7  -justify left -width 12 -text "GV"
label .fr.u8  -justify left -width 12 -text "m"
label .fr.u9  -justify left -width 12 -text "Pmin .. Pmax"
label .fr.u10 -justify left -width 12 -text "deg" 
label .fr.u11 -justify left -width 12 -text "GV"

scale .fr.s0 -orient horizontal -from  -180 -to 180   -resolution 0.01  -length 250 -variable val0 -showvalue 0 -command action0
scale .fr.s1 -orient horizontal -from     0 -to 500   -resolution 0.1   -length 250 -variable val1 -showvalue 0 -command action1
scale .fr.s2 -orient horizontal -from  -180 -to 180   -resolution 1     -length 250 -variable val2 -showvalue 0 -command action2
scale .fr.s3 -orient horizontal -from     0 -to 100   -resolution 0.1   -length 250 -variable val3 -showvalue 0 -command action3
scale .fr.s4 -orient horizontal -from -0.50 -to 0.50  -resolution 0.0001 -length 250 -variable val4 -showvalue 0 -command action4
scale .fr.s5 -orient horizontal -from     0 -to 1     -resolution 0.01  -length 250 -variable val5 -showvalue 0 -command action5
scale .fr.s6 -orient horizontal -from   -180 -to 180  -resolution 0.01  -length 250 -variable val6 -showvalue 0 -command action6
scale .fr.s7 -orient horizontal -from     0 -to 4.000 -resolution 0.001 -length 250 -variable val7 -showvalue 0 -command action7
scale .fr.s8 -orient horizontal -from -0.50 -to 0.50  -resolution 0.0001 -length 250 -variable val8 -showvalue 0 -command action8
scale .fr.s9 -orient horizontal -from     0 -to 1     -resolution 0.01  -length 250 -variable val9 -showvalue 0 -command action9
scale .fr.s10 -orient horizontal -from -180 -to 180   -resolution 1     -length 250 -variable val10 -showvalue 0 -command action10
scale .fr.s11 -orient horizontal -from    0 -to 5.000 -resolution 0.001 -length 250 -variable val11 -showvalue 0 -command action11

button .fr.b0 -text "Reset" -command "reset"
button .fr.b1 -text "Save Plot" -command "save_plot"
button .fr.b2 -text "Save"  -command "save_all"
button .fr.b3 -text "Quit"  -command "quit_gui"

for {set i 0} {$i < 12} {incr i} {
    entry .fr.v$i -width 8 -textvariable val$i -validate key -validatecommand "action$i %P ; return 1 ;"
    grid .fr.l$i -row $i -column 0
    grid .fr.s$i -row $i -column 1
    grid .fr.v$i -row $i -column 2
    grid .fr.u$i -row $i -column 3
}

proc actionX { val } { global gui_to_octave ; set fl [open $gui_to_octave "w" ] ; puts $fl "XLS.X_wakes = $val; % bool" ; close $fl }
proc actionK { val } { global gui_to_octave ; set fl [open $gui_to_octave "w" ] ; puts $fl "XLS.K_wakes = $val; % bool" ; close $fl }

checkbutton .fr.cx -text "X-band wakes" -variable xband_wakes -variable xband_wakes -command { global gui_to_octave ; set fl [open $gui_to_octave "w" ] ; puts $fl "XLS.X_wakes = $xband_wakes; % bool" ; close $fl }
checkbutton .fr.ck -text "K-band wakes" -variable kband_wakes -variable kband_wakes -command { global gui_to_octave ; set fl [open $gui_to_octave "w" ] ; puts $fl "XLS.K_wakes = $kband_wakes; % bool" ; close $fl }

grid .fr.cx -row $i -column 0
grid .fr.ck -row $i -column 1

incr i

grid .fr.b0 -row $i -column 0
grid .fr.b1 -row $i -column 1
grid .fr.b2 -row $i -column 2
grid .fr.b3 -row $i -column 3

wm title . "XLS Optimiser"
wm geometry . +300+300

