function [B2,B1,B0] = track1()
    
    global XLS RF B0 B2
    
    XFEL_Code;
    
    [track_X0, track_BC1] = deal(true);
    if track_X0
        X0 = CavityArray(RF.Xstructure);
        X0.set_voltage(XLS.X0_voltage);
        X0.set_phased (XLS.X0_phase);
        if XLS.X_wakes==0
            X0.disable_wakefields();
        end
        
        K0 = CavityArray(RF.Kstructure);
        K0.set_voltage(XLS.K_voltage);
        K0.set_phased (XLS.K_phase);
        if XLS.K_wakes==0
            K0.disable_wakefields();
        end
        
        L = Lattice();
        L.append(X0); 
        L.append(K0); 
        L.set_reference_momentum(mean(B0.data(:,2)));
        B1 = L.track_z(B0);
    end

    if track_X0 || track_BC1
        BC1 = Chicane(XLS.BC1_R56, XLS.BC1_T566);
        L = Lattice();
        L.append(BC1);
        P_min = min(B1.data(:,2));
        P_max = max(B1.data(:,2));
        L.set_reference_momentum(P_min + (P_max - P_min) * XLS.BC1_P0);
        B2 = L.track_z(B1);
	D = B2.data;
	D(:,1) -= mean(D(:,1));
	B2.set_data(D);
    end
end
