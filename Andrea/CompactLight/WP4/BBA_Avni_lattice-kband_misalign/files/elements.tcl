#here all elements are listed

proc drift {name ldir} {
  global latfile elemno 
  set ldir [format "%.16g" $ldir]
  set u  [format "$name.%04d" $elemno] 
  puts  $latfile "Drift  -name \"$u\"  -length $ldir"
  incr elemno  
}



proc cavity {name lcav lcoup phase grad wake_loss} {
  global latfile elemno e0
  set lcav [format "%.16g" $lcav]
  set lcoup [format "%.16g" $lcoup]
  set u  [format "$name.%04d" $elemno] 
  if {$lcoup>0} {drift d_$name $lcoup }
#   puts  $latfile "Cavity  -name \"$u\"  -length $lcav  -gradient \[expr $$name\_grad\]  -phase \[expr $$name\_phase\]  "
  puts  $latfile "Cavity  -name \"$u\"  -length $lcav  -gradient $grad  -phase $phase "
  if {$lcoup>0} {drift d_$name $lcoup }
  set e0 [expr $e0+cos(acos(-1.0)*$phase/180.)*$grad*$lcav-$wake_loss] 
  puts $latfile "SetReferenceEnergy  $e0"
  incr elemno  
}

proc quad {name lquad kquad} {
  global latfile elemno e0
  set lquad [format "%.16g" $lquad]
  set kquad [format "%.16g" [expr $kquad*$lquad]]
  set u  [format "$name.%04d" $elemno] 
  puts $latfile "Quadrupole  -name \"$u\"  -length $lquad  -strength [expr $kquad*$e0] "
  incr elemno  
}

proc bpm {name lbpm} {
  global latfile elemno e0
  set lbpm [format "%.16g" $lbpm]
  set u  [format "BPM_$name.%04d" $elemno] 
  puts $latfile "Bpm  -name \"$u\"  -length $lbpm"
  incr elemno  
}


proc dir_bpm {name ldir } {
  global latfile elemno e0
  set lbpm 0.0
  set ldir [format "%.16g" [expr ($ldir-$lbpm)*0.5]]
  set u  [format "$name.%04d" $elemno] 
  drift $name $ldir
  bpm $name $lbpm
  drift $name $ldir
  incr elemno  
}

proc quad_bpm {name lquad kquad} {
  global latfile elemno e0
  set lquad [format "%.16g" [expr $lquad*0.5]]
  set u  [format "$name.%04d" $elemno] 
  quad $name $lquad $kquad
  bpm $name 0.0
  quad $name $lquad $kquad
  incr elemno  
}


proc corr {name lcor} {
  global latfile elemno e0
  set lcor [format "%.16g" [expr $lcor]]
  set u  [format "CorXY_$name.%04d" $elemno] 
  puts $latfile "Dipole  -name \"$u\"  -length $lcor"
  incr elemno  
}

proc vcorr {name lcor} {
  global latfile elemno e0
  set lcor [format "%.16g" [expr $lcor]]
  set u  [format "CorY_$name.%04d" $elemno] 
  puts $latfile "Dipole  -name \"$u\"  -length $lcor"
  incr elemno  
}


proc dir_corr {name ldir } {
  global latfile elemno e0
  set lcor [format "%.16g" [expr 0.0]]
  set ldir [format "%.16g" [expr ($ldir-$lcor)/2.]]
  set u  [format "$name.%04d" $elemno] 
  drift $name $ldir
  corr $name $lcor
#   drift $name $ldir
#   vcorr $name $lcor
  drift $name $ldir

  incr elemno  
}

# proc dir_vcorr {name ldir } {
#   global latfile elemno e0
#   set lcor [format "%.16g" 0.0]
#   set ldir [format "%.16g" [expr ($ldir-$lcor)*0.5]]
#   set u  [format "$name.%04d" $elemno] 
#   drift $name $ldir
#   vcorr $name $lcor
#   drift $name $ldir
#   incr elemno  
# }

# proc dir_hvcorr {name ldir } {
#   global latfile elemno e0
#   set lcor [format "%.16g" 0.0]
#   set ldir [format "%.16g" [expr ($ldir-2.*$lcor)/3.0]]
#   set u  [format "$name.%04d" $elemno] 
#   drift $name $ldir
#   hcorr $name $lcor
#   drift $name $ldir
#   vcorr $name $lcor
#   drift $name $ldir
#   incr elemno  
# }

proc bend {name lbend angle e1 e2 srloss } {
  global latfile elemno e0
  set lbend [format "%.16g" $lbend ]
  set angle [format "%.16g" $angle ]
  set e1 [format "%.16g" $e1 ]
  set e2 [format "%.16g" $e2 ]
  set u  [format "$name.%04d" $elemno] 
  
  puts  $latfile "Sbend  -name \"$u\"  -length $lbend  -angle $angle  -E1 $e1  -E2 $e2  -e0 $e0  "
  set e0 [expr $e0-$srloss] 
  puts $latfile "SetReferenceEnergy  $e0"
  incr elemno  
}



