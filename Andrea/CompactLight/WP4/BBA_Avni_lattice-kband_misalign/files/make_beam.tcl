# source tools.tcl
# source random_dist_generate.tcl
# source wake_init.tcl

# create a file containing a particle distibution
proc create_gauss_distribution {outfile bparray} {
    upvar $bparray beamparams
    
    set bx $beamparams(beta_x)
    set ax $beamparams(alpha_x)
    set ex $beamparams(emitt_x)
    set by $beamparams(beta_y)
    set ay $beamparams(alpha_y)
    set ey $beamparams(emitt_y)
    set sz $beamparams(sigma_z)
	set e0 $beamparams(energy)
	set de $beamparams(e_spread)

	if {[info exist beamparams(sigma_cut)]<1} {
	   set s_cut [expr 3.333]
	} else {
	   set s_cut $beamparams(sigma_cut)
	}
   
    set npart [expr $beamparams(n_slice)*$beamparams(n_macro)]

    Octave {
	generatepart("$outfile",$npart,$e0,$de,$ex,$bx,$ax,$ey,$by,$ay,$sz,$s_cut);
    }
}

proc create_sample_distribution {z_list outfile bparray} {
#  (outfile,A,npart,e0,de,ex,bx,ax,ey,by,ay,sz,sigma_cut)
    upvar $bparray beamparams
    
    set AA "{$z_list}"
    set bx $beamparams(beta_x)
    set ax $beamparams(alpha_x)
    set ex $beamparams(emitt_x)
    set by $beamparams(beta_y)
    set ay $beamparams(alpha_y)
    set ey $beamparams(emitt_y)
    set sz $beamparams(sigma_z)
    set e0 $beamparams(energy)
    set de $beamparams(e_spread)
    if {[info exist beamparams(sigma_cut)]<1} {
	   set s_cut [expr 3.333]
	} else {
	   set s_cut $beamparams(sigma_cut)
	}
   
    set npart [expr $beamparams(n_slice)*$beamparams(n_macro)]
    Octave {
	generatepart_zlist("$outfile",$AA,$npart,$e0,$de,$ex,$bx,$ax,$ey,$by,$ay,$sz,$s_cut);
    }
}


proc make_particle_beam {arrayname} {
    global lambda 
    set filename  particles.in
    upvar $arrayname match
    set ch {1.0}
    #particles are generated here 
    create_gauss_distribution $filename match
    set z_list2 [smoothdata [beamhistogram file=$filename nslice=$match(n_slice)]]
    set npart [expr $match(n_slice)*$match(n_macro)]
    calc_beamdat beam.dat $match(charge) $z_list2
    InjectorBeam $match(name) -bunches 1 \
		-macroparticles $match(n_macro) \
		-slices $match(n_slice) \
		-particles $npart \
		-energyspread [expr 0.01*$match(e_spread)*$match(energy)] \
		-ecut 3.0 \
		-e0 $match(energy) \
		-file beam.dat \
		-chargelist $ch \
		-charge $match(charge) \
		-phase 0.0 \
		-last_wgt 1.0 \
		-alpha_y $match(alpha_y) \
		-beta_y $match(beta_y) \
		-emitt_y $match(emitt_y) \
		-alpha_x $match(alpha_x) \
		-beta_x $match(beta_x) \
		-emitt_x $match(emitt_x)


    if {[array names match -exact gradient] == {} } {
	    set beam_grad  1.0 
    } else {
	    set beam_grad $match(gradient)
    }
    set l {}
    lappend l "$beam_grad 0.0 0.0"
    SetRfGradientSingle $match(name) 0 $l

    BeamRead -file $filename -beam $match(name)

}


proc make_particle_beam_type { arrayname type} {
    global lambda 
    set filename  particles.in
    upvar $arrayname match
    set ch {1.0}
    if {$type == "gaussian" } {
	set z_list [gaussList nslice=$match(n_slice) mu=0 sigma=$match(sigma_z) sigma_cut=$match(sigma_cut) ] 
    } elseif {$type == "plateau" } {
	set z_list [plateauList nslice=$match(n_slice) mu=0 sigma=$match(sigma_z) sigma_cut=$match(sigma_cut) ] 
    } elseif {$type == "parabola" } {
	set z_list [parabolaList nslice=$match(n_slice) mu=0 sigma=$match(sigma_z) sigma_cut=$match(sigma_cut) ] 
    } elseif {$type == "uniform" } {
	set z_list [uniformList nslice=$match(n_slice) mu=0 sigma=$match(sigma_z) sigma_cut=$match(sigma_cut) ]
    } else {
	puts " you have not defined known longitudinal distribution type gaussian plateau parabola uniform"
	puts " gaussian type is being created"
	set z_list [gaussList nslice=$match(n_slice) mu=0 sigma=$match(sigma_z) sigma_cut=$match(sigma_cut)] 
    }
    #particles are generated here 
    create_sample_distribution $z_list $filename match
    set z_list2 [smoothdata [beamhistogram file=$filename nslice=$match(n_slice)]]
 
    calc_beamdat beam.dat $match(charge) $z_list2
    after 1000
    InjectorBeam $match(name) -bunches 1 \
		-macroparticles $match(n_macro) \
		-slices $match(n_slice) \
		-particles [expr $match(n_slice)*$match(n_macro)] \
		-energyspread [expr 0.01*$match(e_spread)*$match(energy)] \
		-ecut 3.0 \
		-e0 $match(energy) \
		-file beam.dat \
		-chargelist $ch \
		-charge $match(charge) \
		-phase 0.0 \
		-last_wgt 1.0 \
		-alpha_y $match(alpha_y) \
		-beta_y $match(beta_y) \
		-emitt_y $match(emitt_y) \
		-alpha_x $match(alpha_x) \
		-beta_x $match(beta_x) \
		-emitt_x $match(emitt_x)


    if {[array names match -exact gradient] == {} } {
	    set beam_grad  1.0 
    } else {
	    set beam_grad $match(gradient)
    }
    set l {}
    lappend l "$beam_grad 0.0 0.0"
    SetRfGradientSingle $match(name) 0 $l

    BeamRead -file $filename -beam $match(name)

}





# it reads longitudinal distribution of the file which it reads for calc routine
proc make_beam_particle_read {arrayname indist} {
    global lambda
    set filename  $indist
    upvar $arrayname match
    set ch {1.0}
    set z_list [smoothdata [beamhistogram file=$filename nslice=$match(n_slice)]]
    calc_beamdat beam.dat $match(charge) $z_list
    
    
    InjectorBeam $match(name) -bunches 1 \
	    -macroparticles $match(n_macro) \
	    -slices $match(n_slice) \
	    -particles [expr $match(n_slice)*$match(n_macro)] \
	    -energyspread [expr 0.01*$match(e_spread)*$match(energy)] \
	    -ecut 3.0 \
	    -e0 $match(energy) \
	    -file beam.dat \
	    -chargelist $ch \
	    -charge $match(charge) \
	    -phase 0.0 \
	    -last_wgt 1.0 \
	    -overlapp [expr -576*2*0.3/1.0] \
	    -distance [expr 2.*$lambda] \
	    -alpha_y $match(alpha_y) \
	    -beta_y $match(beta_y) \
	    -emitt_y $match(emitt_y) \
	    -alpha_x $match(alpha_x) \
	    -beta_x $match(beta_x) \
	    -emitt_x $match(emitt_x)


    if {[array names match -exact gradient] == {} } {
	    set beam_grad  1.0 
    } else {
	    set beam_grad $match(gradient)
    }
    set l {}
    lappend l "$beam_grad 0.0 0.0"
    SetRfGradientSingle $match(name) 0 $l

    BeamRead -file $filename -beam $match(name)

}



proc make_slice_beam {arrayname} {
    global lambda
    upvar $arrayname match
    set ch {1.0}
    set z_list [gaussList nslice=$match(n_slice) mu=0 sigma=$match(sigma_z) ] 
    calc_beamdat beam.dat $match(charge) $z_list
    
    InjectorBeam $match(name) -bunches 1 \
	    -macroparticles $match(n_macro) \
	    -slices $match(n_slice) \
	    -energyspread [expr 0.01*$match(e_spread)*$match(energy)] \
 	    -particles [expr $match(n_macro)* $match(n_slice)] \
	    -ecut 3.0 \
	    -e0 $match(energy) \
	    -file beam.dat \
	    -chargelist $ch \
	    -charge $match(charge) \
	    -phase 0.0 \
	    -last_wgt 1.0 \
	    -overlapp [expr 100.0*$lambda] \
	    -distance [expr 2*$lambda] \
	    -alpha_y $match(alpha_y) \
	    -beta_y $match(beta_y) \
	    -emitt_y $match(emitt_y) \
	    -alpha_x $match(alpha_x) \
	    -beta_x $match(beta_x) \
	    -emitt_x $match(emitt_x)

    if {[array names match -exact gradient] == {} } {
	    set beam_grad  1.0
    } else {
	    set beam_grad $match(gradient)
    }
    set l {}
    lappend l "$beam_grad 0.0 0.0"
    SetRfGradientSingle $match(name) 0 $l


}


proc make_slice_beam_type { arrayname type} {
    global lambda 
    set filename  particles.in
    upvar $arrayname match
    set ch {1.0}
    if {$type == "gaussian" } {
	set z_list [gaussList nslice=$match(n_slice) mu=0 sigma=$match(sigma_z) ] 
    } elseif {$type == "plateau" } {
	set z_list [plateauList nslice=$match(n_slice) mu=0 sigma=$match(sigma_z)] 
    } elseif {$type == "parabola" } {
	set z_list [parabolaList nslice=$match(n_slice) mu=0 sigma=$match(sigma_z) ] 
    } elseif {$type == "uniform" } {
	set z_list [uniformList nslice=$match(n_slice) mu=0 sigma=$match(sigma_z) ]
    } else {
	puts " you have not defined known longitudinal distribution type gaussian plateau parabola uniform"
	puts " gaussian type is being created"
	set z_list [gaussList nslice=$match(n_slice) mu=0 sigma=$match(sigma_z)] 
    }
    #particles are generated here 
    
    create_sliced_dist $z_list $filename match
    calc_beamdat beam.dat $match(charge) $z_list

    InjectorBeam $match(name) -bunches 1 \
		-macroparticles $match(n_macro) \
		-slices $match(n_slice) \
		-particles [expr $match(n_slice)*$match(n_macro)] \
		-energyspread [expr 0.01*$match(e_spread)*$match(energy)] \
		-ecut 3.0 \
		-e0 $match(energy) \
		-file beam.dat \
		-chargelist $ch \
		-charge $match(charge) \
		-phase 0.0 \
		-last_wgt 1.0 \
		-alpha_y $match(alpha_y) \
		-beta_y $match(beta_y) \
		-emitt_y $match(emitt_y) \
		-alpha_x $match(alpha_x) \
		-beta_x $match(beta_x) \
		-emitt_x $match(emitt_x)


    if {[array names match -exact gradient] == {} } {
	    set beam_grad  1.0 
    } else {
	    set beam_grad $match(gradient)
    }
    set l {}
    lappend l "$beam_grad 0.0 0.0"
    SetRfGradientSingle $match(name) 0 $l

    BeamLoadAll -file $filename -beam $match(name)

}





