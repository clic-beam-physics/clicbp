## constants
set clight 299792458.0
set Z0 376.730313461771
set pi [expr acos(-1.)]
proc rad {ang} { return [expr $ang*acos(-1.)/180.] }
set eC 1.6021766e-19
set elemno 1

# #  some file names
set  script_dir "../files"
set filename xfel


set injS_grad  [expr 18.00*1e-3]
set injS_phase   32.20

set injX_grad  [expr 33.333*1e-3]
set injX_phase  218.50

set lin1X_grad [expr 70.*1e-3]
set lin1X_phase  22.0

set lin2X_grad [expr 70.*1e-3]
set lin2X_phase  -4

# R56= 0.0454977

set bc1_angle   5.25
set bc1_d_side	4.5
set bc2_angle   2.31
set bc2_d_side	3.21

array set args {
    sigma  0.0
    sigmap 0.0
    bpmres 5.0
    dphase1 30
    dphase2 0
    dcharge 0.8
    machine all
    nm 1
    wgt1 -1
    wgt2 -1
    beta0 5
    beta1 6
    beta2 6
    nbins 1
    noverlap 0.0
}   

array set args $argv

set nm $args(nm)
set sigma $args(sigma)
set sigmap $args(sigmap)
set bpmres $args(bpmres)
set machine $args(machine)
set dphase1 $args(dphase1)
set dphase2 $args(dphase2)
set dcharge $args(dcharge)
set wgt1 $args(wgt1)
set wgt2 $args(wgt2)
set beta0 $args(beta0)
set beta1 $args(beta1)
set beta2 $args(beta2)
set nbins $args(nbins)
set noverlap $args(noverlap)


set outfiles outfiles

if {[file exist $outfiles]} {
    exec rm -rf $outfiles
    exec mkdir -p $outfiles
} else {
    exec mkdir -p $outfiles
}

cd $outfiles

source $script_dir/tools.tcl
Octave {
    scriptdir="$script_dir/";
    source([scriptdir,"tools.m"]);
    source([scriptdir,"octave_beam_statistics.m"]);
}
source $script_dir/wake_init.tcl
source $script_dir/make_beam.tcl
source $script_dir/match.tcl
source $script_dir/elements.tcl

