
source defaults.tcl

array set BeamDefine [bunch_parameters $script_dir/astra.out]
array set BeamDefine "name beam0 charge  1.5603773e+09"
array set BeamDefine "n_slice 201 n_macro 501  n_bunch 1 sigma_cut 3.333  "

set beamlinename "xfel"
# puts [array get BeamDefine]

set e0  $BeamDefine(energy)
set gm0 [expr $e0/$mc2]

source $script_dir/lattice.tcl

### beamline
BeamlineNew
Girder
Drift -length 0.0
source lattice.lat
puts "beamline energy $e0"
BeamlineSet -name $beamlinename
BeamlineUse -name $beamlinename

set BeamDefine(name)  beam1
set BeamDefine(charge) [expr $BeamDefine(charge) * $dcharge ]
make_beam_particle_read BeamDefine $script_dir/astra.out
FirstOrder 1

set BeamDefine(name)  beam0
set BeamDefine(charge) [expr $BeamDefine(charge) / $dcharge ]
make_beam_particle_read BeamDefine $script_dir/astra.out
FirstOrder 1




if { $wgt1 == -1 } {
    set wgt1 [expr round(sqrt(($bpmres * $bpmres + $sigma * $sigma) / (2 * $bpmres * $bpmres)))]
    puts "Using DFS theoretical weight = $wgt1"
}

if { $wgt2 == -1 } {
    set wgt2 [expr round(sqrt(($bpmres * $bpmres + $sigma * $sigma) / (2 * $bpmres * $bpmres)))]
    puts "Using WFS theoretical weight = $wgt2"
}

Octave {
# #  We want tracking in 6d because there is bunch compression
   QI = placet_get_number_list("$beamlinename", "quadrupole");
   placet_element_set_attribute("$beamlinename", QI, "six_dim", true);

   SI = placet_get_number_list("$beamlinename", "sbend");
   placet_element_set_attribute("$beamlinename", SI, "six_dim", true);
   
   CA = placet_get_number_list("$beamlinename", "cavity");
   placet_element_set_attribute("$beamlinename", CA, "six_dim", true);
   
   S_CAVs = placet_get_name_number_list("$beamlinename", "injS*"); 
   X_CAVs = placet_get_name_number_list("$beamlinename", "injX*");
   X_CAVs = [X_CAVs, placet_get_name_number_list("$beamlinename", "lin1X*")];
   X_CAVs = [X_CAVs, placet_get_name_number_list("$beamlinename", "lin2X*")];
   placet_element_set_attribute("$beamlinename", S_CAVs, "short_range_wake", "S_band_SR_W");
   placet_element_set_attribute("$beamlinename", S_CAVs, "lambda", $lambdas);
   placet_element_set_attribute("$beamlinename", X_CAVs, "short_range_wake", "X_band_SR_W");
   placet_element_set_attribute("$beamlinename", X_CAVs, "lambda", $lambdax);

   # puts a K-band wake, just for fun
   placet_element_set_attribute("$beamlinename", X_CAVs(1), "short_range_wake", "K_band_SR_W");


   
   function reduce_energy()
      injS21 =placet_get_name_number_list("$beamlinename", "injS.0021");
      Lin1X1 =placet_get_name_number_list("$beamlinename", "lin1X.M0.0.*");
      Lin1X2 =placet_get_name_number_list("$beamlinename", "lin1X.M0.1.*");
      %placet_element_vary_attribute("$beamlinename", injS21, "phase", +$dphase1);
      placet_element_vary_attribute("$beamlinename", Lin1X1, "phase", +$dphase1);
      placet_element_vary_attribute("$beamlinename", Lin1X2, "phase", +$dphase2);
    end

    function reset_energy()
      injS21 =placet_get_name_number_list("$beamlinename", "injS.0021");
      Lin1X1 =placet_get_name_number_list("$beamlinename", "lin1X.M0.0.*");
      Lin1X2 =placet_get_name_number_list("$beamlinename", "lin1X.M0.1.*");
      %placet_element_vary_attribute("$beamlinename", injS21, "phase", -$dphase1);
      placet_element_vary_attribute("$beamlinename", Lin1X1, "phase", -$dphase1);
      placet_element_vary_attribute("$beamlinename", Lin1X2, "phase", -$dphase2);
    end
 }

Octave {
    if exist('../R_${beamlinename}_p_${dphase1}_${dphase2}_c_${dcharge}.dat', "file")
    load '../R_${beamlinename}_p_${dphase1}_${dphase2}_c_${dcharge}.dat';
    else
    Response.Cx = placet_get_name_number_list("$beamlinename", "CorXY_*");
    Response.Cy = placet_get_name_number_list("$beamlinename", "CorXY_*");
    Response.Bpms = placet_get_number_list("$beamlinename", "bpm");
    
    # picks all correctors preceeding the last bpm, and all bpms following the first corrector
    Response.Cx = Response.Cx(Response.Cx < Response.Bpms(end));
    Response.Cy = Response.Cy(Response.Cy < Response.Bpms(end));
    Response.Bpms = Response.Bpms(Response.Bpms > max(Response.Cx(1), Response.Cy(1)));

    
     # gets the energy at each corrector
    Response.Ex = placet_element_get_attribute("$beamlinename", Response.Cx, "e0");
    Response.Ey = placet_element_get_attribute("$beamlinename", Response.Cy, "e0");
    
    # compute response matrices
    placet_test_no_correction("$beamlinename", "beam0", "Zero");
    Response.B0 = placet_get_bpm_readings("$beamlinename", Response.Bpms);
    Response.R0x = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "x", Response.Cx, "strength_x", "Zero");
    Response.R0y = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "y", Response.Cy, "strength_y", "Zero");
    
    reduce_energy();
    placet_test_no_correction("$beamlinename", "beam0", "Zero");
    Response.B1 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - Response.B0;
    Response.R1x = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "x", Response.Cx, "strength_x", "Zero") - Response.R0x;
    Response.R1y = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "y", Response.Cy, "strength_y", "Zero") - Response.R0y;
    reset_energy();
    placet_test_no_correction("$beamlinename", "beam1", "Zero");
    Response.B2 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - Response.B0;
    Response.R2x = placet_get_response_matrix_attribute ("$beamlinename", "beam1", Response.Bpms, "x", Response.Cx, "strength_x", "Zero") - Response.R0x;
    Response.R2y = placet_get_response_matrix_attribute ("$beamlinename", "beam1", Response.Bpms, "y", Response.Cy, "strength_y", "Zero") - Response.R0y;
    save -text '../R_${beamlinename}_p_${dphase1}_${dphase2}_c_${dcharge}.dat' Response;
    end
}


proc my_survey {} {
  global machine sigma sigmap bpmres beamlinename nm
  Octave {
    randn("seed", $nm * 12345);
      
    BPMs = placet_get_number_list("$beamlinename", "bpm");
    placet_element_set_attribute("$beamlinename", BPMs, "resolution", $bpmres);
    placet_element_set_attribute("$beamlinename", BPMs, "x", randn(size(BPMs)) * $sigma);
    placet_element_set_attribute("$beamlinename", BPMs, "y", randn(size(BPMs)) * $sigma);	
    
    CORs = placet_get_number_list("$beamlinename", "dipole");
    placet_element_set_attribute("$beamlinename", CORs, "strength_x", 0.0);
    placet_element_set_attribute("$beamlinename", CORs, "strength_y", 0.0);
    
    QUAs = placet_get_number_list("$beamlinename", "quadrupole");
    placet_element_set_attribute("$beamlinename", QUAs, "x", randn(size(QUAs)) * $sigma);
    placet_element_set_attribute("$beamlinename", QUAs, "y", randn(size(QUAs)) * $sigma);
    
# 	individual s band structures on injector not located on a a girder
    INJ_S = placet_get_name_number_list("$beamlinename", "injS*"); 
    placet_element_set_attribute("$beamlinename", INJ_S, "x", randn(size(INJ_S)) * $sigma);
    placet_element_set_attribute("$beamlinename", INJ_S, "y", randn(size(INJ_S)) * $sigma);
    placet_element_set_attribute("$beamlinename", INJ_S, "xp", randn(size(INJ_S)) * $sigma);
    placet_element_set_attribute("$beamlinename", INJ_S, "yp", randn(size(INJ_S)) * $sigma);
    
# 	individual X band structure for linerazer not located on a a girder
    INJ_X = placet_get_name_number_list("$beamlinename", "injX*");
    placet_element_set_attribute("$beamlinename", INJ_X, "x", randn(size(INJ_X)) * $sigma);
    placet_element_set_attribute("$beamlinename", INJ_X, "y", randn(size(INJ_X)) * $sigma);
    placet_element_set_attribute("$beamlinename", INJ_X, "xp", randn(size(INJ_X)) * $sigma);
    placet_element_set_attribute("$beamlinename", INJ_X, "yp", randn(size(INJ_X)) * $sigma);	
    
#  	X band structures on Linac 1.. located on a girder 
#       4 structures are located on a girder spaced by quads and correctors are before module..
# 	module is misaligned by DX on the center of module and offset is added
# 	DX(i)= DX+dS(i)*DXP ..  where dS is distance from center 
      # 	if angle is too large error becomes too later too.. reduced by 50%
      for MN=0:4 
      for MINDX=0:1
	LIN1_X = placet_get_name_number_list("$beamlinename", sprintf("lin1X.M%d.%d.*",MN, MINDX));
	if length(LIN1_X) > 0
	  DX = randn() * $sigma;
	  DY = randn() * $sigma;
	  DXP = randn() * $sigma*0.5;
	  DYP = randn() * $sigma*0.5;
	  sAll = placet_element_get_attribute("$beamlinename", LIN1_X, "s");
	  sMin = sAll(1);
	  sMax = sAll(end);
	  sLength = sMax - sMin;
	  placet_element_set_attribute("$beamlinename", LIN1_X, "xp", DXP + randn(size(DXP)) * $sigmap);
	  placet_element_set_attribute("$beamlinename", LIN1_X, "yp", DYP + randn(size(DYP)) * $sigmap);
	  placet_element_set_attribute("$beamlinename", LIN1_X, "x", DX .+ DXP .* ((sAll - sMin) - sLength * 0.5));
	  placet_element_set_attribute("$beamlinename", LIN1_X, "y", DY .+ DYP .* ((sAll - sMin) - sLength * 0.5));
	end
      end
    end

    

#  	X band structures on Linac 2.. located on a girder 
#       4 structures are located on a girder spaced by quads and correctors are before module..
# 	module is misaligned by DX on the center of module and offset is added
# 	DX(i)= DX+dS(i)*DXP ..  where dS is distance from center 
# 	if angle is too large error becomes too later too.. reduced by 50%
    for MN=0:9 
      for MINDX=0:1
	LIN2_X = placet_get_name_number_list("$beamlinename", sprintf("lin2X.M%d.%d.*",MN, MINDX));
	if length(LIN2_X) > 0
	  DX = randn() * $sigma;
	  DY = randn() * $sigma;
	  DXP = randn() * $sigma*0.5;
	  DYP = randn() * $sigma*0.5;
	  sAll = placet_element_get_attribute("$beamlinename", LIN2_X, "s");
	  sMin = sAll(1);
	  sMax = sAll(end);
	  sLength = sMax - sMin;
      	  placet_element_set_attribute("$beamlinename", LIN2_X, "xp", DXP + randn(size(DXP)) * $sigmap);
	  placet_element_set_attribute("$beamlinename", LIN2_X, "yp", DYP + randn(size(DYP)) * $sigmap);
	  placet_element_set_attribute("$beamlinename", LIN2_X, "x", DX .+ DXP .* ((sAll - sMin) - sLength * 0.5));
	  placet_element_set_attribute("$beamlinename", LIN2_X, "y", DY .+ DYP .* ((sAll - sMin) - sLength * 0.5));
	end
      end
    end
  }
}

if { $machine == "all" } {
    set machine_start 1
    set machine_end $nm
} {
    set machine_start $machine
    set machine_end $machine
}

Octave {
    [E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "Zero", "%s %E %dE %ex %ey %sz");
}


Octave {

    B0 = placet_get_beam("$BeamDefine(name)");
    stdX = std(B0(:,2));
    stdY = std(B0(:,3));
    stdXP = std(B0(:,5));
    stdYP = std(B0(:,6));

    Tx = [];
    Ty = [];
    Txp = [];
    Typ = [];

    for X = linspace(-1, 1, 15)
      B = B0;
      B(:,2) += stdX * X;
      placet_set_beam("$BeamDefine(name)", B);
      disp("TRACKING...");
      [E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None", "%s %E %dE %ex %ey %sz");
      Tx = [ Tx ; X median(B(:,2)) std(B(:,2)) E(end,4) ];
    endfor
    save -text table_Kband_x.dat Tx

    for XP = linspace(-1, 1, 15)
      B = B0;
      B(:,5) += stdXP * XP;
      placet_set_beam("$BeamDefine(name)", B);
      disp("TRACKING...");
      [E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None", "%s %E %dE %ex %ey %sz");
      Txp = [ Txp ; XP median(B(:,2)) std(B(:,2)) E(end,4) ];
    end
    save -text table_Kband_xp.dat Txp

    for Y = linspace(-1, 1, 15)
      B = B0;
      B(:,3) += stdY * Y;
      placet_set_beam("$BeamDefine(name)", B);
      disp("TRACKING...");
      [E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None", "%s %E %dE %ex %ey %sz");
      Ty = [ Ty ; Y median(B(:,3)) std(B(:,3)) E(end,5) ];
    end
    save -text table_Kband_y.dat Ty

    for YP = linspace(-1, 1, 15)
      B = B0;
      B(:,6) += stdYP * YP;
      placet_set_beam("$BeamDefine(name)", B);
      disp("TRACKING...");
      [E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None", "%s %E %dE %ex %ey %sz");
      Typ = [ Typ ; YP median(B(:,3)) std(B(:,3)) E(end,5) ];
    end
    save -text table_Kband_yp.dat Typ
}
