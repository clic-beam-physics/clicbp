1;
% file to generate distributions
% 

function x=randpdf(A,dim)
% RANDPDF
% Random numbers from a user defined distribution
%
% SYNTAX:
%   x = randpdf(z, p, dim)
%       randpdf(z, p, dim)
% 
% INPUT:
%   p   - probability density,
%   z  - values for probability density,
%   dim - dimension for the output matrix.
% OUTPUT:
%   x   - random numbers. Run function without output for some plots.
%   one can include math for type of dists here..
%  we use file from outside...
  for i=1:length(A)
	  z(1,i)=A{i}{1};
	  p(1,i)=A{i}{2};
  end

  dim=[dim,1];
  
% vectorization and normalization of the input pdf
  z=z(:);
  p=p(:);
  integral=trapz(z,p);
  p=p./integral;

% interpolation of the input pdf for better integration
% in my opinion 10000 point is sufficient...
  zi=[linspace(min(z),max(z),10000)]';
  pi=interp1(z,p,zi,'linear');

  % computing the cumulative distribution function for input pdf
  cdfp = cumtrapz(zi,pi);

  % finding the parts of cdf parallel to the X axis 
  ind=[true; not(diff(cdfp)==0)];

  % and cut out the parts
  cdfp=cdfp(ind);
  pi=pi(ind);
  zi=zi(ind);

  % generating the uniform distributed random numbers
  uniformDistNum=rand(dim);

  % and distributing the numbers using cdf from input pdf
  userDistNum=interp1(cdfp,zi,uniformDistNum(:)','linear');

  x=reshape(userDistNum,dim);
end


function generate=generatepart(outfile,npart,e0,de,ex,bx,ax,ey,by,ay,sz,sigma_cut)
  ex=ex*1e-7*0.511e-3/e0;
  ey=ey*1e-7*0.511e-3/e0;
  sx=sqrt(ex*bx)*1e6;
  spx=sqrt(ex/bx)*1e6;
  sy=sqrt(ey*by)*1e6;
  spy=sqrt(ey/by)*1e6;
  se=0.01*abs(de);
  sz=sz;
  xxpc=-ax/bx;
  yypc=-ay/by;

  nc=6;
  %  creating all distributions
  allcol=randn(npart,nc);

%    %  %  finding greater elements than sigma
%    for j=1:nc
%  	k1=find(allcol(:,j) > sigma_cut);
%  	k2=find(allcol(:,j) < -sigma_cut);
%  	ki=[k1;k2];
%  	for k=ki 
%  	   u=randn(1);
%  	   while (abs(u) > sigma_cut)
%  		u=randn(1);
%  	   end;
%  	   allcol(k,j)=u;
%  	end;
%    end;

  %  %  and eliminate any offset and correlation
  for i=1:nc
	allcol(:,i)=allcol(:,i)-mean(allcol(:,i),1);
  end;
  
  nslice=20;
  dn=floor(length(allcol(:,4))/nslice);
  rest=length(allcol(:,4))-dn*nslice;
  
  for i=1:nc
      for j=1:nslice
	allcol((j-1)*dn+1:j*dn,i)=allcol((j-1)*dn+1:j*dn,i)-mean(allcol((j-1)*dn+1:j*dn,i),1);
      end;
	allcol(j*dn+1:j*dn+rest,i)=allcol(j*dn+1:j*dn+rest,i)-mean(allcol(j*dn+1:j*dn+rest,i),1);
  end;
  
  for i=1:nc-1
	for j=i+1:nc
	  a=allcol(:,i);
	  b=allcol(:,j);
	  c=ols(b,a);
	  b=b-c*a;
	  allcol(:,j)=b;
	end;
  end;

  for i=1:nc
	allcol(:,i)=allcol(:,i)/std(allcol(:,i),1);
  end;

  x =sx*allcol(:,1);
  xp=spx*allcol(:,2)-ax*x/sx*spx;
  y =sy*allcol(:,3);
  yp=spy*allcol(:,4)-ay*y/sy*spy;
  z=sz*allcol(:,6);
  e=(1.0+allcol(:,5)*se)*e0;

  [zs,id]=sort(z);
  outdata=[e(id),x(id),y(id),z(id),xp(id),yp(id)];

  fid=fopen(outfile,"wt");
  fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",outdata');
  fclose(fid);

endfunction


function generate=generatepart_zlist(outfile,A,npart,e0,de,ex,bx,ax,ey,by,ay,sz,sigma_cut)
  gm=e0/0.00051099893;
  ex=ex/gm*1e-7;
  ey=ey/gm*1e-7;
  sx=sqrt(ex*bx)*1e6;
  spx=sqrt(ex/bx)*1e6;
  sy=sqrt(ey*by)*1e6;
  spy=sqrt(ey/by)*1e6;
  se=0.01*abs(de);
  xxpc=-ax/bx;
  yypc=-ay/by;

  %  creating longitudinal distribution
  zlist=randpdf(A,npart);
  zlist=sort(zlist);
  
  npart=length(zlist);
  disp(npart)
  
  %  creating other dimensions distributions
  nc=5;
  allcol=randn(npart,nc);

%    %  %  finding greater elements than sigma
%    for j=1:nc
%      k1=find(allcol(:,j) > sigma_cut);
%      k2=find(allcol(:,j) < -sigma_cut);
%      ki=[k1;k2];
%      for k=ki 
%        u=randn(1);
%        while (abs(u) > sigma_cut)
%  	u=randn(1);
%        end;
%        allcol(k,j)=u;
%      end;
%    end;

  %  %  and eliminate any offset and correlation
  for i=1:nc
	allcol(:,i)=allcol(:,i)-mean(allcol(:,i),1);
  end;
  
  nslice=20;
  dn=floor(length(allcol(:,4))/nslice);
  rest=length(allcol(:,4))-dn*nslice;
  
  for i=1:nc
      for j=1:nslice
	allcol((j-1)*dn+1:j*dn,i)=allcol((j-1)*dn+1:j*dn,i)-mean(allcol((j-1)*dn+1:j*dn,i),1);
      end;
	allcol(j*dn+1:j*dn+rest,i)=allcol(j*dn+1:j*dn+rest,i)-mean(allcol(j*dn+1:j*dn+rest,i),1);
  end;
  

  for i=1:nc-1
	for j=i+1:nc
	  a=allcol(:,i);
	  b=allcol(:,j);
	  c=ols(b,a);
	  b=b-c*a;
	  allcol(:,j)=b;
	end;
  end;

  for i=1:nc
	allcol(:,i)=allcol(:,i)/std(allcol(:,i),1);
  end;

  x =sx*allcol(:,1);
  xp=spx*allcol(:,2)-ax*x/sx*spx;
  y =sy*allcol(:,3);
  yp=spy*allcol(:,4)-ay*y/sy*spy;
  z=zlist;
  e=(1.0+allcol(:,5)*se)*e0;

  outdata=[e,x,y,z,xp,yp];

  fid=fopen(outfile,"wt");
  fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",outdata');
  fclose(fid);

endfunction


%  %   this function normalizes the bunch and reutrns with file_n.ext
function normout=normalize_bunch(filein,fileout=1) 
   
   B=load(filein);
   [zs,id]=sort(B(:,4));
   B=B(id,:);
   e=B(:,1);
   x=B(:,2);
   y=B(:,3);
   z=B(:,4);
   xp=B(:,5);
   yp=B(:,6);

   e0=mean(e);
   g0=e0/0.00051099893;
   z0=mean(z);
   x0=mean(x);
   y0=mean(y);
   xp0=mean(xp);
   yp0=mean(yp);
   sxx=var(x);
   sxpxp=var(xp);
   sxxp=mean((x-x0).*(xp-xp0));
   epsx=sqrt(sxx*sxpxp-sxxp*sxxp);
   bx=sxx/epsx;
   ax=-sxxp/epsx;
   
   syy=var(y);
   sypyp=var(yp);
   syyp=mean((y-y0).*(yp-yp0));
   epsy=sqrt(syy*sypyp-syyp*syyp);
   by=syy/epsy;
   ay=-syyp/epsy;
   
   gbtx=sqrt(g0/bx);
   gbty=sqrt(g0/by);
   gbtx2=sqrt(g0*bx);
   gbty2=sqrt(g0*by);
   
   gi=e/0.00051099893;
   
   xoff=mean(x);
   yoff=mean(y);
   
   xn=sqrt(gi).* x / sqrt(bx) ;
   yn=sqrt(gi).* y / sqrt(by);
   
   xpn= ax* xn + sqrt(gi).* xp * sqrt(bx); %-ax*sqrt(gi).* xoff/ sqrt(bx);
   ypn= ay* yn + sqrt(gi).* yp * sqrt(by); %-ay*sqrt(gi).* yoff/ sqrt(by);
   

   
   rx=sqrt(xn.*xn+xpn.*xpn);
   
   ry=sqrt(yn.*yn+ypn.*ypn);
    
   tetx=atan2(xpn,xn);
   tety=atan2(ypn,yn);
   
   tetx0=tetx(1);
   tety0=tety(1);
   
   xn2 =rx.*cos(tetx.-tetx0);
   xpn2=rx.*sin(tetx.-tetx0);
   
   yn2 =ry.*cos(tety.-tety0);
   ypn2=ry.*sin(tety.-tety0);
   
   outdata=[e,xn2,yn2,z,xpn2,ypn2];

   if fileout > 0
	s=filein;
	npo=findstr(s,".");
	ext=s(1,(npo:end));
	name=s(1,(1:npo-1));
	fout=[name "_n"];
	fout=[fout ext];
	fprintf("normalize_bunch outdata is %s\n",fout);
	fid=fopen(fout,"wt");
	fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",outdata');
	fclose(fid);
   end
 
   normout=outdata;
   
endfunction


function smoothout=smooth(vector) 
  vector;
  n=length(vector);
  tmp=[];
  for k=1:3
    tmp(1)=vector(1);
    a1=vector(1);
    a2=vector(2);
    a3=vector(3);
    tmp(2)=(a1+a2+a3)/3;
    
    for i=3:n-2
      sum=0;
      for j=1:5
	sum=sum+vector(i+j-3);
      end;
      tmp(i)=sum/5;
    end;
    a1=vector(n-2);
    a2=vector(n-1);
    a3=vector(n);
    tmp(n-1)=(a1+a2+a3)/3;
    tmp(n)=vector(n);
    vector=tmp;
   end;
  smoothout=vector;
  
endfunction



%  %   script for slice analysis
function sliceout=slice_analysis(filein,nslice=23, fileout=1) 
   
   B=load(filein);
   [zs,id]=sort(B(:,4));
   B=B(id,:);
   e=B(:,1);
   x=B(:,2);
   y=B(:,3);
   z=B(:,4);
   xp=B(:,5);
   yp=B(:,6);

   e0=mean(e);
   g0=e0/0.00051099893;
   z0=mean(z);
   x0=mean(x);
   y0=mean(y);
   xp0=mean(xp);
   yp0=mean(yp);

   minz=min(z);
   maxz=max(z);
   dz=(maxz-minz)/nslice;


   slices=[];
   
   for i=2:nslice-2
	zi=minz+(i-0.5)*dz;
	zj=minz+(i+0.5)*dz;
	
	k1=min(find(z >= zi));
	k2=max(find(z < zj));

	xi=x(k1:k2);
	yi=y(k1:k2);
	zi=z(k1:k2);
	ei=e(k1:k2);
	xpi=xp(k1:k2);
	ypi=yp(k1:k2);
	
	zi0=mean(zi);
	ei0=mean(ei);
	gi0=ei0/0.00051099893;
	
	xi0=mean(xi);
	xpi0=mean(xpi);
	sxxi=var(xi);
	sxpxpi=var(xpi);
	sxxpi=mean((xi-xi0).*(xpi-xpi0));
	epsxi=sqrt(sxxi*sxpxpi-sxxpi*sxxpi)*gi0*1e-5;

	yi0=mean(yi);
	ypi0=mean(ypi);
	syyi=var(yi);
	sypypi=var(ypi);
	syypi=mean((yi-yi0).*(ypi-ypi0));
	epsyi=sqrt(syyi*sypypi-syypi*syypi)*gi0*1e-5;

	slices(i-1,1)=zi0;
	slices(i-1,2)=ei0;
	slices(i-1,3)=xi0;
	slices(i-1,4)=xpi0;
	slices(i-1,5)=epsxi;
	slices(i-1,6)=yi0;
	slices(i-1,7)=ypi0;
	slices(i-1,8)=epsyi;
   end

   for i=2:8
     if i!=4 
     slices(:,i)=smooth(slices(:,i));
     end
   end;
   
   if fileout>0 
     
	s=filein;
	npo=findstr(s,".");
	ext=s(1,(npo:end));
	name=s(1,(1:npo-1));
	fout=[name "_s"];
	fout=[fout ext];
	fprintf("slice_analysis outdata is %s\n",fout);
	fid=fopen(fout,"wt");
   
	fprintf(fid,'# zi0(1)  ei0(2)  xi0(3)  xpi0(4)  epsxi(5)  yi0(6) ypi0(7) epsyi(8) \n');
	for i=1:nslice-3
	   fprintf(fid,'%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n' ,slices(i,:));
	end
	
	fclose(fid);
   end
   
   sliceout=slices;

endfunction




%  %   script for slice analysis no out read beam data
function sliceout=slice_analysis2(B,nslice=23) 
   
   [zs,id]=sort(B(:,4));
   B=B(id,:);
   e=B(:,1);
   x=B(:,2);
   y=B(:,3);
   z=B(:,4);
   xp=B(:,5);
   yp=B(:,6);

   e0=mean(e);
   g0=e0/0.00051099893;
   z0=mean(z);
   x0=mean(x);
   y0=mean(y);
   xp0=mean(xp);
   yp0=mean(yp);

   minz=min(z);
   maxz=max(z);
   dz=(maxz-minz)/nslice;


   slices=[];
   
   for i=2:nslice-2
	zi=minz+(i-0.5)*dz;
	zj=minz+(i+0.5)*dz;
%  		zi0=(zi+zj)*0.5;
	
	k1=min(find(z > zi));
	k2=max(find(z < zj));

	xi=x(k1:k2);
	yi=y(k1:k2);
	zi=z(k1:k2);
	ei=e(k1:k2);
	xpi=xp(k1:k2);
	ypi=yp(k1:k2);
	
	zi0=mean(zi);
	ei0=mean(ei);
	gi0=ei0/0.00051099893;
	
	xi0=mean(xi);
	xpi0=mean(xpi);
	sxxi=var(xi);
	sxpxpi=var(xpi);
	sxxpi=mean((xi-xi0).*(xpi-xpi0));
	epsxi=sqrt(sxxi*sxpxpi-sxxpi*sxxpi)*gi0*1e-5;

	yi0=mean(yi);
	ypi0=mean(ypi);
	syyi=var(yi);
	sypypi=var(ypi);
	syypi=mean((yi-yi0).*(ypi-ypi0));
	epsyi=sqrt(syyi*sypypi-syypi*syypi)*gi0*1e-5;

	slices(i-1,4)=zi0;
	slices(i-1,1)=ei0;
	slices(i-1,2)=xi0;
	slices(i-1,5)=xpi0;
	slices(i-1,7)=epsxi;
	slices(i-1,3)=yi0;
	slices(i-1,6)=ypi0;
	slices(i-1,8)=epsyi;
   end
   
   for i=2:8
     if i!=4 
     slices(:,i)=smooth(slices(:,i));
     end
   end;
    
   sliceout=slices;

endfunction



function sliceout=slice_normalize(filein,nslice=23,norm2one=1,fileout=1) 
   
   B=load(filein);
   [zs,id]=sort(B(:,4));
   B=B(id,:);
   e=B(:,1);
   x=B(:,2);
   y=B(:,3);
   z=B(:,4);
   xp=B(:,5);
   yp=B(:,6);

   minz=min(z);
   maxz=max(z);
   dz=(maxz-minz)/nslice;


   slices=[];
   
   for i=2:nslice-1
	if i==2
	  zi=minz;
	else 
	  zi=minz+(i-0.5)*dz;
	end;
   
	if i==nslice-1
	  zj=maxz;
	else 
	  zj=minz+(i+0.5)*dz;
	end;
	
	k1=min(find(z >= zi));
	k2=max(find(z < zj));

   
	ei=e(k1:k2);	
	zi=z(k1:k2);
	   
	xi=x(k1:k2);
	yi=y(k1:k2);
	
	xpi=xp(k1:k2);
	ypi=yp(k1:k2);
	
	zi0=mean(zi);
	ei0=mean(ei);
	gi0=ei0/0.00051099893;
	
%  	center of slices
	xi0=mean(xi);
	yi0=mean(yi);	
	
	xpi0=mean(xpi);
	ypi0=mean(ypi);
	
%  	substract ofsetts
	xi=xi-xi0;
	yi=yi-yi0;
	
	xpi=xpi-xpi0;
	ypi=ypi-ypi0;
	
%   find twiss for each slice
	sxxi=var(xi);
	sxpxpi=var(xpi);
	sxxpi=mean(xi.*xpi);
	
	epsxi=sqrt(sxxi*sxpxpi-sxxpi*sxxpi);
	bxi=sxxi/epsxi;
	axi=-sxxpi/epsxi;
	
	syyi=var(yi);
	sypypi=var(ypi);
	syypi=mean(yi.*ypi);
	epsyi=sqrt(syyi*sypypi-syypi*syypi);
	
	byi=syyi/epsyi;
	ayi=-syypi/epsyi;
	
%   normalize ofsets 
%  
   xni=sqrt(gi0/bxi)*xi0;
   yni=sqrt(gi0/byi)*yi0;
%     
   xpni= axi*sqrt(gi0/bxi)*xi0 + sqrt(gi0*bxi)*xpi0; 
   ypni= ayi*sqrt(gi0/byi)*yi0 + sqrt(gi0*byi)*ypi0; 
%  
%     xpni= sqrt(gi0*bxi)*xpi0; 
%     ypni= sqrt(gi0*byi)*ypi0; 	
%     
%     
   rxi=sqrt(xni*xni+xpni*xpni);
   ryi=sqrt(yni*yni+ypni*ypni);
     
   tetxi=atan2(xpni,xni);
   tetyi=atan2(ypni,yni);
   
   if i==2
     tetx0=tetxi;
     tety0=tetyi;
     rx0=rxi;
     ry0=ryi;
   end
  
  if norm2one > 0 
   xni0=rxi*cos(tetxi-tetx0)/rx0;
   xpni0=rxi*sin(tetxi-tetx0)/rx0;
   
   yni0=ryi*cos(tetyi-tety0)/ry0;
   ypni0=ryi*sin(tetyi-tety0)/ry0;
  else 
   xni0=rxi*cos(tetxi-tetx0);
   xpni0=rxi*sin(tetxi-tetx0);
   
   yni0=ryi*cos(tetyi-tety0);
   ypni0=ryi*sin(tetyi-tety0);
  end
	
	slices(i-1,1)=ei0;
	slices(i-1,2)=xni0;
	slices(i-1,3)=yni0;
	slices(i-1,4)=zi0;
	slices(i-1,5)=xpni0;
	slices(i-1,6)=ypni0;
	slices(i-1,7)=epsxi*gi0*1e-5;
	slices(i-1,8)=epsyi*gi0*1e-5;
   end

   for i=2:8
     if i!=4 
     slices(:,i)=smooth(slices(:,i));
     end
   end;

   
   if fileout>0 
     
	s=filein;
	npo=findstr(s,".");
	ext=s(1,(npo:end));
	name=s(1,(1:npo-1));
	fout=[name "_s_n"];
	fout=[fout ext];
	fprintf("slice_analysis outdata is %s\n",fout);
	fid=fopen(fout,"wt");
   
	fprintf(fid,'# ei0(1)   xi0(2)  yi0(3)   zi0(4)   xpi0(5)   ypi0(6)   emitx(7)   emity(8) \n');
	for i=1:nslice-2
	   fprintf(fid,'%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n' ,slices(i,:));
	end
	
	fclose(fid);
     end

   sliceout=slices;

endfunction


