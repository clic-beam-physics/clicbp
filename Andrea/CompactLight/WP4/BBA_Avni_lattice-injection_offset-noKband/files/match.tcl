#idendtity matrix
proc identyM { }  {
    for {set i 0} {$i<6} {incr i} {
        for {set j 0} {$j<6} {incr j} {
	if {$i==$j} {
	    set miden($i,$j) 1.0
	} else {
	    set miden($i,$j) 0.0
	}
        }
    }
    return [array get miden]
}

#procedure for multiplication two matrix
proc matrix_mult {aa bb}  {
    array set m1l $aa
    array set m2l $bb
    for {set i 0} {$i<6} {incr i} {
        for {set j 0} {$j<6} {incr j} {
	set iprod 0
	for {set k 0} {$k<6} {incr k} {
	    set iprod [expr $iprod + $m1l($i,$k)*$m2l($k,$j)]
	}
	set m($i,$j) [expr $iprod]
        }
    }
    return [array get  m]
}

#procedure for multiplication two matrix
proc transpose {aa}  {
    array set mm $aa
    for {set i 0} {$i<6} {incr i} {
        for {set j 0} {$j<6} {incr j} {
	set m($j,$i) $mm($i,$j)
        }
    }
    return [array get  m]
}

# quadrupole matrix

proc  qdfM {l k}  {
    set ll $l
    set kk $k
    array set mq [identyM ]
    if {$kk>0 } {
        set k [expr sqrt($kk)]
        set  mq(0,0) [expr cos($k*$ll)]
        set  mq(0,1) [expr sin($k*$ll)/$k ]
        set  mq(1,0) [expr -$k*sin($k*$ll) ]
        set  mq(1,1) [expr cos($k*$ll) ]
        set  mq(2,2) [expr cosh($k*$ll) ]
        set  mq(2,3) [expr sinh($k*$ll)/$k ]
        set  mq(3,2) [expr sinh($k*$ll)*$k ]
        set  mq(3,3) [expr cosh($k*$ll) ]
    } else {
        set kk -$kk
        set k [expr sqrt($kk)]
        set  mq(0,0) [expr cosh($k*$ll)]
        set  mq(0,1) [expr sinh($k*$ll)/$k ]
        set  mq(1,0) [expr $k*sinh($k*$ll) ]
        set  mq(1,1) [expr cosh($k*$ll) ]
        set  mq(2,2) [expr cos($k*$ll) ]
        set  mq(2,3) [expr sin($k*$ll)/$k ]
        set  mq(3,2) [expr -$k*sin($k*$ll) ]
        set  mq(3,3) [expr cos($k*$ll) ]
    }
    return [array get  mq]
}

# drift matrix
proc drM {l}  {
    array set md [identyM ]
    set  md(0,1) $l
    set  md(2,3) $l
    return [array get  md]
}

proc show_matrix { matrx } {
array set mtrx $matrx
set n [expr sqrt([array size mtrx])]
  puts " "
 for {set i 0} {$i<$n} {incr i} {
 set line {}
    for {set j 0} {$j<$n} {incr j} {
	  set mij [format %5.5f $mtrx($i,$j)]
         lappend line $mij
     }
     puts $line

  }
  puts " "

}

# check matrix
proc checkmatrix {matrix type}  {
    upvar $matrix md
#     array set md $mdd
    set  a1 [expr abs(($md(0,0) + $md(1,1))/2.) ]
    set  a2 [expr abs(($md(3,3) + $md(4,4))/2.) ]
    if { $a1>1 || $a2 >1} {
	 puts "invalid matrix trace please check the lattice parameters \n$type"
	 exit
    }
}

set fodo "q1     l      q2       l     q1
FF-----------DDDD------------FF"
set trip "q1   l1   q2         l          q2   l1   q1
FF-------DDDD------------------DDDD-------FF"

set doub "  l1   q1         l          q2   l1  
------DDDD------------------FFFF------"

proc matchfodo { args }  {
   global pi fodo trip doub
    
   array set params [regsub -all "=" $args " "]

	   
   if {[info exist params(lq1)]<1} {
	puts "$fodo \n define first quad length lq1=<lq1>"
	exit
   } else {
	set lq1  $params(lq1)
   }
   
   if {[info exist params(kq1)]<1} {
	   puts "$fodo \n define first quad strength kq1=<kq1>"
	   exit
   } else {
	set kq1  $params(kq1)
   } 
   
   if {[info exist params(lq2)]<1} {
	puts "$fodo \n define second quad length lq2=<lq2>"
	exit
   } else {
	set lq2  $params(lq2)
   } 
   
   if {[info exist params(kq2)]<1} {
	puts "$fodo \n define second quad strength kq2=<kq2>"
	exit
   } else {
	set kq2  $params(kq2)
   } 

   if {[info exist params(l)]<1} {
	puts "$fodo \n define spacing between quads l=<l>"
	exit
   } else {
	set ls  $params(l)
   } 


   set lq0 [expr $lq1/2]
   
   set mf [qdfM $lq0 $kq1 ]
   set md [qdfM $lq2 $kq2 ]
   set ld [drM $ls]
   
   lappend allmat $mf $ld $md $ld $mf
   set multiply [identyM ]
   foreach  m $allmat {
		
	set multiply [matrix_mult $m $multiply]
# 	show_matrix $multiply
   }
   array set mtrs $multiply
   
   checkmatrix mtrs $fodo
   
   set phix [expr acos(($mtrs(0,0) + $mtrs(1,1))/2)]
   set phiy [expr acos(($mtrs(2,2) + $mtrs(3,3))/2)]
   set beta_x0 [expr $mtrs(0,1)/sin($phix) ]
   set beta_y0 [expr $mtrs(2,3)/sin($phiy) ]
   set alpha_x0 [expr ($mtrs(0,0) - $mtrs(1,1))/sin($phix)/2]
   set alpha_y0 [expr ($mtrs(2,2) - $mtrs(3,3))/sin($phiy)/2]
   set gamma_x0 [expr (1+$alpha_x0*$alpha_x0)/$beta_x0]
   set gamma_y0 [expr (1+$alpha_y0*$alpha_y0)/$beta_y0]
   
   
   array set match { }
   
   set match(beta_x)   $beta_y0 
   set match(beta_y)   $beta_x0  
   set match(alpha_x)  $alpha_y0 
   set match(alpha_y)  $alpha_x0 

   return [array get  match]
}


proc matchfodo_center { args }  {
    
   global pi fodo trip doub
    
   array set params [regsub -all "=" $args " "]

	   
   if {[info exist params(lq1)]<1} {
	puts "$fodo \n define first quad length lq1=<lq1>"
	exit
   } else {
	set lq1  $params(lq1)
   }
   
   if {[info exist params(kq1)]<1} {
	   puts "$fodo \n define first quad strength kq1=<kq1>"
	   exit
   } else {
	set kq1  $params(kq1)
   } 
   
   if {[info exist params(lq2)]<1} {
	puts "$fodo \n define second quad length lq2=<lq2>"
	exit
   } else {
	set lq2  $params(lq2)
   } 
   
   if {[info exist params(kq2)]<1} {
	puts "$fodo \n define second quad strength kq2=<kq2>"
	exit
   } else {
	set kq2  $params(kq2)
   } 

   if {[info exist params(l)]<1} {
	puts "$fodo \n define spacing between quads l=<l>"
	exit
   } else {
	set ls  $params(l)
   } 
   
    set lq0 [expr $lq1/2]
    
    set mf [qdfM $lq0 $kq1 ]
    set md [qdfM $lq2 $kq2 ]
    set ld [drM $ls]
    
    lappend allmat $mf $ld $md $ld $mf
    set multiply [identyM ]
    foreach  m $allmat {
        set multiply [matrix_mult $m $multiply]
    }
    array set mtrs $multiply
    
    checkmatrix mtrs $fodo
    
    set phix [expr acos(($mtrs(0,0) + $mtrs(1,1))/2)]
    set phiy [expr acos(($mtrs(2,2) + $mtrs(3,3))/2)]
    set beta_x0 [expr $mtrs(0,1)/sin($phix) ]
    set beta_y0 [expr $mtrs(2,3)/sin($phiy) ]
    set alpha_x0 [expr ($mtrs(0,0) - $mtrs(1,1))/sin($phix)/2]
    set alpha_y0 [expr ($mtrs(2,2) - $mtrs(3,3))/sin($phiy)/2]
    set gamma_x0 [expr (1+$alpha_x0*$alpha_x0)/$beta_x0]
    set gamma_y0 [expr (1+$alpha_y0*$alpha_y0)/$beta_y0]
    
    
    set ls0 [expr $ls/2]
    set ldh [drM $ls0]
    
    set allmat2 {}
    lappend allmat2 $mf $ldh
    set multiply [identyM ]
    foreach  m $allmat2 {
        set multiply [matrix_mult $m $multiply]
    }
    array set mtrs $multiply
    
    array set match { }
    
    set match(beta_y) [expr $beta_x0*($mtrs(0,0))**2-2*($mtrs(0,0)*$mtrs(0,1))*$alpha_x0+$gamma_x0*($mtrs(0,1))**2]
    set match(beta_x) [expr $beta_y0*($mtrs(2,2))**2-2*($mtrs(2,2)*$mtrs(2,3))*$alpha_y0+$gamma_y0*($mtrs(2,3))**2]
    set match(alpha_y) [expr (1+2*$mtrs(0,1)*$mtrs(1,0))*$alpha_x0-$mtrs(1,0)*$mtrs(0,0)*$beta_x0-$mtrs(0,1)*$mtrs(1,1)*$gamma_x0]
    set match(alpha_x) [expr (1+2*$mtrs(2,3)*$mtrs(3,2))*$alpha_y0-$mtrs(3,2)*$mtrs(2,2)*$beta_y0-$mtrs(2,3)*$mtrs(3,3)*$gamma_y0]

    return [array get  match]
}


proc matchtrip { args }  {
   global pi fodo trip doub
    
   array set params [regsub -all "=" $args " "]
	   
   if {[info exist params(lq1)]<1} {
	puts "$fodo \n define first quad length lq1=<lq1>"
	exit
   } else {
	set lq1  $params(lq1)
   }
   
   if {[info exist params(kq1)]<1} {
	   puts "$fodo \n define first quad strength kq1=<kq1>"
	   exit
   } else {
	set kq1  $params(kq1)
   } 
   
   if {[info exist params(lq2)]<1} {
	puts "$fodo \n define second quad length lq2=<lq2>"
	exit
   } else {
	set lq2  $params(lq2)
   } 
   
   if {[info exist params(kq2)]<1} {
	puts "$fodo \n define second quad strength kq2=<kq2>"
	exit
   } else {
	set kq2  $params(kq2)
   } 

   if {[info exist params(l1)]<1} {
	puts "$fodo \n define spacing between quads l1=<l1>"
	exit
   } else {
	set ls  $params(l1)
   } 
   
   if {[info exist params(l)]<1} {
	puts "$fodo \n define spacing between triplets l=<l>"
	exit
   } else {
	set lb  $params(l)
   } 
   
    set lq0 [expr $lq1/2]
    set mf [qdfM $lq0 $kq1 ]
    set md [qdfM $lq2 $kq2 ]
    set mds [drM $ls]
    set mdb [drM $lb]
    lappend allmat $mf $mds $md $mdb $md $mds $mf
    set multiply [identyM ]
    foreach  m $allmat {
        set multiply [matrix_mult $m $multiply]
    }
    array set mtrs $multiply
    checkmatrix mtrs $trip
    
    set phix [expr acos(($mtrs(0,0) + $mtrs(1,1))/2)]
    set phiy [expr acos(($mtrs(2,2) + $mtrs(3,3))/2)]
    set beta_x0 [expr $mtrs(0,1)/sin($phix) ]
    set beta_y0 [expr $mtrs(2,3)/sin($phiy) ]
    set alpha_x0 [expr ($mtrs(0,0) - $mtrs(1,1))/sin($phix)/2]
    set alpha_y0 [expr ($mtrs(2,2) - $mtrs(3,3))/sin($phiy)/2]
    set gamma_x0 [expr (1+$alpha_x0*$alpha_x0)/$beta_x0]
    set gamma_y0 [expr (1+$alpha_y0*$alpha_y0)/$beta_y0]
       
    set match(beta_x)  $beta_x0
    set match(beta_y)  $beta_y0 
    set match(alpha_x)  $alpha_x0 
    set match(alpha_y)  $alpha_y0 
    
    return [array get  match]
}

proc matchtrip_center {args}  {
   global pi fodo trip doub
    
   array set params [regsub -all "=" $args " "]
	   
   if {[info exist params(lq1)]<1} {
	puts "$fodo \n define first quad length lq1=<lq1>"
	exit
   } else {
	set lq1  $params(lq1)
   }
   
   if {[info exist params(kq1)]<1} {
	   puts "$fodo \n define first quad strength kq1=<kq1>"
	   exit
   } else {
	set kq1  $params(kq1)
   } 
   
   if {[info exist params(lq2)]<1} {
	puts "$fodo \n define second quad length lq2=<lq2>"
	exit
   } else {
	set lq2  $params(lq2)
   } 
   
   if {[info exist params(kq2)]<1} {
	puts "$fodo \n define second quad strength kq2=<kq2>"
	exit
   } else {
	set kq2  $params(kq2)
   } 

   if {[info exist params(l1)]<1} {
	puts "$fodo \n define spacing between quads l1=<l1>"
	exit
   } else {
	set ls  $params(l1)
   } 
   
   if {[info exist params(l)]<1} {
	puts "$fodo \n define spacing between triplets l=<l>"
	exit
   } else {
	set lb  $params(l)
   } 
   
    set lq0 [expr $lq1/2]
    set mf [qdfM $lq0 $kq1 ]
    set md [qdfM $lq2 $kq2 ]
    set mds [drM $ls]
    set mdb [drM $lb]
    lappend allmat $mf $mds $md $mdb $md $mds $mf
    set multiply [identyM ]
    foreach  m $allmat {
        set multiply [matrix_mult $m $multiply]
    }
    array set mtrs $multiply
    
    checkmatrix mtrs $trip
    
    set phix [expr acos(($mtrs(0,0) + $mtrs(1,1))/2)]
    set phiy [expr acos(($mtrs(2,2) + $mtrs(3,3))/2)]
    
    set beta_x0 [expr $mtrs(0,1)/sin($phix) ]
    set beta_y0 [expr $mtrs(2,3)/sin($phiy) ]
    set alpha_x0 [expr ($mtrs(0,0) - $mtrs(1,1))/sin($phix)/2]
    set alpha_y0 [expr ($mtrs(2,2) - $mtrs(3,3))/sin($phiy)/2]
    set gamma_x0 [expr (1+$alpha_x0*$alpha_x0)/$beta_x0]
    set gamma_y0 [expr (1+$alpha_y0*$alpha_y0)/$beta_y0]
    

    
    set lb0 [expr $lb/2]
    set mdbh [drM $lb0]
    
    set allmat2 {}
    lappend allmat2 $mf $mds $md $mdbh
    set multiply [identyM ]
    foreach  m $allmat2 {
        set multiply [matrix_mult $m $multiply]
    }
    array set mtrs $multiply
    
    array set match { }
    
    set match(beta_x) [expr $beta_x0*($mtrs(0,0))**2-2*($mtrs(0,0)*$mtrs(0,1))*$alpha_x0+$gamma_x0*($mtrs(0,1))**2]
    set match(beta_y) [expr $beta_y0*($mtrs(2,2))**2-2*($mtrs(2,2)*$mtrs(2,3))*$alpha_y0+$gamma_y0*($mtrs(2,3))**2]
    set match(alpha_x) [expr (1+2*$mtrs(0,1)*$mtrs(1,0))*$alpha_x0-$mtrs(1,0)*$mtrs(0,0)*$beta_x0-$mtrs(0,1)*$mtrs(1,1)*$gamma_x0]
    set match(alpha_y) [expr (1+2*$mtrs(2,3)*$mtrs(3,2))*$alpha_y0-$mtrs(3,2)*$mtrs(2,2)*$beta_y0-$mtrs(2,3)*$mtrs(3,3)*$gamma_y0]
    
    return [array get  match]
}





proc matchdoub {args}  {
    global pi fodo trip doub
    
   array set params [regsub -all "=" $args " "]
	   
   if {[info exist params(lq1)]<1} {
	puts "$fodo \n define first quad length lq1=<lq1>"
	exit
   } else {
	set lq1  $params(lq1)
   }
   
   if {[info exist params(kq1)]<1} {
	   puts "$fodo \n define first quad strength kq1=<kq1>"
	   exit
   } else {
	set kq1  $params(kq1)
   } 
   
   if {[info exist params(lq2)]<1} {
	puts "$fodo \n define second quad length lq2=<lq2>"
	exit
   } else {
	set lq2  $params(lq2)
   } 
   
   if {[info exist params(kq2)]<1} {
	puts "$fodo \n define second quad strength kq2=<kq2>"
	exit
   } else {
	set kq2  $params(kq2)
   } 

   if {[info exist params(l1)]<1} {
	puts "$fodo \n define spacing between quads l1=<l1>"
	exit
   } else {
	set ls  $params(l1)
   } 
   
   if {[info exist params(l)]<1} {
	puts "$fodo \n define spacing between triplets l=<l>"
	exit
   } else {
	set lb  $params(l)
   }  
   
   
    set pi acos(-1.0)
    set ls0 [expr $ls/2.]
    set mf [qdfM $lq1 $kq1 ]
    set md [qdfM $lq2 $kq2 ]
    set mds [drM $ls0]
    set mdb [drM $lb]
    lappend allmat  $mds $mf $mdb $md $mds
    set multiply [identyM ]
    foreach  m $allmat {
        set multiply [matrix_mult $m $multiply]
    }
    array set mtrs $multiply
    
    checkmatrix mtrs $doub
    
    
    set phix [expr acos(($mtrs(0,0) + $mtrs(1,1))/2)]
    set phiy [expr acos(($mtrs(2,2) + $mtrs(3,3))/2)]
    
    
    array set match { }
    
    set match(beta_x) [expr $mtrs(0,1)/sin($phix) ]
    set match(beta_y) [expr $mtrs(2,3)/sin($phiy) ]
    set match(alpha_x) [expr ($mtrs(0,0) - $mtrs(1,1))/sin($phix)/2]
    set match(alpha_y) [expr ($mtrs(2,2) - $mtrs(3,3))/sin($phiy)/2]
    
    return [array get  match]
}



proc matchdoub_center {args}  {
    global pi fodo trip doub
    
   array set params [regsub -all "=" $args " "]
	   
   if {[info exist params(lq1)]<1} {
	puts "$fodo \n define first quad length lq1=<lq1>"
	exit
   } else {
	set lq1  $params(lq1)
   }
   
   if {[info exist params(kq1)]<1} {
	   puts "$fodo \n define first quad strength kq1=<kq1>"
	   exit
   } else {
	set kq1  $params(kq1)
   } 
   
   if {[info exist params(lq2)]<1} {
	puts "$fodo \n define second quad length lq2=<lq2>"
	exit
   } else {
	set lq2  $params(lq2)
   } 
   
   if {[info exist params(kq2)]<1} {
	puts "$fodo \n define second quad strength kq2=<kq2>"
	exit
   } else {
	set kq2  $params(kq2)
   } 

   if {[info exist params(l1)]<1} {
	puts "$fodo \n define spacing between quads l1=<l1>"
	exit
   } else {
	set ls  $params(l1)
   } 
   
   if {[info exist params(l)]<1} {
	puts "$fodo \n define spacing between triplets l=<l>"
	exit
   } else {
	set lb  $params(l)
   }  
   
   
    set pi acos(-1.0)
    set ls0 [expr $ls/2]
    set mf [qdfM $lq1 $kq1 ]
    set md [qdfM $lq2 $kq2 ]
    set mds [drM $ls0]
    set mdb [drM $lb]
    lappend allmat  $mds $mf $mdb $md $mds
    set multiply [identyM ]
    foreach  m $allmat {
        set multiply [matrix_mult $m $multiply]
    }
    array set mtrs $multiply
    
    
    checkmatrix mtrs $doub
    
    set phix [expr acos(($mtrs(0,0) + $mtrs(1,1))/2)]
    set phiy [expr acos(($mtrs(2,2) + $mtrs(3,3))/2)]
    set beta_x0 [expr $mtrs(0,1)/sin($phix) ]
    set beta_y0 [expr $mtrs(2,3)/sin($phiy) ]
    set alpha_x0 [expr ($mtrs(0,0) - $mtrs(1,1))/sin($phix)/2]
    set alpha_y0 [expr ($mtrs(2,2) - $mtrs(3,3))/sin($phiy)/2]
    set gamma_x0 [expr (1+$alpha_x0*$alpha_x0)/$beta_x0]
    set gamma_y0 [expr (1+$alpha_y0*$alpha_y0)/$beta_y0]

    set lb0 [expr $lb/2]
    set mdbh [drM $lb0]
    
    set allmat2 {}
    lappend allmat2 $mds $mf $mdbh
    set multiply [identyM ]
    foreach  m $allmat2 {
        set multiply [matrix_mult $m $multiply]
    }
    array set mtrs $multiply
    
    array set match { }
    
    set match(beta_x) [expr $beta_x0*($mtrs(0,0))**2-2*($mtrs(0,0)*$mtrs(0,1))*$alpha_x0+$gamma_x0*($mtrs(0,1))**2]
    set match(beta_y) [expr $beta_y0*($mtrs(2,2))**2-2*($mtrs(2,2)*$mtrs(2,3))*$alpha_y0+$gamma_y0*($mtrs(2,3))**2]
    set match(alpha_x) [expr (1+2*$mtrs(0,1)*$mtrs(1,0))*$alpha_x0-$mtrs(1,0)*$mtrs(0,0)*$beta_x0-$mtrs(0,1)*$mtrs(1,1)*$gamma_x0]
    set match(alpha_y) [expr (1+2*$mtrs(2,3)*$mtrs(3,2))*$alpha_y0-$mtrs(3,2)*$mtrs(2,2)*$beta_y0-$mtrs(2,3)*$mtrs(3,3)*$gamma_y0]
    
    return [array get  match]
}


