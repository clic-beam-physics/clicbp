# ALL routines for random number generation is inside tools
# source $script_dir/tools.tcl

array set kband {grad 25.  freq  35.9751e9 a 0.002     g 2.1772e-3 l 2.7772e-3 cellno 270 length  0.75}
array set xband {grad 70.  freq  11.9917e9 a 0.0031244 g 0.009114  l 0.010414 cellno 75 length  0.75}
array set sband {grad 18.  freq   2.9979e9 a 0.0110025 g 0.028324  l 0.033324 cellno 30 length  4.0}

set lambdas [expr $clight/$sband(freq)]
set lambdax [expr $clight/$xband(freq)]


set lambda $lambdas

set ap $sband(a)
set cl $sband(l)
set gp $sband(g) 
set c  $clight

set structure_gap_length $gp 
set structure_cel_length $cl 
set structure_aperture 	 $ap


# *****************************************************************************************
# transverse short range wake function  s is given in micro metres  return value is in V/pCm^2
# *****************************************************************************************
proc W_trans {s a l g} {
	global Z0 clight pi
	set s0 [expr 0.169*pow($a,1.79)*pow($g,0.38)*pow($l,-1.17)]
   set tmp [expr $s0/($pi*pow($a,4.0))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]
   return [expr 4.0*$Z0*$clight*1e-12*$tmp]
}


# *****************************************************************************************
# longitudinal short range wake function  s is given in micro metres, return value is in V/pCm
# *****************************************************************************************
proc W_long {s a l g} {
	global Z0 clight pi
	set s0 [expr 0.41*pow($a,1.8)*pow($g,1.6)*pow($l,-2.4)]
	set tmp [expr 1.0/($pi*$a*$a)*exp(-sqrt($s*1e-6/$s0))]
	return [expr -$Z0*$clight*$tmp*1e-12]
}


	 
#***************************************************************
# routine to calculate the short range longitudinal wake potantial
#  z_list is of longitudinal position and weight of the slices
#------------------------------------------------------------------

proc V_long {charge z_list a l g} {
	# unit to store in the file is MV/pCm
	
	set z_l0 {}
	set weigth_l0 {}
	set nslice [llength $z_list]
	puts "checking if slice number is power of 2"
	set n2 [check2n [expr 2*$nslice-1]]
	set nslice2 [expr int(2*$nslice-1+$n2)]
	puts $nslice2 

	foreach ll $z_list {
		set z 			 [lindex $ll 0]
		set wgt 			 [expr [lindex $ll 1]]
 		lappend z_l0     $z
 		lappend weigth_l0   $wgt
	}
 
	set minz [::math::statistics::min $z_l0] 
	set maxz [::math::statistics::max $z_l0] 
	set dz [expr ($maxz-$minz)/$nslice]

	set w_long_l {}
	set weigth_l {}
	set z_l {}
	
	for {set i 0 } {$i < $nslice2} {incr i } {
		set z  [expr  0.0+$dz*$i]
		lappend z_l $z
		if {$i<$nslice} {
			lappend weigth_l [lindex $weigth_l0 $i]
			lappend w_long_l [W_long $z $a $l $g ] 
		} else {
			lappend weigth_l 0.0
			lappend w_long_l 0.0
		}
	}
	
	set fft_weigth_l0 [::math::fourier::dft  $weigth_l]
	set fft_w_long_l0 [::math::fourier::dft  $w_long_l]
	
	set convolve [complexmutiply $fft_weigth_l0 $fft_w_long_l0]
	
	set tmp  [::math::fourier::inverse_dft $convolve]
	
	set vlong {}
	set j 0
	foreach z $z_l0   {
		set kick [expr 1.6e-7*1.e-6*$charge*[lindex [lindex $tmp $j] 0]]
		lappend  vlong "$z $kick"
		incr j
	}
	
	return $vlong

}

proc V_long2 { charge z_list aa la ga } {
# unit to store in the file is MV/pCm
	set z_l {}
	set n [llength $z_list]
	set tmp {}
	foreach l $z_list {
		set z0 [lindex $l 0]
		set wgt0 [expr $charge*[lindex $l 1]]
		set sum 0.0
		foreach j $z_l {
			set z [lindex $j 0]
			set wgt [expr [lindex $j 1]*$charge]
			set uu  [expr $z0-$z]
			set sum [expr $sum+$wgt*  [W_long $uu $aa $la $ga ] ]
		}
		
		set sum [expr $sum+0.5*$wgt0*[W_long 0.0 $aa $la $ga ] ] 
		lappend z_l $l
		# multiply with factor for pC and MV
		set sum [expr ($sum*1.6e-7*1.e-6)]
		lappend tmp "$z0 $sum"
	}
	return $tmp
}





proc V_trans {charge z_list a l g } {

   set tmp {}
# unit to store in the file is MV/pCm^2
   set z_l {}
   foreach ll $z_list {
		lappend z_l $ll
		set z0 [lindex $ll 0]
		foreach j $z_l {
			set z  [lindex $j 0]
			set dz [expr $z0-$z]
			# multiply charge with factor for pC and MV
			set kick [expr 1.6e-7*1.e-6*$charge*[W_trans $dz $a $l $g ]]
			lappend tmp  $kick
		}
   }
   return $tmp
}


proc calc_beamdat {f_name charge z_list} {
	global structure_gap_length structure_cel_length structure_aperture
	set gp $structure_gap_length 
	set cl $structure_cel_length 
	set ap $structure_aperture 	

	set nslice [llength $z_list]
	set file [open $f_name w]
   puts $file $nslice
   set long   [V_long $charge $z_list $ap $cl $gp ]
   set transv [V_trans $charge $z_list $ap $cl $gp ]
   set x [get_zero_amp $long ]
   puts $file [lindex $x 1]
   foreach x $long zz $z_list {
		set amp [lindex $zz 1]
		puts $file "$x $amp"
   }
   foreach x $transv {
		puts $file $x
   }
   close $file
}




## output wake parameters
set wake_params(length) 30000.0 ; # [um]
set wake_params(Npoints) 512    ; #  

Octave {
    function p = pow(x,a)
        p = x**a;
    endfunction

    function Wt = W_transv(s,a,g,l) % V/pC/m/m
        s0 = 0.169 * pow(a,1.79) * pow(g,0.38) / pow(l,1.17);
        sqrt_s_s0 = sqrt(s/s0);
        Wt = 4 * $Z0 * s0 * $clight / pi / (a*a*a*a) * (1 - (1 + sqrt_s_s0) .* exp(-sqrt_s_s0)) / 1e12;
    endfunction

    function Wl = W_long(s,a,g,l) % V/pC/m
        s0 = 0.41 * pow(a,1.8) * pow(g,1.6) / pow(l,2.4);
        Wl = $Z0 * $clight / pi / (a*a) * exp(-sqrt(s/s0)) / 1e12;
    endfunction

    Z = linspace(0, $wake_params(length) / 1e6, $wake_params(Npoints));
    
    Wtk = W_transv(Z,$kband(a),$kband(g),$kband(l));
    Wtx = W_transv(Z,$xband(a),$xband(g),$xband(l));
    Wts = W_transv(Z,$sband(a),$sband(g),$sband(l));

    Wlk = W_long(Z,$kband(a),$kband(g),$kband(l));
    Wlx = W_long(Z,$xband(a),$xband(g),$xband(l));
    Wls = W_long(Z,$sband(a),$sband(g),$sband(l));
    
    filetk = fopen('kband_Wt.dat', 'w');
    filelk = fopen('kband_Wl.dat', 'w');
    filetx = fopen('xband_Wt.dat', 'w');
    filelx = fopen('xband_Wl.dat', 'w');
    filets = fopen('sband_Wt.dat', 'w');
    filels = fopen('sband_Wl.dat', 'w');
    
    fprintf(filetk, ' %.15g %.15g\n', [ Z ; Wtk ] );
    fprintf(filelk, ' %.15g %.15g\n', [ Z ; Wlk ] );
    fprintf(filetx, ' %.15g %.15g\n', [ Z ; Wtx ] );
    fprintf(filelx, ' %.15g %.15g\n', [ Z ; Wlx ] );
    fprintf(filets, ' %.15g %.15g\n', [ Z ; Wts ] );
    fprintf(filels, ' %.15g %.15g\n', [ Z ; Wls ] );
	 
	 
    fclose(filetk);
    fclose(filelk);
    fclose(filetx);
    fclose(filelx);
    fclose(filets);
    fclose(filels);
}

SplineCreate "K_band_Wt" -file "kband_Wt.dat"
SplineCreate "K_band_Wl" -file "kband_Wl.dat"

SplineCreate "X_band_Wt" -file "xband_Wt.dat"
SplineCreate "X_band_Wl" -file "xband_Wl.dat"

SplineCreate "S_band_Wt" -file "sband_Wt.dat"
SplineCreate "S_band_Wl" -file "sband_Wl.dat"
#

ShortRangeWake "K_band_SR_W" -type 2 \
    -wx "K_band_Wt" \
    -wy "K_band_Wt" \
    -wz "K_band_Wl"

ShortRangeWake "X_band_SR_W" -type 2 \
    -wx "X_band_Wt" \
    -wy "X_band_Wt" \
    -wz "X_band_Wl"
	 
ShortRangeWake "S_band_SR_W" -type 2 \
    -wx "S_band_Wt" \
    -wy "S_band_Wt" \
    -wz "S_band_Wl"


#
# use this list to create fields
 WakeSet Wake_S_long_range {1. 0 0} 
 WakeSet Wake_X_long_range {1. 0 0}
#
# define structure
InjectorCavityDefine -name 0 -lambda $lambdas -wakelong Wake_S_long_range 
# InjectorCavityDefine -name 1 -lambda $lambdax -wakelong Wake_X_long_range 
