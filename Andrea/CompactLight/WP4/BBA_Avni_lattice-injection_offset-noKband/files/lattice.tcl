
set wake_loss 0.0
set wake_loss_s 0.0
set wake_loss_x 0.0
set wake_loss_x2 0.0
set bc1_srloss 0.0
set bc2_srloss 0.0
set quad_synrad 0.0
set sbend_synrad 0.0
# xband structure
set xcoupler 0.075 				    ;# coupler length  0.075 cm
set xdrift [expr 2.*$xcoupler+$xband(length) ] 	    ;# total length of structures is 0.9 m

;# quadrupoles for all beamline
set l_quad 0.4
set k_quad 0.7575
set space_2_quad 0.3

set n_structure 4
set l_modul [expr $n_structure*$xdrift]
set l_modul_half [expr 0.5*$l_modul]

######################################
###### STRUCTURES#####################

set latfile [open lattice.lat w]
set t 1

puts $latfile "
Girder
SetReferenceEnergy\t $e0"


puts $latfile "
## INJECTOR
"

dir_corr  	injD  0.6660889422689031
quad_bpm  	injQ  $l_quad -1.50490229200356
dir_corr 	injD  0.4961569925234853 
quad_bpm 	injQ  $l_quad 1.411858087315068
dir_corr 	injD  1.606467652783001 
cavity 		injS  $sband(length) 0.0 $injS_phase $injS_grad $wake_loss_s
dir_corr	injD  1.606467652783001
quad_bpm 	injQ  $l_quad -0.7872309973536206
dir_corr 	injD  0.35
cavity 		injS  $sband(length) 0.0 $injS_phase $injS_grad $wake_loss_s
drift		injD  0.35
quad_bpm 	injQ  $l_quad $k_quad
dir_corr 	injD  0.35
cavity 		injS  $sband(length) 0.0 $injS_phase $injS_grad $wake_loss_s
drift 		injD  0.35
quad_bpm 	injQ  $l_quad -$k_quad
dir_corr 	injD  0.35
drift 		injD  0.5
cavity 		injX  $xband(length) $xcoupler $injX_phase $injX_grad $wake_loss_x
dir_corr	injD  0.55



set bc1_l_dip    0.5
set bc1_d_cent   0.5

set bc1_angle_rd 	[format "%10.8f" [rad $bc1_angle]]
set bc1_s_dipole 	[format "%10.8f" [expr $bc1_angle_rd*$bc1_l_dip/sin($bc1_angle_rd)]]
set bc1_d_longed	[format "%10.8f" [expr $bc1_d_side/cos($bc1_angle_rd)]]
set bc1_angle_zr	[format "%10.8f" 0.]

set bc1_r56  [expr 4.*$bc1_s_dipole*(tan($bc1_angle_rd) - $bc1_angle_rd)/sin($bc1_angle_rd) + 2.*$bc1_d_longed*tan($bc1_angle_rd)*tan($bc1_angle_rd) ]

puts "R56 of BC1 $bc1_r56"


puts $latfile "
##  BC1 MATCHING
"

drift	 	bc1D  0.592860634525777
quad_bpm 	bc1Q  $l_quad 0.7458251876982169
dir_corr 	bc1D  7.149295020618984
quad_bpm 	bc1Q  $l_quad -1.391786417214379
dir_corr 	bc1D  0.3000385478726657 
quad_bpm 	bc1Q  $l_quad 1.429375435516461
dir_corr 	bc1D  0.3079046542688584 


puts $latfile "
## CHICANE 1
"
dir_bpm		bc1D  0.25
bend 		bc1S1 $bc1_s_dipole $bc1_angle_rd 0.0  $bc1_angle_rd $bc1_srloss
dir_corr	bc1D  $bc1_d_longed
bpm 		bc1b  0.0 
bend 		bc1S2 $bc1_s_dipole -$bc1_angle_rd -$bc1_angle_rd 0.0 $bc1_srloss
dir_corr	bc1D  $bc1_d_cent
bpm 		bc1b  0.0 
bend 		bc1S3 $bc1_s_dipole -$bc1_angle_rd  0.0 -$bc1_angle_rd  $bc1_srloss
dir_corr	bc1D  $bc1_d_longed
bpm 		bc1b  0.0 
bend 		bc1S4 $bc1_s_dipole $bc1_angle_rd $bc1_angle_rd 0.0   $bc1_srloss
dir_corr	bc1D  0.25


puts $latfile "
## MATCHING LINAC 1
"
drift 		lin1D  0.3000000008569712
quad_bpm 	lin1Q  $l_quad 1.519260835194111
dir_corr 	lin1D  0.3000000001540865
quad_bpm 	lin1Q  $l_quad -1.457491070114606
dir_corr 	lin1D  3.734596275080437
quad_bpm 	lin1Q  $l_quad 0.7581384350148308
dir_corr 	lin1D  0.8

puts $latfile "
## LINAC 1
"

for {set t 0} { $t < 5} {incr t} {
   quad_bpm 	lin1Q.M$t.0  $l_quad -$k_quad
   dir_corr 	lin1D.M$t.0  $space_2_quad
   cavity 	lin1X.M$t.0  $xband(length) $xcoupler $lin1X_phase $lin1X_grad $wake_loss_x
   cavity 	lin1X.M$t.0  $xband(length) $xcoupler $lin1X_phase $lin1X_grad $wake_loss_x
   cavity 	lin1X.M$t.0  $xband(length) $xcoupler $lin1X_phase $lin1X_grad $wake_loss_x
   cavity 	lin1X.M$t.0  $xband(length) $xcoupler $lin1X_phase $lin1X_grad $wake_loss_x
   drift 	lin1D.M$t.0  $space_2_quad
   puts $latfile " "
   quad_bpm 	lin1Q.M$t.1  $l_quad $k_quad
   dir_corr 	lin1D.M$t.1  $space_2_quad
   cavity 	lin1X.M$t.1  $xband(length) $xcoupler $lin1X_phase $lin1X_grad $wake_loss_x
   cavity 	lin1X.M$t.1  $xband(length) $xcoupler $lin1X_phase $lin1X_grad $wake_loss_x
   cavity 	lin1X.M$t.1  $xband(length) $xcoupler $lin1X_phase $lin1X_grad $wake_loss_x
   cavity 	lin1X.M$t.1  $xband(length) $xcoupler $lin1X_phase $lin1X_grad $wake_loss_x
   drift 	lin1D.M$t.1  $space_2_quad
   puts $latfile " "
}

quad_bpm 	lin1Q  $l_quad -$k_quad
dir_corr	lin1D  $space_2_quad
drift  		lin1D  $l_modul_half


set bc2_l_dip    0.5
set bc2_d_cent   0.5

set bc2_angle_rd 	[format "%10.8f" [rad $bc2_angle]]
set bc2_s_dipole 	[format "%10.8f" [expr $bc2_angle_rd*$bc2_l_dip/sin($bc2_angle_rd)]]
set bc2_d_longed	[format "%10.8f" [expr $bc2_d_side/cos($bc2_angle_rd)]]
set bc2_angle_zr	[format "%10.8f" 0.]

set bc2_r56  [expr 4.*$bc2_s_dipole*(tan($bc2_angle_rd) - $bc2_angle_rd)/sin($bc2_angle_rd) + 2.*$bc2_d_longed*tan($bc2_angle_rd)*tan($bc2_angle_rd) ]

puts "R56 of BC2 $bc2_r56"


puts $latfile "
##  MATCHING BC2
"

drift 		bc2D  0.7033896550517043
quad_bpm 	bc2Q  $l_quad 0.7847995432571125
dir_corr 	bc2D  7.048956122671747
quad_bpm 	bc2Q  $l_quad -1.276890732333222
dir_corr 	bc2D  0.3039992694929705 
quad_bpm 	bc2Q  $l_quad 1.284869467435559
dir_corr 	bc2D  0.3564175603800008 

 
puts $latfile "
## CHICANE 2
"

dir_bpm		bc2D  0.25
bend 		bc2S1 $bc2_s_dipole $bc2_angle_rd 0.0  $bc2_angle_rd $bc2_srloss
dir_corr	bc2D  $bc2_d_longed
bpm 		bc2b  0.0 
bend 		bc2S2 $bc2_s_dipole -$bc2_angle_rd -$bc2_angle_rd 0.0 $bc2_srloss
dir_corr	bc2D  $bc2_d_cent
bpm 		bc2b  0.0 
bend 		bc2S3 $bc2_s_dipole -$bc2_angle_rd  0.0 -$bc2_angle_rd  $bc2_srloss
dir_corr	bc2D  $bc2_d_longed
bpm 		bc2b  0.0 
bend 		bc2S4 $bc2_s_dipole $bc2_angle_rd $bc2_angle_rd 0.0   $bc2_srloss
dir_bpm		bc2D  0.25

puts $latfile "
## MATCHING LINAC 2
"
dir_corr 	lin2D  2.752737574069187 
quad_bpm 	lin2Q  $l_quad 1.133490192619586
dir_corr 	lin2D  0.5215941209596189
quad_bpm 	lin2Q  $l_quad -1.125186636721924
dir_corr 	lin2D  4.639311002170912
quad_bpm 	lin2Q  $l_quad 0.7835999013309515
dir_corr 	lin2D  0.86381907415586877

puts $latfile "
## LINAC 2
"

for {set t 0} { $t < 9} {incr t} {
   quad_bpm 	lin2Q.M$t.0  $l_quad -$k_quad
   dir_corr 	lin2D.M$t.0  $space_2_quad
   cavity 	lin2X.M$t.0  $xband(length) $xcoupler $lin2X_phase $lin2X_grad $wake_loss_x2
   cavity 	lin2X.M$t.0  $xband(length) $xcoupler $lin2X_phase $lin2X_grad $wake_loss_x2
   cavity 	lin2X.M$t.0  $xband(length) $xcoupler $lin2X_phase $lin2X_grad $wake_loss_x2
   cavity 	lin2X.M$t.0  $xband(length) $xcoupler $lin2X_phase $lin2X_grad $wake_loss_x2
   drift 	lin2D.M$t.0  $space_2_quad
   puts $latfile " "
   quad_bpm 	lin2Q.M$t.1  $l_quad $k_quad
   dir_corr 	lin2D.M$t.1  $space_2_quad
   cavity 	lin2X.M$t.1  $xband(length) $xcoupler $lin2X_phase $lin2X_grad $wake_loss_x2
   cavity 	lin2X.M$t.1  $xband(length) $xcoupler $lin2X_phase $lin2X_grad $wake_loss_x2
   cavity 	lin2X.M$t.1  $xband(length) $xcoupler $lin2X_phase $lin2X_grad $wake_loss_x2
   cavity 	lin2X.M$t.1  $xband(length) $xcoupler $lin2X_phase $lin2X_grad $wake_loss_x2
   drift 	lin2D.M$t.1  $space_2_quad
   puts $latfile " "
}

quad_bpm 	lin2Q.M$t.0  $l_quad -$k_quad
dir_corr 	lin2D.M$t.0  $space_2_quad
cavity 		lin2X.M$t.0  $xband(length) $xcoupler $lin2X_phase $lin2X_grad $wake_loss_x2
cavity 		lin2X.M$t.0  $xband(length) $xcoupler $lin2X_phase $lin2X_grad $wake_loss_x2
cavity 		lin2X.M$t.0  $xband(length) $xcoupler $lin2X_phase $lin2X_grad $wake_loss_x2
cavity 		lin2X.M$t.0  $xband(length) $xcoupler $lin2X_phase $lin2X_grad $wake_loss_x2
drift	 	lin2D.M$t.0  $space_2_quad 	
puts $latfile " "
quad_bpm 	lin2Q  $l_quad $k_quad
dir_corr	lin2D  $space_2_quad
dir_bpm	 	lin2D  $l_modul_half


close $latfile
