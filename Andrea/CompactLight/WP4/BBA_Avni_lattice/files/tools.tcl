#########################################################################################
#  some constands
#----------------------------------------------------------------------------------------
set clight 2.9979246e+08
set Z0 376.99112
set pi [expr acos(-1.)]
set mc2 0.51099893e-3


#########################################################################################
#  loading required libraries 
#----------------------------------------------------------------------------------------

set auto_path { /opt/local/libexec/macports/lib/tcllib1.18 /opt/local/lib/tcl8.6 /opt/local/lib }

package require math::statistics
package require math::fourier 
package require math::bigfloat
package require  math::special
package require Tcl 
package require struct
package require math::interpolate 

namespace import ::math::bigfloat::*





#########################################################################################
# procedure for normal (Gaussian) distribution with mean zero and standard deviation 1.0
#----------------------------------------------------------------------------------------
proc random {} {
     variable last
     if { [info exists last] } {
		  set retval $last
		  unset last
		  return $retval
     }
     set v1 [expr  2. * rand() - 1 ]
     set v2 [expr  2. * rand() - 1 ]
     set rsq [expr  $v1*$v1 + $v2*$v2 ]
     while { $rsq > 1. } {
		  set v1 [expr   2. * rand() - 1  ]
		  set v2 [expr   2. * rand() - 1  ]
		  set rsq [expr   $v1*$v1 + $v2*$v2  ]
     }
     set fac [expr   sqrt( -2. * log( $rsq ) / $rsq )  ]
     set last [expr   $v1 * $fac  ]
     return [expr   $v2 * $fac  ]
 } 
 

proc randomuni {} {
   return [expr -1.+ 2.*rand()]
}


 
#########################################################################################
# procedure for generating N real number with standard deviation of mu and mean sigma 
#----------------------------------------------------------------------------------------
proc randomN { args } {
	 
	 array set params [regsub -all "=" $args " "]
	 	 
	 if {[info exist params(n)]<1} {
		 puts "you have not given numbers default is taken to :=> n=1"
		 set n 1
	  } else {
		 set n $params(n)
	  }
	  
	 if {[info exist params(mu)]<1} {
		 puts "you have not given a standard deviation default is taken to :=> mu=0"
		 set mu 0.0
	  } else {
		 set mu $params(mu)
	  }
	  
	 if {[info exist params(sigma)]<1} {
		 puts "you have not given a mean default is taken to :=> sigma=1.0"
		 set sigma 1.0
	  } else {
		 set sigma $params(sigma)
	  }
	 
		set tmp {}
		for {set i 0} {$i < $n} {incr i} {
			set x [expr $mu+$sigma*[random]]
			lappend tmp $x
		}
     return $tmp
} 
 
 
#########################################################################################
# gets one column of vector returns two column histogram data with n slices
#----------------------------------------------------------------------------------------
proc histogramdata { args } {
# 	concat $args
	array set params [regsub -all "=" $args " "]
	
	if {[info exist params(L)]<1} {
		puts "you have not given a list of numbers Erorr"
		exit
	} else {
		set lst $params(L)
	}
	
	if {[info exist params(nslice)]<1} {
		puts "you have not define slice number default is taken to :=> nslice=51"
		set nslice 51
	} else {
		set nslice $params(nslice)
	}
	
	if {[info exist params(norm)]<1} {
		puts "you have not define normalizationr default is taken to :=> norm=1.0"
		set norm 1.0
	} else {
		set norm $params(norm)
	}
	
	set nl [llength $lst]
	
	if {$nl < $nslice } {
		puts " number of slices are bigger than number of slices reduced to nslice=length(L)"
		set nslice  $nl
	} else {
		set nslice $nslice
	}

	set minz [::math::statistics::min $lst] 
	set maxz [::math::statistics::max $lst] 
	set dz [expr ($maxz-$minz)/$nslice]
	
	set zstp {}
	set zst {}
	
	for {set i 0} {$i<[expr $nslice+4]} {incr i} {
		set zstpi [expr $minz+($i-1)*$dz]
		set zstpj [expr $minz+($i)*$dz]
		lappend zstp $zstpi
		lappend zst  [expr ($zstpi+$zstpj)/2]
	}
	
   set hst [lreplace [::math::statistics::histogram $zstp $lst] end end]
	
	set zh {}
	foreach z $zst h $hst {
		set hh [expr $norm*$h/$nl]
		lappend zh "$z $hh"
	}
	
	return $zh
   
}

#########################################################################################
# reads file and creaates histogram of longitudinal positions
#----------------------------------------------------------------------------------------
proc beamhistogram { args } {

    array set params [regsub -all "=" $args " "]
    
    if {[info exist params(file)]<1} {
	puts "you have not given a file to read Erorr"
	exit
    } else {
	set fin $params(file)
    }
    
    if {[info exist params(nslice)]<1} {
	puts "you have not define slice number default is taken to :=> nslice=101"
	set nslice 101
    } else {
	set nslice $params(nslice)
    }
    
    set ff [open $fin r]
    set lst {}
    while {[gets $ff line]>=0} {
	lappend lst [lindex $line 3]
    }
    close $ff
    

    
    set nl [llength $lst]
    
    if {$nl < $nslice } {
	puts " number of slices are bigger than number of slices reduced to nslice=length(L)"
	set nslice  $nl
    } else {
	set nslice $nslice
    }

    set minz [::math::statistics::min $lst] 
    set maxz [::math::statistics::max $lst] 
    set nslice2 [expr $nslice-2]
    set dz [expr ($maxz-$minz)/$nslice]
    
    set zst {}
    set hst {}
    set sum 1
    lappend zst $minz
    lappend hst 1.
    for {set i 1} {$i<[expr $nslice-1]} {incr i} {
	set zstpi [expr $minz+($i-0.5)*$dz]
	set zstpj [expr $minz+($i+0.5)*$dz]
	set zstp  [expr $minz+($i+0.0)*$dz]
	set amp 0
	foreach zl $lst {
	    if {$zl >= $zstpi && $zl < $zstpj } {
		incr amp
		incr sum
	    }
	}
	lappend zst $zstp
	lappend hst $amp
    }
    lappend zst $maxz
    lappend hst 1.
    incr sum
    
    set norm [expr 1./$sum]
    
    set zh {}
    foreach z $zst h $hst {
	set hh [expr $norm*$h]
	lappend zh "$z $hh"
    }
    
   return $zh
   
}

#########################################################################################
# checks if the number power of 2 if not adds required number importand for fourier transform
#----------------------------------------------------------------------------------------
proc check2n {x} {
	set k 0
	while {[expr pow(2,$k)] < $x} {
		incr k
	}
	set diff [expr pow(2,$k) - $x]
	
	puts "$x  [expr pow(2,$k)] $diff "
	
	return [expr int($diff)]
}

#########################################################################################
# checks if the number is even if not adds 1
#----------------------------------------------------------------------------------------
proc checkdobule {n} {
	set rest 	 [expr $n/2.]
	set integer  [expr floor($rest)]
	if {[expr $rest-$integer]==0} {
		set tmp $n
	} else {
		set tmp [expr $n+1]
	}
	return $tmp
}


#########################################################################################
#  Multiplies two array of complex numbers 
#----------------------------------------------------------------------------------------
proc complexmutiply {a_lst b_lst} {
	set tmp {}
	foreach f_wgt $a_lst f_wtr $b_lst {
		set a1 [lindex $f_wgt 0]
		set a2 [lindex $f_wgt 1]
		set b1 [lindex $f_wtr 0]
		set b2 [lindex $f_wtr 1]
		set convr [expr $a1*$b1-$a2*$b2]
		set convi [expr $a1*$b2+$a2*$b1]
		lappend tmp "$convr $convi"
	}
	return $tmp
}


proc smoothdata {zlist} {
	set zlist [lsort -real -index 0 $zlist]
	
 	for {set k 0} {$k<3} {incr k} {
# 		puts "smoothing histogram [expr $k +1]"
		set z {}
		set a {}
		set n [llength $zlist]
		foreach line $zlist {
			lappend z [lindex $line 0]
			lappend a [lindex $line 1]
		}
		set tmp {}
		
		
		lappend tmp "[lindex $z 0] [lindex $a 0]"   
		set a1 [lindex $a [expr 0]]
		set a2 [lindex $a [expr 1 ]]
		set a3 [lindex $a [expr 2]]
		set av [expr ($a1+$a2+$a3)/3.]
		lappend tmp "[lindex $z 1] $av"
		
		for {set i 2} {$i<[expr $n-2]} {incr i} {
			set sum 0.
			for {set j 0} {$j<5} {incr j} {
				set sum [expr $sum + [lindex $a [expr $i+$j-2]]]
			}
			set av [expr $sum/5.]
			lappend tmp "[lindex $z $i] $av"
		}
		
		set a1 [lindex $a [expr $n-3]]
		set a2 [lindex $a [expr $n-2 ]]
		set a3 [lindex $a [expr $n-1]]
		set av [expr ($a1+$a2+$a3)/3.]
		lappend tmp "[lindex $z $n-1] $av"

		
		lappend tmp "[lindex $z [expr $n-1]] [lindex $a [expr $n-1]]"
 		set zlist $tmp
 	}
	
	
	return $tmp
	
}
#########################################################################################
#  gauss function with mean sigma standard deviation 0. 
#----------------------------------------------------------------------------------------
proc gaussian {s sigma} {
	global pi
	return [expr 1./$sigma/sqrt(2.*$pi)*exp(-($s*$s)/2./$sigma/$sigma)]
}


proc plateau {s sigma} {
	global pi
	set ll [expr 3.333*$sigma] 
	set rt [expr $ll/10.]
	return [expr 1./$ll/(1.+exp(2./$rt*(2.*abs($s)-$ll)))]
}


proc parabola {s sigma} {
	global pi
	set zmax [expr sqrt(5.)*$sigma] 
	set zmin [expr -sqrt(5.)*$sigma] 
	
	if {$zmin<$s<$zmax} {
		set tmp [expr 3./4./$zmax*(1.-$s*$s/$zmax/$zmax)]
	} else {
		set tmp 0.0
	}
	return $tmp
}


proc uniform {s sigma} {
	global pi
	set fwhm [expr $sigma*2.*sqrt(3)]
	set zmax [expr $fwhm*0.5] 
	set zmin [expr -1.*$zmax] 
	
	if {$zmin<$s<$zmax} {
		set tmp [expr 1./$fwhm]
	} else {
		set tmp 0.0
	}
	return $tmp
}


proc gauss_sig1_norm { nmacro norm } {
	set sigma 1.0
	set sigma_cut [expr 10./3.]
	set minz [expr -$sigma_cut*$sigma]

	set dz [expr 2.*$sigma_cut*$sigma/($nmacro+1)]
	
	set sum 0.0
	set al  {}
	for {set i 0} {$i<[expr $nmacro]} {incr i } {
		set z   [expr $minz+($i+1)*$dz]
		set a   [expr [gaussian $z $sigma]]
		set sum [expr $sum+$a]
		lappend al  "$a "
	}
	
	set norm [expr $norm/$sum]
	
	set tmp {}
	
	foreach a $al {
		set a [expr $a*$norm]
		lappend tmp "$a"
	}

	return $tmp
}


proc uniform_sig1_norm { nmacro norm } {
	set sigma 1.0
	set sigma_cut [expr 2.*sqrt(3)]
	set minz [expr -$sigma_cut*$sigma]

	set dz [expr 2.*$sigma_cut*$sigma/($nmacro+1)]
	
	set sum 0.0
	set al  {}
	for {set i 0} {$i<[expr $nmacro]} {incr i } {
		set z   [expr $minz+$i*$dz]
		set a   [expr [uniform $z $sigma]]
		set sum [expr $sum+$a]
		lappend al  "$a "
	}
	
	set norm [expr $norm/$sum]
	
	set tmp {}
	
	foreach a $al {
		set a [expr $a*$norm]
		lappend tmp "$a"
	}

	return $tmp
}



proc gaussList { args } {
	array set params [regsub -all "=" $args " "]

	if {[info exist params(mu)]<1} {
		set mu 0.0
		puts "you have not given a standard deviation default is taken to :=> mu=$mu"
	} else {
		set mu $params(mu)
	}
	
	if {[info exist params(sigma)]<1} {
		set sigma 1.0
		puts "you have not given a mean default is taken to :=> sigma=$sigma"
	} else {
		set sigma $params(sigma)
	}
	
	if {[info exist params(sigma_cut)]<1} {
		set sigma_cut [expr 10./3.]
		puts "you have not define sigma_cut default is taken to :=> sigma_cut=$sigma_cut"
		
	} else {
		set sigma_cut $params(sigma_cut)
	}
	
	if {[info exist params(nslice)]<1} {
		set nslice 11
		puts "you have not given number of slices default is taken to :=> nslice=$nslice"
	} else {
		set nslice $params(nslice)
	}
	
	set minz [expr -$sigma_cut*$sigma]
	
	set dz [expr 2.*$sigma_cut*$sigma/($nslice)]
	
	set sum 0.0
	set al  {}
	set zl  {}
	for {set i 0} {$i<[expr $nslice]} {incr i } {
		set z   [expr $minz+($i+0.5)*$dz]
		set zz  [expr $z+$mu]
		set a   [expr [gaussian $z $sigma]]
		set sum [expr $sum+$a]
		lappend zl  "$zz "
		lappend al  "$a "
	}
	
	set norm [expr 1./$sum]
	
	set tmp {}
# 	lappend tmp  "[expr $minz+$mu-$dz] 0.0"
		foreach z  $zl a $al {
		set a [expr $a*$norm]
		lappend tmp "$z $a"
	}
# 	lappend tmp "[expr $zz+$dz] 0.0"
	return $tmp
}





proc plateauList { args } {
	array set params [regsub -all "=" $args " "]

	if {[info exist params(mu)]<1} {
		set mu 0.0
		puts "you have not given a standard deviation default is taken to :=> mu=$mu"
	} else {
		set mu $params(mu)
	}
	
	if {[info exist params(sigma)]<1} {
		set sigma 1.0
		puts "you have not given a mean default is taken to :=> sigma=$sigma"
	} else {
		set sigma $params(sigma)
	}
	
	if {[info exist params(sigma_cut)]<1} {
	   set sigma_cut [expr 10./3./sqrt(2)]
	   puts "the sigma_cut variable is fixed for this distribution :=> sigma_cut=$sigma_cut"
		
	} else {
		set sigma_cut [expr 10./3./sqrt(2)]
	}

	
	if {[info exist params(nslice)]<1} {
		set nslice 11
		puts "you have not given number of slices default is taken to :=> nslice=$nslice"
	} else {
		set nslice $params(nslice)
	}
	
	set minz [expr -$sigma_cut*$sigma]
	set dz  [expr 2*$sigma_cut*$sigma/($nslice)]
	
	set minz2 [expr -$sigma_cut*$sigma]
	while {[plateau $minz2 $sigma] < 2.e-7 } {
		set minz2 [expr $minz2+$dz]
	}

	set minz $minz2
	set dz [expr 2.*abs($minz)/$nslice]

	set sum 0.0
	set al  {}
	set zl  {}
	for {set i 0} {$i<[expr $nslice]} {incr i } {
		set z   [expr $minz+($i+0.5)*$dz]
		set zz  [expr $z+$mu]
		set a   [expr [plateau $z $sigma]]
		set sum [expr $sum+$a]
		lappend zl  "$zz "
		lappend al  "$a "
	}
	
	set norm [expr 1./$sum]
	
	set tmp {}
# 	lappend tmp  "[expr $minz+$mu-$dz] 0.0"
	foreach z  $zl a $al {
		set a [expr $a*$norm]
		lappend tmp "$z $a"
	}
# 	lappend tmp "[expr $zz+$dz] 0.0"
	return $tmp
}


proc parabolaList { args } {
	array set params [regsub -all "=" $args " "]

	if {[info exist params(mu)]<1} {
		set mu 0.0
		puts "you have not given a standard deviation default is taken to :=> mu=$mu"
	} else {
		set mu $params(mu)
	}
	
	if {[info exist params(sigma)]<1} {
		set sigma 1.0
		puts "you have not given a mean default is taken to :=> sigma=$sigma"
	} else {
		set sigma $params(sigma)
	}
	
	if {[info exist params(sigma_cut)]<1} {
		set sigma_cut [expr sqrt(5.)]
		puts "the sigma_cut variable is fixed for this distribution :=> sigma_cut=$sigma_cut"
		
	} else {
		set sigma_cut [expr sqrt(5.)]
	}
	
	if {[info exist params(nslice)]<1} {
		set nslice 11
		puts "you have not given number of slices default is taken to :=> nslice=$nslice"
	} else {
		set nslice $params(nslice)
	}
	

	set minz [expr -$sigma_cut*$sigma]
	set dz  [expr 2*$sigma_cut*$sigma/($nslice)]
	
	set sum 0.0
	set al  {}
	set zl  {}
	for {set i 0} {$i<[expr $nslice]} {incr i } {
		set z   [expr $minz+($i+0.5)*$dz]
		set zz  [expr $z+$mu]
		set a   [expr [parabola $z $sigma]]
		set sum [expr $sum+$a]
		lappend zl  "$zz "
		lappend al  "$a "
	}
	
	set norm [expr 1./$sum]
	
	set tmp {}
# 	lappend tmp  "[expr $minz+$mu-$dz] 0.0"
		foreach z  $zl a $al {
		set a [expr $a*$norm]
		lappend tmp "$z $a"
	}
# 	lappend tmp "[expr $zz+$dz] 0.0"
	return $tmp
}


proc uniformList { args } {
	array set params [regsub -all "=" $args " "]

	if {[info exist params(mu)]<1} {
		set mu 0.0
		puts "you have not given a standard deviation default is taken to :=> mu=$mu"
	} else {
		set mu $params(mu)
	}
	
	if {[info exist params(sigma)]<1} {
		set sigma 1.0
		puts "you have not given a mean default is taken to :=> sigma=$sigma"
	} else {
		set sigma $params(sigma)
	}
	
	if {[info exist params(sigma_cut)]<1} {
		set sigma_cut [expr 2.*sqrt(3)]
		puts "the sigma_cut variable is fixed for this distribution :=> sigma_cut=$sigma_cut"
		
	} else {
		set sigma_cut [expr 2.*sqrt(3)]
	}
	
	if {[info exist params(nslice)]<1} {
		set nslice 11
		puts "you have not given number of slices default is taken to :=> nslice=$nslice"
	} else {
		set nslice $params(nslice)
	}
	

	set minz [expr -0.5*$sigma_cut*$sigma]
	set dz  [expr $sigma_cut*$sigma/$nslice]

	set sum 0.0
	set al  {}
	set zl  {}
	for {set i 0} {$i<[expr $nslice]} {incr i } {
		set z   [expr $minz+($i+0.5)*$dz]
		set zz  [expr $z+$mu]
		set a   [expr [uniform $z $sigma]]
		set sum [expr $sum+$a]
		lappend zl  "$zz "
		lappend al  "$a "
	}
	
	set norm [expr 1./$sum]
	
	set tmp {}
# 	lappend tmp  "[expr $minz+$mu-$dz] 0.0"
		foreach z  $zl a $al {
		set a [expr $a*$norm]
		lappend tmp "$z $a"
	}
# 	lappend tmp "[expr $zz+$dz] 0.0"
	return $tmp
}


proc get_zero_amp { llist } {
	set zmin 1.e300
	set a0 0.0
	foreach line $llist {
		set z [expr abs([lindex $line 0])]
		set a [lindex $line 1]
		if {$z < $zmin} { 
			set zmin $z 
			set a0 $a
		} else { 
			set zmin $zmin
			set a0 $a0
		}
	}
	return "$zmin $a0"
}


##################################################################
#### Weigthed lists not required anymore.. 
### maybe used for next read create sliced beam
###################3
proc gaussWeigthedList { args } {
	array set params [regsub -all "=" $args " "]

	if {[info exist params(mu)]<1} {
		set mu 0.0
		puts "you have not given a standard deviation default is taken to :=> mu=$mu"
	} else {
		set mu $params(mu)
	}
	
	if {[info exist params(sigma)]<1} {
		set sigma 1.0
		puts "you have not given a mean default is taken to :=> sigma=$sigma"
	} else {
		set sigma $params(sigma)
	}
	
	if {[info exist params(sigma_cut)]<1} {
		set sigma_cut [expr 10./3.]
		puts "you have not define sigma_cut default is taken to :=> sigma_cut=$sigma_cut"
		
	} else {
		set sigma_cut $params(sigma_cut)
	}
	
	if {[info exist params(nslice)]<1} {
		set nslice 11
		puts "you have not given number of slices default is taken to :=> nslice=$nslice"
	} else {
		set nslice $params(nslice)
	}
		
	if {[info exist params(nmacro)]<1} {
		set nmacro 1
		puts "you have not define number per silice default is taken to :=> nmacro=1"
	} else {
		set nmacro $params(nmacro)
	}
	
	set minz [expr -$sigma_cut*$sigma]
	
	set dz [expr 2.*$sigma_cut*$sigma/($nslice)]
	
	set sum 0.0
	set al  {}
	set zl  {}
	for {set i 0} {$i<[expr $nslice]} {incr i } {
		set z   [expr $minz+($i+0.5)*$dz]
		set zz  [expr $z+$mu]
		set a   [expr [gaussian $z $sigma]]
		set sum [expr $sum+$a]
		lappend zl  "$zz "
		lappend al  "$a "
	}
	
	set norm [expr 1./$sum]
	
	set tmp {}
	foreach z  $zl a $al {
		set a [expr $a*$norm]
		set wgt [gauss_sig1_norm $nmacro $a]
		foreach w $wgt {
			lappend tmp "$z $w"
		}		
	}
	return $tmp
}


proc plateauWeigthedList { args } {
	array set params [regsub -all "=" $args " "]

	if {[info exist params(mu)]<1} {
		set mu 0.0
		puts "you have not given a standard deviation default is taken to :=> mu=$mu"
	} else {
		set mu $params(mu)
	}
	
	if {[info exist params(sigma)]<1} {
		set sigma 1.0
		puts "you have not given a mean default is taken to :=> sigma=$sigma"
	} else {
		set sigma $params(sigma)
	}
	if {[info exist params(sigma_cut)]<1} {
		set sigma_cut [expr 10./3./sqrt(2)]
		puts "you have not define sigma_cut default is taken to :=> sigma_cut=$sigma_cut"
		
	} else {
		set sigma_cut $params(sigma_cut)
	}
	
	if {[info exist params(nslice)]<1} {
		set nslice 11
		puts "you have not given number of slices default is taken to :=> nslice=$nslice"
	} else {
		set nslice $params(nslice)
	}
	
	if {[info exist params(nmacro)]<1} {
		set nmacro 1
		puts "you have not define number per silice default is taken to :=> nmacro=1"
	} else {
		set nmacro $params(nmacro)
	}
	
	set minz [expr -$sigma_cut*$sigma]
	set dz  [expr 2*$sigma_cut*$sigma/($nslice)]
	
	set minz2 [expr -$sigma_cut*$sigma]
	while {[plateau $minz2 $sigma] < 1.e-7 } {
		set minz2 [expr $minz2+$dz]
	}

	set minz $minz2
	set dz [expr 2.*abs($minz)/$nslice]
	
	set sum 0.0
	set al  {}
	set zl  {}
	for {set i 0} {$i<[expr $nslice]} {incr i } {
		set z   [expr $minz+($i+0.5)*$dz]
		set zz  [expr $z+$mu]
		set a   [expr [plateau $z $sigma]]
		set sum [expr $sum+$a]
		lappend zl  "$zz "
		lappend al  "$a "
	}
	
	set norm [expr 1./$sum]
	
	set tmp {}
# 	lappend tmp  "[expr $minz+$mu-$dz] 0.0 0.0"
	foreach z  $zl a $al {
		set a [expr $a*$norm]
		set wgt [gauss_sig1_norm $nmacro $a]
		foreach w $wgt {
			lappend tmp "$z $w"
		}		
	}
# 	lappend tmp "[expr $zz+$dz] 0.0 0.0"
	return $tmp
}


proc parabolaWeigthedList { args } {
	array set params [regsub -all "=" $args " "]

	if {[info exist params(mu)]<1} {
		set mu 0.0
		puts "you have not given a standard deviation default is taken to :=> mu=$mu"
	} else {
		set mu $params(mu)
	}
	
	if {[info exist params(sigma)]<1} {
		set sigma 1.0
		puts "you have not given a mean default is taken to :=> sigma=$sigma"
	} else {
		set sigma $params(sigma)
	}
	
	if {[info exist params(sigma_cut)]<1} {
		set sigma_cut [expr sqrt(5.)]
		puts "the sigma_cut variable is fixed for this distribution :=> sigma_cut=$sigma_cut"
		
	} else {
		set sigma_cut [expr sqrt(5.)]
	}
	
	if {[info exist params(nslice)]<1} {
		set nslice 11
		puts "you have not given number of slices default is taken to :=> nslice=$nslice"
	} else {
		set nslice $params(nslice)
	}
	
	if {[info exist params(nmacro)]<1} {
		set nmacro 1
		puts "you have not define number per silice default is taken to :=> nmacro=1"
	} else {
		set nmacro $params(nmacro)
	}
	
	set minz [expr -$sigma_cut*$sigma]
	
	set dz [expr 2.*$sigma_cut*$sigma/($nslice)]
	
	set sum 0.0
	set al  {}
	set zl  {}
	for {set i 0} {$i<[expr $nslice]} {incr i } {
		set z   [expr $minz+($i+0.5)*$dz]
		set zz  [expr $z+$mu]
		set a   [expr [parabola $z $sigma]]
		set sum [expr $sum+$a]
		lappend zl  "$zz "
		lappend al  "$a "
	}
	
	set norm [expr 1./$sum]
	
	set tmp {}
# 	lappend tmp  "[expr $minz+$mu-$dz] 0.0 0.0"
	foreach z  $zl a $al {
		set a [expr $a*$norm]
		set wgt [gauss_sig1_norm $nmacro $a]
		foreach w $wgt {
			lappend tmp "$z $w"
		}		
	}
# 	lappend tmp "[expr $zz+$dz] 0.0 0.0"
	return $tmp
}


proc uniformWeigthedList { args } {
	array set params [regsub -all "=" $args " "]

	if {[info exist params(mu)]<1} {
		set mu 0.0
		puts "you have not given a standard deviation default is taken to :=> mu=$mu"
	} else {
		set mu $params(mu)
	}
	
	if {[info exist params(sigma)]<1} {
		set sigma 1.0
		puts "you have not given a mean default is taken to :=> sigma=$sigma"
	} else {
		set sigma $params(sigma)
	}
	
	if {[info exist params(sigma_cut)]<1} {
		set sigma_cut [expr 2.*sqrt(3)]
		puts "the sigma_cut variable is fixed for this distribution :=> sigma_cut=$sigma_cut"
		
	} else {
		set sigma_cut [expr 2.*sqrt(3)]
	}
	
	if {[info exist params(nslice)]<1} {
		set nslice 11
		puts "you have not given number of slices default is taken to :=> nslice=$nslice"
	} else {
		set nslice $params(nslice)
	}
	
	if {[info exist params(nmacro)]<1} {
		set nmacro 1
		puts "you have not define number per silice default is taken to :=> nmacro=1"
	} else {
		set nmacro $params(nmacro)
	}
	
	set minz [expr -$sigma_cut*$sigma]
	
	set dz [expr 2.*$sigma_cut*$sigma/($nslice)]
	
	set sum 0.0
	set al  {}
	set zl  {}
	for {set i 0} {$i<[expr $nslice]} {incr i } {
		set z   [expr $minz+($i+0.5)*$dz]
		set zz  [expr $z+$mu]
		set a   [expr [uniform $z $sigma]]
		set sum [expr $sum+$a]
		lappend zl  "$zz "
		lappend al  "$a "
	}
	
	set norm [expr 1./$sum]
	
	set tmp {}
# 	lappend tmp  "[expr $minz+$mu-$dz] 0.0 0.0"
	foreach z  $zl a $al {
		set a [expr $a*$norm]
		set wgt [gauss_sig1_norm $nmacro $a]
		foreach w $wgt {
			lappend tmp "$z $w"
		}		
	}
# 	lappend tmp "[expr $zz+$dz] 0.0 0.0"
	return $tmp
}


proc create_energy_slice { args } {
	
	array set params [regsub -all "=" $args " "]

	if {[info exist params(e0)]<1} {
		set e0 1.0
		puts "you have not define mean energy default is taken to :=> energy=$e0"
	} else {
		set e0 $params(e0)
	}
	
	if {[info exist params(e_spread)]<1} {
		set e_spread 1.0
		puts "you have not define energy spread default is taken to :=> e_spread=$e_spread"
	} else {
		set e_spread $params(e_spread)
	}
	
	if {[info exist params(e_sigma_cut)]<1} {
		set e_sigma_cut 3.
		puts "you have not define spread cut default is taken to :=> e_sigma_cut=$e_sigma_cut"
	} else {
		set e_sigma_cut $params(e_sigma_cut)
	}
	
	if {[info exist params(nslice)]<1} {
		set nslice 11
		puts "you have not define number of slices default is taken to :=> nslice=$nslice"
	} else {
		set nslice $params(nslice)
	}
	
	if {[info exist params(nmacro)]<1} {
		set nmacro 1
		puts "you have not define number per silice default is taken to :=> nmacro=1"
	} else {
		set nmacro $params(nmacro)
	}
	
	set de   [expr $e0*$e_spread/100.]
	set demax [expr $de*3.0]
	
	set destp [expr 2.*$demax/$nmacro]
	
	set e_lst {}
	for {set i 0} { $i<$nslice} {incr i} {
		set e00 [expr $e0-$demax]
		for {set j 0} { $j<$nmacro} {incr j} {
			set e [expr $e00+$destp*($j+0.5)]
			lappend e_lst $e
		}
	}
	return $e_lst
	
}



proc normalize_slice_beam { filein } {
	set zlist   {}  ;# 0
	set elist   {}  ;# 2
	set xlist   {}  ;# 3
	set xplist  {}  ;# 4
	set ylist   {}  ;# 5
	set yplist  {}  ;# 6
	set s11list {}  ;# 7
	set s12list {}  ;# 8
	set s22list {}  ;# 9
	set s33list {}  ;# 10
	set s34list {}  ;# 11
	set s44list {}  ;# 12

	set nbunch 0
	set fin [open $filein r]
	while {[gets $fin line] >= 0}  {
		if {[llength $line ]==0 } {
			incr nbunch
		} else {
			lappend zlist   [lindex $line 0]
			lappend elist   [lindex $line 2]
			lappend xlist   [lindex $line 3]
			lappend xplist  [lindex $line 4]
			lappend ylist   [lindex $line 5]
			lappend yplist  [lindex $line 6]
			lappend s11list [lindex $line 7]
			lappend s12list [lindex $line 8]
			lappend s22list [lindex $line 9]
			lappend s33list [lindex $line 10]
			lappend s34list [lindex $line 11]
			lappend s44list [lindex $line 12]
		}
	}

	set npart_total [llength $zlist]
	set npart_per_bunch [expr $npart_total /$nbunch]

	set zlist_single {}
	for {set i 0} {$i < $npart_per_bunch} {incr i} {
		lappend zlist_single [lindex $zlist $i]
	}

	set nslice [llength [lsort -unique $zlist_single]]
	set center_slice [expr int(($nslice+1)/2)]
	set nmacro [expr $npart_per_bunch/$nslice]
	set center_macro  [expr int(($nmacro+1)/2)]


	set c_m [expr $center_macro-1]
	set c_s [expr ($center_slice-1)*$nmacro+$c_m]


	#### first slice 
	set z_1   [lindex $zlist $c_m]
	set e_1   [lindex $elist $c_m]
	set g_1   [expr sqrt($e_1/0.51099893e-3)]
	set x_1   [lindex $xlist $c_m]
	set xp_1  [lindex $xplist $c_m]
	set y_1   [lindex $ylist $c_m]
	set yp_1  [lindex $yplist $c_m]
	set s11_1 [lindex $s11list $c_m]
	set s12_1 [lindex $s12list $c_m]
	set s22_1 [lindex $s22list $c_m]
	set s33_1 [lindex $s33list $c_m]
	set s34_1 [lindex $s34list $c_m]
	set s44_1 [lindex $s44list $c_m]
	# x-direction
	set eps_x_1 [expr ($s11_1*$s22_1 - $s12_1*$s12_1)]
	set b_x_1   [expr sqrt($s11_1/sqrt($eps_x_1))]
	set a_x_1   [expr -$s12_1/sqrt($eps_x_1)]
	set x_n_1   [expr ($g_1/$b_x_1)*$x_1] 
	set xp_n_1  [expr $g_1*($a_x_1/$b_x_1*$x_1+$b_x_1*$xp_1)] 
	set r_x_1   [expr sqrt($x_n_1**2+$xp_n_1**2)]
	set tet_x_1 [expr atan2($xp_n_1,$x_n_1)]
	# y-direction
	set eps_y_1 [expr ($s33_1*$s44_1 - $s34_1*$s34_1)]
	set b_y_1   [expr sqrt($s33_1/sqrt($eps_y_1))]
	set a_y_1   [expr -$s34_1/sqrt($eps_y_1)]
	set y_n_1   [expr ($g_1/$b_y_1)*$y_1] 
	set yp_n_1  [expr $g_1*($a_y_1/$b_y_1*$y_1+$b_y_1*$yp_1)] 
	set r_y_1   [expr sqrt($y_n_1**2+$yp_n_1**2)]
	set tet_y_1 [expr atan2($yp_n_1,$y_n_1)]

	##### center slice
	set z_c   [lindex $zlist $c_s]
	set e_c   [lindex $elist $c_s]
	set g_c   [expr sqrt($e_c/0.51099893e-3)]
	set x_c   [lindex $xlist $c_s]
	set xp_c  [lindex $xplist $c_s]
	set y_c   [lindex $ylist $c_s]
	set yp_c  [lindex $yplist $c_s]
	set s11_c [lindex $s11list $c_s]
	set s12_c [lindex $s12list $c_s]
	set s22_c [lindex $s22list $c_s]
	set s33_c [lindex $s33list $c_s]
	set s34_c [lindex $s34list $c_s]
	set s44_c [lindex $s44list $c_s]

	# x-direction
	set eps_x_c [expr ($s11_c*$s22_c - $s12_c*$s12_c)]
	set b_x_c   [expr sqrt($s11_c/sqrt($eps_x_c))]
	set a_x_c   [expr -$s12_c/sqrt($eps_x_c)]
	set x_n_c   [expr ($g_c/$b_x_c)*$x_c] 
	set xp_n_c  [expr $g_c*($a_x_c/$b_x_c*$x_c+$b_x_c*$xp_c)] 
	set r_x_c   [expr sqrt($x_n_c**2+$xp_n_c**2)]
	set tet_x_c [expr atan2($xp_n_c,$x_n_c)]
	# y-direction
	set eps_y_c [expr ($s33_c*$s44_c - $s34_c*$s34_c)]
	set b_y_c   [expr sqrt($s33_c/sqrt($eps_y_c))]
	set a_y_c   [expr -$s34_c/sqrt($eps_y_c)]
	set y_n_c   [expr ($g_c/$b_y_c)*$y_c] 
	set yp_n_c  [expr $g_c*($a_y_c/$b_y_c*$y_c+$b_y_c*$yp_c)] 
	set r_y_c   [expr sqrt($y_n_c**2+$yp_n_c**2)]
	set tet_y_c [expr atan2($yp_n_c,$y_n_c)]
	

   puts " _____________________________X________________Y_____________"
	puts " beta first           = [expr $b_x_1**2]    [expr $b_y_1**2]   "
	puts " beta centr           = [expr $b_x_c**2]    [expr $b_y_c**2]    "
	puts " alpa first           = $a_x_1    $a_y_1    "
	puts " alpa centr           = $a_x_c    $a_y_c    "
	puts " ____________________________________________________________"
	puts " center slice energy                 = $e_c "
	puts " number of bunches                   =$nbunch"
	puts " number of macroparticles per bunch  =$npart_per_bunch"
	puts " number of slices                    =$nslice"
	puts " number of macroparticles per slice  =$nmacro"
	puts " center slice index                  =$center_slice"
	puts " center of macroparticles index      =$center_macro"
		
	set name   [lreplace [ string map { . " "} $filein] end end]
	set ext    [lreplace [ string map { . " "} $filein] 0 0]

	if {$nbunch > 1} {
		set f1 [open $name.1st_n.$ext w]
		set f2 [open $name.rem_n.$ext w]
		set f3 [open $name.all_n.$ext w]
	} else {
		set f3 [open [append name _n].$ext w]
	}
	set tmp [open tmp.dat w]
	set nline 0
	foreach s $zlist e $elist x $xlist xp $xplist y $ylist yp $yplist s11 $s11list s12 $s12list s22 $s22list s33 $s33list s34 $s34list s44 $s44list {
		set g [expr sqrt($e/0.51099893e-3)]
		# x-direction
		set eps_x [expr ($s11*$s22 - $s12*$s12)]
		set b_x   [expr sqrt($s11/sqrt($eps_x))]
		set a_x   [expr -$s12/sqrt($eps_x)]
		set x_n   [expr ($g/$b_x)*$x] 
		set xp_n  [expr $g*($a_x/$b_x*$x+$b_x*$xp)] 
		set r_x   [expr sqrt($x_n**2+$xp_n**2)]
		set tet_x [expr atan2($xp_n,$x_n)]
		set x_nr   [expr $r_x*cos($tet_x-$tet_x_c)]
		set xp_nr  [expr $r_x*sin($tet_x-$tet_x_c)]
		set x_nr2   [expr $r_x*cos($tet_x-$tet_x_1)]
		set xp_nr2  [expr $r_x*sin($tet_x-$tet_x_1)]
			
		# y-direction
		set eps_y [expr ($s33*$s44 - $s34*$s34)]
		set b_y   [expr sqrt($s33/sqrt($eps_y))]
		set a_y   [expr -$s34/sqrt($eps_y)]
		set y_n   [expr ($g/$b_y)*$y] 
		set yp_n  [expr $g*($a_y/$b_y*$y+$b_y*$yp)] 
		set r_y   [expr sqrt($y_n**2+$yp_n**2)]
		set tet_y [expr atan2($yp_n,$y_n)]
		set y_nr   [expr $r_y*cos($tet_y-$tet_y_c)]
		set yp_nr  [expr $r_y*sin($tet_y-$tet_y_c)]	
		set y_nr2   [expr $r_y*cos($tet_y-$tet_y_1)]
		set yp_nr2  [expr $r_y*sin($tet_y-$tet_y_1)]	
		
# 		puts " $r_x  $r_y" 


		if {$nbunch > 1} {
			puts $f3 [format "%.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f " $s $e $x_nr $xp_nr $y_nr $yp_nr $x_nr2 $xp_nr2 $y_nr2 $yp_nr2  ]
			if {$nline < $npart_per_bunch } {
				puts $f1 [format "%.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f " $s $e $x_nr $xp_nr $y_nr $yp_nr $x_nr2 $xp_nr2 $y_nr2 $yp_nr2  ]
			} else {
				puts $f2 [format "%.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f " $s $e $x_nr $xp_nr $y_nr $yp_nr $x_nr2 $xp_nr2 $y_nr2 $yp_nr2  ]
			}
		} else {
			puts $f3 [format "%.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f " $s $e $x_nr $xp_nr $y_nr $yp_nr $x_nr2 $xp_nr2 $y_nr2 $yp_nr2  ]
		}
		incr nline
	}

	if {$nbunch > 1} {
		close $f1 
		close $f2 
		close $f3 
	} else {
		close $f3  
	}

}


proc mean {lst} {
	set sum1 [::math::statistics::mean $lst]
	return $sum1
}

proc rms {lst} {
	set sum1 [::math::statistics::var $lst]
	return $sum1
}



proc mean2 {lst1 lst2} {
	set ml1 [mean $lst1]
	set ml2 [mean $lst2]
	set product {}
	foreach l1 $lst1 l2 $lst2 {
		lappend product [expr ($l1-$ml1)*($l2-$ml2)]
	}
	return [::math::statistics::mean $product]
}

proc bunch_parameters {filein} {
	

  set elist   {}  ;# 0
  set xlist   {}  ;# 1
  set ylist   {}  ;# 2
  set zlist   {}  ;# 3
  set xplist  {}  ;# 4
  set yplist  {}  ;# 5

  set fin [open $filein r]
  while {[gets $fin line] >= 0}  {
	  if {[llength $line ]==0 } {
		  incr nbunch
	  } else {
		  lappend elist   [lindex $line 0]
		  lappend xlist   [lindex $line 1]
		  lappend ylist   [lindex $line 2]
		  lappend zlist   [lindex $line 3]
		  lappend xplist  [lindex $line 4]
		  lappend yplist  [lindex $line 5]
	  }
  }
  
  set emean [mean $elist]
  set gamma [expr $emean/0.5109989e-3]
  set sxx   [rms $xlist]
  set sxpxp [rms $xplist]
  set sxxp  [mean2 $xlist $xplist]
  set epsx  [expr sqrt($sxx*$sxpxp-$sxxp*$sxxp)]
  set emtx  [expr $epsx*$gamma*1e-5]
  set bx    [expr $sxx/$epsx]
  set ax	 [expr -$sxxp/$epsx]
  
  set syy   [rms $ylist]
  set sypyp [rms $yplist]
  set syyp  [mean2 $ylist $yplist]
  set epsy  [expr sqrt($syy*$sypyp-$syyp*$syyp)]
  set emty  [expr $epsy*$gamma*1e-5]
  set by    [expr $syy/$epsy]
  set ay	 [expr -$syyp/$epsy]
  
  set sz [expr sqrt([rms $zlist])]
  set de [expr sqrt([rms $elist])/$emean*100.]

  array set beampar {}
  set beampar(energy) $emean
  set beampar(sigma_z) $sz
  set beampar(e_spread) $de
  set beampar(beta_x) $bx
  set beampar(beta_y) $by
  set beampar(alpha_x) $ax
  set beampar(alpha_y) $ay
  set beampar(emitt_x) $emtx
  set beampar(emitt_y) $emty
  
  return [array get beampar]
  
}


proc integrate_linac {twissfile } {
	
  set fin [open $twissfile r]

  set beta_x {}
  set beta_y {}
  set leng {}
  set ener {}
  set nline 0
  while {[gets $fin line] >= 0}  {
	if {$nline > 18 } {
	  lappend leng [lindex $line 1]
	  lappend ener [expr [lindex $line 2]*1e3]
	  lappend beta_x [expr [lindex $line 5]]
	  lappend beta_y [expr [lindex $line 9]]
	}
	incr nline
  }
  close $fin

  set int_x_ary {}
  set int_y_ary {}
  set int_x 0.0
  set int_y 0.0
  set phase_x 0.0
  set phase_y 0.0

  set totline [llength $leng]

  for {set i 0} {$i< [expr $totline-1]} {incr i} {
	set s_ii [lindex $leng [expr $i]]
	set s_jj [lindex $leng [expr $i+1]]
	set beta_xii [lindex $beta_x [expr $i]]
	set beta_xjj [lindex $beta_x [expr $i+1]]
	set beta_yii [lindex $beta_y [expr $i]]
	set beta_yjj [lindex $beta_y [expr $i+1]]
	set ener_ii  [lindex $ener [expr $i]]
	set ener_jj  [lindex $ener [expr $i+1]]

	set phase_x [expr $phase_x + 0.5*($s_jj-$s_ii)*(1./$beta_xii +1./$beta_xjj)]
	set phase_y [expr $phase_y + 0.5*($s_jj-$s_ii)*(1./$beta_yii +1./$beta_yjj)]
		

	if { [expr ($ener_jj + $ener_ii)*0.5] <= $ener_ii } {
	  set beta_xii 0
	  set beta_xjj 0
	  set beta_yii 0
	  set beta_yjj 0
	} else {
	  set beta_xii $beta_xii
	  set beta_xjj $beta_xjj
	  set beta_yii $beta_yii
	  set beta_yjj $beta_yjj
	}

	set int_x [expr $int_x + 0.5*($s_jj-$s_ii)*($beta_xii/$ener_ii + $beta_xjj/$ener_jj)]
	set int_y [expr $int_y + 0.5*($s_jj-$s_ii)*($beta_yii/$ener_ii + $beta_yjj/$ener_jj)]

  }


  array set lin_integrate {}
  set lin_integrate(phase_advance_x) $phase_x
  set lin_integrate(phase_advance_y) $phase_y
  set lin_integrate(beta_over_energy_x) $int_x
  set lin_integrate(beta_over_energy_y) $int_y

  return [array get lin_integrate]

} 

proc get_max_amplification {filein} {
	

set max_axn -1e10
set max_ayn -1e10

  set fin [open $filein r]
  set nline 0
  while {[gets $fin line] >= 0}  {
    if {$nline > 0 } {
      set xn  [lindex $line 1]
      set yn  [lindex $line 2]
      set xpn [lindex $line 4]
      set ypn [lindex $line 5]
      set axn [expr sqrt($xn*$xn+$xpn*$xpn)]
      set ayn [expr sqrt($yn*$yn+$ypn*$ypn)]
      
      if { $axn > $max_axn } {set max_axn $axn} else { set max_axn $max_axn}
      if { $ayn > $max_ayn } {set max_ayn $ayn} else { set max_ayn $max_ayn}
    }
    incr nline
   
  }
  close $fin
  return [format "%12.8f %12.8f " $max_axn $max_ayn]
}
   
