
source defaults.tcl

array set BeamDefine [bunch_parameters $script_dir/astra.out]
array set BeamDefine "name beam0 charge  1.5603773e+09"
array set BeamDefine "n_slice 201 n_macro 501  n_bunch 1 sigma_cut 3.333  "

set beamlinename "xfel"
# puts [array get BeamDefine]

set e0  $BeamDefine(energy)
set gm0 [expr $e0/$mc2]

source $script_dir/lattice.tcl

### beamline
BeamlineNew
Girder
Drift -length 0.0
source lattice.lat
puts "beamline energy $e0"
BeamlineSet -name $beamlinename
BeamlineUse -name $beamlinename

set BeamDefine(name)  beam1
set BeamDefine(charge) [expr $BeamDefine(charge) * $dcharge ]
make_beam_particle_read BeamDefine $script_dir/astra.out
FirstOrder 1

set BeamDefine(name)  beam0
set BeamDefine(charge) [expr $BeamDefine(charge) / $dcharge ]
make_beam_particle_read BeamDefine $script_dir/astra.out
FirstOrder 1




if { $wgt1 == -1 } {
    set wgt1 [expr round(sqrt(($bpmres * $bpmres + $sigma * $sigma) / (2 * $bpmres * $bpmres)))]
    puts "Using DFS theoretical weight = $wgt1"
}

if { $wgt2 == -1 } {
    set wgt2 [expr round(sqrt(($bpmres * $bpmres + $sigma * $sigma) / (2 * $bpmres * $bpmres)))]
    puts "Using WFS theoretical weight = $wgt2"
}

Octave {
# #  We want tracking in 6d because there is bunch compression
   QI = placet_get_number_list("$beamlinename", "quadrupole");
   placet_element_set_attribute("$beamlinename", QI, "six_dim", true);

   SI = placet_get_number_list("$beamlinename", "sbend");
   placet_element_set_attribute("$beamlinename", SI, "six_dim", true);
   
   CA = placet_get_number_list("$beamlinename", "cavity");
   placet_element_set_attribute("$beamlinename", CA, "six_dim", true);
   
   S_CAVs = placet_get_name_number_list("$beamlinename", "injS*"); 
   X_CAVs = placet_get_name_number_list("$beamlinename", "injX*");
   X_CAVs = [X_CAVs, placet_get_name_number_list("$beamlinename", "lin1X*")];
   X_CAVs = [X_CAVs, placet_get_name_number_list("$beamlinename", "lin2X*")];
   placet_element_set_attribute("$beamlinename", S_CAVs, "short_range_wake", "S_band_SR_W");
   placet_element_set_attribute("$beamlinename", S_CAVs, "lambda", $lambdas);
   placet_element_set_attribute("$beamlinename", X_CAVs, "short_range_wake", "X_band_SR_W");
   placet_element_set_attribute("$beamlinename", X_CAVs, "lambda", $lambdax);

   # puts a K-band wake, just for fun
   placet_element_set_attribute("$beamlinename", X_CAVs(1), "short_range_wake", "K_band_SR_W");


   
   function reduce_energy()
      injS21 =placet_get_name_number_list("$beamlinename", "injS.0021");
      Lin1X1 =placet_get_name_number_list("$beamlinename", "lin1X.M0.0.*");
      Lin1X2 =placet_get_name_number_list("$beamlinename", "lin1X.M0.1.*");
      %placet_element_vary_attribute("$beamlinename", injS21, "phase", +$dphase1);
      placet_element_vary_attribute("$beamlinename", Lin1X1, "phase", +$dphase1);
      placet_element_vary_attribute("$beamlinename", Lin1X2, "phase", +$dphase2);
    end

    function reset_energy()
      injS21 =placet_get_name_number_list("$beamlinename", "injS.0021");
      Lin1X1 =placet_get_name_number_list("$beamlinename", "lin1X.M0.0.*");
      Lin1X2 =placet_get_name_number_list("$beamlinename", "lin1X.M0.1.*");
      %placet_element_vary_attribute("$beamlinename", injS21, "phase", -$dphase1);
      placet_element_vary_attribute("$beamlinename", Lin1X1, "phase", -$dphase1);
      placet_element_vary_attribute("$beamlinename", Lin1X2, "phase", -$dphase2);
    end
 }

Octave {
    if exist('../R_${beamlinename}_p_${dphase1}_${dphase2}_c_${dcharge}.dat', "file")
    load '../R_${beamlinename}_p_${dphase1}_${dphase2}_c_${dcharge}.dat';
    else
    Response.Cx = placet_get_name_number_list("$beamlinename", "CorXY_*");
    Response.Cy = placet_get_name_number_list("$beamlinename", "CorXY_*");
    Response.Bpms = placet_get_number_list("$beamlinename", "bpm");
    
    # picks all correctors preceeding the last bpm, and all bpms following the first corrector
    Response.Cx = Response.Cx(Response.Cx < Response.Bpms(end));
    Response.Cy = Response.Cy(Response.Cy < Response.Bpms(end));
    Response.Bpms = Response.Bpms(Response.Bpms > max(Response.Cx(1), Response.Cy(1)));

    
     # gets the energy at each corrector
    Response.Ex = placet_element_get_attribute("$beamlinename", Response.Cx, "e0");
    Response.Ey = placet_element_get_attribute("$beamlinename", Response.Cy, "e0");
    
    # compute response matrices
    placet_test_no_correction("$beamlinename", "beam0", "Zero");
    Response.B0 = placet_get_bpm_readings("$beamlinename", Response.Bpms);
    Response.R0x = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "x", Response.Cx, "strength_x", "Zero");
    Response.R0y = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "y", Response.Cy, "strength_y", "Zero");
    
    reduce_energy();
    placet_test_no_correction("$beamlinename", "beam0", "Zero");
    Response.B1 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - Response.B0;
    Response.R1x = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "x", Response.Cx, "strength_x", "Zero") - Response.R0x;
    Response.R1y = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "y", Response.Cy, "strength_y", "Zero") - Response.R0y;
    reset_energy();
    placet_test_no_correction("$beamlinename", "beam1", "Zero");
    Response.B2 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - Response.B0;
    Response.R2x = placet_get_response_matrix_attribute ("$beamlinename", "beam1", Response.Bpms, "x", Response.Cx, "strength_x", "Zero") - Response.R0x;
    Response.R2y = placet_get_response_matrix_attribute ("$beamlinename", "beam1", Response.Bpms, "y", Response.Cy, "strength_y", "Zero") - Response.R0y;
    save -text '../R_${beamlinename}_p_${dphase1}_${dphase2}_c_${dcharge}.dat' Response;
    end
}


proc my_survey {} {
  global machine sigma sigmap bpmres beamlinename nm
  Octave {
    randn("seed", $nm * 12345);
      
    BPMs = placet_get_number_list("$beamlinename", "bpm");
    placet_element_set_attribute("$beamlinename", BPMs, "resolution", $bpmres);
    placet_element_set_attribute("$beamlinename", BPMs, "x", randn(size(BPMs)) * $sigma);
    placet_element_set_attribute("$beamlinename", BPMs, "y", randn(size(BPMs)) * $sigma);	
    
    CORs = placet_get_number_list("$beamlinename", "dipole");
    placet_element_set_attribute("$beamlinename", CORs, "strength_x", 0.0);
    placet_element_set_attribute("$beamlinename", CORs, "strength_y", 0.0);
    
    QUAs = placet_get_number_list("$beamlinename", "quadrupole");
    placet_element_set_attribute("$beamlinename", QUAs, "x", randn(size(QUAs)) * $sigma);
    placet_element_set_attribute("$beamlinename", QUAs, "y", randn(size(QUAs)) * $sigma);
    
# 	individual s band structures on injector not located on a a girder
    INJ_S = placet_get_name_number_list("$beamlinename", "injS*"); 
    placet_element_set_attribute("$beamlinename", INJ_S, "x", randn(size(INJ_S)) * $sigma);
    placet_element_set_attribute("$beamlinename", INJ_S, "y", randn(size(INJ_S)) * $sigma);
    placet_element_set_attribute("$beamlinename", INJ_S, "xp", randn(size(INJ_S)) * $sigma);
    placet_element_set_attribute("$beamlinename", INJ_S, "yp", randn(size(INJ_S)) * $sigma);
    
# 	individual X band structure for linerazer not located on a a girder
    INJ_X = placet_get_name_number_list("$beamlinename", "injX*");
    placet_element_set_attribute("$beamlinename", INJ_X, "x", randn(size(INJ_X)) * $sigma);
    placet_element_set_attribute("$beamlinename", INJ_X, "y", randn(size(INJ_X)) * $sigma);
    placet_element_set_attribute("$beamlinename", INJ_X, "xp", randn(size(INJ_X)) * $sigma);
    placet_element_set_attribute("$beamlinename", INJ_X, "yp", randn(size(INJ_X)) * $sigma);	
    
#  	X band structures on Linac 1.. located on a girder 
#       4 structures are located on a girder spaced by quads and correctors are before module..
# 	module is misaligned by DX on the center of module and offset is added
# 	DX(i)= DX+dS(i)*DXP ..  where dS is distance from center 
      # 	if angle is too large error becomes too later too.. reduced by 50%
      for MN=0:4 
      for MINDX=0:1
	LIN1_X = placet_get_name_number_list("$beamlinename", sprintf("lin1X.M%d.%d.*",MN, MINDX));
	if length(LIN1_X) > 0
	  DX = randn() * $sigma;
	  DY = randn() * $sigma;
	  DXP = randn() * $sigma*0.5;
	  DYP = randn() * $sigma*0.5;
	  sAll = placet_element_get_attribute("$beamlinename", LIN1_X, "s");
	  sMin = sAll(1);
	  sMax = sAll(end);
	  sLength = sMax - sMin;
	  placet_element_set_attribute("$beamlinename", LIN1_X, "xp", DXP + randn(size(DXP)) * $sigmap);
	  placet_element_set_attribute("$beamlinename", LIN1_X, "yp", DYP + randn(size(DYP)) * $sigmap);
	  placet_element_set_attribute("$beamlinename", LIN1_X, "x", DX .+ DXP .* ((sAll - sMin) - sLength * 0.5));
	  placet_element_set_attribute("$beamlinename", LIN1_X, "y", DY .+ DYP .* ((sAll - sMin) - sLength * 0.5));
	end
      end
    end

    

#  	X band structures on Linac 2.. located on a girder 
#       4 structures are located on a girder spaced by quads and correctors are before module..
# 	module is misaligned by DX on the center of module and offset is added
# 	DX(i)= DX+dS(i)*DXP ..  where dS is distance from center 
# 	if angle is too large error becomes too later too.. reduced by 50%
    for MN=0:9 
      for MINDX=0:1
	LIN2_X = placet_get_name_number_list("$beamlinename", sprintf("lin2X.M%d.%d.*",MN, MINDX));
	if length(LIN2_X) > 0
	  DX = randn() * $sigma;
	  DY = randn() * $sigma;
	  DXP = randn() * $sigma*0.5;
	  DYP = randn() * $sigma*0.5;
	  sAll = placet_element_get_attribute("$beamlinename", LIN2_X, "s");
	  sMin = sAll(1);
	  sMax = sAll(end);
	  sLength = sMax - sMin;
      	  placet_element_set_attribute("$beamlinename", LIN2_X, "xp", DXP + randn(size(DXP)) * $sigmap);
	  placet_element_set_attribute("$beamlinename", LIN2_X, "yp", DYP + randn(size(DYP)) * $sigmap);
	  placet_element_set_attribute("$beamlinename", LIN2_X, "x", DX .+ DXP .* ((sAll - sMin) - sLength * 0.5));
	  placet_element_set_attribute("$beamlinename", LIN2_X, "y", DY .+ DYP .* ((sAll - sMin) - sLength * 0.5));
	end
      end
    end
  }
}

if { $machine == "all" } {
    set machine_start 1
    set machine_end $nm
} {
    set machine_start $machine
    set machine_end $machine
}

Octave {
    [E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "Zero", "%s %E %dE %ex %ey %sz");
}


#  save beam on different positions
proc reset_global_vars {} {
    global dump_index
    set dump_index 0
}

proc dump_beam {} {
  global dump_index
  set dumpnames {inj_in.dump inj_out.dump corr_out.dump bc1_out.dump lin1_out.dump  bc2_out.dump lin2_out.dump}
  BeamDump -file [lindex  $dumpnames $dump_index]
  incr dump_index
}

Octave {
  
  DUMPS = [ placet_get_name_number_list("$beamlinename", "injD.*")(1)
	    placet_get_name_number_list("$beamlinename", "d_injX.*")(1) 
	    placet_get_name_number_list("$beamlinename", "injD.*")(end)
	    placet_get_name_number_list("$beamlinename", "bc1D.*")(end)
	    placet_get_name_number_list("$beamlinename", "lin1D.*")(end)
	    placet_get_name_number_list("$beamlinename", "bc2D.*")(end)
	    placet_get_name_number_list("$beamlinename", "lin2D.*")(end) ];
  placet_element_set_attribute("$beamlinename", 0, "tclcall_entrance", "reset_global_vars");
  placet_element_set_attribute("$beamlinename", DUMPS, "tclcall_exit", "dump_beam");

}

Octave {
  [s, beta_x, beta_y, alpha_x, alpha_y] = placet_evolve_beta_function("$beamlinename", $BeamDefine(beta_x), $BeamDefine(alpha_x), $BeamDefine(beta_y), $BeamDefine(alpha_x));
  twiss= [s  beta_x  beta_y  alpha_x alpha_y];
  save -text $beamlinename.twi twiss
  plot(s,beta_x,s,beta_y)
}


# Binning
Octave {
    nBpms = length(Response.Bpms);
    printf("total number of bpms %g \n", nBpms);
    Blen = nBpms / ($nbins * (1 - $noverlap) + $noverlap);
    for i=1:$nbins
      Bmin = floor((i - 1) * Blen - (i - 1) * Blen * $noverlap) + 1;
      Bmax = floor((i)     * Blen - (i - 1) * Blen * $noverlap);
      Cxmin = find(Response.Cx < Response.Bpms(Bmin))(end);
      Cxmax = find(Response.Cx < Response.Bpms(Bmax))(end);
      Cymin = find(Response.Cy < Response.Bpms(Bmin))(end);
      Cymax = find(Response.Cy < Response.Bpms(Bmax))(end);
      Bins(i).Bpms = Bmin:Bmax;
      Bins(i).Cx = Cxmin:Cxmax;
      Bins(i).Cy = Cymin:Cymax;
    end
    
    printf("Each bin contains approx %g bpms.\n", round(Blen));
}



for {set machine $machine_start} {$machine <= $machine_end} {incr machine} {
    
    puts "MACHINE: $machine/$nm"
    
    my_survey
    
    Octave {
	disp("TRACKING...");
	[E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None", "%s %E %dE %ex %ey %sz");
	if exist('E0', 'var')
	E0 = E0 .+ E;
	else
	E0 = E;
	end

	save -text ${beamlinename}_beam_no_${machine}.dat B;
	
	disp("1-TO-1 CORRECTION");
        for bin = 1:$nbins
	  Bin = Bins(bin);
	  nCx = length(Bin.Cx);
  	  nCy = length(Bin.Cy);
   	  R0x = Response.R0x(Bin.Bpms,Bin.Cx);
   	  R0y = Response.R0y(Bin.Bpms,Bin.Cy);
	  for i=1:3
	    B0 = placet_get_bpm_readings("$beamlinename", Response.Bpms);
	    B0 -= Response.B0;
   	    B0 = B0(Bin.Bpms,:);
            Cx = -[ R0x ; $beta0 * eye(nCx) ] \ [ B0(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $beta0 * eye(nCy) ] \ [ B0(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamlinename", Response.Cx(Bin.Cx), "strength_x", Cx);
	    placet_element_vary_attribute("$beamlinename", Response.Cy(Bin.Cy), "strength_y", Cy);
	    [E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None", 1, 0, Response.Bpms(Bin.Bpms(end)), "%s %E %dE %ex %ey %sz");
	  end
	end

	save -text ${beamlinename}_beam_121_${machine}.dat B;

	if exist('E1', 'var')
	E1 = E1 .+ E;
	else
	E1 = E;
	end

	disp("DFS CORRECTION");
        for bin = 1:$nbins
	  Bin = Bins(bin);
	  nCx = length(Bin.Cx);
  	  nCy = length(Bin.Cy);
   	  R0x = Response.R0x(Bin.Bpms,Bin.Cx);
   	  R0y = Response.R0y(Bin.Bpms,Bin.Cy);
   	  R1x = Response.R1x(Bin.Bpms,Bin.Cx);
   	  R1y = Response.R1y(Bin.Bpms,Bin.Cy);
	  for i=1:3
	    B0 = placet_get_bpm_readings("$beamlinename", Response.Bpms);
	    reduce_energy();
	    placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None", 1, 0, Response.Bpms(Bin.Bpms(end)));
	    reset_energy();
	    B1 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - B0;
	    B0 -= Response.B0;
	    B1 -= Response.B1;
            B0 = B0(Bin.Bpms,:);
            B1 = B1(Bin.Bpms,:);
	    Cx = -[ R0x ; $wgt1 * R1x ; $beta1 * eye(nCx) ] \ [ B0(:,1) ; $wgt1 * B1(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $wgt1 * R1y ; $beta1 * eye(nCy) ] \ [ B0(:,2) ; $wgt1 * B1(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamlinename", Response.Cx(Bin.Cx), "strength_x", Cx);
	    placet_element_vary_attribute("$beamlinename", Response.Cy(Bin.Cy), "strength_y", Cy);
	    [E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None", 1, 0, Response.Bpms(Bin.Bpms(end)), "%s %E %dE %ex %ey %sz");
	  end
	end

	if exist('E2', 'var')
	E2 = E2 .+ E;
	else
	E2 = E;
	end

	save -text ${beamlinename}_beam_dfs_${machine}.dat B;

	
	disp("WFS CORRECTION");
        for bin = 1:$nbins
	  Bin = Bins(bin);
	  nCx = length(Bin.Cx);
  	  nCy = length(Bin.Cy);
   	  R0x = Response.R0x(Bin.Bpms,Bin.Cx);
   	  R0y = Response.R0y(Bin.Bpms,Bin.Cy);
   	  R1x = Response.R1x(Bin.Bpms,Bin.Cx);
   	  R1y = Response.R1y(Bin.Bpms,Bin.Cy);
   	  R2x = Response.R2x(Bin.Bpms,Bin.Cx);
   	  R2y = Response.R2y(Bin.Bpms,Bin.Cy);
	  for i=1:3
	    B0 = placet_get_bpm_readings("$beamlinename", Response.Bpms);
	    % DFS
	    reduce_energy();
	    placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None", 1, 0, Response.Bpms(Bin.Bpms(end)));
	    reset_energy();
	    B1 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - B0;
            % WFS
	    placet_test_no_correction("$beamlinename", "beam1", "None", 1, 0, Response.Bpms(Bin.Bpms(end)));
	    B2 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - B0;
	    %
	    B0 -= Response.B0;
	    B1 -= Response.B1;
	    B2 -= Response.B2;
            B0 = B0(Bin.Bpms,:);
            B1 = B1(Bin.Bpms,:);
            B2 = B2(Bin.Bpms,:);
	    Cx = -[ R0x ; $wgt1 * R1x ; $wgt2 * R2x ; $beta2 * eye(nCx) ] \ [ B0(:,1) ; $wgt1 * B1(:,1) ; $wgt2 * B2(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $wgt1 * R1y ; $wgt2 * R2y ; $beta2 * eye(nCy) ] \ [ B0(:,2) ; $wgt1 * B1(:,2) ; $wgt2 * B2(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamlinename", Response.Cx(Bin.Cx), "strength_x", Cx);
	    placet_element_vary_attribute("$beamlinename", Response.Cy(Bin.Cy), "strength_y", Cy);
	    [E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None", 1, 0, Response.Bpms(Bin.Bpms(end)), "%s %E %dE %ex %ey %sz");
	  end
	end

        save -text ${beamlinename}_beam_wfs_${machine}.dat B;

	if exist('E3', 'var')
	E3 = E3 .+ E;
	else
	E3 = E;
	end
    }
}

set nm [expr $machine_end - $machine_start + 1]

Octave {
    E0 = E0 / $nm;
    E1 = E1 / $nm;
    E2 = E2 / $nm;
    E3 = E3 / $nm;
    save -text ${beamlinename}_emitt_no_n_${nm}.dat E0
    save -text ${beamlinename}_emitt_simple_b_${beta0}_n_${nm}.dat E1
    save -text ${beamlinename}_emitt_dfs_w_${wgt1}_b_${beta0}_${beta1}_n_${nm}.dat E2
    save -text ${beamlinename}_emitt_wfs_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat E3

}

exec echo "set grid lt 0 \n \\
plot '${beamlinename}_emitt_simple_b_${beta0}_n_${nm}.dat' u 1:4 w l ti 'simple cor', \\
     '${beamlinename}_emitt_dfs_w_${wgt1}_b_${beta0}_${beta1}_n_${nm}.dat' u 1:4 w l ti 'dfs cor', \\
     '${beamlinename}_emitt_wfs_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat' u 1:4 w l ti 'wfs cor' " | gnuplot -persist


