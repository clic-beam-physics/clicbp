set term post eps enh col sol 24 lw 2

set out 'plot_bba.eps'

set xlabel 's [m]'
set ylabel '{/Symbol e}_y [{/Symbol m}m]'

plot 'outfiles-tilt/xfel_emitt_simple_b_5_n_100.dat' u 1:($5/10.0) w l ti '1:1' lw 2, \
 'outfiles-tilt/xfel_emitt_dfs_w_14_b_5_6_n_100.dat' u 1:($5/10.0) w l ti 'DFS' lw 2, \
 'outfiles-tilt/xfel_emitt_wfs_w_14_14_b_5_6_6_c_0.8_n_100.dat' u 1:($5/10.0) w l ti 'WFS' lw 2


