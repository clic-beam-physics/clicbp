addpath('../../XFEL_Code');
addpath('common');

pkg load parallel

XFEL_Code;

%% set a/lambda for the K-band structure
global RF
RF = setup_RF();

%% read in the particle distribution
T = load('files/xband_gun_xband_inj_out.dat.gz');
T = T(1:50:end,:); % takes 1 particle every 100 (for speed!)
Z = T(:,5) * 299792458000000; % micron/c
P = T(:,7) * 0.0005109989275678127; % GeV/c

% Prepares the beam
E = hypot(0.00051099893, P); % GeV
V = P ./ E; % c

P_ = polyfit(Z, P, 2); % parabolic fit
Z_max = -P_(2) / 2 / P_(1); % um
sigmaP = std(P .- polyval(P_, Z)); % GeV/c, uncorrelated momentum spread
P_max = polyval(P_, Z_max); % GeV/c, max momentum
Z -= Z_max; % moves the distribution to Z_max = 0

Beam.charge = 250; % pC
Beam.sigmaZ = std(Z); % um
Beam.n = P_(1); % GeV/c/um^2, curvature
Beam.sigmaP = sigmaP; % GeV/c, uncorrelated
Beam.P_max = P_max; % GeV/c, max momentum
Beam.mass = 0.00051099893 % [GeV/c^2]

global B0
B0 = Bunch1d(Beam.mass, Beam.charge * 6241509.343260179, [ Z P ]);     

%% load a good solution
global XLS
source('Kband_layout.dat')

%% main loop
global B2
B2 = track1();

sigmaz_um = std(B2.data(:,1))
energy_GeV = mean(B2.data(:,2))
kurt = kurtosis(B2.data(:,1))

%% Optimization
N = 500;

function [F_,X_] = func(idx)
    O = optimset('TolFun', 1e-10, 'TolX', 1e-10, 'MaxFunEvals', 100000, 'MaxIter', 100000);
    [X_, F_] = fminsearch ("merit2", 3 * randn(7,1), O);
end

[F,X] = pararrayfun(nproc, "func", 1:N, "UniformOutput", false);

F_min = Inf;
X_min = [];

for i=1:N
    if F{i}<F_min
        X_min = X{i};
        F_min = F{i};
    end
end

save -text optimum2_9GeV.dat X_min F_min

merit2(X_min);

global B2 B5
BC1_sigmaz_um = std(B2.data(:,1))
BC1_energy_GeV = mean(B2.data(:,2))
BC2_sigmaz_um = std(B5.data(:,1))
BC2_energy_GeV = mean(B5.data(:,2))

fid = fopen('Kband_layout_9GeV.dat', 'w');
fields = fieldnames(XLS);
for i = 1:length(fields)
    fprintf(fid, 'XLS.%s = %.15g;\n', fields{i}, eval( [ 'XLS.' fields{i} ]))
end
fclose(fid);
            
