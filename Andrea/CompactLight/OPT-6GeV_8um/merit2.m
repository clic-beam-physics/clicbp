function M = merit2(X)

    global XLS
    
    XLS.X1_phase   = constrain(X(1), -180, 180);
    XLS.X1_voltage = constrain(X(2), 1, 6.000);
    XLS.X2_phase   = constrain(X(3), -180, 180);
    XLS.X2_voltage = constrain(X(4), 1, 9.000);
    XLS.BC2_R56    = constrain(X(5), -0.050, 0.050);
    XLS.BC2_P0     = constrain(X(6), 0, 1);
    XLS.BC2_T566   = constrain(X(7), -0.1, 0.1);

    [B5,B4,B3,Length] = track2();
    
    Z = sort(B5.data(:,1));
    Z_ = linspace(0, 100, length(Z));
    Z_min = spline(Z_, Z, 1);
    Z_max = spline(Z_, Z, 99);
    
    sigmaz_um = Z_max - Z_min;
    energy_GeV = mean(B5.data(:,2));
    espread_GeV = std(B5.data(:,2));

    % 98% particle espread
    Z_min = spline(Z_, Z, 1);
    Z_max = spline(Z_, Z, 99);
    M = Z>=Z_min & Z<Z_max;
    espread_90slice_GeV = std(B5.data(M,2));
    
    skew = skewness(B5.data(:,1));
    kurt = kurtosis(B5.data(:,1));

    M = 10 * abs(sigmaz_um - 10) + ...
        10 * abs(energy_GeV - 9) + ...
        5 * abs(espread_GeV) + ...
        500 * abs(skew) + ...
        500 * abs(kurt - 1.8) + ...
	0.1 * Length;

    %disp([ M sigmaz_um energy_GeV skew kurt ]);
    
end
