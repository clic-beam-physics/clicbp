#! /opt/local/bin/octave -q --persist

addpath('../XFEL_Code');
addpath('common');

XFEL_Code;

%% Inizialization

global RF

a_div_lambda_K = 0.15;

RF = setup_RF(a_div_lambda_K);

function f = constrain(X, a, b)
  X = X / (1+abs(X));
  f = ((b-a)*X+(a+b))/2;
end

function X = iconstrain(f, a, b)
  f = (2*f-b-a)/(b-a);
  X = f / (1-abs(f));
end

global Beam B0

load 'in_dist.dat.gz';
Z = T(:,1) * 1e3; % um
P = T(:,2) / 1e3; % GeV/c
E = hypot(0.00051099893, P); % GeV
V = P ./ E; % c
T = -Z ./ V; % um/c, arrival time
Z = T; % Z = c*T, um

P_ = polyfit(Z, P, 2); % parabolic fit
Z_max = -P_(2) / 2 / P_(1); % um
sigmaP = std(P .- polyval(P_, Z)); % GeV/c, uncorrelated momentum spread
P_max = polyval(P_, Z_max); % GeV/c, max momentum
Z -= Z_max; % moves the distribution to Z_max = 0

Beam.charge = 250; % pC
Beam.sigmaZ = std(Z); % um
Beam.n = P_(1); % GeV/c/um^2, curvature
Beam.sigmaP = sigmaP; % GeV/c, uncorrelated
Beam.P_max = P_max; % GeV/c, max momentum
Beam.mass = 0.00051099893; % [GeV/c^2]

B0 = Bunch1d(Beam.mass, Beam.charge * 6241509.343260179, [ Z P ]);  

function [M,B0,B1,B2,B3] = track_bc1(X, verbose)

  global Beam

  P0 = Beam.P_max; % GeV, at injection
  P1 = 0.160;  % GeV, at first chicane
  P2 = 2.600;  % GeV, at second chicane
  P3 = 4.600;  % GeV, final energy

  sZ0 = Beam.sigmaZ; % um
  sZ1 = 64;          % um, after first chicane
  sZ2 = 8;           % um, after second chicane
  
  %% linacs
  X0_phase = constrain(X(1), -90, 90); % deg
  X1_phase = constrain(X(2), -90, 90); % deg

  %% lineariser
  K_phase   = constrain(X(3), -10, 370); % deg
  K_voltage = constrain(X(4), 0, 0.050); % GV

  dV1 = (P1 - P0) / cosd(X0_phase); % GV
  dV2 = (P2 - P1) / cosd(X1_phase); % GV
  
  X0_voltage = constrain(X(5), 0 * dV1, dV1); % GV
  X1_voltage = constrain(X(6), 0 * dV2, dV2); % GV

  %% chicane
  R0_56 = constrain(X(7),  -0.50, 0.50); % m, first chicane, must be negative
  T0_566 = 0 * -3/2 * R0_56; % m

  %% tracking
  global RF B0

  XFEL_Code

  X0 = CavityArray(RF.Xstructure);
  X0.set_voltage(X0_voltage);
  X0.set_phased (X0_phase);
  X0.disable_wakefields();
  
  K0 = CavityArray(RF.Kstructure);
  K0.set_voltage(K_voltage);
  K0.set_phased (K_phase);
  K0.disable_wakefields();
  
  L = Lattice();
  L.append(X0); 
  L.append(K0); 
  L.set_reference_momentum(Beam.P_max);
  B1 = L.track_z(B0);
  
  BC1 = Chicane(R0_56, T0_566);
  L = Lattice();
  L.append(BC1);
  L.set_reference_momentum(min(B1.data(:,2)));
  B2 = L.track_z(B1);

  X1 = CavityArray(RF.Xstructure);
  X1.set_voltage(X1_voltage);
  X1.set_phased (X1_phase);
  X1.disable_wakefields();
  
  L = Lattice();
  L.append(X1); 
  L.set_reference_momentum(mean(B2.data(:,2)));
  B3 = L.track_z(B2);

  P = polyfit(B3.data(:,1), B3.data(:,2), 2);
  curvature_1 = P(1);

  %%{  
  M(1) = (B2.get_std_1() / sZ1 - 1);
  M(2) = (B1.get_mean_2() / P1 - 1);
  M(3) = (B3.get_mean_2() / P2 - 1);
  M(4) = (curvature_1)
  %%}
  
  if nargin > 1
    B1_ = [ std(B1.data(:,1)) mean(B1.data(:,2)) ]
    B2_ = [ std(B2.data(:,1)) mean(B2.data(:,2)) ]
    B3_ = [ std(B3.data(:,1)) mean(B3.data(:,2)) ]

    X0_phase
    X1_phase
    
    K_phase
    K_voltage

    R0_56

    %% linacs
    X0_voltage
    X1_voltage
  end
  
end

function [M,B0,B1,B2,B3,B4,B5] = track(X, verbose)

  global X_bc1
  global Beam

  
  X_bc1(10) = 0;
  X_bc1(8:10) = X;
  X = X_bc1;
  
  P0 = Beam.P_max; % GeV, at injection
  P1 = 0.160;  % GeV, at first chicane
  P2 = 2.600;  % GeV, at second chicane
  P3 = 4.600;  % GeV, final energy

  sZ0 = Beam.sigmaZ; % um
  sZ1 = 64;          % um, after first chicane
  sZ2 = 8;           % um, after second chicane
  
  %% linacs
  X0_phase = constrain(X(1), -90, 90); % deg
  X1_phase = constrain(X(2), -90, 90); % deg

  %% lineariser
  K_phase   = constrain(X(3), -10, 370); % deg
  K_voltage = constrain(X(4), 0, 0.050); % GV

  dV1 = (P1 - P0) / cosd(X0_phase); % GV
  dV2 = (P2 - P1) / cosd(X1_phase); % GV
  
  X0_voltage = constrain(X(5), 0 * dV1, dV1); % GV
  X1_voltage = constrain(X(6), 0 * dV2, dV2); % GV

  %% chicane
  R0_56 = constrain(X(7),  -0.50, 0.50); % m, first chicane, must be negative
  T0_566 = 0 * -3/2 * R0_56; % m
  
  %%
  X2_phase = constrain(X(8), 0, 30); % deg
  dV3 = (P3 - P2) / cosd(X2_phase); % GV

  X2_voltage = constrain(X(9), 0 * dV3, dV3); % GV
  
  %% chicane
  R1_56 = constrain(X(10), -0.50, 0.0); % m, second chicane, must be negative
  T1_566 = 0 * -3/2 * R1_56; % m

  %% tracking
  global RF B0

  XFEL_Code

  X0 = CavityArray(RF.Xstructure);
  X0.set_voltage(X0_voltage);
  X0.set_phased (X0_phase);
  X0.disable_wakefields();
  
  K0 = CavityArray(RF.Kstructure);
  K0.set_voltage(K_voltage);
  K0.set_phased (K_phase);
  K0.disable_wakefields();
  
  L = Lattice();
  L.append(X0); 
  L.append(K0); 
  L.set_reference_momentum(Beam.P_max);
  B1 = L.track_z(B0);
  
  BC1 = Chicane(R0_56, T0_566);
  L = Lattice();
  L.append(BC1);
  L.set_reference_momentum(min(B1.data(:,2)));
  B2 = L.track_z(B1);

  X1 = CavityArray(RF.Xstructure);
  X1.set_voltage(X1_voltage);
  X1.set_phased (X1_phase);
  X1.disable_wakefields();
  
  L = Lattice();
  L.append(X1); 
  L.set_reference_momentum(mean(B2.data(:,2)));
  B3 = L.track_z(B2);
  
  BC2 = Chicane(R1_56, T1_566);
  L = Lattice();
  L.append(BC2);
  L.set_reference_momentum(min(B3.data(:,2)));
  B4 = L.track_z(B3);
  
  X2 = CavityArray(RF.Xstructure);
  X2.set_voltage(X2_voltage);
  X2.set_phased (X2_phase);
  X2.disable_wakefields();
  
  L = Lattice();
  L.append(X2);
  L.set_reference_momentum(mean(B4.data(:,2)));
  B5 = L.track_z(B4);
  
  P = polyfit(B3.data(:,1), B3.data(:,2), 2);
  curvature_1 = P(1);

  P = polyfit(B5.data(:,1), B5.data(:,2), 3);
  curvature_2 = P(1);

  %%{  
  M(1) = (B2.get_std_1() / sZ1 - 1);
  M(2) = (B4.get_std_1() / sZ2 - 1);
  M(3) = (B1.get_mean_2() / P1 - 1);
  M(4) = (B3.get_mean_2() / P2 - 1);
  M(5) = (B5.get_mean_2() / P3 - 1);
  M(6) = (curvature_1);
  M(7) = (curvature_2)
  %%}
  
  if nargin > 1
    B1_ = [ std(B1.data(:,1)) mean(B1.data(:,2)) ]
    B2_ = [ std(B2.data(:,1)) mean(B2.data(:,2)) ]
    B3_ = [ std(B3.data(:,1)) mean(B3.data(:,2)) ]
    B4_ = [ std(B4.data(:,1)) mean(B4.data(:,2)) ]
    B5_ = [ std(B5.data(:,1)) mean(B5.data(:,2)) ]

    X0_phase
    X1_phase
    X2_phase
    
    K_phase
    K_voltage

    R0_56
    R1_56

    %% linacs
    X0_voltage
    X1_voltage
    X2_voltage
  end
  
end

%% Run the optimization

global X_bc1
O = optimset('TolFun', 1e-10, 'TolX', 1e-10, 'MaxIter', 100000);
%[X,fval] = fminunc("track", zeros(10,1), O)

[X_bc1,fval] = fsolve("track_bc1", zeros(7,1), O)

track_bc1(X_bc1, true)
save -text X_bc1.dat X_bc1

[X,fval] = fsolve("track", zeros(3,1), O)
save -text X.dat X

[M,B0,B1,B2,B3,B4,B5] = track(X, true);

figure(1)
clf
hold on
scatter(B1.data(:,1), B1.data(:,2));
scatter(B2.data(:,1), B2.data(:,2));
scatter(B3.data(:,1), B3.data(:,2));
%scatter(B4.data(:,1), B4.data(:,2));
%scatter(B5.data(:,1), B5.data(:,2));

figure(2)
subplot(3,2,1)
scatter(B0.data(:,1), B0.data(:,2));

subplot(3,2,2)
scatter(B1.data(:,1), B1.data(:,2));

subplot(3,2,3)
scatter(B2.data(:,1), B2.data(:,2));

subplot(3,2,4)
scatter(B3.data(:,1), B3.data(:,2));

subplot(3,2,5)
scatter(B4.data(:,1), B4.data(:,2));

subplot(3,2,6)
scatter(B5.data(:,1), B5.data(:,2));

print -dpng plot_Kband_new3.png
