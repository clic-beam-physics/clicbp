function RF = setup_RF(a_div_lambda_K)
    XFEL_Code;
    
    clight = 0.29979246; % m/ns
    
    ## SPARC X-band structure parameters
    RF.Xcell = Cell();
    RF.Xcell.frequency = 11.9942; % GHz
    RF.Xcell.a = 0.00320; % m
    RF.Xcell.g = 0.006495; % m
    RF.Xcell.l = clight / RF.Xcell.frequency * (2*pi/3) / (2*pi); % m, 2*pi/3 phase advance
    
    ## K-band structure parameters
    RF.Kcell = Cell();
    RF.Kcell.frequency = 3 * 11.9942; % GHz
    lambda_K = clight / RF.Kcell.frequency; % m
    RF.Kcell.a = a_div_lambda_K * lambda_K; % m
    RF.Kcell.g = 0.006495 / 3; % m
    RF.Kcell.l = lambda_K * (2*pi/3) / (2*pi); % m, 2*pi/3 phase advance
    
    RF.Xstructure = AcceleratingStructure(RF.Xcell, 60);
    RF.Xstructure.max_gradient = 66e-3; % GV/m

    RF.Kstructure = AcceleratingStructure(RF.Kcell, 86);
    RF.Kstructure.max_gradient = 300e-3; % GV/m

endfunction