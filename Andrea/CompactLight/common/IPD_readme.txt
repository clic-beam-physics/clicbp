On 7/4/18 8:10 PM, Mohsen Dayyani Kelisani wrote:
Dear Steffen and Andrea

Please find the attached file (IPD) for the distribution of the
particles at the end of 164 MeV compact electron injector. The total
charge is 100 pC.

Here, there are 6 different columns for 10000 particles as following 

1-X(mm)
2-Y(mm)
3-Z(mm)

4-Px(permil)
5-Py(permil)
6-Pz(permil)

Where

Px=Gamma*BetaX
Py=Gamma*BetaY
Pz=Gamma*BetaZ

Indicate the normalized momentum of the particles in x,y and z directions, respectively.

Please do not hesitate to ask me if you need any more information.

Sincerely

Mohsen

