function Wl = W_long(s) % s [m]. return value is in V/pC/m 
    global Kcell
    a = Kcell.a;
    g = Kcell.g;
    l = Kcell.l;   
    pow = @(x,e) x**e;
    C_LIGHT = 299792458; % velocity of light [m/s]
    Z0 = (C_LIGHT*4e-7*pi); % characteristic impedance [Ohm]
    s0 = 0.41 * pow(a,1.8) * pow(g,1.6) / pow(l,2.4);
    Wl = Z0 * C_LIGHT / pi / (a**2) * exp(-sqrt(s/s0)) / 1e12;
    Wl(find(s<0)) = 0;
    Wl(find(s==0)) *= 0.5;
endfunction
