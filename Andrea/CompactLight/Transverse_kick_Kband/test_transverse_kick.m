clight = 299792458; % m/s
global Kcell
Kcell.frequency = 3 * 11.994; % GHz

lambda_K = clight / Kcell.frequency / 1e9; % m
Lstruct = 1.25; % m, 1 superstructures
Kcell.a = 0.003; % m
Kcell.l = lambda_K / 3; % m, 2*pi/3 phase advance
Kcell.g = Kcell.l - 0.0006; % m

Lstruct = 0.75; % m, 1 superstructures
Kcell.a = 0.002; % m
Kcell.l = lambda_K / 3; % m, 2*pi/3 phase advance
Kcell.g = Kcell.l - 0.0006; % m

Ne = 250; % pC
sigmaZ = 300e-6; % m, 2.12 ps
P = 230e6; % eV/c total momentum

sigmaX = 0.07302165926933421; % mm

DX = 0.15 * sigmaX; % mm transverse offset

a = Kcell.a; % m
g = Kcell.g; % m
l = Kcell.l; % m
C_LIGHT = 299792458; % velocity of light [m/s]
Z0 = (C_LIGHT*4e-7*pi); % characteristic impedance [Ohm]
linear_kick = Ne * Lstruct * DX * 2*C_LIGHT*Z0/(a^4*pi) / 1e15 % V/pC/m/mm/m * pC * m * mm = V/m

Nbins = 1024;

h = linspace(-3,3,Nbins); % sigma
Z = h * sigmaZ; % m
Q = Ne * normpdf(h) * 6 / Nbins; % pC
%%Q = Ne * unifpdf(h) * 6 / Nbins; % pC

S = Z .- min(Z); % m
dS = 6 * sigmaZ / Nbins; % m

Wl = Lstruct * W_long(S); % V/pC/m * m = V/pC
Wt = Lstruct * DX * W_transv(S); % V/pC/m/mm * m * mm = V/pC

dPx = conv(Q, Wt); % V

S_ = linspace(0, dS*length(dPx), length(dPx));
Q_ = Q;
Q_(length(dPx)) = 0;

figure(1)
clf; hold on

plot(S_*1e3, W_transv(S_) * Ne * Lstruct * DX / 1e3, 'b--');
hax = plotyy(S_*1e3, dPx / 1e3, S_*1e3, Q_ / dS / 1e3);

xlabel(hax(1), 'S [mm]')
ylabel(hax(1), 'Vt [keV]')
ylabel(hax(2), 'Q [pC/mm]')
legend('single-particle wake', 'total wake', 'charge profile')

print -depsc2 -F:14 plot_Vt.eps

average_kick_keV = sum(Q_.*dPx) / sum(Q_) / 1e3
average_kick_mrad = average_kick_keV / P * 1e6

linear_wake_V_pC_mm_m = average_kick_keV / Ne / Lstruct / DX * 1e3

figure(2)
clf; hold on

Q(:) = 0;
Q(1) = Ne;

dPx = conv(Q, Wt); % V
plot(S_, linear_kick * S_)
plot(S_, W_transv(S_) * Ne * Lstruct * DX);
plot(S_, dPx, 'r-');



