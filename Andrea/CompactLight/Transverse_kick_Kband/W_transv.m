function Wt = W_transv(s) % s [m]. return value is in V/pC/m/mm
    global Kcell
    a = Kcell.a; % m
    g = Kcell.g; % m
    l = Kcell.l; % m
    pow = @(x,e) x**e;
    C_LIGHT = 299792458; % velocity of light [m/s]
    Z0 = (C_LIGHT*4e-7*pi); % characteristic impedance [Ohm]
    s0 = 0.169 * pow(a,1.79) * pow(g,0.38) / pow(l,1.17); % m
    sqrt_s_s0 = sqrt(s/s0);
    Wt = 4 * Z0 * s0 * C_LIGHT / pi / (a**4) * (1 - (1 + sqrt_s_s0) .* exp(-sqrt_s_s0)) / 1e15;
    Wt(find(s<0)) = 0;
endfunction
