addpath('../../');

%% Load RF-Track
RF_Track;

%% Global variables for finding the phase
global RF TW T B

%% Load the beam
T = load('for_andrea/part_10k_2ps_0.25mm.dat');
T(2:end,1:7) += repmat(T(1,1:7), size(T,1)-1,1); % ASTRA convention

% create a 10-column matrix for Bunch6dT
B = zeros(size(T,1), 10);
B(:,1) = T(:,1) * 1e3; % mm
B(:,2) = T(:,4) / 1e6; % MeV/c
B(:,3) = T(:,2) * 1e3; % mm
B(:,4) = T(:,5) / 1e6; % MeV/c
B(:,5) = T(:,3) * 1e3; % mm
B(:,6) = T(:,6) / 1e6; % MeV/c
B(:,7) = RF_Track.electronmass; % MeV/c^2
B(:,8) = -1; % e^+
B(:,9) = abs(T(:,8)) * RF_Track.nC; % e^+
B(:,10) = T(:,7) * RF_Track.ns; % mm/c

%% Define the beam
B0 = Bunch6dT(B);

%% Load field maps of the gun
T = load('for_andrea/X-BAND_GUN_4.6CELL.DAT');
Gun.S  = T(:,1); % m
Gun.Ez = T(:,2) * 250e6; % V/m
Gun.freq = 11.9917e9; % Hz

T = load('for_andrea/XBAND_CAV_SOL.DAT');
S_mask = T(:,1) >= 0.0;
Solenoid.S  = T(S_mask,1); % m
Solenoid.Bz = T(S_mask,2) * 0.482; % T

% Regularize the mesh
Z_max = max(Solenoid.S(end), Gun.S(end)); % m
Z_dlt_S = (Solenoid.S(end) - Solenoid.S(1)) / (length(Solenoid.S)-1); % m
Z_dlt_G = (Gun.S(end) - Gun.S(1)) / (length(Gun.S)-1); % m
Z_dlt = min(Z_dlt_S, Z_dlt_G); % m
Z_N   = floor(Z_max / Z_dlt);
Z_len = Z_N * Z_dlt; % m
Z = linspace(0, Z_len, Z_N+1); % m
%
Bz = interp1 (Solenoid.S(:), Solenoid.Bz(:), Z, 'spline');
Ez = interp1 (Gun.S(:), Gun.Ez(:), Z, 'spline');
Bz(isnan(Bz)) = 0.0;
Ez(isnan(Ez)) = 0.0;

% Create an RF-Track element
RF = RF_Field_1d(Ez, Bz, Z_dlt, Z_max, Gun.freq, -1);
RF.set_t0(0.0);
RF.set_phid(235.21);

%% Load the field map of the TW structure
FID = fopen('for_andrea/TWS_XBAND_120DEG.DAT');
T = cell2mat(textscan(FID, '%f'));
fclose(FID);

% Prepare for the generation of the TW
TWS.z1 = T(1); % m
TWS.z2 = T(2); % m
TWS.n  = T(3);
TWS.m  = T(4);
TWS.S  = T(5:2:end); % m
TWS.Ez = T(6:2:end); % V/m
TWS.C_numb = 108; % such that C_numb * n / m is integer
TWS.W = TWS.z2 - TWS.z1; % m, width
TWS.C_len = TWS.W / TWS.m; % m
TWS.Lambda = TWS.W / TWS.n; % m
TWS.K = 2*pi / TWS.Lambda; % 1/m
TWS.freq =  RF_Track.clight / TWS.Lambda; % Hz

Z_len = TWS.S(end) - TWS.S(1) + TWS.C_len * (TWS.C_numb - TWS.m); % m
Z_dlt = (TWS.S(end) - TWS.S(1)) / (length(TWS.S)-1); % m
Z_N  = round(Z_len / Z_dlt);

Z_N1 = ceil(TWS.z1 / Z_dlt) + 1;
Z_N2 = floor((TWS.z1 + TWS.C_numb * TWS.C_len) / Z_dlt) - 1;

Z_0 = zeros(Z_N,1); % m
Z_1 = zeros(Z_N,1); % m
for i=1:Z_N
    Z_i = Z_len * (i - 1) / Z_N; % m
    if Z_i < TWS.z1 %% entrance coupler
        Z_0(i) = Z_i;
    else
        Z_cell_i = floor((Z_i - TWS.z1) / TWS.C_len);
        if Z_cell_i >= TWS.C_numb %% exit coupler
            Z_0(i) = TWS.z2 + Z_i - (TWS.z1 + TWS.C_numb * TWS.C_len);
        else %% cells
            Z_0(i) = TWS.z1 + rem(Z_i - TWS.z1, TWS.W);
            Z_1(i) = TWS.z1 + rem(Z_i - TWS.z1 + TWS.C_len, TWS.W);
        end
    end
end

% Build the traveling wave
Ez_0 = interp1 (TWS.S(:), TWS.Ez(:), Z_0, 'spline');
Ez_1 = interp1 (TWS.S(:), TWS.Ez(:), Z_1, 'spline');

KL = TWS.K * TWS.C_len;
Ez = Ez_0;
Ez(Z_N1:Z_N2) = conj((Ez_1(Z_N1:Z_N2) - Ez_0(Z_N1:Z_N2) * exp(sqrt(-1)*KL)) / sqrt(-1)); % V/m
Ez(Z_N1:Z_N2) /= real(Ez(Z_N1));

% scale to 87 MV/m
Ez *= 87e6;

%{
load('xband_108cell_120deg.txt');
Ez = complex(allmap(:,2), allmap(:,3));
Z_dlt = mean(diff(allmap(:,1)));
Z_len = allmap(end,1);
Z_N = floor(Z_len/Z_dlt) + 1;
%}

figure(1)
clf
hold on
plot(linspace(0, Z_len, Z_N), real(Ez) / 1e6);
plot(linspace(0, Z_len, Z_N), imag(Ez) / 1e6);
xlabel('S [m]');
ylabel('Ez [MV/m]');
print -dpng plot_TWS.png

% Create an RF-Track element
TW = RF_Field_1d(Ez, Z_dlt, Z_len, TWS.freq, -1);
TW.set_t0(0.0);
TW.set_phid(249.97);
TW.set_static_Bfield(0, 0, 0.3);

%% Prepare for tracking
T = TrackingOpts();
T.dt_mm = Z_dlt * 0.1e3; % mm/c
T.nsteps = 0; % if 0, tracks all the way to the end of the lattice
T.sc_nsteps = 0; % if != 0, applies a space charge kick every sc_nsteps steps
T.backtrack_at_entrance = false; % bring the foremost particle to S=0 before strarting to track
T.odeint_algorithm = 'rk2'; % pick your favorite algorithm, 'rk2', 'rkf45'
T.wp_basename = 'test';
T.wp_gzip = true;
T.wp_nsteps = 0; % X [mm] Px [MeV/c] Y [mm] Py [MeV/c] S [mm] Pz [MeV/c]
T.verbose = 0;

%% Tracking
if true
    function Pz = longitudinal_momentum(phid)
        global RF TW T B
        RF_Track;
        RF.set_phid(phid);
        L = Lattice();
        L.append(RF);
        %L.append(Drift(0.3));
        %L.append(TW);
        B0 = Bunch6dT(B(1,:));
        B1 = L.track(B0, T);
        Pz = mean(B1.get_phase_space("%Pz"))
    endfunction
    phid = fminbnd(@(X) abs(longitudinal_momentum(X)-6.939120809561594), 0, 360)
    RF.set_phid(phid);
endif

%T.wp_nsteps = 100; % X [mm] Px [MeV/c] Y [mm] Py [MeV/c] S [mm] Pz [MeV/c]

%% Lattice definition
L = Lattice();
L.append(RF);
L.append(Drift(0.5 - RF.get_length()));
%L.append(Drift(0.3));
%L.append(TW);

tic
B1 = L.track(B0, T);
toc

%% Save datafiles
A0 = B0.get_phase_space('%x %Px %y %Py %t0 %Pz');
save -text input_beam_RFGun.dat A0

A1 = B1.get_phase_space('%x %Px %y %Py %dt %Pz');
save -text output_beam_RFGun.dat A1

%% Plots
figure(2)
clf
hold on
scatter(B1.get_phase_space('%x'), B1.get_phase_space('%Px'))
xlabel('x [mm]');
ylabel('P_x [MeV/c]');
print -dpng plot_xPx.png

figure(3)
clf
hold on
scatter(B1.get_phase_space('%y'), B1.get_phase_space('%Py'))
xlabel('y [mm]');
ylabel('P_y [MeV/c]');
print -dpng plot_yPy.png

figure(4)
clf
hold on
scatter(B1.get_phase_space('%dt') / RF_Track.ns, B1.get_phase_space('%Pz'))
xlabel('dt [ns]');
ylabel('P_z [MeV/c]');
print -dpng plot_dtPz.png

% Plot field
S_axis = linspace(0, RF.get_length()*1e3, 100); % mm
Tx = [];
Tz = [];
for S = S_axis
    [E,B0_] = RF.get_field(0, 0, S, 0);
    [E,B1_] = RF.get_field(0, 1, S, 0);
    [E,B2_] = RF.get_field(0, 2, S, 0);
    Tx = [ Tx ; S B0_(1) B1_(1) B2_(1) ];
    Tz = [ Tz ; S B0_(3) B1_(3) B2_(3) ];
end

figure(5)
clf
hold on
plot(Tx(:,1), Tx(:,2), 'b-');
plot(Tx(:,1), Tx(:,3), 'r-');
plot(Tx(:,1), Tx(:,4), 'g-');
legend('r=0 mm ', 'r=1 mm ', 'r=2 mm ');
xlabel('S [mm]');
ylabel('B_x [T]');
print -dpng plot_RF_Bx.png

figure(6)
clf
hold on
plot(Tz(:,1), Tz(:,2), 'b-');
plot(Tz(:,1), Tz(:,3), 'r-');
plot(Tz(:,1), Tz(:,4), 'g-');
legend('r=0 mm ', 'r=1 mm ', 'r=2 mm ');
xlabel('S [mm]');
ylabel('B_z [T]');
print -dpng plot_RF_Bz.png

T = L.get_transport_table(['%t %emitt_x %emitt_y %emitt_4d %sigma_X %sigma_Y %rmax']);
figure(7)
clf
hold on
plot(T(:,1), T(:,2), T(:,1), T(:,3), T(:,1), T(:,4));
legend('emitt_x ', 'emitt_y ', 'emitt_{4d} ');
xlabel('t [mm/c]');
ylabel('emitt [mm.mrad]');
print -dpng plot_emitt.png

figure(8)
clf
hold on
plot(T(:,1), T(:,5), T(:,1), T(:,6), T(:,1), T(:,7));
legend('sigma_x ', 'sigma_y ', 'Envelope ');
xlabel('t [mm/c]');
ylabel('mm');
print -dpng plot_sigma.png

T = L.get_transport_table(['%t %sigma_Px %sigma_Py']);
figure(9)
clf
hold on
Pz_ref = B1.get_phase_space("%Pz")(1);
plot(T(:,1), 1e3*T(:,2)/Pz_ref, T(:,1), 1e3*T(:,3)/Pz_ref);
legend('sigma_{x''} ', 'sigma_{y''} ');
xlabel('t [mm/c]');
ylabel('mrad');
print -dpng plot_sigma_prime.png

