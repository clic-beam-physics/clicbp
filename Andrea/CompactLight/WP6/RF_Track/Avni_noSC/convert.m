A = load('xband_gun1_scoff.0040.001');
A(2:end,1:7) += repmat(A(1,1:7), size(A,1)-1,1); % ASTRA convention

save -text xband_gun1_scoff.0040.001.dat A;

A = load('xband_gun1_scoff.0050.001');
A(2:end,1:7) += repmat(A(1,1:7), size(A,1)-1,1); % ASTRA convention

save -text xband_gun1_scoff.0050.001.dat A;

A = load('xband_gun1_scoff.0180.001');
A(2:end,1:7) += repmat(A(1,1:7), size(A,1)-1,1); % ASTRA convention

save -text xband_gun1_scoff.0180.001.dat A;
