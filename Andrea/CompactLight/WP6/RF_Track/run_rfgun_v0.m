addpath('../../');

RF_Track;

%% Load field maps
T = load('for_andrea/X-BAND_GUN_4.6CELL.DAT');
Gun.S  = T(:,1); % m
Gun.Ez = T(:,2) * 250e6; % V/m
Gun.freq = 11.9917e9; % Hz

T = load('for_andrea/XBAND_CAV_SOL.DAT');
S_mask = T(:,1) >= 0.0;
Solenoid.S  = T(S_mask,1); % m
Solenoid.Bz = T(S_mask,2) * 0.482; % T

FID = fopen('for_andrea/TWS_XBAND_120DEG.DAT');
T = cell2mat(textscan(FID, '%f'));
fclose(FID);

%%% Regularize the mesh
Z_max = max(Solenoid.S(end), Gun.S(end)); % m
Z_dlt_S = (Solenoid.S(end) - Solenoid.S(1)) / (length(Solenoid.S)-1); % m
Z_dlt_G = (Gun.S(end) - Gun.S(1)) / (length(Gun.S)-1); % m
Z_dlt = min(Z_dlt_S, Z_dlt_G); % m
Z_N   = floor(Z_max / Z_dlt);
Z_len = Z_N * Z_dlt; % m
Z = linspace(0, Z_len, Z_N+1); % m
%
Bz = interp1 (Solenoid.S(:), Solenoid.Bz(:), Z, 'spline');
Ez = interp1 (Gun.S(:), Gun.Ez(:), Z, 'spline');
Bz(isnan(Bz)) = 0.0;
Ez(isnan(Ez)) = 0.0;

%% Create an RF-Track element
RF = RF_Field_1d(Ez, Bz, Z_dlt, Z_max, Gun.freq, -1);
RF.set_t0(0.0);
RF.set_phid(197);

%% TW Structure
TWS.z1 = T(1); % m
TWS.z2 = T(2); % m
TWS.n  = T(3);
TWS.m  = T(4);
TWS.S  = T(5:2:end); % m
TWS.Ez = T(6:2:end) * 87e6; % V/m
TWS.C_numb = 108; % such that C_numb * n / m is integer
TWS.W = TWS.z2 - TWS.z1; % m, width
TWS.C_len = TWS.W / TWS.m; % m
TWS.Lambda = TWS.W / TWS.n; % m
TWS.K = 2*pi / TWS.Lambda; % 1/m
TWS.freq =  RF_Track.clight / TWS.Lambda; % Hz

Z_len = TWS.S(end) - TWS.S(1) + TWS.C_len * (TWS.C_numb - TWS.m); % m
Z_dlt = (TWS.S(end) - TWS.S(1)) / (length(TWS.S)-1); % m
Z_N = round(Z_len / Z_dlt);
Z_z2 = TWS.z2 + (TWS.C_numb-1) * TWS.C_len; % m

Z_0 = zeros(Z_N,1); % m
for i=1:Z_N
    Z_i = Z_len * (i - 1) / Z_N; % m
    if Z_i < TWS.z1 %% entrance coupler
        Z_0(i) = Z_i;
    else
        Z_cell_i = floor((Z_i - TWS.z1) / TWS.C_len);
        if Z_cell_i >= TWS.C_numb %% exit coupler
            Z_0(i) = TWS.z2 + Z_i - (TWS.z1 + TWS.C_numb * TWS.W / TWS.m);
        else %% cells
            Z_0(i) = TWS.z1 + rem(Z_i - TWS.z1, TWS.W);
        end
    end
end

Z_1 = zeros(Z_N,1); % m
for i=1:Z_N
    Z_i = Z_len * (i - 1) / Z_N + TWS.C_len; % m
    if Z_i < TWS.z1 %% entrance coupler
        Z_1(i) = Z_i;
    else
        Z_cell_i = floor((Z_i - TWS.z1) / TWS.C_len);
        if Z_cell_i >= TWS.C_numb %% exit coupler
            Z_1(i) = TWS.z2 + Z_i - (TWS.z1 + TWS.C_numb * TWS.W / TWS.m);
        else %% cells
            Z_1(i) = TWS.z1 + rem(Z_i - TWS.z1, TWS.W);
        end
    end
end

%% Build the traveling wave
Ez_0 = interp1 (TWS.S(:), TWS.Ez(:), Z_0, 'spline');
Ez_1 = interp1 (TWS.S(:), TWS.Ez(:), Z_1, 'spline');
Ez_1(isnan(Ez_1)) = 0;

kl = TWS.K * TWS.C_len;
Ez = sqrt(-1) * (Ez_1 - Ez_0 * exp(sqrt(-1)*kl)) / (2*sin(kl)); % V/m

figure(1)
clf
hold on
plot(linspace(0, Z_len, Z_N), real(Ez));
plot(linspace(0, Z_len, Z_N), imag(Ez));

TW = RF_Field_1d(Ez, Z_dlt, Z_len, TWS.freq, -1);
TW.set_t0(0.0);
TW.set_phid(21);
TW.set_static_Bfield(0, 0, 0.3);

%% Define the lattice
L = Lattice();
L.append(RF);
L.append(Drift(0.3));
L.append(TW);

%% Load the beam
T = load('for_andrea/part_10k_2ps_0.25mm.dat');
T(2:end,1:7) += repmat(T(1,1:7), size(T,1)-1,1); % ASTRA convention

% create a 10-column matrix for Bunch6dT
B = zeros(size(T,1), 10);
B(:,1) = T(:,1) * 1e3; % mm
B(:,3) = T(:,2) * 1e3; % mm
B(:,5) = T(:,3) * 1e3; % mm
B(:,2) = T(:,4) / 1e6; % MeV/c
B(:,4) = T(:,5) / 1e6; % MeV/c
B(:,6) = T(:,6) / 1e6; % MeV/c
B(:,7) = RF_Track.electronmass; % MeV/c^2
B(:,8) = -1; % e^+
B(:,9) = T(:,8) * -RF_Track.nC; % e^+
B(:,10) = T(:,7) * RF_Track.ns; % mm/c

%% Define the beam
B0 = Bunch6dT(B);

%% Prepare for tracking
T = TrackingOpts();
T.dt_mm = Z_dlt * 1e3; % mm/c
T.nsteps = 0; % if 0, tracks all the way to the end of the lattice
T.sc_nsteps = 0; % if != 0, applies a space charge kick every sc_nsteps steps
T.backtrack_at_entrance = false; % bring the foremost particle to S=0 before strarting to track
T.odeint_algorithm = 'leapfrog'; % pick your favorite algorithm, 'rk2', 'rkf45'
T.wp_basename = 'test';
T.wp_gzip = true;
T.wp_nsteps = 0; % X [mm] Px [MeV/c] Y [mm] Py [MeV/c] S [mm] Pz [MeV/c]

%% Tracking
tic
B1 = L.track(B0, T);
toc

%% Plots

figure(2)
clf
hold on
scatter(B0.get_phase_space('%Px'), B0.get_phase_space('%Pz'))
scatter(B1.get_phase_space('%Px'), B1.get_phase_space('%Pz'))

figure(3)
clf
hold on
scatter(B0.get_phase_space('%S'), B0.get_phase_space('%K'))
scatter(B1.get_phase_space('%S'), B1.get_phase_space('%K'))


A0 = B0.get_phase_space('%x %Px %y %Py %t0 %Pz');
save -text input_beam_RFGun.dat A0

A1 = B1.get_phase_space('%x %Px %y %Py %t %Pz');
save -text output_beam_RFGun.dat A1

