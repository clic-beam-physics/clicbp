1;
% file to generate distributions
% 


A = importdata('for_andrea/TWS_XBAND_120DEG.DAT');

firstc = A(1,1);                #; location where initial coupler cell ends
lastc = A(1,2);                 #; ocation where exit coupler cell starts 
n=A(1,3);
ncellmap=A(1,4);                #; (* number of cells for TW section *)
phasead    = 2*pi*n/ncellmap;   #; (*phase advance *)

fld=A(2:end,1:2);               #; (* z E along structure *)
endl = fld(end,1);              #; total length of map

ltwsec     = lastc - firstc;    #;  (*length of TW section*)
cellL      = ltwsec/ncellmap;   #;  (*length of TW cells*)
ncellconst = 108;               #;  (* no of cells to be constructed*)
Ltwsec     = cellL*ncellconst;  #;  total tw section 
cellL

zcouinstrt = 0;
zcouinend  = firstc;
ztwstrt    = zcouinend;
ztwend     = zcouinend + Ltwsec;
zcoupoustrt= ztwend;
zcoupouend = ztwend + endl - lastc;



[nl0 nc]=size(fld);

coupin = []; twsec = []; coupou = [];

for i=1:nl0
    z=fld(i,1);
    
    if (z<= firstc) 
        coupin=[coupin ; fld(i,:)];
    endif 

    if ( z>= firstc && z <=  lastc) 
        twsec=[twsec ; fld(i,:)];
    endif 
    
    if (z >= lastc) 
        coupou=[coupou ; fld(i,:)];
    endif 
    
endfor


tmpcouz=coupou(:, 1) + zcoupoustrt - lastc;
tmpcoua=coupou(:, 2);
coupou = [ tmpcouz, tmpcoua];


twall = twsec;

for i=1:floor(ncellconst/ncellmap + 2)
    zistart = twall(end,1);
    ztmp=twsec(:,1)+ zistart - firstc;
    atmp=twsec(:,2);
    tmp=[ztmp'; atmp']';
    twall=[twall; tmp];
endfor

indx1=find(twall(:,1) >=  ztwstrt & twall(:,1) <= ztwend);
indx2=find(twall(:,1) >=  ztwstrt+cellL & twall(:,1) <= ztwend+cellL);
twallz=twall(indx1,:);
twallzc=twall(indx2,:);

[s0 n0]=size(coupin);
[s1 n1]=size(twallz);
[s2 n2]=size(twallzc);
[s3 n3]=size(coupou);
ntw=s1;
nsw=s0;

if (s2 > ntw); ntw=s2; end
if (s3 > nsw); nsw=s3; end

zint1=[linspace(ztwstrt, ztwend,2*ntw)]';
zint2=[linspace(ztwstrt+cellL, ztwend+cellL,2*ntw)]';

twallzz=interp1(twallz(:,1),twallz(:,2),zint1,'linear');
twallzcc=interp1(twallzc(:,1),twallzc(:,2),zint2,'linear');

zsw1=[linspace(zcouinstrt, zcouinend,2*nsw)]';
zsw2=[linspace(zcoupoustrt, zcoupouend,2*nsw)]';

coupinb=interp1(coupin(:,1),coupin(:,2),zsw1,'linear');
coupoub=interp1(coupou(:,1),coupou(:,2),zsw2,'linear');


phase0=pi/2; 

twsect0=(twallzcc-exp(j*phasead)*twallzz)*exp(-j*phase0);
twsect0=twsect0/real(twsect0(2));
coupinb0=coupinb*j*exp(-j*phase0);
coupoub0=coupoub*j*exp(-j*phase0);

phase1=pi;

twsect1=(twallzcc-exp(j*phasead)*twallzz)*exp(-j*phase1);
twsect1=twsect1/real(twsect0(2));
coupinb1=coupinb*j*exp(-j*phase1);
coupoub1=coupoub*j*exp(-j*phase1);

zall=[zsw1; zint1; zsw2];
twfr=[real(coupinb0); real(twsect0); real(coupoub0)];
twfi=[real(coupinb1); real(twsect1); real(coupoub1)];

# size(zall)
# size(twfr)
# size(twfi)

dz=0.00005;
nfin=floor((zcoupouend-zcouinstrt)/dz);

zfinal=[linspace(zcouinstrt, zcoupouend,nfin)]';

twfrf=interp1(zall,twfr,zfinal,'linear');
twfif=interp1(zall,twfi,zfinal,'linear');




# fid = fopen('sband_93cell_120deg.txt','wt');
# fprintf(fid, 'z\t ReEz\t ImEz \n');
# 
# # formatSpec = '%12.8f \n';
# fprintf(fid, '%12.9f %12.9f %12.9f \n',allmap)



plot(zfinal,twfrf)
hold on

plot(zfinal,twfif)
legend ("Re Ez","Im Ez");


# allmap=['z' ,'ReEz','ImEz' ];
allmap=[ zfinal, twfrf*87e6, twfif*87e6];



save -text xband_108cell_120deg.txt allmap
