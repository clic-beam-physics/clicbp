#!/usr/bin/tclsh

proc  generate_particles { args } {
  if { [llength $args] < 4} {
    puts "not enough argument for generate_particles routine"
    puts "usage  generate_particles dist_name=\$name  pulse_length=\$tt(ps) radius=\$R(mm) npart=\$N(#)\n ERROR "
    puts "please enter "
    puts -nonewline "file_name pulse_length(ps) radius(mm) npart(#)\n"
    flush stdout
    set names [gets stdin]
    if {[llength $names] < 4 } {
	puts "not enough argument"
	exit
    } else {
	set fname   [lindex $names 0]
	set tau     [lindex $names 1]
	set radius  [lindex $names 2]
	set np      [lindex $names 3]
    }
  } else {
    set args2 [string map {= " "} $args]

    array set req $args2
    puts [array get req]
    set fname   $req(dist_name)
    set tau   $req(pulse_length)
    set radius   $req(radius)
    set np    $req(npart)
 }
 
 set lt [format "%.6f" [expr $tau*1.e-3]]
 set rt [format "%.6f" [expr $lt/10]]
 set rr [format "%.6f" $radius]
 
 set fout [open "generator.in" w]
 
 puts $fout " &INPUT
  FNAME = '$fname'
  ADD=.F
  N_add=0
  IPart = $np
  Species='electrons'
  Probe=.True.
  Noise_reduc=.T.
  Cathode=.T.
  Q_total = 0.25
  Ref_zpos=0.0E0
  Ref_clock=0.0E0
  Ref_Ekin=0.0E0
  Dist_z='p',  
  Lt=$lt, 
  rt=$rt
  Dist_pz='i',  
  sig_Ekin=0.0E0,  
  emit_z=0.00E0 ,    
  cor_Ekin=0.E0, 
  LE=0.05e-3
  Dist_x='r',  
  sig_x=$rr
  Dist_y='r',  
  sig_y=$rr
  Dist_px='r', 
  Nemit_x=0.0E0,   
  cor_px=0.0E0
  LPROMPT=.F
 /" 
 
 close $fout
 
exec generator.bin generator.in  >@stdout

after 250

# file delete  -force generator.in
# file delete -force NORRAN


set m0c2 510998.928
set mev  1.e-6
set pi [expr acos(-1.)]


set fin [open $fname r]

set xx_l {}
set yy_l {}
set px_l {}
set py_l {}
set zz_l {}
set dw_l {}
set pz_l {}
set w_l {}
set p_l {}
set xp_l {}
set yp_l {}
set xxp_l {}
set i 0
set npart 0
# set fout [open particlesa.in w]

while {[gets $fin line] >= 0} {

	if { $i < 1 } {
		set pz0 [lindex $line 5]
		set w0  [expr sqrt($pz0*$pz0 + $m0c2*$m0c2)-$m0c2]
		set t0  [expr [lindex $line 6]]
		set btgm0 [expr $pz0/$m0c2]
	} else {
	  if {[lindex $line 9]!=-3 } {
	  set x [expr [lindex $line 0]*100.]
	  set y [expr [lindex $line 1]*100.]
	  set z [expr ([lindex $line 6]+$t0)*360.*1.3]
	  set px [lindex $line 3]
	  set py [lindex $line 4]
	  set pz [expr  [lindex $line 5]+$pz0]
	  set p [expr sqrt($px**2+$py**2+$pz**2)]
	  set xp [expr $px/$p]
	  set yp [expr $py/$p]
	  set w  [expr (sqrt($p**2 + $m0c2**2)-$m0c2)*$mev]
	  set dw [expr (sqrt($p**2 + $m0c2**2)-$m0c2-$w0)*$mev]


# 	  puts $fout [format "%g %g %g %g %g %g" $x $xp $y $yp $z $dw ]
	  lappend xx_l  $x
	  lappend yy_l  $y
	  lappend xp_l $xp
	  lappend yp_l $yp
	  lappend xxp_l [expr $xp*$x]
	  lappend dw_l $dw
	  lappend  w_l $w
	  lappend zz_l  $z
	  lappend p_l  $p
	  incr npart
	  }
	}
	incr i
 }
close $fin

# file delete -force new.part

package require math::statistics

set w_mean [::math::statistics::mean $w_l]
set p_mean [::math::statistics::mean $p_l]
set z_mean [::math::statistics::mean $zz_l]

set s_xx    [::math::statistics::var  $xx_l ]
set s_xpxp  [::math::statistics::var  $xp_l]
set s_xxp   [::math::statistics::mean $xxp_l ]



set btgm [expr $p_mean/$m0c2]

set em_x  [expr sqrt($s_xx*$s_xpxp - $s_xxp*$s_xxp)]

puts "no of particles $npart"
puts "normalized emittance [expr $em_x*$btgm*1e4]"
puts "mean energy $w_mean  (MeV)"
puts "reference energy $w0  (eV)"

exec rm NORRAN

}

generate_particles 




