#! /opt/local/bin/octave -q --persist

addpath('../XFEL_Code');
addpath('common');

XFEL_Code;

%% Inizialization

global RF

a_div_lambda_K = 0.15;

RF = setup_RF(a_div_lambda_K);

function f = constrain(X, a, b)
  X = X / (1+abs(X));
  f = ((b-a)*X+(a+b))/2;
end

function X = iconstrain(f, a, b)
  f = (2*f-b-a)/(b-a);
  X = f / (1-abs(f));
end

load 'in_dist.dat.gz';
Z = T(:,1) * 1e3; % um
P = T(:,2) / 1e3; % GeV/c
E = hypot(0.00051099893, P); % GeV
V = P ./ E; % c
T = -Z ./ V; % um/c, arrival time
Z = T; % Z = c*T, um

P_ = polyfit(Z, P, 2); % parabolic fit
Z_max = -P_(2) / 2 / P_(1); % um
sigmaP = std(P .- polyval(P_, Z)); % GeV/c, uncorrelated momentum spread
P_max = polyval(P_, Z_max); % GeV/c, max momentum
Z -= Z_max; % moves the distribution to Z_max = 0

global Beam
Beam.charge = 250; % pC
Beam.sigmaZ = std(Z); % um
Beam.n = P_(1); % GeV/c/um^2, curvature
Beam.sigmaP = sigmaP; % GeV/c, uncorrelated
Beam.P_max = P_max; % GeV/c, max momentum
Beam.mass = 0.00051099893 % [GeV/c^2]
Beam.B0 = Bunch1d(Beam.mass, Beam.charge * 6241509.343260179, [ Z P ]);  

function XLS = init_bc1(X)
  global Beam
  
  XLS.P0 = Beam.P_max; % GeV, at injection
  XLS.P1 = 0.160;  % GeV, at first chicane
  XLS.P2 = 2.600;  % GeV, at second chicane
  XLS.P3 = 4.600;  % GeV, final energy

  XLS.sZ0 = Beam.sigmaZ; % um
  XLS.sZ1 = 64;          % um, after first chicane
  XLS.sZ2 = 8;           % um, after second chicane

  %% linacs
  XLS.X0_phase = constrain(X(1), -90, 90); % deg
  XLS.X1_phase = constrain(X(2), -90, 90); % deg

  %% lineariser
  XLS.K_phase   = constrain(X(3), -10, 370); % deg
  XLS.K_voltage = constrain(X(4), 0, 0.050); % GV

  dV1 = (XLS.P1 - XLS.P0) / cosd(XLS.X0_phase); % GV
  dV2 = (XLS.P2 - XLS.P1) / cosd(XLS.X1_phase); % GV
  
  XLS.X0_voltage = constrain(X(5), 0, dV1); % GV
  XLS.X1_voltage = constrain(X(6), 0, dV2); % GV

  %% chicane
  XLS.R0_56 = constrain(X(7),  -0.20, 0.20); % m, first chicane, must be negative
  XLS.T0_566 = 0 * -3/2 * XLS.R0_56; % m

endfunction

function XLS = init_xls(X)
  
  XLS = init_bc1(X);

  XLS.X2_phase = constrain(X(8), 0, 30); % deg
  dV3 = (XLS.P3 - XLS.P2) / cosd(XLS.X2_phase); % GV
  
  XLS.X2_voltage = constrain(X(9), 0, dV3); % GV
  
  %% chicane
  XLS.R1_56 = constrain(X(10), -0.50, 0.0); % m, second chicane, must be negative
  XLS.T1_566 = 0 * -3/2 * XLS.R1_56; % m
end

function [B1,B2,B3] = track_bc1(XLS, Beam)
  
  %% tracking
  global RF

  XFEL_Code
  
  X0 = CavityArray(RF.Xstructure);
  X0.set_voltage(XLS.X0_voltage);
  X0.set_phased (XLS.X0_phase);
  X0.disable_wakefields();
  
  K0 = CavityArray(RF.Kstructure);
  K0.set_voltage(XLS.K_voltage);
  K0.set_phased (XLS.K_phase);
  K0.disable_wakefields();
  
  L = Lattice();
  L.append(X0); 
  L.append(K0); 
  L.set_reference_momentum(XLS.P0);
  B1 = L.track_z(Beam.B0);
  
  BC1 = Chicane(XLS.R0_56, XLS.T0_566);
  L = Lattice();
  L.append(BC1);
  L.set_reference_momentum(min(B1.data(:,2)));
  B2 = L.track_z(B1);

  X1 = CavityArray(RF.Xstructure);
  X1.set_voltage(XLS.X1_voltage);
  X1.set_phased (XLS.X1_phase);
  X1.disable_wakefields();
  
  L = Lattice();
  L.append(X1); 
  L.set_reference_momentum(mean(B2.data(:,2)));
  B3 = L.track_z(B2);

end

function [B1,B2,B3,B4,B5] = track(XLS, Beam)

  %% tracking
  global RF

  XFEL_Code

  [B1,B2,B3] = track_bc1(XLS, Beam);

  BC2 = Chicane(XLS.R1_56, XLS.T1_566);
  L = Lattice();
  L.append(BC2);
  L.set_reference_momentum(min(B3.data(:,2)));
  B4 = L.track_z(B3);
  
  X2 = CavityArray(RF.Xstructure);
  X2.set_voltage(XLS.X2_voltage);
  X2.set_phased (XLS.X2_phase);
  X2.disable_wakefields();
  
  L = Lattice();
  L.append(X2);
  L.set_reference_momentum(mean(B4.data(:,2)));
  B5 = L.track_z(B4);
  
endfunction

function [M,B1,B2,B3] = optim_bc1(X)

  global Beam

  XLS = init_bc1(X);
  
  [B1,B2,B3] = track_bc1(XLS, Beam);

  P = polyfit(B3.data(:,1), B3.data(:,2), 2);
  curvature_1 = P(1);
  
  %% merit function
  M(1) = (B2.get_std_1() - XLS.sZ1);
  M(2) = (B1.get_mean_2() / XLS.P1 - 1);
  M(3) = (B3.get_mean_2() / XLS.P2 - 1);
  M(4) = (curvature_1);
  
end

function [M,B1,B2,B3,B4,B5] = optim_bc2(X)

  global Beam
  global X_bc1

  %X = [ X_bc1 ; X ];

  XLS = init_xls(X);

  [B1,B2,B3,B4,B5] = track(XLS, Beam);
  
  P = polyfit(B5.data(:,1), B5.data(:,2), 3);

  %%{  
  M(1) = (B2.get_std_1() / XLS.sZ1 - 1);
  M(2) = (B4.get_std_1() / XLS.sZ2 - 1);
  M(3) = (B1.get_mean_2() / XLS.P1 - 1);
  M(4) = (B3.get_mean_2() / XLS.P2 - 1);
  M(5) = (B5.get_mean_2() / XLS.P3 - 1);
  M(6) = (P(1));
  M(7) = (P(2));
  M(8) = (P(3));
  %%}
  
endfunction

%% Run the optimization
if true

  global X_bc1
  O = optimset('TolFun', 1e-8, 'TolX', 1e-8, 'MaxIter', 10000);
  [X_bc1,fval] = fsolve("optim_bc1", zeros(7,1), O)

  X_bc1(10) = 0;
  [X,fval] = fsolve("optim_bc2", X_bc1, O)

  XLS = init_xls(X);

  save -text XLS_new.dat XLS

else

  load XLS_new.dat

end

B0 = Beam.B0;
[B1,B2,B3,B4,B5] = track(XLS, Beam);

B1_ = [ std(B1.data(:,1)) mean(B1.data(:,2)) ]
B2_ = [ std(B2.data(:,1)) mean(B2.data(:,2)) ]
B3_ = [ std(B3.data(:,1)) mean(B3.data(:,2)) ]
B4_ = [ std(B4.data(:,1)) mean(B4.data(:,2)) ]
B5_ = [ std(B5.data(:,1)) mean(B5.data(:,2)) ]

figure(1)
clf
hold on
scatter(B1.data(:,1), B1.data(:,2));
scatter(B2.data(:,1), B2.data(:,2));
scatter(B3.data(:,1), B3.data(:,2));
%%scatter(B4.data(:,1), B4.data(:,2));
%%scatter(B5.data(:,1), B5.data(:,2));

figure(2)
subplot(3,2,1)
scatter(B0.data(:,1), B0.data(:,2));

subplot(3,2,2)
scatter(B1.data(:,1), B1.data(:,2));

subplot(3,2,3)
scatter(B2.data(:,1), B2.data(:,2));

subplot(3,2,4)
scatter(B3.data(:,1), B3.data(:,2));

subplot(3,2,5)
scatter(B4.data(:,1), B4.data(:,2));

subplot(3,2,6)
scatter(B5.data(:,1), B5.data(:,2));

print -dpng plot_Kband_new.png
