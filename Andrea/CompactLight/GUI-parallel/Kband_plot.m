addpath('../../XFEL_Code');
addpath('common');

pkg load parallel

XFEL_Code;

%% set a/lambda for the K-band structure
global RF
RF = setup_RF();

%% read in the particle distribution
T = load('files/xband_gun_xband_inj_out.dat.gz');
Z = T(:,5) * 299792458000000; % micron/c
P = T(:,7) * 0.0005109989275678127; % GeV/c

P_ = polyfit(Z, P, 2); % parabolic fit
Z_max = -P_(2) / 2 / P_(1); % um
Z -= Z_max; % moves the distribution to Z_max = 0

% store some bunch information 
Beam.charge = 250; % pC
Beam.sigmaZ = std(Z); % um
Beam.sigmaP = std(P); % GeV/c, uncorrelated
Beam.P_max = max(P); % GeV/c, max momentum
Beam.mass = 0.00051099893 % [GeV/c^2]

global B0
B0 = Bunch1d(Beam.mass, Beam.charge * 6241509.343260179, [ Z P ]);     

%% load a good solution
source('Kband_layout.dat')

%% main loop
[track_X0, track_BC1, track_X1, track_BC2, track_X2] = deal(true);

%% track
if track_X0
    X0 = CavityArray(RF.Xstructure);
    X0.set_voltage(XLS.X0_voltage);
    X0.set_phased (XLS.X0_phase);
    if !XLS.X_wakes
        X0.disable_wakefields();
    end
    
    K0 = CavityArray(RF.Kstructure);
    K0.set_voltage(XLS.K_voltage);
    K0.set_phased (XLS.K_phase);
    if !XLS.K_wakes
        K0.disable_wakefields();
    end
    
    L = Lattice();
    L.append(X0); 
    L.append(K0); 
    L.set_reference_momentum(mean(B0.data(:,2)));
    B1 = L.track_z(B0);
    
    graphics_toolkit gnuplot
    figure('visible','off');
    subplot(2,3,1);
    scatter(B1.data(:,1)/1e3, 1e3*B1.data(:,2));
    title('after Linac-0 and K-band')
    xlabel('t [mm/c]')
    ylabel('P [MeV/c]')
    
end

if track_BC1
    BC1 = Chicane(XLS.BC1_R56, XLS.BC1_T566);
    L = Lattice();
    L.append(BC1);
    P_min = min(B1.data(:,2));
    P_max = max(B1.data(:,2));
    L.set_reference_momentum(P_min + (P_max - P_min) * XLS.BC1_P0);
    B2 = L.track_z(B1);
    D = B2.data;
    D(:,1) -= mean(D(:,1));
    B2.set_data(D);
    
    %figure(2)
    subplot(2,3,2);
    scatter(B2.data(:,1), 1e3*B2.data(:,2));
    title('after BC1')
    xlabel('t [um/c]')
    ylabel('P [MeV/c]')
    
end

if track_X1
    X1 = CavityArray(RF.Xstructure);
    X1.set_voltage(XLS.X1_voltage);
    X1.set_phased (XLS.X1_phase);
    if !XLS.X_wakes
        X1.disable_wakefields();
    end
    
    L = Lattice();
    L.append(X1); 
    L.set_reference_momentum(mean(B2.data(:,2)));
    B3 = L.track_z(B2);
    
    %figure(3)
    subplot(2,3,3);
    scatter(B3.data(:,1), B3.data(:,2));
    title('after Linac-1')
    xlabel('t [um/c]')
    ylabel('P [GeV/c]')
end

if track_BC2
    BC2 = Chicane(XLS.BC2_R56, XLS.BC2_T566);
    L = Lattice();
    L.append(BC2);
    P_min = min(B3.data(:,2));
    P_max = max(B3.data(:,2));
    L.set_reference_momentum(P_min + (P_max - P_min) * XLS.BC2_P0);
    B4 = L.track_z(B3);
    D = B4.data;
    D(:,1) -= mean(D(:,1));
    B4.set_data(D);
    
    %figure(4)
    subplot(2,3,4);
    scatter(B4.data(:,1), B4.data(:,2));
    title('after BC2')
    xlabel('t [um/c]')
    ylabel('P [GeV/c]')
end

if track_X2
    X2 = CavityArray(RF.Xstructure);
    X2.set_voltage(XLS.X2_voltage);
    X2.set_phased (XLS.X2_phase);
    if !XLS.X_wakes
        X2.disable_wakefields();
    end
    
    L = Lattice();
    L.append(X2);
    L.set_reference_momentum(mean(B4.data(:,2)));
    B5 = L.track_z(B4);
    
    %figure(5)
    subplot(2,3,5);
    scatter(B5.data(:,1), B5.data(:,2));
    title('after Linac-2')
    xlabel('t [um/c]')
    ylabel('P [GeV/c]')
    
    %figure(6);
    subplot(2,3,6);
    [H,X] = hist((B5.data(:,1) - mean(B5.data(:,1))) / 0.29979246, 40, Beam.charge); % pC, fs
    H /= X(2) - X(1); % kA = pC/fs
    plot(X,H);
    title('Current')
    ylabel('I_{peak} [kA]')
    xlabel('t [fs]')
    drawnow;
end

print -dpng tmp.png;
system("pngtopnm tmp.png | pnmcrop | pnmtopng -compression=9 > plot_Kband_gui.png && rm -f tmp.png")

%% print out machine info
XLS

%% print out some bunch info  
bc1_sigmaZ = std(B2.data(:,1))
bc1_meanE = mean(B2.data(:,2))
bc2_sigmaZ = std(B5.data(:,1)) 
bc2_meanE = mean(B5.data(:,2))
sigmaE = std(B5.data(:,2))
sigmaE_E_percent = sigmaE / bc2_meanE * 100

% Sliced quantities
Z = sort(B5.data(:,1));
Z_ = linspace(0, 100, length(Z));
Z_min = spline(Z_, Z, 47.5);
Z_max = spline(Z_, Z, 52.5);
M = Z>=Z_min & Z<Z_max;
meanE_slice = mean(B5.data(M,2));
sigmaE_slice = std(B5.data(M,2))
sigmaE_E_slice_percent = sigmaE_slice / meanE_slice * 100

T_fs = (B5.data(:,1) - mean(B5.data(:,1))) / 0.29979246; % fs
E_GeV = B5.data(:,2); % GeV
Q_pC = Beam.charge / length(T_fs) * ones(size(T_fs)); % pC

T = [ T_fs E_GeV Q_pC ];

save -text Kband_beam.dat T;

[H,X] = hist((B5.data(:,1) - mean(B5.data(:,1))) / 0.29979246, 40, Beam.charge); % pC, fs
H /= X(2) - X(1); % kA = pC/fs

T = [ X H ];
save -text Kband_current.dat T;

