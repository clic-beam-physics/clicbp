addpath('../../XFEL_Code');
addpath('common');

XFEL_Code;

%% set a/lambda for the K-band structure
a_over_lambda_K = 1.2; % a = 10 mm
RF = setup_RF(a_over_lambda_K);

%% read in the particle distribution
T = load('files/xband_gun_xband_inj_out.dat.gz');
T = T(1:50:end,:); % takes 1 particle every 100 (for speed!)
Z = T(:,5) * 299792458000000; % micron/c
P = T(:,7) * 0.0005109989275678127; % GeV/c

% Prepares the beam
E = hypot(0.00051099893, P); % GeV
V = P ./ E; % c

P_ = polyfit(Z, P, 2); % parabolic fit
Z_max = -P_(2) / 2 / P_(1); % um
sigmaP = std(P .- polyval(P_, Z)); % GeV/c, uncorrelated momentum spread
P_max = polyval(P_, Z_max); % GeV/c, max momentum
Z -= Z_max; % moves the distribution to Z_max = 0

Beam.charge = 250; % pC
Beam.sigmaZ = std(Z); % um
Beam.n = P_(1); % GeV/c/um^2, curvature
Beam.sigmaP = sigmaP; % GeV/c, uncorrelated
Beam.P_max = P_max; % GeV/c, max momentum
Beam.mass = 0.00051099893 % [GeV/c^2]

B0 = Bunch1d(Beam.mass, Beam.charge * 6241509.343260179, [ Z P ]);     
     
%% load a good solution
source('Kband_layout.dat')

%% init and start the gui
octave_to_gui = tempname;
gui_to_octave = tempname;

system([ 'mkfifo ' octave_to_gui ]);
system([ 'mkfifo ' gui_to_octave ]);
system([ 'wish Kband_gui.tcl ' octave_to_gui ' ' gui_to_octave ], false, 'async');

oct2gui_fd = fopen(octave_to_gui, 'w');
fprintf(oct2gui_fd, 'set val0 %g\n', XLS.X0_phase);
fprintf(oct2gui_fd, 'set val1 [expr %g * 1e3]\n', XLS.X0_voltage);
fprintf(oct2gui_fd, 'set val2 %g\n', XLS.K_phase);
fprintf(oct2gui_fd, 'set val3 [expr %g * 1e3]\n', XLS.K_voltage);
fprintf(oct2gui_fd, 'set val4 %g\n', XLS.BC1_R56);
fprintf(oct2gui_fd, 'set val5 %g\n', XLS.BC1_P0);
fprintf(oct2gui_fd, 'set val6 %g\n', XLS.X1_phase);
fprintf(oct2gui_fd, 'set val7 %g\n', XLS.X1_voltage);
fprintf(oct2gui_fd, 'set val8 %g\n', XLS.BC2_R56);
fprintf(oct2gui_fd, 'set val9 %g\n', XLS.BC2_P0);
fprintf(oct2gui_fd, 'set val10 %g\n', XLS.X2_phase);
fprintf(oct2gui_fd, 'set val11 %g\n', XLS.X2_voltage);
fprintf(oct2gui_fd, 'set xband_wakes %g\n', XLS.X_wakes);
fprintf(oct2gui_fd, 'set kband_wakes %g\n', XLS.K_wakes);
fclose(oct2gui_fd);

%% main loop
[track_X0, track_BC1, track_X1, track_BC2, track_X2] = deal(true);
QUIT = false;
do

    %% track
    if track_X0
        X0 = CavityArray(RF.Xstructure);
        X0.set_voltage(XLS.X0_voltage);
        X0.set_phased (XLS.X0_phase);
        if !XLS.X_wakes
            X0.disable_wakefields();
        end
        
        K0 = CavityArray(RF.Kstructure);
        K0.set_voltage(XLS.K_voltage);
        K0.set_phased (XLS.K_phase);
        if !XLS.K_wakes
            K0.disable_wakefields();
        end
        
        L = Lattice();
        L.append(X0); 
        L.append(K0); 
        L.set_reference_momentum(mean(B0.data(:,2)));
        B1 = L.track_z(B0);
        
        figure(1)
        subplot(2,3,1);
        scatter(B1.data(:,1), B1.data(:,2));
        title('after Linac-0 and K-band')
        xlabel('t [um/c]')
        ylabel('P [GeV/c]')
        drawnow;

    end
    
    if track_BC1
        BC1 = Chicane(XLS.BC1_R56, XLS.BC1_T566);
        L = Lattice();
        L.append(BC1);
        P_min = min(B1.data(:,2));
        P_max = max(B1.data(:,2));
        L.set_reference_momentum(P_min + (P_max - P_min) * XLS.BC1_P0);
        B2 = L.track_z(B1);
        D = B2.data;
        D(:,1) -= mean(D(:,1));
        B2.set_data(D);

        %figure(2)
        subplot(2,3,2);
        scatter(B2.data(:,1), B2.data(:,2));
        title('after BC1')
        xlabel('t [um/c]')
        ylabel('P [GeV/c]')
        drawnow;

    end
    
    if track_X1
        X1 = CavityArray(RF.Xstructure);
        X1.set_voltage(XLS.X1_voltage);
        X1.set_phased (XLS.X1_phase);
        if !XLS.X_wakes
            X1.disable_wakefields();
        end
        
        L = Lattice();
        L.append(X1); 
        L.set_reference_momentum(mean(B2.data(:,2)));
        B3 = L.track_z(B2);
        
        %figure(3)
        subplot(2,3,3);
        scatter(B3.data(:,1), B3.data(:,2));
        title('after Linac-1')
        xlabel('t [um/c]')
        ylabel('P [GeV/c]')
        drawnow;
    end
    
    if track_BC2
        BC2 = Chicane(XLS.BC2_R56, XLS.BC2_T566);
        L = Lattice();
        L.append(BC2);
        P_min = min(B3.data(:,2));
        P_max = max(B3.data(:,2));
        L.set_reference_momentum(P_min + (P_max - P_min) * XLS.BC2_P0);
        B4 = L.track_z(B3);
        D = B4.data;
        D(:,1) -= mean(D(:,1));
        B4.set_data(D);
        
        %figure(4)
        subplot(2,3,4);
        scatter(B4.data(:,1), B4.data(:,2));
        title('after BC2')
        xlabel('t [um/c]')
        ylabel('P [GeV/c]')
        drawnow;
    end
    
    if track_X2
        X2 = CavityArray(RF.Xstructure);
        X2.set_voltage(XLS.X2_voltage);
        X2.set_phased (XLS.X2_phase);
        if !XLS.X_wakes
            X2.disable_wakefields();
        end
        
        L = Lattice();
        L.append(X2);
        L.set_reference_momentum(mean(B4.data(:,2)));
        B5 = L.track_z(B4);

        %figure(5)
        subplot(2,3,5);
        scatter(B5.data(:,1), B5.data(:,2));
        title('after Linac-2')
        xlabel('t [um/c]')
        ylabel('P [GeV/c]')
        drawnow;
        
        %figure(6);
        subplot(2,3,6);
        [H,X] = hist((B5.data(:,1) - mean(B5.data(:,1))) / 0.29979246, 60, Beam.charge); % pC, fs
        H /= X(2) - X(1); % kA = pC/fs
        plot(X,H);
        title('Current')
        ylabel('I_{peak} [kA]')
        xlabel('t [fs]')
        drawnow;
    end
  
    %% print out some bunch info  
    system('clear');
    bc1_sigmaZ = std(B2.data(:,1))
    bc1_meanP = mean(B2.data(:,2))
    sigmaZ = std(B5.data(:,1)) 
    meanP = mean(B5.data(:,2))
    sigmaP = std(B5.data(:,2))
    sigmaP_P_percent = sigmaP / meanP * 100

    %% end of main loop
    XLS_ = XLS;

    % process any input from the gui
    source(gui_to_octave)
    
    %% check what subsystems need to be tracked
    track_X0 = ...
        XLS_.X_wakes != XLS.X_wakes || ...
        XLS_.K_wakes != XLS.K_wakes || ...
        XLS_.X0_phase != XLS.X0_phase || ...
        XLS_.X0_voltage != XLS.X0_voltage || ...
        XLS_.K_phase != XLS.K_phase || ...
        XLS_.K_voltage != XLS.K_voltage;
    track_BC1 = track_X0 || ...
        XLS_.BC1_R56 != XLS.BC1_R56 || ...
        XLS_.BC1_P0 != XLS.BC1_P0;
    track_X1 = track_BC1 || ...
        XLS_.X1_phase != XLS.X1_phase || ...
        XLS_.X1_voltage != XLS.X1_voltage;
    track_BC2 = track_X1 || ...
        XLS_.BC2_R56 != XLS.BC2_R56 || ...
        XLS_.BC2_P0 != XLS.BC2_P0;
    track_X2 = track_BC2 || ...
        XLS_.X2_phase != XLS.X2_phase || ...
        XLS_.X2_voltage != XLS.X2_voltage;
    
until QUIT == true;

system([ 'rm -f ' octave_to_gui ]);
system([ 'rm -f ' gui_to_octave ]);
