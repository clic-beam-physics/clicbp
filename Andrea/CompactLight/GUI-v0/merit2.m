function M = merit2(X)

    global XLS
    
    X /= 100;
    
    XLS.X1_phase   = constrain(X(1), -90, 90);
    XLS.X1_voltage = constrain(X(2), 2, 4.000);
    XLS.X2_phase   = constrain(X(3), -90, 90);
    XLS.X2_voltage = constrain(X(4), 2, 4.000);
    XLS.BC2_R56    = constrain(X(5), -0.5, 0.5);
    XLS.BC2_P0     = constrain(X(6), 0, 1);

    [B5,B4,B3] = track2();
    
    Z = sort(B5.data(:,1));
    Z_ = linspace(0, 100, length(Z));
    Z_min = spline(Z_, Z, 2);
    Z_max = spline(Z_, Z, 98);
    
    sigmaz_um = Z_max - Z_min;
    energy_GeV = mean(B5.data(:,2));
    espread_GeV = std(B5.data(:,2));
    skew = skewness(B5.data(:,1));
    kurt = kurtosis(B5.data(:,1));

    M = 1 * abs(sigmaz_um - 24) + ...
        10 * abs(energy_GeV - 6) + ...
        10 * abs(espread_GeV) + ...
        100 * abs(skew) + ...
        100 * abs(kurt - 1.8);

    disp([ M sigmaz_um energy_GeV skew kurt ]);
    
end
