function Beam = setup_beam()
    %%%% Avni
    A = load('gun_inj3.1200.001.gz');
    Z = ([ A(1,3) ; A(1,3) + A(2:end,3) ]) * 1e6; % microm
    E = ([ A(1,6) ; A(1,6) + A(2:end,6) ]) / 1e9; % GeV
    Z = Z - mean(Z);
    Z = -Z; % in the file the head of the bunch is z>0; need to flip it
            % to have the head for z<0
    [Z,I] = sort(Z);
    E = E(I);

    %Z = Z(1:10:end);
    %E = E(1:10:end);

    Fit = polyfit(Z,E,5); % Linearize the input distribution
    EL = E - polyval(Fit,Z);
    
    beam_linear = [ Z EL ];
    
    Beam.charge = 1.5603773e+09; % # of electrons 
    Beam.sigmaz = std(Z);
    Beam.pc0 = mean(E);
    Beam.mass0 = 0.00051099893; % [GeV/c/c]
    Beam.etotal0 = sqrt(Beam.pc0**2+Beam.mass0**2); % GeV
    Beam.energy = Beam.pc0; % GeV
    Beam.espread_abs = std(E);
    Beam.espread_lin = std(EL);
    Beam.espread_slc = Slice_Espread_abs([ Z EL ]);
    Beam.espread_rel = std(E) / mean(E) * 100;
    Beam.data = [ Z E ]; % Injected beam
    Beam.emittance  = std(Z) * std(EL); % linearized projected
                                        % emittance [microm * GeV]
endfunction
