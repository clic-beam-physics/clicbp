function RF = setup_RF()
    XFEL_Code;
    
    clight = 0.29979246; % m/ns
    
    ## Latest Frascati's X-band structure for CompactLight
    RF.Xcell = Cell();
    RF.Xcell.frequency = 11.9942; % GHz
    lambda_X = clight / RF.Xcell.frequency; % m
    RF.Xcell.a = 0.0035; % m
    RF.Xcell.l = lambda_X / 3; % m, 2*pi/3 phase advance
    RF.Xcell.g = RF.Xcell.l - 0.002; % m
    
    ## K-band structure parameters (Graeme's Burt, XLS Barcelona meeting)
    RF.Kcell = Cell();
    RF.Kcell.frequency = 3 * 11.9942; % GHz
    lambda_K = clight / RF.Kcell.frequency; % m
    RF.Kcell.a = 0.002; % m
    RF.Kcell.l = lambda_K / 3; % m, 2*pi/3 phase advance
    RF.Kcell.g = RF.Kcell.l - 0.0006; % m
    
    RF.Xstructure = AcceleratingStructure(RF.Xcell, 108);
    RF.Xstructure.max_gradient = 65e-3; % GV/m

    RF.Kstructure = AcceleratingStructure(RF.Kcell, ceil(0.75 / RF.Kcell.l)); % 0.75 m long
    RF.Kstructure.max_gradient = 25e-3; % GV/m

endfunction
