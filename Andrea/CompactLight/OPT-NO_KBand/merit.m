function M = merit(X)

    global XLS B0 B5

    XLS.X0_phase = constrain(X(1), -90, 90);
    XLS.X0_voltage = constrain(X(2), 0, 0.500);
    XLS.BC1_R56 =  constrain(X(3), -0.050, 0.050);
    XLS.BC1_P0 = constrain(X(4), 0, 1);
    XLS.BC1_T566 =  constrain(X(5), -0.1, 0.1);

    [B2,B1] = track1(B0);
    
    Z = sort(B2.data(:,1));
    Z_ = linspace(0, 100, length(Z));
    Z_min = spline(Z_, Z, 2);
    Z_max = spline(Z_, Z, 98);
    
    sigmaz_um = Z_max - Z_min;
    energy_GeV = mean(B2.data(:,2));
    skew = skewness(B2.data(:,1));
    kurt = kurtosis(B2.data(:,1));

    M1 = 1 * abs(sigmaz_um - 115) + ...
        10 * abs(energy_GeV - 0.3) + ...
        100 * abs(skew) + ...
        1000 * abs(kurt - 1.8);

    XLS.X1_phase   = constrain(X(6), -90, 90);
    XLS.X1_voltage = constrain(X(7), 0, 3.000);
    XLS.X2_phase   = constrain(X(8), -90, 90);
    XLS.X2_voltage = constrain(X(9), 0, 5.000);
    XLS.BC2_R56    = constrain(X(10), -0.050, 0.050);
    XLS.BC2_P0     = constrain(X(11), 0, 1);
    XLS.BC2_T566   = constrain(X(12), -0.1, 0.1);

    [B5,B4,B3,Length] = track2(B2);
    
    Z = sort(B5.data(:,1));
    Z_ = linspace(0, 100, length(Z));
    Z_min = spline(Z_, Z, 1);
    Z_max = spline(Z_, Z, 99);
    
    sigmaz_um = Z_max - Z_min;
    energy_GeV = mean(B5.data(:,2));
    espread_GeV = std(B5.data(:,2));

    % 98% particle espread
    Z_min = spline(Z_, Z, 1);
    Z_max = spline(Z_, Z, 99);
    M = Z>=Z_min & Z<Z_max;
    espread_90slice_GeV = std(B5.data(M,2));
    
    skew = skewness(B5.data(:,1));
    kurt = kurtosis(B5.data(:,1));

    M2 = 10 * abs(range(Z) - 30) + ...
        1000 * abs(energy_GeV - 7) + ...
        10 * abs(espread_GeV) + ...
        100 * abs(skew) + ...
        1000 * abs(kurt - 1.8) + ...
	0.1 * Length;

	M = M1 + 100 * M2;

    %disp([ M sigmaz_um energy_GeV skew kurt ]);

end
