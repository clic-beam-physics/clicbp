addpath('../../XFEL_Code');
addpath('common');

pkg load parallel

XFEL_Code;

%% set a/lambda for the K-band structure
global RF
RF = setup_RF();

%% read in the particle distribution
T = load('files/xband_gun_xband_inj_out.dat.gz');
T = T(1:50:end,:); % takes 1 particle every 50 (for speed!)
Z = T(:,5) * 299792458000000; % micron/c
P = T(:,7) * 0.0005109989275678127; % GeV/c

P_ = polyfit(Z, P, 2); % parabolic fit
Z_max = -P_(2) / 2 / P_(1); % um
Z -= Z_max; % moves the distribution to Z_max = 0

% store some bunch information 
Beam.charge = 250; % pC
Beam.sigmaZ = std(Z); % um
Beam.sigmaP = std(P); % GeV/c, uncorrelated
Beam.P_max = max(P); % GeV/c, max momentum
Beam.mass = 0.00051099893 % [GeV/c^2]

global B0
B0 = Bunch1d(Beam.mass, Beam.charge * 6241509.343260179, [ Z P ]);     

%% load a good solution
global XLS
source('Kband_layout.dat')

%% Optimization
N = 500;

function [F_,X_] = func(idx)
    O = optimset('TolFun', 1e-10, 'TolX', 1e-10, 'MaxFunEvals', 100000, 'MaxIter', 100000);
    [X_, F_] = fminsearch ("merit", randn(12,1), O);
end

[F,X] = pararrayfun(nproc, "func", 1:N, "UniformOutput", false);

F_min = Inf;
X_min = [];

for i=1:N
    if F{i}<F_min
        X_min = X{i};
        F_min = F{i};
    end
end

save -text optimum.dat X_min F_min

global B5
M = merit(X_min);

BC2_sigmaz_um = std(B5.data(:,1))
BC2_energy_GeV = mean(B5.data(:,2))

fid = fopen('Kband_layout.dat', 'w');
fields = fieldnames(XLS);
for i = 1:length(fields)
    fprintf(fid, 'XLS.%s = %.15g;\n', fields{i}, eval( [ 'XLS.' fields{i} ]))
end
fclose(fid);
