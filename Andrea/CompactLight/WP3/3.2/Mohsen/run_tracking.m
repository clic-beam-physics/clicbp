#! /opt/local/bin/octave -q --persist

%% Load RF_Track

RF_Track;

% columns= x [m], xp, y [m], yp, t [s], K [MeV], p [m_e c]
B = load('IPD-2.txt.gz');
B_ = zeros(size(B,1), 6);
B_(:,1) =  B(:,1) * 1e3; % mm
B_(:,3) =  B(:,2) * 1e3; % mm
B_(:,5) = -B(:,3) * 1e3; % mm/c arrival time

Px =  RF_Track.electronmass * B(:,4); % MeV/c
Py =  RF_Track.electronmass * B(:,5); % MeV/c
Pz =  RF_Track.electronmass * B(:,6); % MeV/c

B_(:,2) = Px ./ Pz * 1e3; % mrad
B_(:,4) = Py ./ Pz * 1e3; % mrad
B_(:,6) = hypot(Px, Py, Pz); % MeV/c

B0 = Bunch6d(RF_Track.electronmass, ...
	     250 * RF_Track.pC, ...
	     -1, ...
	     B_);

Nx = 64;
Ny = 64;
Nz = 64;
SC = SpaceCharge_PIC_FreeSpace(Nx,Ny,Nz);

% Lattice
D = Drift(0.01);
D.set_odeint_algorithm("rk2");
D.set_static_Efield(0.0, 0.0, -66e6);
D.set_static_Bfield(0.0, 0.0, 2);
%D.set_sc_nsteps(1);
L = Lattice();
L.append(D);

%% Main loop
S = 0; % mm
dS = 10; % mm
T = [];

figure(1);
clf

B1 = B0;
do
  B1 = L.track(B1);
  S += L.get_length() * 1e3;
  mean_Kz = mean(B1.get_phase_space("%Kz"))
  scatter(B1.get_phase_space("%x"), B1.get_phase_space("%xp"));
  axis([-0.8 0.8 -0.8 0.8]);
  drawnow;
  T = [ T ; S ...
	1e3 * sqrt(det(cov([ B1.get_phase_space("%x") B1.get_phase_space("%Px") ]))) / RF_Track.electronmass ...
        1e3 * sqrt(det(cov([ B1.get_phase_space("%y") B1.get_phase_space("%Py") ]))) / RF_Track.electronmass ... % normalised emittance
        std(B1.get_phase_space("%x")) ...
	std(B1.get_phase_space("%y")) ];
												     
until mean_Kz > 200

dt = B1.get_phase_space("%dt");
Pz = B1.get_phase_space("%Pz");

dt -= mean(dt);

P = polyfit(dt, Pz, 3)
Pz_0 = P(4) % MeV/c
dPz_dt = P(3) % MeV/c * c/mm
d2Pz_dt2 = P(2) % MeV/c * (c/mm)**2
d3Pz_dt3 = P(1) % MeV/c * (c/mm)**3

figure(1)
clf
hist(B0.get_phase_space("%dt") * 1e3, 64, 250)
xlabel('c*dt [um]');
ylabel("Q' [pC / micron]");
print -dpng plot_Qhist_init.png

scatter(B0.get_phase_space("%x"), B0.get_phase_space("%xp"));
axis([-0.8 0.8 -0.8 0.8]);
xlabel('x [mm]');
ylabel("x' [mrad]");
print -dpng plot_xxp_init.png

scatter(B0.get_phase_space("%y"), B0.get_phase_space("%yp"));
axis([-0.8 0.8 -0.8 0.8]);
xlabel('y [mm]');
ylabel("y' [mrad]");
print -dpng plot_yyp_init.png

scatter(B0.get_phase_space("%dt") * 1e3, B0.get_phase_space("%K"));
xlabel('c*dt [um]');
ylabel('K [MeV]');
print -dpng plot_long_init.png

clf
hold on
plot(T(:,1) / 1e3, T(:,2))
plot(T(:,1) / 1e3, T(:,3))
xlabel('S [m]');
ylabel('normalised emittance X - Y [mm.mrad]');
print -dpng plot_emitt.png

figure(2)
clf
hold on
plot(T(:,1) / 1e3, T(:,4))
plot(T(:,1) / 1e3, T(:,5))
xlabel('S [m]');
ylabel('sigma X - Y [mm]');
print -dpng plot_size.png

figure(3)
scatter(B1.get_phase_space("%dt") * 1e3, B1.get_phase_space("%K"));
xlabel('c*dt [um]');
ylabel('K [MeV]');
print -dpng plot_long.png

figure(4)
clf ; hold on
dt_axis = linspace(min(dt) * 1.2, max(dt) * 1.2, 100);
plot(dt_axis * 1e3, polyval(P, dt_axis), 'r-');
scatter(dt * 1e3, Pz);
xlabel('c*dt [um]');
ylabel('Pz [MeV/c]');
print -dpng plot_long_fit.png
