addpath('../../XFEL_Code');
addpath('common');

pkg load parallel

XFEL_Code;

%% set a/lambda for the K-band structure
global RF
RF = setup_RF();

%% read in the particle distribution
T = load('files/xband_gun_xband_inj_out.dat.gz');
T = T(1:50:end,:); % takes 1 particle every 100 (for speed!)
Z = T(:,5) * 299792458000000; % micron/c
P = T(:,7) * 0.0005109989275678127; % GeV/c

% Prepares the beam
E = hypot(0.00051099893, P); % GeV
V = P ./ E; % c

P_ = polyfit(Z, P, 2); % parabolic fit
Z_max = -P_(2) / 2 / P_(1); % um
sigmaP = std(P .- polyval(P_, Z)); % GeV/c, uncorrelated momentum spread
P_max = polyval(P_, Z_max); % GeV/c, max momentum
Z -= Z_max; % moves the distribution to Z_max = 0

Beam.charge = 250; % pC
Beam.sigmaZ = std(Z); % um
Beam.n = P_(1); % GeV/c/um^2, curvature
Beam.sigmaP = sigmaP; % GeV/c, uncorrelated
Beam.P_max = P_max; % GeV/c, max momentum
Beam.mass = 0.00051099893 % [GeV/c^2]

global B0
B0 = Bunch1d(Beam.mass, Beam.charge * 6241509.343260179, [ Z P ]);     

%% load a good solution
global XLS
source('Kband_gui-saved.dat')

%% Optimization
global X F
N = 100;
X = 3 * randn(6,N);
X(:,1) = 0;
F = zeros(1,N);

function func(idx)
    global X F
    O = optimset('TolFun', 1e-10, 'TolX', 1e-10, 'MaxFunEvals', 100000, 'MaxIter', 100000);
    [X_, F_] = fminsearch ("merit1", X(:,idx), O);
    X(:,idx) = X_;
    F(idx) = F_;
end

pararrayfun(nproc, "func", 1:N)

F_min = Inf;
X_min = [];

for i=1:100
    disp(F);
    if F(i)<F_min
        X_min = X(:,i);
        F_min = F(i);
        save -text optimum1.dat X_min F_min i
    end
end

merit1(X_min);

global B2
BC1_sigmaz_um = std(B2.data(:,1))
BC1_energy_GeV = mean(B2.data(:,2))

fid = fopen('Kband_gui-saved.dat', 'w');
fields = fieldnames(XLS);
for i = 1:length(fields)
    fprintf(fid, 'XLS.%s = %g;\n', fields{i}, eval( [ 'XLS.' fields{i} ]))
end
fclose(fid);
            
